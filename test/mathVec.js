QUnit.module("mathVec");


//------------------------------------
// Equal up to specified tolerance
//------------------------------------

var EPS_DEFAULT = 0.0000001;

var fuzzyEqualFloat = function(a, b, eps){
  var eps = eps || EPS_DEFAULT;
  
  return (Math.abs(a - b) < eps);
};

var fuzzyEqualVec3 = function(u, v, eps){
  var eps = eps || EPS_DEFAULT;
  
  return (
    fuzzyEqualFloat(u[0], v[0], eps) && 
    fuzzyEqualFloat(u[1], v[1], eps) &&
    fuzzyEqualFloat(u[2], v[2], eps)
  );
};

//------------------------------------
// Vec3
//------------------------------------

QUnit.test("Vec3.zero", function(assert){
  var v = [1,2,3];
  
  Fig.Vec3.zero(v);
  assert.deepEqual(v, [0,0,0]);
});

QUnit.test("Vec3.copy", function(assert){
  var u = [1,2,3], v = [4,5,6];
  
  Fig.Vec3.copy(u, v);
  assert.deepEqual(u, v);
});

QUnit.test("Vec3.add", function(assert){
  var u = [1,2,3], v = [4,5,6];
  
  Fig.Vec3.add(u, u,v);
  assert.deepEqual(u, [5,7,9]);
});

QUnit.test("Vec3.subtract", function(assert){
  var u = [1,2,3], v = [4,5,6];
  
  Fig.Vec3.subtract(u, u,v);
  assert.deepEqual(u, [-3,-3,-3]);
});

QUnit.test("Vec3.addMultiple", function(assert){
  var u = [2,4,6], v = [3,2,1];
  
  Fig.Vec3.addMultiple(u, u,v,2);
  assert.deepEqual(u, [8,8,8]);
});

QUnit.test("Vec3.linearCombination", function(assert){
  var u = [1,2,3], v = [3,2,1];
  
  Fig.Vec3.linearCombination(u, 2,u,2,v);
  assert.deepEqual(u, [8,8,8]);
});

QUnit.test("Vec3.scale", function(assert){
  var u = [1,2,3];
  
  Fig.Vec3.scale(u, u,2);
  assert.deepEqual(u, [2,4,6]);
});

QUnit.test("Vec3.flip", function(assert){
  var u = [1,2,3];
  
  Fig.Vec3.flip(u, u);
  assert.deepEqual(u, [-1,-2,-3]);
});

QUnit.test("Vec3.dot", function(assert){
  var x = Fig.Vec3.dot([1,2,3], [4,5,6]);
  assert.equal(x, 32);
});

QUnit.test("Vec3.normsq", function(assert){
  var x = Fig.Vec3.normsq([1,2,3]);
  assert.equal(x, 14);
});

QUnit.test("Vec3.norm", function(assert){
  var x = Fig.Vec3.norm([4,13,16]);
  assert.equal(x, 21);
});

QUnit.test("Vec3.normalize", function(assert){
  var u = [4,13,16];
  
  Fig.Vec3.normalize(u, u);
  assert.ok(fuzzyEqualVec3(u, [4/21, 13/21, 16/21]));
});

QUnit.test("Vec3.distsq", function(assert){
  var x = Fig.Vec3.distsq([3,-1,2], [2,1,5]);
  assert.equal(x, 14);
});

QUnit.test("Vec3.dist", function(assert){
  var x = Fig.Vec3.dist([-1,6,-13], [3,-7,3]);
  assert.equal(x, 21);
});

QUnit.test("Vec3.proj", function(assert){
  var u = [1,2,3], v = [0,0,1];
  
  Fig.Vec3.proj(u, u, v);
  assert.deepEqual(u, [0,0,3]);
});

QUnit.test("Vec3.projn", function(assert){
  var u = [1,2,3], v = [0,0,1];
  
  Fig.Vec3.projn(u, u, v);
  assert.deepEqual(u, [1,2,0]);
});

QUnit.test("Vec3.cross", function(assert){
  var u = [1,2,3], v = [4,5,6];
  
  Fig.Vec3.cross(u, u,v);
  assert.deepEqual(u, [-3,6,-3]);
});

QUnit.test("Vec3.augment", function(assert){
  var u = [1,2,3], v = [0,0,0,0];
  
  Fig.Vec3.augment(v, u, 4);
  assert.deepEqual(v, [1,2,3,4]);
});

QUnit.test("Vec3.orthogonal", function(assert){
  var u = [0,0,0];
  
  Fig.Vec3.orthogonal(u, [1,0,0]);
  assert.equal( Fig.Vec3.dot(u, [1,0,0]), 0 );
  
  Fig.Vec3.orthogonal(u, [0,-1,0]);
  assert.equal( Fig.Vec3.dot(u, [0,-1,0]), 0 );
});

//------------------------------------
// Vec4
//------------------------------------

//TODO

QUnit.test("Vec4.qMultiply", function(assert){
  var q = [0,0,0,0];
  
  Fig.Vec4.qMultiply(q, [1,0,0,0],[1,0,0,0]);
  assert.deepEqual(q, [1,0,0,0]);
  
  Fig.Vec4.qMultiply(q, [0,1,0,0],[0,0,1,0]);
  assert.deepEqual(q, [0,0,0,1]);
});

QUnit.test("Vec4.qInverse", function(assert){
  var q = [0,0,0,0];
  
  Fig.Vec4.qInverse(q, [1,0,0,0]);
  assert.deepEqual(q, [1,0,0,0]);
  
  Fig.Vec4.qInverse(q, [0,1,0,0]);
  assert.deepEqual(q, [0,-1,0,0]);
});
