var CylinderCube = function(){
  this.cpct = 0.75;

  this.rIn = 1;
  this.rOut = 2;
  this.xmin = -1;
  this.xmax = 1;
  this.color = [1,0.2,0.2];

  this.xMaxDiskInstances = [];
  this.xMinDiskInstances = [];
  this.squareEndInstances = [];

  this.initialize();
};

CylinderCube.prototype.generateSquare = function(sq){
  var sqN = [0,0,1];

  sq.setPosition(0, [this.xmin, this.rIn, 0]);
  sq.setColor(0, this.color);
  sq.setNormal(0, sqN);

  sq.setPosition(1, [this.xmax, this.rIn, 0]);
  sq.setColor(1, this.color);
  sq.setNormal(1, sqN);

  sq.setPosition(2, [this.xmax, this.rOut, 0]);
  sq.setColor(2, this.color);
  sq.setNormal(2, sqN);

  sq.setPosition(3, [this.xmin, this.rOut, 0]);
  sq.setColor(3, this.color);
  sq.setNormal(3, sqN);

  sq.setFace(0, 0, 0,1,2);
  sq.setFace(1, 0, 0,2,3);

  sq.setLine(0, 0, 0,1);
  sq.setLine(1, 0, 1,2);
  sq.setLine(2, 0, 2,3);
  sq.setLine(3, 0, 3,0);
};

CylinderCube.prototype.initialize = function(){
  var _this = this;
  var scolor = function(u,v,p){ return _this.color; };
  var ntheta = 50;

  //end caps
  this.square = new JS3D.GeomData();
  this.square.initialize({
    vertexCount: 4,
    elementCount: 6,
    elementWireCount: 8,
    hasNormals: true
  });
  this.generateSquare(this.square);

  //outer radius
  this.sOuter = new JS3D.GeomSurface({
    n: [1,ntheta], rangeIn: {u:[0,1], v:[0,2*Math.PI]},
    loopU: false, loopV: false, color: scolor,

    f: function(u,v){
      return [
        _this.xmin*(1-u) + _this.xmax*u,
        _this.rOut*Math.cos(v*_this.cpct),
        _this.rOut*Math.sin(v*_this.cpct)
      ];
    }
  });

  //inner radius
  this.sInner = new JS3D.GeomSurface({
    n: [1,ntheta], rangeIn: {u:[0,1], v:[0,2*Math.PI]},
    loopU: false, loopV: false, color: scolor,

    f: function(u,v){
      return [
        _this.xmin*(1-u) + _this.xmax*u,
        _this.rIn*Math.cos(v*_this.cpct),
        _this.rIn*Math.sin(v*_this.cpct)
      ];
    }
  });

  //disk segment
  this.sDisk = new JS3D.GeomSurface({
    n: [1,ntheta], rangeIn: {u:[0,1], v:[0,2*Math.PI]},
    loopU: false, loopV: false, color: scolor,

    f: function(u,v){
      var ru = _this.rIn*(1-u) + _this.rOut*u;
      return [
        0,
        ru*Math.cos(v*_this.cpct),
        ru*Math.sin(v*_this.cpct)
      ];
    }
  });
};

CylinderCube.prototype.generate = function(){
  this.sOuter.generate();
  this.sInner.generate();
  this.sDisk.generate();

  for(var i=0; i<this.xMaxDiskInstances.length; i++){
    this.xMaxDiskInstances[i].transformation.offset = [-this.xmax,0,0];
  }
  for(var i=0; i<this.xMinDiskInstances.length; i++){
    this.xMinDiskInstances[i].transformation.offset = [-this.xmin,0,0];
  }

  this.generateSquare(this.square);
  for(var i=0; i<this.squareEndInstances.length; i++){
    var sqt = this.squareEndInstances[i].transformation;
    sqt.identity();
    sqt.rotation.leftMultiplyAxisAngle([1,0,0], 2*Math.PI*this.cpct);
  }
};

CylinderCube.prototype.instance = function(target){
  target.push(this.sOuter.instance());
  target.push(this.sInner.instance());

  var xMaxDisk = this.sDisk.instance();
  var xMinDisk = this.sDisk.instance();
  xMaxDisk.transformation.offset = [-this.xmax,0,0];
  xMinDisk.transformation.offset = [-this.xmin,0,0];
  target.push(xMaxDisk, xMinDisk);
  this.xMaxDiskInstances.push(xMaxDisk);
  this.xMinDiskInstances.push(xMinDisk);

  var squareStart = this.square.instance();
  var squareEnd = this.square.instance();
  squareStart.wireframeVisible = true;
  squareEnd.wireframeVisible = true;
  squareEnd.transformation.rotation.leftMultiplyAxisAngle([1,0,0], 2*Math.PI*this.cpct);
  target.push(squareStart, squareEnd);
  this.squareEndInstances.push(squareEnd);
};
