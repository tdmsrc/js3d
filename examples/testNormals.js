var prepareTorusNormals = function(){

  //GRAPH
  //----------------------------
  var gt = 0;
  var graphOpts = {
    n: [50,30],
    rangeIn: {u:[0,2*Math.PI], v:[0,2*Math.PI]},
    loopU: true, loopV: true,

    f: function(u,v){
      var a = 1.5; //big radius
      var b = 0.8 + 0.1*Math.cos(3*u-3*gt);
      return [
        Math.cos(u)*(a + b*Math.cos(v)),
        Math.sin(u)*(a + b*Math.cos(v)),
        b*Math.sin(v)
      ];
    },

    color: function(u,v,p){
      var lu = (u-this.rangeIn.u[0])/(this.rangeIn.u[1]-this.rangeIn.u[0]);
      var lv = (v-this.rangeIn.v[0])/(this.rangeIn.v[1]-this.rangeIn.v[0]);
      return [0.5+0.5*Math.sin((lv+0.25)*2*Math.PI), 0.5, 0.5+0.5*Math.sin(lu*2*Math.PI)];
    }
  };
  var graph = new JS3D.GeomSurface(graphOpts);

  graph.generate();

  var graphSO = graph.instance();
  graphSO.emissiveCoefficient = 0.3;
  graphSO.wireframeVisible = true;
  graphSO.wireframeColor = [0,0,0,0.3];
  graphSO.solidVisible = true;
  graphSO.transformation.rotation.leftMultiplyAxisAngle([1,0,0], -0.3);

  //VECTORS
  //----------------------------
  //TODO: add "probes" feature for GeomGraph, GeomCurve
  //in the meantime, pull gridVerts directly

  var vectorlist = [];
  var nn = (graphOpts.n[0]+1)*(graphOpts.n[1]+1);
  for(var i=0; i<nn; i+=5){
    vectorlist.push(graph._gridVerts[i]);
  }

  var vector = new JS3D.GeomVectors({
    vectors: vectorlist,
    color: [0.2,0.5,1],
    radius: 0.06, size: 0.36
  });

  vector.generate();

  var vectorSO = vector.instance();
  vectorSO.emissiveCoefficient = 1;
  vectorSO.stickTo(graphSO);
  vectorSO.translucent = true;
  vectorSO.solidVisible = true;
  vectorSO.wireframeVisible = true;
  vectorSO.wireframeColor = [0,0,0,0.6];
  vectorSO.alpha = 0.4;

  //TEXT
  //----------------------------
  var textOpts = {
    maxlen: 40,
    text: "Text can move along animated surfaces!",

    tessellation: [1,1],
    pos: function(u,v){
      u*=0.004;
      v*=0.006;

      u += gt;
      v += Math.PI*0.5;

      var a = 1.5; //big radius
      var b = 0.9 + 0.1*Math.cos(3*u-3*gt);
      return [
        Math.cos(u)*(a + b*Math.cos(v)),
        Math.sin(u)*(a + b*Math.cos(v)),
        b*Math.sin(v)
      ];
    },
  };

  var gtext = new JS3D.GeomText(textOpts);

  gtext.generate();

  var gtextSO = gtext.instance();
  gtextSO.emissiveCoefficient = 1.0;
  gtextSO.emissiveColor = [1,1,1];
  gtextSO.stickTo(graphSO);
  gtextSO.wireframeVisible = false;
  gtextSO.wireframeColor = [0,0,0,0.8];
  gtextSO.solidVisible = true;
  gtextSO.castsShadows = true;

  //INITIALIZE
  //----------------------------
  var initTorusNormals = function(gl, fig){

    //TODO: FONT TEXTURE
    //var texFont = new JS3D.WGLTexture(gl, gl.canvas.width, gl.canvas.height);
    //texFont.setImage(gl, "img/bg.jpg");

    //LIGHTS AND SCENE
    var light1 = new JS3D.Light();
    light1.camera.position = [3,4,5];
    light1.camera.lookAt([0,0,0]);
    light1.color = [0.8,0.8,1];

    var light2 = new JS3D.Light();
    light2.camera.position = [-3,4,5];
    light2.camera.lookAt([0,0,0]);
    light2.color = [1,0.8,0.7];

    fig.renderer.lights = [light1, light2];

    fig.addToScene(gl, graphSO);
    fig.addToScene(gl, vectorSO);
    fig.addToScene(gl, gtextSO);

    //MOUSEMOVE
    //----------------------------
    //graph/vector update
    var RADIANS_PER_PIXEL = 0.007;

    fig.onMousemove = function(dx, dy, dt){
      graphSO.transformation.rotation.leftMultiplyAxisAngle([0,1,0], dx*RADIANS_PER_PIXEL);
      graphSO.transformation.rotation.leftMultiplyAxisAngle([1,0,0], dy*RADIANS_PER_PIXEL);
    };

    fig.animationLoop = function(dt){
      //update graph geometry
      gt += dt*0.001;

      graph.generate();
      fig.update(gl, graphSO);

      gtext.generate();
      fig.update(gl, gtextSO);

      vector.generate();
      fig.update(gl, vectorSO);
    };
  };

  return initTorusNormals;
};
