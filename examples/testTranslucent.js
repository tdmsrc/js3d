var prepareTranslucent = function(){

  //CUBE GEOMETRY
  //----------------------------
  var cvs = [
    [0,0,0], [0,0,1], [0,1,0], [0,1,1],
    [1,0,0], [1,0,1], [1,1,0], [1,1,1]
  ];
  for(var i=0; i<cvs.length; i++){
    var p = cvs[i];
    cvs[i] = [3*(p[0]-0.5), 3*(p[1]-0.5), 3*(p[2]-0.5)];
  }

  var ces = [
    [0,1], [2,3], [4,5], [6,7],
    [0,2], [1,3], [4,6], [5,7],
    [0,4], [1,5], [2,6], [3,7]
  ];
  var cfn = [
    [-1,0,0], [ 1,0,0], [0,0,-1],
    [0,0, 1], [0,-1,0], [0, 1,0]
  ];
  var cfs = [
    [0,1,3,2], [5,4,6,7], [0,2,6,4],
    [1,5,7,3], [0,4,5,1], [2,3,7,6]
  ];

  //STICKS
  //----------------------------
  var stdvs = [];
  for(var i=0; i<cvs.length; i++){
    stdvs.push({ position: cvs[i] });
  }

  var polyV = [];
  for(var i=0; i<stdvs.length; i++){
    polyV.push({ vertex: stdvs[i] });
  }

  var polyE = [];
  for(var i=0; i<ces.length; i++){
    var j = ces[i][0];
    var k = ces[i][1];
    polyE.push({ vertices: [stdvs[j], stdvs[k]] });
  }

  var skele = new JS3D.GeomSkeleton({
    balls: polyV, tubes: polyE,
    ballRadius: 0.08, tubeRadius: 0.04,
    ballColor: [1,0.5,0.8], tubeColor: [0.2,0.5,1]
  });

  skele.generate();

  var skeleSO = skele.instance();
  skeleSO.emissiveCoefficient = 0.2;
  skeleSO.wireframeVisible = true;
  skeleSO.wireframeColor = [0,0,0,0.1];
  skeleSO.solidVisible = true;
  skeleSO.translucent = false;

  //POLYHEDRON
  //----------------------------
  //TODO: modify GeomSkeleton so that it can produce faces
  //in the meantime, create the geometry directly
  var poly = new JS3D.GeomData();
  var polySO = poly.instance();

  skeleSO.stickTo(polySO);

  polySO.translucent = false;
  polySO.emissiveCoefficient = 0.2;

  (function(){
    poly.initialize({
      vertexCount: 4*cfs.length,
      elementCount: 3*12,
      elementWireCount: 2,
      hasNormals: true
    });

    var polyC = [1.0, 1.0, 1.0];
    var vi = 0, fi = 0;

    var putFace = function(faceN, i,j,k,l){
      poly.setPosition(vi+0, cvs[i]);
      poly.setColor(vi+0, polyC);
      poly.setNormal(vi+0, faceN);

      poly.setPosition(vi+1, cvs[j]);
      poly.setColor(vi+1, polyC);
      poly.setNormal(vi+1, faceN);

      poly.setPosition(vi+2, cvs[k]);
      poly.setColor(vi+2, polyC);
      poly.setNormal(vi+2, faceN);

      poly.setPosition(vi+3, cvs[l]);
      poly.setColor(vi+3, polyC);
      poly.setNormal(vi+3, faceN);

      poly.setFace(fi++, vi, 0,1,2);
      poly.setFace(fi++, vi, 0,2,3);
      vi+=4;
    };

    var i;
    i=3; putFace(cfn[i], cfs[i][0], cfs[i][1], cfs[i][2], cfs[i][3]);
    i=0; putFace(cfn[i], cfs[i][0], cfs[i][1], cfs[i][2], cfs[i][3]);
  })();

  //GRAPH
  //----------------------------
  var graphOpts = {
    n: [100,100],
    rangeIn: {u:[0,1], v:[0,1]},
    loopU: false, loopV: false,

    f: function(u,v){
      return [
        3*(u-0.5),3*(v-0.5),3*(0-0.5)
      ];
    },

    color: function(u,v,p){
      var d = Math.sqrt(2.0 * ((u-0.5)*(u-0.5)+(v-0.5)*(v-0.5)));
      var stripeu = 0.5+0.5*Math.sin(150*u);
      var stripev = 0.5+0.5*Math.sin(150*v);
      return [
        stripeu,stripev,1-stripeu
      ];
    }
  };
  var graph = new JS3D.GeomSurface(graphOpts);

  graph.generate();

  var graphSO = graph.instance();
  graphSO.emissiveCoefficient = 0.2;
  graphSO.wireframeVisible = false;
  graphSO.solidVisible = true;
  graphSO.translucent = true;
  graphSO.alpha = 0.5;

  graphSO.stickTo(polySO);

  //INITIALIZE
  //----------------------------
  var initTranslucent = function(gl, fig){

    fig.addToScene(gl, graphSO);
    fig.addToScene(gl, polySO);
    fig.addToScene(gl, skeleSO);

    //MOUSEMOVE
    //----------------------------
    //graph/vector update
    var RADIANS_PER_PIXEL = 0.007;

    fig.onMousemove = function(dx, dy, dt){
      polySO.transformation.rotation.leftMultiplyAxisAngle([0,1,0], dx*RADIANS_PER_PIXEL);
      polySO.transformation.rotation.leftMultiplyAxisAngle([1,0,0], dy*RADIANS_PER_PIXEL);
    };
  };


  return initTranslucent;
};
