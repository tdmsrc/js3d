var prepareHopf = function(){

  //parameterization of the fiber over (a,b,c) \in S^2
  var hopf = function(a,b,c,t){
      var ct = Math.cos(t), st = Math.sin(t);

      var x = (1+c)*ct, y = a*st-b*ct, z = a*ct+b*st, w = (1+c)*st;
      var d = 1 / (Math.sqrt(2*(1+c)) - x);
      return [y*d, z*d, w*d];
  };

  var makeFiber = function(a,b,c){
    return {
      n: 150, tubeN: 6, tubeRadius: 0.05,
      range: [0,2*Math.PI],
      loop: true,
      f: function(t){
        return hopf(a,b,c,t);
      },
      color: function(t,theta,p){
        return [0.5+0.5*Math.sin(theta), 0.5, 0.5+0.5*Math.sin(t)];
      }
    };
  };

  var hopfs = [], hopfsSO = [];
  for(var i=0; i<50; i++){
    var t = 2*Math.PI*i/50;

    var theta = 4*t;
    var phi = -Math.PI/2+0.1 + 0.85*t*1/2;

    var a = Math.cos(theta), b = Math.sin(theta), c = 0;
    a = a*Math.cos(phi);
    b = b*Math.cos(phi);
    c = Math.sin(phi);

    var hfOpts = makeFiber(a,b,c);
    var hf = new JS3D.GeomCurve(hfOpts);
    hf.generate();
    hopfs.push(hf);
    hopfsSO.push(hf.instance());
  }

  for(var i=1; i<hopfsSO.length; i++){
    hopfsSO[i].stickTo(hopfsSO[0]);
  }
  for(var i=0; i<hopfsSO.length; i++){
    hopfsSO[i].emissiveCoefficient = 0.2;
  }

  //INITIALIZE
  //----------------------------
  var initHopf = function(gl, fig){
    fig.renderer.colorAdjustSaturation = -0.5;

    for(var i=0; i<hopfsSO.length; i++){
      fig.addToScene(gl, hopfsSO[i]);
    }

    var RADIANS_PER_PIXEL = 0.007;
    fig.onMousemove = function(dx, dy, dt){
      hopfsSO[0].transformation.rotation.leftMultiplyAxisAngle([0,1,0], dx*RADIANS_PER_PIXEL);
      hopfsSO[0].transformation.rotation.leftMultiplyAxisAngle([1,0,0], dy*RADIANS_PER_PIXEL);
    };
  };

  return initHopf;
};
