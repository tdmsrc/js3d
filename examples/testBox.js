var prepareCurveInBox = function(){

  //RAW GEOMETRY
  //----------------------------
  var cvs = [
    [0,0,0], [0,0,1], [0,1,0], [0,1,1],
    [1,0,0], [1,0,1], [1,1,0], [1,1,1]
  ];
  for(var i=0; i<cvs.length; i++){
    var p = cvs[i];
    cvs[i] = [3*(p[0]-0.5), 3*(p[1]-0.5), 3*(p[2]-0.5)];
  }

  var ces = [
    [0,1], [2,3], [4,5], [6,7],
    [0,2], [1,3], [4,6], [5,7],
    [0,4], [1,5], [2,6], [3,7]
  ];
  var cfn = [
    [-1,0,0], [ 1,0,0], [0,0,-1],
    [0,0, 1], [0,-1,0], [0, 1,0]
  ];
  var cfs = [
    [0,1,3,2], [5,4,6,7], [0,2,6,4],
    [1,5,7,3], [0,4,5,1], [2,3,7,6]
  ];

  //STICKS
  //----------------------------
  var stdvs = [];
  for(var i=0; i<cvs.length; i++){
    stdvs.push({ position: cvs[i] });
  }

  var polyV = [];
  for(var i=0; i<stdvs.length; i++){
    polyV.push({ vertex: stdvs[i] });
  }

  var polyE = [];
  for(var i=0; i<ces.length; i++){
    var j = ces[i][0];
    var k = ces[i][1];
    polyE.push({ vertices: [stdvs[j], stdvs[k]] });
  }

  var skele = new JS3D.GeomSkeleton({
    balls: polyV, tubes: polyE,
    ballRadius: 0.08, tubeRadius: 0.04,
    ballColor: [1,0.5,0.8], tubeColor: [0.2,0.5,1]
  });

  skele.generate();

  var skeleSO = skele.instance();

  //POLYHEDRON
  //----------------------------
  //TODO: modify GeomSkeleton so that it can produce faces
  //in the meantime, create the geometry directly
  var poly = new JS3D.GeomData();
  var polySO = poly.instance();

  skeleSO.stickTo(polySO);

  polySO.translucent = true;
  polySO.alpha = 0.8;

  (function(){
    poly.initialize({
      vertexCount: 4*cfs.length,
      elementCount: 3*12,
      elementWireCount: 2,
      hasNormals: true
    });

    var polyC = [0.4, 0.4, 1];
    var vi = 0, fi = 0;

    var putFace = function(faceN, i,j,k,l){
      poly.setPosition(vi+0, cvs[i]);
      poly.setColor(vi+0, polyC);
      poly.setNormal(vi+0, faceN);

      poly.setPosition(vi+1, cvs[j]);
      poly.setColor(vi+1, polyC);
      poly.setNormal(vi+1, faceN);

      poly.setPosition(vi+2, cvs[k]);
      poly.setColor(vi+2, polyC);
      poly.setNormal(vi+2, faceN);

      poly.setPosition(vi+3, cvs[l]);
      poly.setColor(vi+3, polyC);
      poly.setNormal(vi+3, faceN);

      poly.setFace(fi++, vi, 0,1,2);
      poly.setFace(fi++, vi, 0,2,3);
      vi+=4;
    };

    for(var i=0; i<cfs.length; i++){
      putFace(cfn[i], cfs[i][0], cfs[i][1], cfs[i][2], cfs[i][3]);
    }
  })();

  //CURVE
  //----------------------------
  var cparam = 0;
  var curveOpts = {
    n: 400, tubeN: 15, tubeRadius: 0.2,
    range: [0,2*Math.PI],
    loop: true,
    f: function(t){
      var x1 = Math.cos(2*t), y1 = Math.sin(2*t);
      var x2 = x1*Math.cos(5*t), y2 = y1*Math.cos(5*t), z2 = Math.sin(5*t);

      var outr = 1.5, inr = 0.7 + 0.4*Math.sin(2*cparam);
      return [outr*x1 + inr*x2, outr*y1 + inr*y2, inr*z2];
    },
    color: function(t,theta,p){
      return [0.5+0.5*Math.sin(theta), 0.5, 0.5+0.5*Math.sin(t)];
    }
  };
  var curve = new JS3D.GeomCurve(curveOpts);

  curve.generate();
  var curveSO = curve.instance();
  curveSO.emissiveCoefficient = 0.4;
  curveSO.wireframeVisible = true;
  curveSO.wireframeColor = [0.0, 0.0, 0.0, 0.5];
  curveSO.solidVisible = true;

  curveSO.stickTo(polySO);

  //INITIALIZE
  //----------------------------
  var initCurveInBox = function(gl, fig){

    fig.addToScene(gl, polySO);
    fig.addToScene(gl, skeleSO);
    fig.addToScene(gl, curveSO);

    //MOUSEMOVE
    //----------------------------
    //graph/vector update
    var RADIANS_PER_PIXEL = 0.007;

    fig.onMousemove = function(dx, dy, dt){
      polySO.transformation.rotation.leftMultiplyAxisAngle([0,1,0], dx*RADIANS_PER_PIXEL);
      polySO.transformation.rotation.leftMultiplyAxisAngle([1,0,0], dy*RADIANS_PER_PIXEL);
    };

    fig.animationLoop = function(dt){
      //update curve geometry
      cparam += dt*0.001;

      curve.generate();
      fig.update(gl, curveSO);
    };
  };

  return initCurveInBox;
};
