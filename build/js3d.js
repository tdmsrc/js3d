(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.JS3D = f()}})(function(){var define,module,exports;return (function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathCam = require('../wglcore/mathCam');

var _light = require('../wglcore/light');

var _light2 = _interopRequireDefault(_light);

var _geomSceneObject = require('../wglcore/geomSceneObject');

var _geomSceneObject2 = _interopRequireDefault(_geomSceneObject);

var _wglDrawable = require('../wglcore/wglDrawable');

var _wglDrawable2 = _interopRequireDefault(_wglDrawable);

var _figRender = require('./figRender');

var _figRender2 = _interopRequireDefault(_figRender);

var _touches = require('../wgltools/touches');

var _touches2 = _interopRequireDefault(_touches);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FIGURE_COUNT = 0;

// ===================================
// FIGURE
// single-canvas figure
// ===================================

/** Maintains a collection of {@link SceneObject} to be rendered.
*   <ul>
*   <li>HTMLCanvasElement and {@link WGLRenderer} are required for rendering.
*     Creation and maintenance of these is handled automatically.
*   <li>{@link GeomData} need associated {@link WGLDrawable} to be rendered.
*     Creation and association of drawables to geometry is handled automatically.
*   </ul>
*
* @param {HTMLElement} target - Where the canvas will be put
* @param {Function} onInitialize - Invoked when WebGLRenderingContext is ready
*   <br>(<code>function(gl, fig){ ... }</code>, "gl" is WebGLRenderingContext, "fig" is this {@link Figure})
*
* @prop {WGLRenderer} renderer - The {@link WGLRenderer} for this figure
* @prop {boolean} animate - If true, will continually invoke animationLoop
* @prop {Function} animationLoop - Can be modified freely
*   <br>(<code>function(dt){ ... }</code>, "dt" = time elapsed since last frame)
* @prop {autoResize} boolean - Continually check for canvas resize during main loop (default true)
*/

var Figure = function () {
  function Figure(target, onInitialize) {
    var _this = this;

    _classCallCheck(this, Figure);

    this._uniqueID = FIGURE_COUNT++;

    // scene
    this._objects = []; // SceneObject[]

    // figure options: auto-resize, animate
    this.autoResize = true;
    this.animate = false;
    this.animationLoop = function (dt) {};

    // init canvas, renderer, and GL
    var canvas = document.createElement('canvas');
    canvas.className = 'fig-canvas';
    target.appendChild(canvas);

    var renderer = new _figRender2.default();
    var gl = renderer.initialize(canvas);
    if (!gl) {
      return;
    } // TODO

    this.renderer = renderer;

    // default camera and light(s)
    renderer.camera.position = [0, 0, 6];
    renderer.camera.lookAt([0, 0, 0]);

    var light1 = new _light2.default();
    renderer.lights.push(light1);
    light1.camera.position = [2, 2, 5];
    light1.camera.lookAt([0, 0, 0]);
    light1.color = [1, 1, 1];

    // tell renderer how to traverse the scene
    renderer.sceneTraversal = function (handler) {
      _this.traverseScene(handler);
    };

    // user initialization
    onInitialize(gl, this);
    this._postInitialize(gl, canvas, renderer);
  }

  // -----------------------------------


  _createClass(Figure, [{
    key: '_postInitialize',
    value: function _postInitialize(gl, canvas, renderer) {
      var _this2 = this;

      // prevent right-click context menu
      canvas.oncontextmenu = function () {
        return false;
      };

      // TODO: (this.animate = !this.animate) on left click (?)
      // TODO: canvas.toDataURL('image/png') on right click (?)
      var mouseDP = [0, 0];
      var mouseMoved = false;
      (0, _touches2.default)(canvas, {
        start: function start(buttons, p) {},
        end: function end(buttons, p) {},
        move: function move(buttons, p, q) {
          if (buttons !== 1) {
            return;
          }
          mouseMoved = true;
          mouseDP[0] += q[0] - p[0];
          mouseDP[1] += q[1] - p[1];
        },
        click: function click(buttons, p) {
          if (buttons === 1) {
            _this2.animate = !_this2.animate;
          } else if (buttons === 2) {
            console.log(canvas.toDataURL('image/png'));
          }
        }
      });

      // main loop
      var frameCount = 0;
      var lastTime = Date.now();
      var lastFPS = lastTime;

      var loop = function loop() {
        requestAnimationFrame(loop);

        // timing and fps
        var curTime = Date.now();

        var dt = curTime - lastTime;
        var dtFPS = curTime - lastFPS;

        lastTime = curTime;
        if (dtFPS > 2000) {
          frameCount = 0;
          lastFPS = curTime;
        }

        frameCount++;

        // check for redraw
        var redraw = false;

        // auto-resize
        if (_this2.autoResize) {
          var resized = renderer.resize(gl);
          if (resized) {
            redraw = true;
          }
        }

        // handle mousemove
        if (mouseMoved) {
          mouseMoved = false;
          redraw = true;

          var dx = mouseDP[0];
          var dy = mouseDP[1];
          mouseDP[0] = 0;mouseDP[1] = 0;

          if (typeof _this2.onMousemove === 'function') {
            _this2.onMousemove(dx, dy, dt);
          }
        }

        // handle animation
        if (_this2.animate) {
          redraw = true;
          _this2.animationLoop(dt);
        }

        if (redraw) {
          _this2.draw(gl);
        }
      };
      loop();

      // initial draw
      this.draw(gl);
    }

    // store drawables as geometry._drawable[fig._uniqueID]
    // NOTE: why not just store a WGLDrawable directly in the geometry?
    // because a geometry can be shared across multiple figures, and
    // each figure must have its own WGLDrawable instance)
    // -----------------------------------

  }, {
    key: '_getDrawable',
    value: function _getDrawable(geometry) {
      // ensure array is at least not undefined or null
      if (!geometry._drawable) {
        geometry._drawable = [];
      }

      // entry for this Figure may be undefined or null
      return geometry._drawable[this._uniqueID];
    }
  }, {
    key: '_createDrawable',
    value: function _createDrawable(gl, geometry) {
      // ensure array is at least not undefined or null
      if (!geometry._drawable) {
        geometry._drawable = [];
      }

      // create and set drawable
      var drawable = new _wglDrawable2.default(gl, geometry);
      geometry._drawable[this._uniqueID] = drawable;
      return drawable;
    }

    // -----------------------------------
    /** Add a {@link SceneObject} to the scene.
    *   <br>Creates any necessary {@link WGLDrawable} objects.
    * @param {WebGLRenderingContext} gl
    * @param {SceneObject} sceneObject
    */

  }, {
    key: 'addToScene',
    value: function addToScene(gl, sceneObject) {
      var geomArray = sceneObject.geomArray;


      for (var i = 0; i < geomArray.length; i++) {
        var geometry = geomArray[i];

        // make sure the geometry has an associated drawable
        if (!this._getDrawable(geometry)) {
          this._createDrawable(gl, geometry);
        }
      }

      // add to scene
      this._objects.push(sceneObject);
    }

    /** Update any {@link WGLDrawable} objects underlying a {@link SceneObject}.
    * @param {WebGLRenderingContext} gl
    * @param {SceneObject} sceneObject
    */

  }, {
    key: 'update',
    value: function update(gl, sceneObject) {
      var geomArray = sceneObject.geomArray;


      for (var i = 0; i < geomArray.length; i++) {
        var geometry = geomArray[i];

        // create if drawable doesn't exist, else just update existing
        var drawable = this._getDrawable(geometry);
        if (drawable) {
          drawable.update(gl, geometry);
        }
      }
    }

    /** Traverse the scene, utilizing the handler.
    * @param {WGLRenderer~sceneTraversalHandler} handler
    */

  }, {
    key: 'traverseScene',
    value: function traverseScene(handler) {
      // [lazy evaluation] reset flag signifying whether a "draw" occurred
      var _hadFirstDraw = false;

      for (var k = 0; k < this._objects.length; k++) {
        var sObj = this._objects[k];

        // if handler rejects sceneObject, do nothing
        if (!handler.filterSceneObject(sObj)) {
          continue;
        }

        // [lazy evaluation] reset flag signifying whether a drawable for sObj passed the filter
        var _hadFirstDrawable = false;

        // loop through each of sObj's drawables
        var geomArray = sObj.geomArray;

        for (var i = 0; i < geomArray.length; i++) {
          var drawable = this._getDrawable(geomArray[i]);

          // if handler rejects drawable, do nothing
          if (!handler.filterDrawable(drawable)) {
            continue;
          }

          // [lazy evaluation] invoke "before"
          if (!_hadFirstDraw) {
            _hadFirstDraw = true;
            handler.before();
          }

          // [lazy evaluation] invoke "execSceneObject"
          if (!_hadFirstDrawable) {
            _hadFirstDrawable = true;
            handler.execSceneObject(sObj);
          }

          // draw the drawable
          handler.execDrawable(drawable);
        }
      }

      // [lazy evaluation] invoke "after"
      if (_hadFirstDraw) {
        handler.after();
      }
    }

    /** Render the scene.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'draw',
    value: function draw(gl) {
      this.renderer.render(gl);
    }
  }]);

  return Figure;
}();

exports.default = Figure;

},{"../wglcore/geomSceneObject":13,"../wglcore/light":18,"../wglcore/mathCam":19,"../wglcore/wglDrawable":25,"../wgltools/touches":30,"./figRender":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathCam = require('../wglcore/mathCam');

var _wglTexture = require('../wglcore/wglTexture');

var _wglQuad = require('../wglcore/wglQuad');

var _wglQuad2 = _interopRequireDefault(_wglQuad);

var _figShaderQuad = require('./figShaderQuad');

var _figShader = require('./figShader');

var _glsl = require('../glsl');

var GLSL = _interopRequireWildcard(_glsl);

var _preprocessGLSLES = require('../wgltools/preprocessGLSLES');

var _preprocessGLSLES2 = _interopRequireDefault(_preprocessGLSLES);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// RENDERER
// ===================================

var LINE_WIDTH = 3;
var SUPERSAMPLE_FACTOR = 2;

var SHADOW_MAP_WIDTH = 512;
var SHADOW_MAP_HEIGHT = 512;

// TODO used for blur; ideally pegged to size of canvas
var MINI_BUFFER_WIDTH = 512;
var MINI_BUFFER_HEIGHT = 512;

/** The methods of the handler will be invoked repeatedly:
*   <br>For each {@link SceneObject}, call "filterSceneObject"; for each {@link WGLDrawable}
*   making up the geometry of the object, call "filterDrawable". If both are true, call "draw".
*   <br>The order is: "before" (once) -> loop{ "execSceneObject" (multiple) -> "execDrawable" (multiple) } -> "after" (once)
* @typedef {Object} WGLRenderer~sceneTraversalHandler
*
* @prop {Function} before - <code>function(){ ... }</code>;
*   <br>Guaranteed to be invoked before any execSceneObject, but may never be used during a traversal.
* @prop {Function} filterSceneObject - <code>function(sObj){ ... }</code>; "sObj" is a {@link SceneObject}.
*   <br>Return true if this object's geometry should be passed to prepareDrawable; return false otherwise.
* @prop {Function} filterDrawable - <code>function(drawable){ ... }</code>; "drawable" is a {@link WGLDrawable}.
*   <br>Return true if this drawable should be handled via "draw"; return false otherwise.
* @prop {Function} execSceneObject - <code>function(sObj){ ... }</code>; "sObj" is a {@link SceneObject}.
* @prop {Function} execDrawable - <code>function(drawable){ ... }</code>; "drawable" is a {@link WGLDrawable}
* @prop {Function} after - <code>function(){ ... }</code>;
*   <br>Guaranteed to be invoked after every execDrawable has been completed, but may never be used during a traversal.
*/

/** Renderer for a scene.
*   <br>The properties below define what is rendered; these can be modified directly.
*   <br>Call {@link WGLRenderer#initialize} before rendering or resizing.
*
* @prop {number[]} backgroundColor - Clear color (as [r,g,b,a], no premultiplied alpha)
* @prop {Light[]} lights - Unlimited light sources
* @prop {Camera} camera
* @prop {boolean} enableTranslucent - If false, skip any {@link SceneObject} marked as translucent
* @prop {boolean} enableShadows - If false, override individual {@link Light} options and disable all shadows
* @prop {number} blurAmount - How much to blend a blurred layer into the final image
* @prop {number} colorAdjustBrightness
* @prop {number} colorAdjustContrast
* @prop {number} colorAdjustSaturation
* @prop {Function} sceneTraversal - Function which performs a traversal of the scene, utilizing the handler
*   <br>(<code>function(handler){ ... }</code>; "handler" is a {@link WGLRenderer~sceneTraversalHandler})
*/

var WGLRenderer = function () {
  function WGLRenderer() {
    _classCallCheck(this, WGLRenderer);

    // options, can be modified/replaced directly without incident
    this.lights = [];
    this.camera = new _mathCam.Camera();

    this.backgroundColor = [0, 0, 0, 0]; // no premultiplied alpha

    this.blurAmount = 0.3;
    this.colorAdjustBrightness = 0.0;
    this.colorAdjustContrast = 1.0;
    this.colorAdjustSaturation = 1.0;

    this.enableShadows = true;
    this.enableTranslucent = true;

    // handler: see doc for WGLRenderer~sceneTraversalHandler
    this.sceneTraversal = function (handler) {};
  }

  // initialization
  // -----------------------------------
  /** Get context for a canvas and prepare necessary shaders, textures, etc.
  * @param {HTMLCanvasElement} canvas
  * @return {WebGLRenderingContext}
  */


  _createClass(WGLRenderer, [{
    key: 'initialize',
    value: function initialize(canvas) {
      var _this = this;

      // get context
      var gl = null;

      var opts = {
        premultipliedAlpha: true, // is default, but make sure
        preserveDrawingBuffer: true // allow toDataURL (snapshots), etc
      };
      try {
        gl = canvas.getContext('webgl', opts) || canvas.getContext('experimental-webgl', opts);
      } catch (e) {
        console.error(e);
      }

      if (!gl) {
        console.error('Unable to obtain WebGLRenderingContext!');
        return null;
      }

      // proprocess GLSL
      var texIncludes = {
        lighting: GLSL.inclLighting,
        packDepthColor: GLSL.inclPackDepthColor,
        isShadowedPCF: GLSL.inclIsShadowedPCF,
        getColorVert: GLSL.inclGetColorTexVert,
        getColorFrag: GLSL.inclGetColorTexFrag
      };

      var colorIncludes = {
        lighting: GLSL.inclLighting,
        packDepthColor: GLSL.inclPackDepthColor,
        isShadowedPCF: GLSL.inclIsShadowedPCF,
        getColorVert: GLSL.inclGetColorVert,
        getColorFrag: GLSL.inclGetColorFrag
      };

      // no tex version (color attribute only)
      this.shaderPrepass = new _figShader.WGLShaderPrepass(gl, false, (0, _preprocessGLSLES2.default)(GLSL.prepassVert, colorIncludes), (0, _preprocessGLSLES2.default)(GLSL.prepassFrag, colorIncludes));

      this.shaderDepth = new _figShader.WGLShaderDepth(gl, false, (0, _preprocessGLSLES2.default)(GLSL.depthVert, colorIncludes), (0, _preprocessGLSLES2.default)(GLSL.depthFrag, colorIncludes));
      this.shaderDepthColor = new _figShader.WGLShaderDepthColor(gl, false, (0, _preprocessGLSLES2.default)(GLSL.depthColorVert, colorIncludes), (0, _preprocessGLSLES2.default)(GLSL.depthColorFrag, colorIncludes));

      this.shaderLightpass = new _figShader.WGLShaderLightpass(gl, false, (0, _preprocessGLSLES2.default)(GLSL.lightpassVert, colorIncludes), (0, _preprocessGLSLES2.default)(GLSL.lightpassFrag, colorIncludes));
      this.shaderLightpassShadow = new _figShader.WGLShaderLightpassShadow(gl, false, (0, _preprocessGLSLES2.default)(GLSL.lightpassShadowVert, colorIncludes), (0, _preprocessGLSLES2.default)(GLSL.lightpassShadowFrag, colorIncludes));

      // tex version
      this.shaderPrepassTex = new _figShader.WGLShaderPrepass(gl, true, (0, _preprocessGLSLES2.default)(GLSL.prepassVert, texIncludes), (0, _preprocessGLSLES2.default)(GLSL.prepassFrag, texIncludes));

      this.shaderDepthTex = new _figShader.WGLShaderDepth(gl, true, (0, _preprocessGLSLES2.default)(GLSL.depthVert, texIncludes), (0, _preprocessGLSLES2.default)(GLSL.depthFrag, texIncludes));
      this.shaderDepthColorTex = new _figShader.WGLShaderDepthColor(gl, true, (0, _preprocessGLSLES2.default)(GLSL.depthColorVert, texIncludes), (0, _preprocessGLSLES2.default)(GLSL.depthColorFrag, texIncludes));

      this.shaderLightpassTex = new _figShader.WGLShaderLightpass(gl, true, (0, _preprocessGLSLES2.default)(GLSL.lightpassVert, texIncludes), (0, _preprocessGLSLES2.default)(GLSL.lightpassFrag, texIncludes));
      this.shaderLightpassShadowTex = new _figShader.WGLShaderLightpassShadow(gl, true, (0, _preprocessGLSLES2.default)(GLSL.lightpassShadowVert, texIncludes), (0, _preprocessGLSLES2.default)(GLSL.lightpassShadowFrag, texIncludes));

      // create shaders
      this.shaderWireframe = new _figShader.WGLShaderWireframe(gl, GLSL.wireframeVert, GLSL.wireframeFrag);

      this.shaderQuadCopy = new _figShaderQuad.WGLShaderQuadCopy(gl, 'quad copy', GLSL.quadVert, GLSL.quadFragCopy);
      this.shaderQuadBlurH = new _figShaderQuad.WGLShaderQuadBlur(gl, 'quad blur H', GLSL.quadVert, GLSL.quadFragBlurH);
      this.shaderQuadBlurV = new _figShaderQuad.WGLShaderQuadBlur(gl, 'quad blur V', GLSL.quadVert, GLSL.quadFragBlurV);
      this.shaderQuadPost = new _figShaderQuad.WGLShaderQuadPost(gl, 'quad post-processing', GLSL.quadVert, GLSL.quadFragPost);

      // create FBO and texture(s)
      this.fbo = new _wglTexture.WGLFramebufferObject(gl);
      this.texRender = new _wglTexture.WGLTexture(gl, gl.canvas.width, gl.canvas.height);
      this.texBlend = new _wglTexture.WGLTexture(gl, gl.canvas.width, gl.canvas.height);
      this.texShadowMap = new _wglTexture.WGLTexture(gl, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT, false);

      this.texMiniFront = new _wglTexture.WGLTexture(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
      this.texMiniBack = new _wglTexture.WGLTexture(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);

      // create quad
      this.quad = new _wglQuad2.default(gl);

      // TODO
      // ---------------------------------------
      this.texTest = new _wglTexture.WGLTexture(gl, gl.canvas.width, gl.canvas.height);
      this.texTest.setImage(gl, 'img/glfont.png', function () {
        _this.render(gl);
      });
      //---------------------------------------


      // return context
      return gl;
    }

    /** Resize canvas and drawing buffer to specified size.
    *   <br>Also changes the aspect ratio of the currently set {@link Camera}.
    * @param {WebGLRenderingContext} gl
    * @param {number} w - New canvas width
    * @param {number} h - New canvas height
    */

  }, {
    key: 'setSize',
    value: function setSize(gl, w, h) {
      gl.canvas.width = w;
      gl.canvas.height = h;

      this.camera.setAspect(w / h);

      this.texRender.resize(gl, SUPERSAMPLE_FACTOR * w, SUPERSAMPLE_FACTOR * h);
      this.texBlend.resize(gl, w, h);
    }

    /** Resize canvas and drawing buffer if clientWidth/clientHeight do not match.
    * @param {WebGLRenderingContext} gl
    * @return {boolean} - Returns true iff a resize occurred
    * @see {@link WGLRenderer#setSize}
    */

  }, {
    key: 'resize',
    value: function resize(gl) {
      var w = gl.canvas.clientWidth;
      var h = gl.canvas.clientHeight;

      var resizeW = w > 0 && gl.canvas.width !== w;
      var resizeH = h > 0 && gl.canvas.height !== h;

      // this will not trigger if w==0 and h==0, which
      // seems to happen if the canvas is hidden
      if (resizeW || resizeH) {
        this.setSize(gl, w, h);
        return true;
      }
      return false;
    }

    // QUAD DRAWING/POST-PROCESSING
    // ==================================================

  }, {
    key: '_drawQuadInitialize',
    value: function _drawQuadInitialize(gl, texSrc, shader) {
      shader.use(gl);
      shader.prepareVertexAttribs(gl, this.quad);
      texSrc.bind(gl, _figShaderQuad.TEXUNIT_QUAD_COLOR);
    }
  }, {
    key: '_drawQuadFinish',
    value: function _drawQuadFinish(gl, texSrc, shader) {
      this.quad.drawTriangles(gl);
      texSrc.unbind(gl, _figShaderQuad.TEXUNIT_QUAD_COLOR);
      shader.unuse(gl);
    }

    // copy texSrc onto currently bound write texture

  }, {
    key: '_drawQuadCopy',
    value: function _drawQuadCopy(gl, texSrc) {
      this._drawQuadInitialize(gl, texSrc, this.shaderQuadCopy);
      this._drawQuadFinish(gl, texSrc, this.shaderQuadCopy);
    }

    // blur texture "texSrc" into "texMiniBack"

  }, {
    key: '_performBlur',
    value: function _performBlur(gl, texSrc) {
      // copy texSrc into texMiniBack
      this.texMiniBack.bindWrite(gl);
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

      this._drawQuadCopy(gl, texSrc);

      this.texMiniBack.unbindWrite(gl);

      // copy texMiniBack to texMiniFront using blur H
      this.texMiniFront.bindWrite(gl);
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

      this._drawQuadInitialize(gl, this.texMiniBack, this.shaderQuadBlurH);
      this.shaderQuadBlurH.setTextureSize(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
      this._drawQuadFinish(gl, this.texMiniBack, this.shaderQuadBlurH);

      this.texMiniFront.unbindWrite(gl);

      // copy texMiniFront to texMiniBack using blur V
      this.texMiniBack.bindWrite(gl);
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

      this._drawQuadInitialize(gl, this.texMiniFront, this.shaderQuadBlurV);
      this.shaderQuadBlurV.setTextureSize(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
      this._drawQuadFinish(gl, this.texMiniFront, this.shaderQuadBlurV);

      this.texMiniBack.unbindWrite(gl);
    }
    // ==================================================


    // RENDERING CORE
    // ==================================================

    /** Render a scene to the default framebuffer.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'render',
    value: function render(gl) {
      // everything is two-sided
      gl.disable(gl.CULL_FACE);
      gl.lineWidth(LINE_WIDTH);

      this.fbo.bind(gl);

      // opaque pass
      // ----------------------
      // tex cleared with backgroundColor
      // render opaque objects using premultiplied alpha
      this._renderOpaque(gl, this.texRender);

      // copy texRender -> texBlend
      this.texBlend.bindWrite(gl);
      gl.disable(gl.DEPTH_TEST);

      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);
      this._drawQuadCopy(gl, this.texRender);

      this.texBlend.unbindWrite(gl);

      // translucent pass
      // ----------------------
      if (this.enableTranslucent) {
        // render to texRender; use premultiplied alpha
        // texRender cleared with (0,0,0,0), but zbuffer maintained
        this._renderTranslucent(gl, this.texRender);

        // blend tex into texBlend
        this.texBlend.bindWrite(gl);
        gl.disable(gl.DEPTH_TEST);

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        this._drawQuadCopy(gl, this.texRender);
        gl.disable(gl.BLEND);

        this.texBlend.unbindWrite(gl);
      }

      // copy texBlend to canvas
      // ----------------------
      // draw blur -> texMiniBack
      this._performBlur(gl, this.texBlend);

      // prepare to draw to canvas
      this.fbo.unbind(gl);
      gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

      // copy texBlend -> canvas using QuadPost
      gl.disable(gl.DEPTH_TEST);
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);

      this._drawQuadInitialize(gl, this.texBlend, this.shaderQuadPost);
      this.shaderQuadPost.setOptions(gl, this.blurAmount, this.colorAdjustBrightness, this.colorAdjustContrast, this.colorAdjustSaturation);

      this.texMiniBack.bind(gl, _figShaderQuad.TEXUNIT_QUAD_BLUR);
      this._drawQuadFinish(gl, this.texBlend, this.shaderQuadPost);
      this.texMiniBack.unbind(gl, _figShaderQuad.TEXUNIT_QUAD_BLUR);
    }
  }, {
    key: '_renderOpaque',
    value: function _renderOpaque(gl) {
      // prepare: clear and prepass
      // ---------------------------
      // start render to texRender
      this.texRender.bindWrite(gl);

      // everything rendered here needs to use premultiplied alpha
      var a = this.backgroundColor[3];
      gl.clearColor(a * this.backgroundColor[0], a * this.backgroundColor[1], a * this.backgroundColor[2], a);
      gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

      // solid objects
      gl.enable(gl.DEPTH_TEST);
      this._renderPrepass(gl, false);

      // stop render to texRender
      this.texRender.unbindWrite(gl);
      // ---------------------------

      // solid objects
      for (var i = 0; i < this.lights.length; i++) {
        var light = this.lights[i];

        if (light.shadows && this.enableShadows) {
          this._renderShadowMapDepth(gl, light, false);
          if (this.enableTranslucent) {
            this._renderShadowMapColor(gl, light);
          }
          this._renderLightpassShadow(gl, light, false);
        } else {
          this._renderLightpassNoShadow(gl, light, false);
        }
      }

      // wireframe objects
      this._renderWireframe(gl, false);
    }
  }, {
    key: '_renderTranslucent',
    value: function _renderTranslucent(gl) {
      // prepare: clear and prepass
      // ---------------------------
      // start render to texRender
      this.texRender.bindWrite(gl);

      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);

      // solid objects
      gl.enable(gl.DEPTH_TEST);
      this._renderPrepass(gl, true);

      // stop render to texRender
      this.texRender.unbindWrite(gl);
      // ---------------------------

      // solid objects
      for (var i = 0; i < this.lights.length; i++) {
        var light = this.lights[i];

        if (light.shadows && this.enableShadows) {
          this._renderShadowMapDepth(gl, light, true);
          this._renderLightpassShadow(gl, light, true);
        } else {
          this._renderLightpassNoShadow(gl, light, true);
        }
      }

      // wireframe objects
      this._renderWireframe(gl, true);
    }

    // RENDER PASSES/SCENE TRAVERSALS
    // ==================================================
    // prepass: no need to bind/unbind texture
    // ----------------------------------------

  }, {
    key: '_renderPrepass',
    value: function _renderPrepass(gl, translucent) {
      var _this2 = this;

      var traversal = function traversal(hasTexture, shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            return sObj.solidVisible && sObj.translucent === translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return drawable.hasTexture === hasTexture;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, _this2.camera);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
            shader.setEmissiveUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            drawable.drawTriangles(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      gl.depthFunc(gl.LESS);

      // TODO color vs tex
      // ---------------------------
      this.sceneTraversal(traversal(false, this.shaderPrepass));

      this.texTest.bind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      this.sceneTraversal(traversal(true, this.shaderPrepassTex));
      this.texTest.unbind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      // ---------------------------
    }

    // wireframe pass
    // ----------------------------------------

  }, {
    key: '_renderWireframe',
    value: function _renderWireframe(gl, translucent) {
      var _this3 = this;

      var traversal = function traversal(shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            return sObj.wireframeVisible && sObj.translucent === translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return true;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, _this3.camera);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
            shader.setWireframeUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            drawable.drawLines(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      // start render to texRender
      this.texRender.bindWrite(gl);

      gl.enable(gl.BLEND);
      gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

      gl.depthFunc(gl.LEQUAL);
      gl.depthMask(false);

      this.sceneTraversal(traversal(this.shaderWireframe));

      gl.depthMask(true);
      gl.disable(gl.BLEND);

      // stop render to texRender
      this.texRender.unbindWrite(gl);
    }

    // render objects; single light, no shadow mapping
    // ----------------------------------------

  }, {
    key: '_renderLightpassNoShadow',
    value: function _renderLightpassNoShadow(gl, light, translucent) {
      var _this4 = this;

      var traversal = function traversal(hasTexture, shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            return sObj.solidVisible && sObj.translucent === translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return drawable.hasTexture === hasTexture;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, _this4.camera);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            shader.setLightUniforms(gl, light);
            drawable.drawTriangles(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      // start render to texRender
      this.texRender.bindWrite(gl);

      gl.depthFunc(gl.LEQUAL);

      gl.enable(gl.BLEND);
      gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ZERO, gl.ONE);

      // TODO color vs tex
      // ---------------------------
      this.sceneTraversal(traversal(false, this.shaderLightpass));

      this.texTest.bind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      this.sceneTraversal(traversal(true, this.shaderLightpassTex));
      this.texTest.unbind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      // ---------------------------

      gl.disable(gl.BLEND);

      // stop render to texRender
      this.texRender.unbindWrite(gl);
    }

    // render objects; single light with shadow mapping
    // ----------------------------------------

  }, {
    key: '_renderLightpassShadow',
    value: function _renderLightpassShadow(gl, light, translucent) {
      var _this5 = this;

      var traversal = function traversal(hasTexture, shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            return sObj.solidVisible && sObj.translucent === translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return drawable.hasTexture === hasTexture;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, _this5.camera);
            shader.setLightCameraUniforms(gl, light);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            shader.setLightUniforms(gl, light);
            drawable.drawTriangles(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      // start render to texRender
      this.texRender.bindWrite(gl);

      gl.depthFunc(gl.LEQUAL);

      gl.enable(gl.BLEND);
      gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ZERO, gl.ONE);

      this.texShadowMap.bind(gl, _figShader.TEXUNIT_OBJ_SHADOW_MAP);

      // TODO color vs tex
      // ---------------------------
      this.sceneTraversal(traversal(false, this.shaderLightpassShadow));

      this.texTest.bind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      this.sceneTraversal(traversal(true, this.shaderLightpassShadowTex));
      this.texTest.unbind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      // ---------------------------

      this.texShadowMap.unbind(gl, _figShader.TEXUNIT_OBJ_SHADOW_MAP);

      gl.disable(gl.BLEND);

      // stop render to texRender
      this.texRender.unbindWrite(gl);
    }
  }, {
    key: '_renderShadowMapDepth',
    value: function _renderShadowMapDepth(gl, light, translucent) {
      var traversal = function traversal(hasTexture, shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            if (!sObj.solidVisible) {
              return false;
            }
            if (!sObj.castsShadows) {
              return false;
            }

            // render all if translucent renderpass, render only solid if solid renderpass
            if (translucent) {
              return true;
            }
            return sObj.translucent === translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return drawable.hasTexture === hasTexture;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, light.camera);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            drawable.drawTriangles(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      // start render to texShadowMap
      this.texShadowMap.bindWrite(gl);

      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

      gl.colorMask(true, true, false, false);

      // TODO color vs tex
      // ---------------------------
      this.sceneTraversal(traversal(false, this.shaderDepth));

      this.texTest.bind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      this.sceneTraversal(traversal(true, this.shaderDepthTex));
      this.texTest.unbind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      // ---------------------------

      gl.colorMask(true, true, true, true);

      // stop render to texShadowMap
      this.texShadowMap.unbindWrite(gl);
    }
  }, {
    key: '_renderShadowMapColor',
    value: function _renderShadowMapColor(gl, light) {
      var traversal = function traversal(hasTexture, shader) {
        return {
          // filter
          filterSceneObject: function filterSceneObject(sObj) {
            return sObj.solidVisible && sObj.castsShadows && sObj.translucent;
          },
          filterDrawable: function filterDrawable(drawable) {
            return drawable.hasTexture === hasTexture;
          },

          // draw
          before: function before() {
            shader.use(gl);
            shader.setCameraUniforms(gl, light.camera);
          },
          execSceneObject: function execSceneObject(sObj) {
            shader.setObjectUniforms(gl, sObj);
          },
          execDrawable: function execDrawable(drawable) {
            shader.prepareVertexAttribs(gl, drawable);
            drawable.drawTriangles(gl);
          },
          after: function after() {
            shader.unuse(gl);
          }
        };
      };

      // start render to texShadowMap
      this.texShadowMap.bindWrite(gl);

      gl.colorMask(false, false, true, true);

      // TODO color vs tex
      // ---------------------------
      this.sceneTraversal(traversal(false, this.shaderDepthColor));

      this.texTest.bind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      this.sceneTraversal(traversal(true, this.shaderDepthColorTex));
      this.texTest.unbind(gl, _figShader.TEXUNIT_OBJ_COLOR);
      // ---------------------------

      gl.colorMask(true, true, true, true);

      // stop render to texShadowMap
      this.texShadowMap.unbindWrite(gl);
    }
    // ==================================================

  }]);

  return WGLRenderer;
}();

exports.default = WGLRenderer;

},{"../glsl":6,"../wglcore/mathCam":19,"../wglcore/wglQuad":26,"../wglcore/wglTexture":28,"../wgltools/preprocessGLSLES":29,"./figShader":3,"./figShaderQuad":4}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WGLShaderWireframe = exports.WGLShaderLightpassShadow = exports.WGLShaderLightpass = exports.WGLShaderDepthColor = exports.WGLShaderDepth = exports.WGLShaderPrepass = exports.TEXUNIT_OBJ_COLOR = exports.TEXUNIT_OBJ_SHADOW_MAP = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathObjTrans = require('../wglcore/mathObjTrans');

var _mathObjTrans2 = _interopRequireDefault(_mathObjTrans);

var _wglShader = require('../wglcore/wglShader');

var _wglShader2 = _interopRequireDefault(_wglShader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VERTEX_POSITION_ATTRIBUTE = 0;
var VERTEX_TEX_ATTRIBUTE = 1;
var VERTEX_COLOR_ATTRIBUTE = 2;
var VERTEX_NORMAL_ATTRIBUTE = 3;

var TEXUNIT_OBJ_SHADOW_MAP = 0;
var TEXUNIT_OBJ_COLOR = 1;

var TEMP_OBJTRANS = new _mathObjTrans2.default();

// ===================================
// COMMON SHADER
// common superclass for the default shaders
// ===================================

/** Abstract shader program for use with {@link SceneObject} and {@link WGLDrawable}.
* <br>(When extending: call _setEnabledAttributes and _initialize)
* @see {@link WGLShaderPrepass}
* @see {@link WGLShaderDepth}
* @see {@link WGLShaderLightpass}
* @see {@link WGLShaderLightpassShadow}
* @see {@link WGLShaderWireframe}
*
* @extends WGLShaderProgram
*/

var WGLShaderCommon = function (_WGLShaderProgram) {
  _inherits(WGLShaderCommon, _WGLShaderProgram);

  function WGLShaderCommon() {
    _classCallCheck(this, WGLShaderCommon);

    return _possibleConstructorReturn(this, (WGLShaderCommon.__proto__ || Object.getPrototypeOf(WGLShaderCommon)).apply(this, arguments));
  }

  _createClass(WGLShaderCommon, [{
    key: '_setEnabledAttributes',

    /**
    * @param {boolean} useTex - Indicates that the texture coord vertex attribute will be used
    * @param {boolean} useColor - Indicates that the color vertex attribute will be used
    * @param {boolean} useNormal - Indicates that the normal vertex attribute will be used
    */
    value: function _setEnabledAttributes(useTex, useColor, useNormal) {
      this._useTex = useTex;
      this._useColor = useColor;
      this._useNormal = useNormal;
    }
  }, {
    key: '_setVertexAttributes',
    value: function _setVertexAttributes(gl) {
      this._addVertexAttribute(gl, VERTEX_POSITION_ATTRIBUTE, 'vertexPosition');

      if (this._useTex) {
        this._addVertexAttribute(gl, VERTEX_TEX_ATTRIBUTE, 'vertexTex');
      }
      if (this._useColor) {
        this._addVertexAttribute(gl, VERTEX_COLOR_ATTRIBUTE, 'vertexColor');
      }
      if (this._useNormal) {
        this._addVertexAttribute(gl, VERTEX_NORMAL_ATTRIBUTE, 'vertexNormal');
      }
    }

    /** Specify a drawable's vertex attributes.
    *   Use this before {@link WGLDrawable#drawTriangles} or {@link WGLDrawable#drawLines}.
    * @param {WebGLRenderingContext} gl
    * @param {WGLDrawable} drawable
    */

  }, {
    key: 'prepareVertexAttribs',
    value: function prepareVertexAttribs(gl, drawable) {
      gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufPosition);
      gl.vertexAttribPointer(VERTEX_POSITION_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);

      if (this._useTex) {
        gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufTex);
        gl.vertexAttribPointer(VERTEX_TEX_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
      }
      if (this._useColor) {
        gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufColor);
        gl.vertexAttribPointer(VERTEX_COLOR_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);
      }
      if (this._useNormal) {
        gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufNormal);
        gl.vertexAttribPointer(VERTEX_NORMAL_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);
      }
    }
  }, {
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      uniformNames.push('camPosition', 'camProjection', 'camRotation', 'objRotation', 'objTranslation', 'objOffset', 'alpha');
    }

    /** Set uniforms associated with a {@link Camera}.
    * @param {WebGLRenderingContext} gl
    * @param {Camera} camera
    */

  }, {
    key: 'setCameraUniforms',
    value: function setCameraUniforms(gl, camera) {
      this.setUVec3(gl, 'camPosition', camera.position);
      this.setUMat4(gl, 'camProjection', camera.matrix);
      this.setUMat3(gl, 'camRotation', camera.rotation.matrix);
    }

    /** Set basic uniforms associated with a {@link SceneObject}.
    * @param {WebGLRenderingContext} gl
    * @param {SceneObject} object
    */

  }, {
    key: 'setObjectUniforms',
    value: function setObjectUniforms(gl, object) {
      // object transformation
      var t = object.transformation;
      if (t.parent !== null) {
        TEMP_OBJTRANS.copyFlat(t);
        t = TEMP_OBJTRANS;
      }

      this.setUMat3(gl, 'objRotation', t.rotation.matrix);
      this.setUVec3(gl, 'objTranslation', t.translation);
      this.setUVec3(gl, 'objOffset', t.offset);

      this.setU1f(gl, 'alpha', object.translucent ? object.alpha : 1);
    }
  }]);

  return WGLShaderCommon;
}(_wglShader2.default);

// ===================================
// PREPASS SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for emissive/ambient lighting; normals, light properties, and some material properties are not used.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderPrepass = function (_WGLShaderCommon) {
  _inherits(WGLShaderPrepass, _WGLShaderCommon);

  function WGLShaderPrepass(gl, hasTexture, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderPrepass);

    var _this2 = _possibleConstructorReturn(this, (WGLShaderPrepass.__proto__ || Object.getPrototypeOf(WGLShaderPrepass)).call(this));

    _this2.hasTexture = hasTexture;
    _this2._setEnabledAttributes(hasTexture, !hasTexture, false);
    _this2._initialize(gl, 'prepass', shaderVert, shaderFrag);
    return _this2;
  }

  _createClass(WGLShaderPrepass, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderPrepass.prototype.__proto__ || Object.getPrototypeOf(WGLShaderPrepass.prototype), '_getUniformNames', this).call(this, uniformNames);

      uniformNames.push('emissiveCoefficient', 'emissiveColor');
      if (this.hasTexture) {
        uniformNames.push('texColor');
      }
    }

    /** Set emissive coefficient uniform for a {@link SceneObject}.
    * @param {WebGLRenderingContext} gl
    * @param {SceneObject} object
    */

  }, {
    key: 'setEmissiveUniforms',
    value: function setEmissiveUniforms(gl, object) {
      this.setU1f(gl, 'emissiveCoefficient', object.emissiveCoefficient);
      this.setUVec3(gl, 'emissiveColor', object.emissiveColor);
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderPrepass.prototype.__proto__ || Object.getPrototypeOf(WGLShaderPrepass.prototype), 'use', this).call(this, gl);

      if (this.hasTexture) {
        this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
      }
    }
  }]);

  return WGLShaderPrepass;
}(WGLShaderCommon);

// ===================================
// DEPTH SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for writing depth only; no lighting or material properties are used.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderDepth = function (_WGLShaderCommon2) {
  _inherits(WGLShaderDepth, _WGLShaderCommon2);

  function WGLShaderDepth(gl, hasTexture, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderDepth);

    var _this3 = _possibleConstructorReturn(this, (WGLShaderDepth.__proto__ || Object.getPrototypeOf(WGLShaderDepth)).call(this));

    _this3.hasTexture = hasTexture;
    _this3._setEnabledAttributes(hasTexture, !hasTexture, false);
    _this3._initialize(gl, 'depth', shaderVert, shaderFrag);
    return _this3;
  }

  _createClass(WGLShaderDepth, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderDepth.prototype.__proto__ || Object.getPrototypeOf(WGLShaderDepth.prototype), '_getUniformNames', this).call(this, uniformNames);

      if (this.hasTexture) {
        uniformNames.push('texColor');
      }
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderDepth.prototype.__proto__ || Object.getPrototypeOf(WGLShaderDepth.prototype), 'use', this).call(this, gl);

      if (this.hasTexture) {
        this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
      }
    }
  }]);

  return WGLShaderDepth;
}(WGLShaderCommon);

// ===================================
// DEPTH COLOR SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for writing translucent layer on top of existing shadow map.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderDepthColor = function (_WGLShaderCommon3) {
  _inherits(WGLShaderDepthColor, _WGLShaderCommon3);

  function WGLShaderDepthColor(gl, hasTexture, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderDepthColor);

    var _this4 = _possibleConstructorReturn(this, (WGLShaderDepthColor.__proto__ || Object.getPrototypeOf(WGLShaderDepthColor)).call(this));

    _this4.hasTexture = hasTexture;
    _this4._setEnabledAttributes(hasTexture, !hasTexture, false);
    _this4._initialize(gl, 'depth/color', shaderVert, shaderFrag);
    return _this4;
  }

  _createClass(WGLShaderDepthColor, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderDepthColor.prototype.__proto__ || Object.getPrototypeOf(WGLShaderDepthColor.prototype), '_getUniformNames', this).call(this, uniformNames);

      if (this.hasTexture) {
        uniformNames.push('texColor');
      }
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderDepthColor.prototype.__proto__ || Object.getPrototypeOf(WGLShaderDepthColor.prototype), 'use', this).call(this, gl);

      if (this.hasTexture) {
        this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
      }
    }
  }]);

  return WGLShaderDepthColor;
}(WGLShaderCommon);

// ===================================
// LIGHTPASS (NO SHADOW) SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for rendering an object lit by a "shadows = false" {@link Light}.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderLightpass = function (_WGLShaderCommon4) {
  _inherits(WGLShaderLightpass, _WGLShaderCommon4);

  function WGLShaderLightpass(gl, hasTexture, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderLightpass);

    var _this5 = _possibleConstructorReturn(this, (WGLShaderLightpass.__proto__ || Object.getPrototypeOf(WGLShaderLightpass)).call(this));

    _this5.hasTexture = hasTexture;
    _this5._setEnabledAttributes(hasTexture, !hasTexture, true);
    _this5._initialize(gl, 'lightpass', shaderVert, shaderFrag);
    return _this5;
  }

  _createClass(WGLShaderLightpass, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderLightpass.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpass.prototype), '_getUniformNames', this).call(this, uniformNames);

      uniformNames.push('lightPosition', 'lightColor', 'lightAttenuation', 'diffuseCoefficient', 'specularCoefficient', 'specularExponent');

      if (this.hasTexture) {
        uniformNames.push('texColor');
      }
    }

    /** Set uniforms associated with a {@link Light}.
    * @param {WebGLRenderingContext} gl
    * @param {Light} light
    */

  }, {
    key: 'setLightUniforms',
    value: function setLightUniforms(gl, light) {
      this.setUVec3(gl, 'lightPosition', light.camera.position);
      this.setUVec3(gl, 'lightColor', light.color);
      this.setUVec3(gl, 'lightAttenuation', light.attenuation);
    }
  }, {
    key: 'setObjectUniforms',
    value: function setObjectUniforms(gl, object) {
      _get(WGLShaderLightpass.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpass.prototype), 'setObjectUniforms', this).call(this, gl, object);

      this.setU1f(gl, 'diffuseCoefficient', object.diffuseCoefficient);
      this.setU1f(gl, 'specularCoefficient', object.specularCoefficient);
      this.setU1f(gl, 'specularExponent', object.specularExponent);
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderLightpass.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpass.prototype), 'use', this).call(this, gl);

      if (this.hasTexture) {
        this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
      }
    }
  }]);

  return WGLShaderLightpass;
}(WGLShaderCommon);

// ===================================
// LIGHTPASS (SHADOW) SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for rendering an object lit by a "shadows = true" {@link Light}.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderLightpassShadow = function (_WGLShaderCommon5) {
  _inherits(WGLShaderLightpassShadow, _WGLShaderCommon5);

  function WGLShaderLightpassShadow(gl, hasTexture, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderLightpassShadow);

    var _this6 = _possibleConstructorReturn(this, (WGLShaderLightpassShadow.__proto__ || Object.getPrototypeOf(WGLShaderLightpassShadow)).call(this));

    _this6.hasTexture = hasTexture;
    _this6._setEnabledAttributes(hasTexture, !hasTexture, true);
    _this6._initialize(gl, 'lightpass shadow', shaderVert, shaderFrag);
    return _this6;
  }

  _createClass(WGLShaderLightpassShadow, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderLightpassShadow.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpassShadow.prototype), '_getUniformNames', this).call(this, uniformNames);

      uniformNames.push('lightPosition', 'lightColor', 'lightAttenuation', 'diffuseCoefficient', 'specularCoefficient', 'specularExponent', 'texShadowMap', 'lightCamProjection', 'lightCamRotation');

      if (this.hasTexture) {
        uniformNames.push('texColor');
      }
    }

    /** Set uniforms associated with a {@link Light}.
    * @param {WebGLRenderingContext} gl
    * @param {Light} light
    */

  }, {
    key: 'setLightUniforms',
    value: function setLightUniforms(gl, light) {
      this.setUVec3(gl, 'lightPosition', light.camera.position);
      this.setUVec3(gl, 'lightColor', light.color);
      this.setUVec3(gl, 'lightAttenuation', light.attenuation);
    }
  }, {
    key: 'setObjectUniforms',
    value: function setObjectUniforms(gl, object) {
      _get(WGLShaderLightpassShadow.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpassShadow.prototype), 'setObjectUniforms', this).call(this, gl, object);

      this.setU1f(gl, 'diffuseCoefficient', object.diffuseCoefficient);
      this.setU1f(gl, 'specularCoefficient', object.specularCoefficient);
      this.setU1f(gl, 'specularExponent', object.specularExponent);
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderLightpassShadow.prototype.__proto__ || Object.getPrototypeOf(WGLShaderLightpassShadow.prototype), 'use', this).call(this, gl);

      this.setU1i(gl, 'texShadowMap', TEXUNIT_OBJ_SHADOW_MAP);
      if (this.hasTexture) {
        this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
      }
    }

    /** Set uniforms associated with the {@link Camera} for the {@link Light} used to draw the shadow map.
    * @param {WebGLRenderingContext} gl
    * @param {Light} light
    */

  }, {
    key: 'setLightCameraUniforms',
    value: function setLightCameraUniforms(gl, light) {
      this.setUMat4(gl, 'lightCamProjection', light.camera.matrix);
      this.setUMat3(gl, 'lightCamRotation', light.camera.rotation.matrix);
    }
  }]);

  return WGLShaderLightpassShadow;
}(WGLShaderCommon);

// ===================================
// WIREFRAME SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used to draw wireframe objects; normals, light properties, and some material properties are not used.
* @param {WebGLRenderingContext} gl
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/


var WGLShaderWireframe = function (_WGLShaderCommon6) {
  _inherits(WGLShaderWireframe, _WGLShaderCommon6);

  function WGLShaderWireframe(gl, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderWireframe);

    var _this7 = _possibleConstructorReturn(this, (WGLShaderWireframe.__proto__ || Object.getPrototypeOf(WGLShaderWireframe)).call(this));

    _this7._setEnabledAttributes(false, false, false);
    _this7._initialize(gl, 'wireframe', shaderVert, shaderFrag);
    return _this7;
  }

  _createClass(WGLShaderWireframe, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      _get(WGLShaderWireframe.prototype.__proto__ || Object.getPrototypeOf(WGLShaderWireframe.prototype), '_getUniformNames', this).call(this, uniformNames);

      uniformNames.push('zOffset', 'wireframeColor');
    }

    /** Set wireframe options for a {@link SceneObject}.
    * @param {WebGLRenderingContext} gl
    * @param {SceneObject} object
    */

  }, {
    key: 'setWireframeUniforms',
    value: function setWireframeUniforms(gl, object) {
      this.setU1f(gl, 'zOffset', 0.01);
      this.setUVec4(gl, 'wireframeColor', object.wireframeColor);
    }
  }]);

  return WGLShaderWireframe;
}(WGLShaderCommon);

exports.TEXUNIT_OBJ_SHADOW_MAP = TEXUNIT_OBJ_SHADOW_MAP;
exports.TEXUNIT_OBJ_COLOR = TEXUNIT_OBJ_COLOR;
exports.WGLShaderPrepass = WGLShaderPrepass;
exports.WGLShaderDepth = WGLShaderDepth;
exports.WGLShaderDepthColor = WGLShaderDepthColor;
exports.WGLShaderLightpass = WGLShaderLightpass;
exports.WGLShaderLightpassShadow = WGLShaderLightpassShadow;
exports.WGLShaderWireframe = WGLShaderWireframe;

},{"../wglcore/mathObjTrans":22,"../wglcore/wglShader":27}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WGLShaderQuadPost = exports.WGLShaderQuadBlur = exports.WGLShaderQuadCopy = exports.TEXUNIT_QUAD_BLUR = exports.TEXUNIT_QUAD_COLOR = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wglShader = require('../wglcore/wglShader');

var _wglShader2 = _interopRequireDefault(_wglShader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VERTEX_POSITION_ATTRIBUTE = 0;
var VERTEX_TEX_ATTRIBUTE = 1;

var TEXUNIT_QUAD_COLOR = 0;
var TEXUNIT_QUAD_BLUR = 1;

// ===================================
// SHADER PROGRAM: QUAD COMMON
// ===================================

/** Shader program for use with {@link WGLQuad}.
*
* @extends WGLShaderProgram
*/

var WGLShaderQuadCommon = function (_WGLShaderProgram) {
  _inherits(WGLShaderQuadCommon, _WGLShaderProgram);

  function WGLShaderQuadCommon() {
    _classCallCheck(this, WGLShaderQuadCommon);

    return _possibleConstructorReturn(this, (WGLShaderQuadCommon.__proto__ || Object.getPrototypeOf(WGLShaderQuadCommon)).apply(this, arguments));
  }

  _createClass(WGLShaderQuadCommon, [{
    key: '_setVertexAttributes',
    value: function _setVertexAttributes(gl) {
      this._addVertexAttribute(gl, VERTEX_POSITION_ATTRIBUTE, 'vertexPosition');
      this._addVertexAttribute(gl, VERTEX_TEX_ATTRIBUTE, 'vertexTex');
    }

    /** Specify a quad's vertex attributes.  Use this before {@link WGLQuad#draw}.
    * @param {WebGLRenderingContext} gl
    * @param {WGLQuad} quad
    */

  }, {
    key: 'prepareVertexAttribs',
    value: function prepareVertexAttribs(gl, quad) {
      gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufPosition);
      gl.vertexAttribPointer(VERTEX_POSITION_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);

      gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufTex);
      gl.vertexAttribPointer(VERTEX_TEX_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
    }
  }]);

  return WGLShaderQuadCommon;
}(_wglShader2.default);

// ===================================
// SHADER PROGRAM: QUAD COPY
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/


var WGLShaderQuadCopy = function (_WGLShaderQuadCommon) {
  _inherits(WGLShaderQuadCopy, _WGLShaderQuadCommon);

  function WGLShaderQuadCopy(gl, description, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderQuadCopy);

    var _this2 = _possibleConstructorReturn(this, (WGLShaderQuadCopy.__proto__ || Object.getPrototypeOf(WGLShaderQuadCopy)).call(this));

    _this2._initialize(gl, description, shaderVert, shaderFrag);
    return _this2;
  }

  _createClass(WGLShaderQuadCopy, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      uniformNames.push('texColor');
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderQuadCopy.prototype.__proto__ || Object.getPrototypeOf(WGLShaderQuadCopy.prototype), 'use', this).call(this, gl);

      this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
    }
  }]);

  return WGLShaderQuadCopy;
}(WGLShaderQuadCommon);

// ===================================
// SHADER PROGRAM: QUAD BLUR V/H
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/


var WGLShaderQuadBlur = function (_WGLShaderQuadCommon2) {
  _inherits(WGLShaderQuadBlur, _WGLShaderQuadCommon2);

  function WGLShaderQuadBlur(gl, description, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderQuadBlur);

    var _this3 = _possibleConstructorReturn(this, (WGLShaderQuadBlur.__proto__ || Object.getPrototypeOf(WGLShaderQuadBlur)).call(this));

    _this3._initialize(gl, description, shaderVert, shaderFrag);
    return _this3;
  }

  _createClass(WGLShaderQuadBlur, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      uniformNames.push('texColor');

      // TODO
      uniformNames.push('texSize');
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderQuadBlur.prototype.__proto__ || Object.getPrototypeOf(WGLShaderQuadBlur.prototype), 'use', this).call(this, gl);

      this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
    }

    // TODO
    /** Set uniforms associated with the source texture's pixel size.
    * @param {WebGLRenderingContext} gl
    * @param {number} texWidth
    * @param {number} texHeight
    */

  }, {
    key: 'setTextureSize',
    value: function setTextureSize(gl, texWidth, texHeight) {
      this.setU2i(gl, 'texSize', texWidth, texHeight);
    }
  }]);

  return WGLShaderQuadBlur;
}(WGLShaderQuadCommon);

// ===================================
// SHADER PROGRAM: QUAD POST-PROCESSING
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/


var WGLShaderQuadPost = function (_WGLShaderQuadCommon3) {
  _inherits(WGLShaderQuadPost, _WGLShaderQuadCommon3);

  function WGLShaderQuadPost(gl, description, shaderVert, shaderFrag) {
    _classCallCheck(this, WGLShaderQuadPost);

    var _this4 = _possibleConstructorReturn(this, (WGLShaderQuadPost.__proto__ || Object.getPrototypeOf(WGLShaderQuadPost)).call(this));

    _this4._initialize(gl, description, shaderVert, shaderFrag);
    return _this4;
  }

  _createClass(WGLShaderQuadPost, [{
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {
      uniformNames.push('texColor', 'texBlur', 'bloomFactor', 'colorAdjustBrightness', 'colorAdjustContrast', 'colorAdjustSaturation');
    }
  }, {
    key: 'use',
    value: function use(gl) {
      _get(WGLShaderQuadPost.prototype.__proto__ || Object.getPrototypeOf(WGLShaderQuadPost.prototype), 'use', this).call(this, gl);

      this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
      this.setU1i(gl, 'texBlur', TEXUNIT_QUAD_BLUR);
    }

    /** Set uniforms associated with post-processing effects.
    * @param {WebGLRenderingContext} gl
    * @param {number} bloomFactor
    * @param {number} colorAdjustBrightness
    * @param {number} colorAdjustContrast
    * @param {number} colorAdjustSaturation
    */

  }, {
    key: 'setOptions',
    value: function setOptions(gl, bloomFactor, colorAdjustBrightness, colorAdjustContrast, colorAdjustSaturation) {
      this.setU1f(gl, 'bloomFactor', bloomFactor);
      this.setU1f(gl, 'colorAdjustBrightness', colorAdjustBrightness);
      this.setU1f(gl, 'colorAdjustContrast', colorAdjustContrast);
      this.setU1f(gl, 'colorAdjustSaturation', colorAdjustSaturation);
    }
  }]);

  return WGLShaderQuadPost;
}(WGLShaderQuadCommon);

exports.TEXUNIT_QUAD_COLOR = TEXUNIT_QUAD_COLOR;
exports.TEXUNIT_QUAD_BLUR = TEXUNIT_QUAD_BLUR;
exports.WGLShaderQuadCopy = WGLShaderQuadCopy;
exports.WGLShaderQuadBlur = WGLShaderQuadBlur;
exports.WGLShaderQuadPost = WGLShaderQuadPost;

},{"../wglcore/wglShader":27}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
// This file was generated automatically by GLFontData.
// Git repository: https://bitbucket.org/tdmsrc/glfont

var _M = {
  32: {
    p: [2, 74],
    s: [0, 0],
    o: [0, 0]
  },
  33: {
    p: [4, 74],
    s: [12, 67],
    o: [7, 0]
  },
  34: {
    p: [18, 32],
    s: [28, 25],
    o: [5, -42]
  },
  35: {
    p: [48, 74],
    s: [42, 65],
    o: [1, 0]
  },
  36: {
    p: [92, 82],
    s: [43, 80],
    o: [1, 8]
  },
  37: {
    p: [137, 75],
    s: [67, 69],
    o: [2, 1]
  },
  38: {
    p: [206, 75],
    s: [51, 68],
    o: [3, 1]
  },
  39: {
    p: [259, 32],
    s: [10, 25],
    o: [4, -42]
  },
  40: {
    p: [271, 94],
    s: [23, 91],
    o: [4, 20]
  },
  41: {
    p: [296, 94],
    s: [24, 91],
    o: [0, 20]
  },
  42: {
    p: [322, 44],
    s: [30, 37],
    o: [0, -30]
  },
  43: {
    p: [354, 65],
    s: [40, 48],
    o: [4, -9]
  },
  44: {
    p: [396, 89],
    s: [13, 30],
    o: [4, 15]
  },
  45: {
    p: [411, 55],
    s: [21, 12],
    o: [3, -19]
  },
  46: {
    p: [434, 74],
    s: [12, 15],
    o: [5, 0]
  },
  47: {
    p: [448, 76],
    s: [20, 73],
    o: [1, 2]
  },
  48: {
    p: [470, 75],
    s: [39, 69],
    o: [3, 1]
  },
  49: {
    p: [511, 74],
    s: [37, 67],
    o: [5, 0]
  },
  50: {
    p: [550, 74],
    s: [39, 68],
    o: [3, 0]
  },
  51: {
    p: [591, 75],
    s: [40, 69],
    o: [2, 1]
  },
  52: {
    p: [633, 74],
    s: [42, 67],
    o: [2, 0]
  },
  53: {
    p: [677, 75],
    s: [40, 68],
    o: [2, 1]
  },
  54: {
    p: [719, 75],
    s: [39, 69],
    o: [3, 1]
  },
  55: {
    p: [760, 74],
    s: [38, 67],
    o: [3, 0]
  },
  56: {
    p: [800, 75],
    s: [40, 69],
    o: [2, 1]
  },
  57: {
    p: [842, 75],
    s: [38, 69],
    o: [3, 1]
  },
  58: {
    p: [882, 74],
    s: [12, 49],
    o: [8, 0]
  },
  59: {
    p: [896, 89],
    s: [13, 64],
    o: [7, 15]
  },
  60: {
    p: [911, 69],
    s: [40, 53],
    o: [3, -5]
  },
  61: {
    p: [953, 60],
    s: [40, 36],
    o: [3, -14]
  },
  62: {
    p: [2, 161],
    s: [40, 53],
    o: [3, -5]
  },
  63: {
    p: [44, 166],
    s: [41, 68],
    o: [4, 0]
  },
  64: {
    p: [87, 181],
    s: [68, 85],
    o: [5, 15]
  },
  65: {
    p: [157, 166],
    s: [53, 67],
    o: [2, 0]
  },
  66: {
    p: [212, 166],
    s: [49, 67],
    o: [5, 0]
  },
  67: {
    p: [263, 167],
    s: [51, 69],
    o: [3, 1]
  },
  68: {
    p: [316, 166],
    s: [49, 67],
    o: [5, 0]
  },
  69: {
    p: [367, 166],
    s: [45, 67],
    o: [5, 0]
  },
  70: {
    p: [414, 166],
    s: [40, 67],
    o: [5, 0]
  },
  71: {
    p: [456, 167],
    s: [54, 69],
    o: [3, 1]
  },
  72: {
    p: [512, 166],
    s: [47, 67],
    o: [5, 0]
  },
  73: {
    p: [561, 166],
    s: [12, 67],
    o: [5, 0]
  },
  74: {
    p: [575, 167],
    s: [38, 68],
    o: [1, 1]
  },
  75: {
    p: [615, 166],
    s: [51, 67],
    o: [5, 0]
  },
  76: {
    p: [668, 166],
    s: [41, 67],
    o: [5, 0]
  },
  77: {
    p: [711, 166],
    s: [56, 67],
    o: [5, 0]
  },
  78: {
    p: [769, 166],
    s: [47, 67],
    o: [5, 0]
  },
  79: {
    p: [818, 167],
    s: [56, 69],
    o: [3, 1]
  },
  80: {
    p: [876, 166],
    s: [45, 67],
    o: [5, 0]
  },
  81: {
    p: [923, 185],
    s: [56, 87],
    o: [3, 19]
  },
  82: {
    p: [2, 258],
    s: [51, 67],
    o: [5, 0]
  },
  83: {
    p: [55, 259],
    s: [48, 69],
    o: [2, 1]
  },
  84: {
    p: [105, 258],
    s: [46, 67],
    o: [1, 0]
  },
  85: {
    p: [153, 259],
    s: [48, 68],
    o: [5, 1]
  },
  86: {
    p: [203, 258],
    s: [52, 67],
    o: [0, 0]
  },
  87: {
    p: [257, 258],
    s: [74, 67],
    o: [0, 0]
  },
  88: {
    p: [333, 258],
    s: [52, 67],
    o: [0, 0]
  },
  89: {
    p: [387, 258],
    s: [51, 67],
    o: [1, 0]
  },
  90: {
    p: [440, 258],
    s: [45, 67],
    o: [2, 0]
  },
  91: {
    p: [487, 278],
    s: [22, 91],
    o: [4, 20]
  },
  92: {
    p: [511, 260],
    s: [20, 73],
    o: [1, 2]
  },
  93: {
    p: [533, 278],
    s: [22, 91],
    o: [1, 20]
  },
  94: {
    p: [557, 234],
    s: [44, 43],
    o: [1, -24]
  },
  95: {
    p: [603, 270],
    s: [46, 5],
    o: [-1, 12]
  },
  96: {
    p: [651, 203],
    s: [21, 16],
    o: [3, -55]
  },
  97: {
    p: [674, 259],
    s: [43, 54],
    o: [2, 1]
  },
  98: {
    p: [719, 259],
    s: [42, 72],
    o: [4, 1]
  },
  99: {
    p: [763, 259],
    s: [39, 54],
    o: [3, 1]
  },
  100: {
    p: [804, 259],
    s: [41, 72],
    o: [3, 1]
  },
  101: {
    p: [847, 259],
    s: [39, 54],
    o: [3, 1]
  },
  102: {
    p: [888, 258],
    s: [24, 71],
    o: [2, 0]
  },
  103: {
    p: [914, 278],
    s: [41, 73],
    o: [3, 20]
  },
  104: {
    p: [957, 258],
    s: [39, 71],
    o: [5, 0]
  },
  105: {
    p: [998, 258],
    s: [12, 71],
    o: [5, 0]
  },
  106: {
    p: [2, 371],
    s: [18, 91],
    o: [0, 20]
  },
  107: {
    p: [22, 351],
    s: [39, 71],
    o: [5, 0]
  },
  108: {
    p: [63, 351],
    s: [12, 71],
    o: [5, 0]
  },
  109: {
    p: [77, 351],
    s: [62, 53],
    o: [4, 0]
  },
  110: {
    p: [141, 351],
    s: [40, 53],
    o: [4, 0]
  },
  111: {
    p: [183, 352],
    s: [43, 54],
    o: [3, 1]
  },
  112: {
    p: [228, 371],
    s: [42, 73],
    o: [4, 20]
  },
  113: {
    p: [272, 371],
    s: [41, 73],
    o: [3, 20]
  },
  114: {
    p: [315, 351],
    s: [26, 53],
    o: [4, 0]
  },
  115: {
    p: [343, 352],
    s: [40, 54],
    o: [2, 1]
  },
  116: {
    p: [385, 352],
    s: [23, 64],
    o: [2, 1]
  },
  117: {
    p: [410, 352],
    s: [39, 53],
    o: [5, 1]
  },
  118: {
    p: [451, 351],
    s: [44, 52],
    o: [0, 0]
  },
  119: {
    p: [497, 351],
    s: [62, 52],
    o: [-1, 0]
  },
  120: {
    p: [561, 351],
    s: [44, 52],
    o: [0, 0]
  },
  121: {
    p: [607, 371],
    s: [43, 72],
    o: [0, 20]
  },
  122: {
    p: [652, 351],
    s: [36, 52],
    o: [2, 0]
  },
  123: {
    p: [690, 371],
    s: [27, 91],
    o: [2, 20]
  },
  124: {
    p: [719, 372],
    s: [11, 92],
    o: [6, 21]
  },
  125: {
    p: [732, 371],
    s: [27, 91],
    o: [2, 20]
  },
  126: {
    p: [761, 327],
    s: [41, 16],
    o: [3, -24]
  },
  128: {
    p: [804, 351],
    s: [17, 65],
    o: [2, 0]
  },
  129: {
    p: [823, 351],
    s: [17, 65],
    o: [2, 0]
  },
  130: {
    p: [842, 351],
    s: [17, 65],
    o: [2, 0]
  },
  131: {
    p: [861, 351],
    s: [17, 65],
    o: [2, 0]
  },
  132: {
    p: [880, 351],
    s: [17, 65],
    o: [2, 0]
  },
  133: {
    p: [899, 351],
    s: [17, 65],
    o: [2, 0]
  },
  134: {
    p: [918, 351],
    s: [17, 65],
    o: [2, 0]
  },
  135: {
    p: [937, 351],
    s: [17, 65],
    o: [2, 0]
  },
  136: {
    p: [956, 351],
    s: [17, 65],
    o: [2, 0]
  },
  137: {
    p: [975, 351],
    s: [17, 65],
    o: [2, 0]
  },
  138: {
    p: [994, 351],
    s: [17, 65],
    o: [2, 0]
  },
  139: {
    p: [2, 447],
    s: [17, 65],
    o: [2, 0]
  },
  140: {
    p: [21, 447],
    s: [17, 65],
    o: [2, 0]
  },
  141: {
    p: [40, 447],
    s: [17, 65],
    o: [2, 0]
  },
  142: {
    p: [59, 447],
    s: [17, 65],
    o: [2, 0]
  },
  143: {
    p: [78, 447],
    s: [17, 65],
    o: [2, 0]
  },
  144: {
    p: [97, 447],
    s: [17, 65],
    o: [2, 0]
  },
  145: {
    p: [116, 447],
    s: [17, 65],
    o: [2, 0]
  },
  146: {
    p: [135, 447],
    s: [17, 65],
    o: [2, 0]
  },
  147: {
    p: [154, 447],
    s: [17, 65],
    o: [2, 0]
  },
  148: {
    p: [173, 447],
    s: [17, 65],
    o: [2, 0]
  },
  149: {
    p: [192, 447],
    s: [17, 65],
    o: [2, 0]
  },
  150: {
    p: [211, 447],
    s: [17, 65],
    o: [2, 0]
  },
  151: {
    p: [230, 447],
    s: [17, 65],
    o: [2, 0]
  },
  152: {
    p: [249, 447],
    s: [17, 65],
    o: [2, 0]
  },
  153: {
    p: [268, 447],
    s: [17, 65],
    o: [2, 0]
  },
  154: {
    p: [287, 447],
    s: [17, 65],
    o: [2, 0]
  },
  155: {
    p: [306, 447],
    s: [17, 65],
    o: [2, 0]
  },
  156: {
    p: [325, 447],
    s: [17, 65],
    o: [2, 0]
  },
  157: {
    p: [344, 447],
    s: [17, 65],
    o: [2, 0]
  },
  158: {
    p: [363, 447],
    s: [17, 65],
    o: [2, 0]
  },
  159: {
    p: [382, 447],
    s: [17, 65],
    o: [2, 0]
  },
  160: {
    p: [401, 447],
    s: [0, 0],
    o: [0, 0]
  },
  161: {
    p: [403, 463],
    s: [13, 68],
    o: [7, 16]
  },
  162: {
    p: [418, 449],
    s: [40, 69],
    o: [2, 2]
  },
  163: {
    p: [460, 447],
    s: [41, 68],
    o: [2, 0]
  },
  164: {
    p: [503, 440],
    s: [42, 49],
    o: [1, -7]
  },
  165: {
    p: [547, 447],
    s: [43, 67],
    o: [0, 0]
  },
  166: {
    p: [592, 469],
    s: [11, 91],
    o: [6, 22]
  },
  167: {
    p: [605, 458],
    s: [40, 79],
    o: [2, 11]
  },
  168: {
    p: [647, 391],
    s: [25, 11],
    o: [1, -56]
  },
  169: {
    p: [674, 448],
    s: [56, 69],
    o: [1, 1]
  },
  170: {
    p: [732, 413],
    s: [27, 33],
    o: [2, -34]
  },
  171: {
    p: [761, 441],
    s: [38, 39],
    o: [3, -6]
  },
  172: {
    p: [801, 439],
    s: [40, 31],
    o: [4, -8]
  },
  173: {
    p: [843, 428],
    s: [21, 12],
    o: [3, -19]
  },
  174: {
    p: [866, 448],
    s: [56, 69],
    o: [1, 1]
  },
  175: {
    p: [924, 379],
    s: [28, 5],
    o: [0, -68]
  },
  176: {
    p: [954, 410],
    s: [31, 31],
    o: [4, -37]
  },
  177: {
    p: [2, 558],
    s: [48, 60],
    o: [3, 0]
  },
  178: {
    p: [52, 525],
    s: [24, 35],
    o: [1, -33]
  },
  179: {
    p: [78, 526],
    s: [23, 36],
    o: [2, -32]
  },
  180: {
    p: [103, 503],
    s: [21, 16],
    o: [3, -55]
  },
  181: {
    p: [126, 578],
    s: [39, 72],
    o: [5, 20]
  },
  182: {
    p: [167, 570],
    s: [39, 79],
    o: [3, 12]
  },
  183: {
    p: [208, 533],
    s: [14, 15],
    o: [7, -25]
  },
  184: {
    p: [224, 578],
    s: [17, 21],
    o: [3, 20]
  },
  185: {
    p: [243, 525],
    s: [21, 34],
    o: [3, -33]
  },
  186: {
    p: [266, 524],
    s: [27, 33],
    o: [2, -34]
  },
  187: {
    p: [295, 552],
    s: [38, 39],
    o: [3, -6]
  },
  188: {
    p: [335, 558],
    s: [66, 67],
    o: [4, 0]
  },
  189: {
    p: [403, 558],
    s: [67, 67],
    o: [4, 0]
  },
  190: {
    p: [472, 558],
    s: [65, 68],
    o: [5, 0]
  },
  191: {
    p: [539, 574],
    s: [40, 68],
    o: [4, 16]
  },
  192: {
    p: [581, 558],
    s: [53, 86],
    o: [2, 0]
  },
  193: {
    p: [636, 558],
    s: [53, 86],
    o: [2, 0]
  },
  194: {
    p: [691, 558],
    s: [53, 87],
    o: [2, 0]
  },
  195: {
    p: [746, 558],
    s: [53, 86],
    o: [2, 0]
  },
  196: {
    p: [801, 558],
    s: [53, 83],
    o: [2, 0]
  },
  197: {
    p: [856, 558],
    s: [53, 86],
    o: [2, 0]
  },
  198: {
    p: [911, 558],
    s: [76, 67],
    o: [0, 0]
  },
  199: {
    p: [2, 687],
    s: [51, 88],
    o: [3, 20]
  },
  200: {
    p: [55, 667],
    s: [45, 86],
    o: [5, 0]
  },
  201: {
    p: [102, 667],
    s: [45, 86],
    o: [5, 0]
  },
  202: {
    p: [149, 667],
    s: [45, 87],
    o: [5, 0]
  },
  203: {
    p: [196, 667],
    s: [45, 83],
    o: [5, 0]
  },
  204: {
    p: [243, 667],
    s: [21, 86],
    o: [-3, 0]
  },
  205: {
    p: [266, 667],
    s: [21, 86],
    o: [4, 0]
  },
  206: {
    p: [289, 667],
    s: [29, 87],
    o: [-4, 0]
  },
  207: {
    p: [320, 667],
    s: [26, 83],
    o: [-2, 0]
  },
  208: {
    p: [348, 667],
    s: [54, 67],
    o: [0, 0]
  },
  209: {
    p: [404, 667],
    s: [47, 86],
    o: [5, 0]
  },
  210: {
    p: [453, 668],
    s: [56, 87],
    o: [3, 1]
  },
  211: {
    p: [511, 668],
    s: [56, 87],
    o: [3, 1]
  },
  212: {
    p: [569, 668],
    s: [56, 88],
    o: [3, 1]
  },
  213: {
    p: [627, 668],
    s: [56, 87],
    o: [3, 1]
  },
  214: {
    p: [685, 668],
    s: [56, 84],
    o: [3, 1]
  },
  215: {
    p: [743, 660],
    s: [41, 49],
    o: [3, -7]
  },
  216: {
    p: [786, 671],
    s: [56, 74],
    o: [3, 4]
  },
  217: {
    p: [844, 668],
    s: [48, 87],
    o: [5, 1]
  },
  218: {
    p: [894, 668],
    s: [48, 87],
    o: [5, 1]
  },
  219: {
    p: [944, 668],
    s: [48, 88],
    o: [5, 1]
  },
  220: {
    p: [2, 776],
    s: [48, 84],
    o: [5, 1]
  },
  221: {
    p: [52, 775],
    s: [51, 86],
    o: [1, 0]
  },
  222: {
    p: [105, 775],
    s: [45, 67],
    o: [5, 0]
  },
  223: {
    p: [152, 776],
    s: [41, 72],
    o: [5, 1]
  },
  224: {
    p: [195, 776],
    s: [43, 72],
    o: [2, 1]
  },
  225: {
    p: [240, 776],
    s: [43, 72],
    o: [2, 1]
  },
  226: {
    p: [285, 776],
    s: [43, 73],
    o: [2, 1]
  },
  227: {
    p: [330, 776],
    s: [43, 71],
    o: [2, 1]
  },
  228: {
    p: [375, 776],
    s: [43, 68],
    o: [2, 1]
  },
  229: {
    p: [420, 776],
    s: [43, 81],
    o: [2, 1]
  },
  230: {
    p: [465, 776],
    s: [65, 54],
    o: [3, 1]
  },
  231: {
    p: [532, 795],
    s: [39, 73],
    o: [3, 20]
  },
  232: {
    p: [573, 776],
    s: [39, 72],
    o: [3, 1]
  },
  233: {
    p: [614, 776],
    s: [39, 72],
    o: [3, 1]
  },
  234: {
    p: [655, 776],
    s: [39, 73],
    o: [3, 1]
  },
  235: {
    p: [696, 776],
    s: [39, 68],
    o: [3, 1]
  },
  236: {
    p: [737, 775],
    s: [21, 71],
    o: [-3, 0]
  },
  237: {
    p: [760, 775],
    s: [21, 71],
    o: [4, 0]
  },
  238: {
    p: [783, 775],
    s: [29, 72],
    o: [-4, 0]
  },
  239: {
    p: [814, 775],
    s: [25, 67],
    o: [-2, 0]
  },
  240: {
    p: [841, 776],
    s: [43, 72],
    o: [3, 1]
  },
  241: {
    p: [886, 775],
    s: [40, 70],
    o: [4, 0]
  },
  242: {
    p: [928, 776],
    s: [43, 72],
    o: [3, 1]
  },
  243: {
    p: [973, 776],
    s: [43, 72],
    o: [3, 1]
  },
  244: {
    p: [2, 870],
    s: [43, 73],
    o: [3, 1]
  },
  245: {
    p: [47, 870],
    s: [43, 71],
    o: [3, 1]
  },
  246: {
    p: [92, 870],
    s: [43, 68],
    o: [3, 1]
  },
  247: {
    p: [137, 861],
    s: [49, 49],
    o: [2, -8]
  },
  248: {
    p: [188, 872],
    s: [48, 57],
    o: [0, 3]
  },
  249: {
    p: [238, 870],
    s: [39, 72],
    o: [5, 1]
  },
  250: {
    p: [279, 870],
    s: [39, 72],
    o: [5, 1]
  },
  251: {
    p: [320, 870],
    s: [39, 73],
    o: [5, 1]
  },
  252: {
    p: [361, 870],
    s: [39, 68],
    o: [5, 1]
  },
  253: {
    p: [402, 889],
    s: [43, 91],
    o: [0, 20]
  },
  254: {
    p: [447, 889],
    s: [41, 91],
    o: [5, 20]
  },
  255: {
    p: [490, 889],
    s: [43, 87],
    o: [0, 20]
  }
};

var fontMetrics = {
  fontName: 'Liberation Sans Narrow (bold) (size: 96)',
  spaceWidth: 22.0,
  lineHeight: 111,
  getTexPos: function getTexPos(k) {
    return _M[k].p;
  },
  getSize: function getSize(k) {
    return _M[k].s;
  },
  getOffset: function getOffset(k) {
    return _M[k].o;
  }
};

exports.default = fontMetrics;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// This file was automatically generated by staticrc.js.
// Do not edit this file!  Edit the inputs and rebuild instead.

var depthColorFrag = "precision highp float;\n\nuniform mat4 camProjection;\n\nuniform float alpha;\n\nvarying vec3 position;\n\n#shared getColorFrag\n#shared packDepthColor\n\n\nvoid main(){\n  //TODO: discard if alpha < thresh\n  //---------------------------\n  vec4 fc4 = getFragColor();\n  vec3 fragColor = fc4.rgb;\n  if(fc4.a < 0.9){ discard; }\n  //---------------------------\n  \n  vec4 rgba = vec4(fragColor, alpha);\n  vec2 packedRGBA = packDepthColor(rgba);\n  \n  gl_FragColor = vec4(1.0, 1.0, packedRGBA.x, packedRGBA.y);\n}\n";

var depthColorVert = "attribute vec3 vertexPosition;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nvarying vec3 position;\n\n#shared getColorVert\n\n\nvoid main(){\n  position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vec3 vertexPosCam = camRotation * (position - camPosition);\n  \n  tex = getVertexTex();\n  color = getVertexColor();\n  \n  gl_Position = camProjection * vec4(vertexPosCam, 1.0);\n}\n";

var depthFrag = "precision highp float;\n\nuniform mat4 camProjection;\n\nvarying vec3 vertexPosCam;\n\n#shared getColorFrag\n#shared packDepthColor\n\n\nvoid main(){\n  //TODO: discard if alpha < thresh\n  //---------------------------\n  vec4 fc4 = getFragColor();\n  vec3 fragColor = fc4.rgb;\n  if(fc4.a < 0.9){ discard; }\n  //---------------------------\n  \n  //get z clip coordinate\n  vec4 vertexPosHomogClip = camProjection * vec4(vertexPosCam, 1.0);\n  float vertexPosClipZ = vertexPosHomogClip.z / vertexPosHomogClip.w;\n  \n  //rescale clip coord [-1,1] -> [0,1]\n  float vertexPosClipZAdjusted = (vertexPosClipZ + 1.0)/2.0;\n  \n  vec2 packedDepth = packDepth(vertexPosClipZAdjusted);\n  gl_FragColor = vec4(packedDepth, 0.0, 1.0);\n}\n";

var depthVert = "attribute vec3 vertexPosition;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nvarying vec3 vertexPosCam;\n\n#shared getColorVert\n\n\nvoid main(){\n  vec3 position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vertexPosCam = camRotation * (position - camPosition);\n  \n  tex = getVertexTex();\n  color = getVertexColor();\n  \n  gl_Position = camProjection * vec4(vertexPosCam, 1.0);\n}\n";

var inclGetColorFrag = "varying vec3 color;\n\n\nvec4 getFragColor(){\n  return vec4(color, 1.0);\n}\n";

var inclGetColorTexFrag = "uniform sampler2D texColor;\n\nvarying vec2 tex;\n\n\n//4x4 ordered dithering\n/*float ditherThresh(){\n  \n  vec2 texTargetSize = vec2(1536.0, 1536.0);\n  \n  float tarr[16];\n  tarr[ 0] =  1.0; tarr[ 1] =  9.0; tarr[ 2] =  3.0; tarr[ 3] = 11.0;\n  tarr[ 4] = 13.0; tarr[ 5] =  5.0; tarr[ 6] = 15.0; tarr[ 7] =  7.0;\n  tarr[ 8] =  4.0; tarr[ 9] = 12.0; tarr[10] =  2.0; tarr[11] = 10.0;\n  tarr[12] = 16.0; tarr[13] =  8.0; tarr[14] = 14.0; tarr[15] =  6.0;\n  \n  int tci = int(mod(tex.x * float(texTargetSize.x), 4.0));\n  int tcj = int(mod(tex.y * float(texTargetSize.y), 4.0));\n  \n  float thresh = 1.0;\n  for(int i=0; i<4; i++){\n  for(int j=0; j<4; j++){\n    if((i == tci) && (j == tcj)){ thresh = tarr[i*4+j]/17.0; }\n  }}\n  \n  return thresh;\n}*/\n\nvec4 getFragColor(){\n  vec4 tc = texture2D(texColor, tex);\n  \n  float thresh = 0.75; //ditherThresh();\n  float alphaClamp = (tc.a < thresh) ? 0.0 : 1.0;\n  \n  vec3 baseColor = tc.rgb;\n  return vec4(baseColor, alphaClamp);\n}\n";

var inclGetColorTexVert = "attribute vec2 vertexTex;\n\nvarying vec3 color;\nvarying vec2 tex;\n\n\nvec3 getVertexColor(){\n  return vec3(1.0, 0.5, 0.0);\n}\n\nvec2 getVertexTex(){\n  return vertexTex;\n}\n";

var inclGetColorVert = "attribute vec3 vertexColor;\n\nvarying vec3 color;\nvarying vec2 tex;\n\n\nvec3 getVertexColor(){\n  return clamp(vertexColor, 0.0, 1.0);\n}\n\nvec2 getVertexTex(){\n  return vec2(0.0, 0.0);\n}\n";

var inclIsShadowedPCF = "\nuniform sampler2D texShadowMap;\n\nuniform mat4 lightCamProjection;\nuniform mat3 lightCamRotation;\n\n\n#shared packDepthColor\n\n\n//----------------------\n//SHADOW MAPPING (PCF)\n//----------------------\n\n//use camera properties and shadow map to determine if a point in space is shadowed\n//also, determine color of translucent overlay (only relevant if not shadowed)\nvoid isFragShadowed(in vec3 fragPosition, out bool fragShadowed, out vec4 fragDepthColor){\n  \n  //get frag position in light's eye coordinates\n  vec3 fragPositionLEC = lightCamRotation * (fragPosition - lightPosition);\n  \n  //get frag position in light's clip coordinates\n  vec4 fragPositionHomogLCC = lightCamProjection * vec4(fragPositionLEC, 1.0);\n  vec3 fragPositionLCC = vec3(fragPositionHomogLCC / fragPositionHomogLCC.w);\n  \n  //rescale clip coords [-1,1] -> [0,1]\n  vec3 fragPositionLCCAdjusted = (fragPositionLCC + vec3(1.0, 1.0, 1.0))/2.0;\n  \n  //check if this is outside the light's frustum\n  if(fragPositionLCC.z >  1.0 ||\n     fragPositionLCC.x < -1.0 || fragPositionLCC.x > 1.0 ||\n     fragPositionLCC.y < -1.0 || fragPositionLCC.y > 1.0 ){\n    \n    fragShadowed = true;\n    return;\n  }\n  \n  //unpack depth and translucent overlay\n  vec4 packedShadowMap = texture2D(texShadowMap, fragPositionLCCAdjusted.xy);  \n  float depthShadowMap = unpackDepth(packedShadowMap.rg);\n  \n  fragShadowed = (fragPositionLCCAdjusted.z > depthShadowMap);\n  \n  if(fragShadowed){\n    fragDepthColor = vec4(0.0, 0.0, 0.0, 1.0);\n  }else{\n    fragDepthColor = unpackDepthColor(packedShadowMap.ba);\n  }\n}\n\n//get any vector orthogonal to v\nvec3 getPerp(vec3 v){\n  \n  if((v.x > -0.6) && (v.x < 0.6)){\n    return vec3(0, v.z, -v.y);\n  }else if((v.y > -0.6) && (v.y < 0.6)){\n    return vec3(-v.z, 0, v.x);\n  }else{\n    return vec3(v.y, -v.x, 0);\n  }\n}\n\n//PCF: shadow map anti-aliasing\nvoid shadowFilter(\n  in vec3 fragPosition, in vec2 kernelOffset, in vec3 normalizedNormal, \n  out bool fragShadowed, out float fragLightPercentage, out vec4 fragDepthColor){\n  \n  //randomly rotating PCF kernel\n  //--------------------------------\n  vec2 offset[8]; float w[8];\n  \n  float rand = fract(sin(dot(kernelOffset,vec2(12.9898,78.233))) * 43758.5453);\n  \n  float dsk = 0.02;\n  float ra = rand;\n  float rs = sin(ra);\n  float rc = cos(ra);\n  offset[0] = dsk*vec2( rc, rs); w[0] = 0.15;\n  offset[1] = dsk*vec2(-rs, rc); w[1] = 0.15;\n  offset[2] = dsk*vec2(-rc,-rs); w[2] = 0.15;\n  offset[3] = dsk*vec2( rs,-rc); w[3] = 0.15;\n  \n  dsk *= 2.0;\n  ra += 0.785398163;\n  rs = sin(ra);\n  rc = cos(ra);\n  offset[4] = dsk*vec2( rc, rs); w[4] = 0.1;\n  offset[5] = dsk*vec2(-rs, rc); w[5] = 0.1;\n  offset[6] = dsk*vec2(-rc,-rs); w[6] = 0.1;\n  offset[7] = dsk*vec2( rs,-rc); w[7] = 0.1;\n  //--------------------------------\n  \n  vec3 perp = normalize(getPerp(normalizedNormal));\n  vec3 perp2 = cross(normalizedNormal, perp);\n  \n  //initial values for outputs\n  fragShadowed = true;\n  fragLightPercentage = 1.0;\n  fragDepthColor = vec4(0.0, 0.0, 0.0, 0.0);\n  \n  //use shadow map to correct values\n  bool fragShadowedTemp;\n  vec4 fragDepthColorTemp;\n  \n  for(int i=0; i<8; i++){\n    vec3 perpOffset = offset[i].x*perp + offset[i].y*perp2;\n    isFragShadowed(fragPosition+perpOffset, fragShadowedTemp, fragDepthColorTemp);\n    \n    fragShadowed = fragShadowed && fragShadowedTemp;\n    fragLightPercentage -= fragDepthColorTemp.a * w[i];\n    fragDepthColor += fragDepthColorTemp * w[i];\n  }\n}\n\n";

var inclLighting = "uniform vec3 camPosition;\n\nuniform vec3 lightPosition;\nuniform vec3 lightAttenuation;\n\nuniform float diffuseCoefficient;\nuniform float specularCoefficient;\nuniform float specularExponent;\n\n//----------------------------------------------------\n//PHONG/CEL SHADING\n//compute diffuse and specular intensity using light uniforms and specified normal and position\n//----------------------------------------------------\n\n//PHONG SHADING\n//compute diffuse and specular intensity using light uniforms and specified normal and position\nvoid getPhong(out float intensityDiffuse, out float intensitySpecular, vec3 normal, vec3 fragPosition){\n  \n  //frag to light vector and distance\n  vec3 fragToLightVector = lightPosition - fragPosition;  \n  float dLight = length(fragToLightVector);  \n  vec3 normalizedFragToLightVector = fragToLightVector / dLight;\n  \n  //compute light attenuation\n  float invAttenuation = dot(lightAttenuation, vec3(1.0, dLight, dLight*dLight));\n  float factorAttenuation = clamp(1.0/invAttenuation, 0.0, 1.0);\n  \n  //compute diffuse intensity\n  intensityDiffuse = clamp(dot(normal, normalizedFragToLightVector), 0.0, 1.0);\n  \n  //compute specular intensity\n  vec3 normalizedFragToEyeVector = normalize(camPosition - fragPosition);\n  vec3 blinnH = normalize(normalizedFragToLightVector + normalizedFragToEyeVector);\n  intensitySpecular = pow(clamp(dot(normal, blinnH), 0.0, 1.0), specularExponent);\n  \n  //adjust for coefficients and attenuation\n  intensityDiffuse = intensityDiffuse * diffuseCoefficient * factorAttenuation;\n  intensitySpecular = intensitySpecular * specularCoefficient * factorAttenuation;\n}\n\n//CEL SHADING\n//snap light intensity to one of three values, with smooth transition between regions\n//can mix with original light intensity for a more subtle effect\nvoid getCel(inout float li, float alpha){\n  \n  //light intensities in dark, medium, and bright regions\n  float outA = 0.5, outB = 0.8, outC = 1.2;  \n  //original light intensities bounding dark, medium, and bright regions\n  float inAB = 0.5, inBC = 0.8; \n  //governs transition between regions; smaller = sharper, bigger = smoother\n  float bw = 0.01; \n  \n  float tli = 0.0;\n  if(li < inAB-bw){ tli = outA; }\n  else if(li < inAB+bw){ tli = outA + (outB-outA)*(li-inAB+bw)/(2.0*bw); }\n  else if(li < inBC-bw){ tli = outB; }\n  else if(li < inBC+bw){ tli = outB + (outC-outB)*(li-inBC+bw)/(2.0*bw); }\n  else{ tli = outC; }\n  \n  li = mix(li, tli, alpha);\n}\n\nvec3 getLighting(vec3 twoSidedNormal, vec3 fragPosition, vec3 lightColor, vec3 color){\n  \n  //shadowed/discarded if facing away from light\n  //----------------------\n  //frag to light vector\n  vec3 normalizedFragToLightVector = normalize(lightPosition - fragPosition);\n  \n  //cosNL < this value should be discarded or hidden (avoids some shadow map artifacts)\n  float minCosNL = 0.2, wCosNL = 0.05; //smaller = sharper transition, bigger = smoother\n  \n  float cosNL = dot(twoSidedNormal, normalizedFragToLightVector);\n  float lightFacingPercentage = clamp((cosNL - minCosNL)/wCosNL, 0.0, 1.0);\n  //----------------------\n  \n  //phong/cel light model\n  //----------------------\n  float celCoefficient = 0.3;\n  float intensityDiffuse;\n  float intensitySpecular;\n  \n  getPhong(intensityDiffuse, intensitySpecular, twoSidedNormal, fragPosition);\n  getCel(intensityDiffuse, celCoefficient);\n  \n  //blend final color\n  vec3 colorDiffuse = color * lightColor;\n  vec3 colorSpecular = lightColor; \n  \n  vec3 finalColor = clamp(intensityDiffuse*colorDiffuse + intensitySpecular*colorSpecular, 0.0, 1.0);\n  //----------------------\n  \n  //outline\n  //----------------------\n  //frag to cam vector and distance\n  vec3 normalizedFragToCamVector = normalize(camPosition - fragPosition);\n  \n  //outline intensity\n  float intensityOutline = clamp(2.0 * dot(twoSidedNormal, normalizedFragToCamVector), 0.0, 1.0);\n  //----------------------\n  \n  return clamp(intensityOutline*lightFacingPercentage*finalColor, 0.0, 1.0);\n}\n";

var inclPackDepthColor = "\n//----------------------------------------------------\n//PACK/UNPACK DEPTH AND COLOR\n//----------------------------------------------------\n\n//depth\n//----------------------------------------------------\n//pack: float in [0,1] -> 2 bytes in [0,1]\nvec2 packDepth(float d){\n  float dpack = d * 255.0;\n  \n  float dpackx = floor(dpack) / 255.0;\n  float dpacky = fract(dpack);\n  \n  return vec2(dpackx, dpacky);\n}\n\n//unpack: 2 bytes in [0,1] -> float in [0,1]\nfloat unpackDepth(vec2 v){\n  return (floor(v.x * 255.0) + v.y) / 255.0;\n}\n\n//color\n//----------------------------------------------------\n//pack: 4 bytes in [0,1] -> 2 bytes [0,1]\nvec2 packDepthColor(vec4 c){\n  vec4 cint = floor(15.0 * c);\n  float rgpack = (16.0*cint.r + cint.g) / 255.0;\n  float bapack = (16.0*cint.b + cint.a) / 255.0;\n  \n  return vec2(rgpack, bapack);\n}\n\n//unpack: 2 bytes in [0,1] -> 4 bytes in [0,1]\nvec4 unpackDepthColor(vec2 v){\n  float rgpack = floor(v.x * 255.0) / 16.0;\n  float bapack = floor(v.y * 255.0) / 16.0;\n  \n  float r = floor(rgpack) / 16.0;\n  float g = fract(rgpack);\n  float b = floor(bapack) / 16.0;\n  float a = fract(bapack);\n  \n  return vec4(r,g,b,a);\n}\n\n";

var lightpassFrag = "precision highp float;\n\nuniform vec3 lightColor;\n\nuniform float alpha;\n\nvarying vec3 position;\nvarying vec3 normal;\n\n#shared getColorFrag\n#shared lighting\n\n\nvoid main(){\n  //TODO: discard if alpha < thresh\n  //---------------------------\n  vec4 fc4 = getFragColor();\n  vec3 fragColor = fc4.rgb;\n  if(fc4.a < 0.9){ discard; }\n  //---------------------------\n  \n  //flip normal if face is back-facing\n  vec3 twoSidedNormal = gl_FrontFacing ? normal : (-1.0*normal);\n  twoSidedNormal = normalize(twoSidedNormal);\n  \n  gl_FragColor = vec4(alpha*getLighting(twoSidedNormal, position, lightColor, fragColor), alpha);\n}\n";

var lightpassShadowFrag = "precision highp float;\n\nuniform vec3 lightPosition;\nuniform vec3 lightColor;\n\nuniform float alpha;\n\nvarying vec3 position;\nvarying vec3 normal;\nvarying vec2 posClipXY;\n\n#shared getColorFrag\n#shared lighting\n#shared packDepthColor\n#shared isShadowedPCF\n\n\nvoid main(){\n  //TODO: discard if alpha < thresh\n  //---------------------------\n  vec4 fc4 = getFragColor();\n  vec3 fragColor = fc4.rgb;\n  if(fc4.a < 0.5){ discard; }\n  //---------------------------\n  \n  //flip normal if face is back-facing\n  vec3 twoSidedNormal = gl_FrontFacing ? normal : (-1.0*normal);\n  twoSidedNormal = normalize(twoSidedNormal);\n  \n  //normalized frag to light vector\n  vec3 normalizedFragToLightVector = normalize(lightPosition - position);\n  \n  //shadow mapping\n  //----------------------\n  float fragLightPercentage;\n  bool fragShadowed;\n  vec4 fragDepthColor;\n  \n  vec3 fragPositionSM = position + 0.05*normalizedFragToLightVector; //shift to avoid z-fighting\n  shadowFilter(fragPositionSM, posClipXY, twoSidedNormal, fragShadowed, fragLightPercentage, fragDepthColor);\n  if(fragShadowed){ discard; } //optional\n  \n  vec3 lightColorFinal = mix(fragDepthColor.rgb*lightColor, lightColor, fragLightPercentage);\n  lightColorFinal *= fragLightPercentage;\n  //----------------------\n  \n  gl_FragColor = vec4(alpha*getLighting(twoSidedNormal, position, lightColorFinal, fragColor), alpha);\n}\n";

var lightpassShadowVert = "attribute vec3 vertexPosition;\nattribute vec3 vertexNormal;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nvarying vec3 position;\nvarying vec3 normal;\nvarying vec2 posClipXY;\n\n#shared getColorVert\n\nvoid main(){\n  position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vec3 vertexPosCam = camRotation * (position - camPosition);\n  \n  tex = getVertexTex();\n  color = getVertexColor();\n  \n  normal = normalize(objRotation * vertexNormal);\n  \n  vec4 posClip = camProjection * vec4(vertexPosCam, 1.0);\n  posClipXY = posClip.xy/posClip.w;\n  gl_Position = posClip;\n}\n";

var lightpassVert = "attribute vec3 vertexPosition;\nattribute vec3 vertexNormal;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nvarying vec3 position;\nvarying vec3 normal;\n\n#shared getColorVert\n\nvoid main(){\n  position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vec3 vertexPosCam = camRotation * (position - camPosition);\n  \n  tex = getVertexTex();\n  color = getVertexColor();\n  \n  normal = normalize(objRotation * vertexNormal);\n  \n  gl_Position = camProjection * vec4(vertexPosCam, 1.0);\n}\n";

var prepassFrag = "precision highp float;\n\nuniform vec3 camPosition;\n\nuniform float alpha;\nuniform float emissiveCoefficient;\nuniform vec3 emissiveColor;\n\nvarying vec3 position;\n\n#shared getColorFrag\n\n\nvoid main(){\n  //TODO: discard if alpha < thresh\n  //---------------------------\n  vec4 fc4 = getFragColor();\n  vec3 fragColor = fc4.rgb;\n  if(fc4.a < 0.9){ discard; }\n  //---------------------------\n\n  vec3 finalColor = emissiveCoefficient*emissiveColor*fragColor;\n  \n  gl_FragColor = vec4(alpha*clamp(finalColor,0.0,1.0), alpha);\n}\n";

var prepassVert = "attribute vec3 vertexPosition;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nvarying vec3 position;\n\n#shared getColorVert\n\nvoid main(){\n  position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vec3 vertexPosCam = camRotation * (position - camPosition);\n  \n  tex = getVertexTex();\n  color = getVertexColor();\n  \n  gl_Position = camProjection * vec4(vertexPosCam, 1.0);\n}\n";

var quadFragBlurH = "precision highp float;\n\nuniform sampler2D texColor;\nuniform ivec2 texSize;\n\nvarying vec2 texCoord;\n\n\nvoid main()\n{\n  float pw = 1.0/float(texSize.x);\n  float ph = 1.0/float(texSize.y);\n  \n  vec4 color = vec4(0.0, 0.0, 0.0, 0.0);\n  \n  color += 0.0702702703 * texture2D(texColor, texCoord + vec2(-3.2307692308 * pw, 0.0));\n  color += 0.3162162162 * texture2D(texColor, texCoord + vec2(-1.3846153846 * pw, 0.0));\n  color += 0.2270270270 * texture2D(texColor, texCoord + vec2( 0.0          * pw, 0.0));\n  color += 0.3162162162 * texture2D(texColor, texCoord + vec2( 1.3846153846 * pw, 0.0));\n  color += 0.0702702703 * texture2D(texColor, texCoord + vec2( 3.2307692308 * pw, 0.0));\n  \n  gl_FragColor = clamp(color, 0.0, 1.0);\n}\n";

var quadFragBlurV = "precision highp float;\n\nuniform sampler2D texColor;\nuniform ivec2 texSize;\n\nvarying vec2 texCoord;\n\n\nvoid main()\n{\n  float pw = 1.0/float(texSize.x);\n  float ph = 1.0/float(texSize.y);\n  \n  vec4 color = vec4(0.0, 0.0, 0.0, 0.0);\n  \n  color += 0.0702702703 * texture2D(texColor, texCoord + vec2(0.0, -3.2307692308 * ph));\n  color += 0.3162162162 * texture2D(texColor, texCoord + vec2(0.0, -1.3846153846 * ph));\n  color += 0.2270270270 * texture2D(texColor, texCoord + vec2(0.0,  0.0          * ph));\n  color += 0.3162162162 * texture2D(texColor, texCoord + vec2(0.0,  1.3846153846 * ph));\n  color += 0.0702702703 * texture2D(texColor, texCoord + vec2(0.0,  3.2307692308 * ph));\n  \n  gl_FragColor = clamp(color, 0.0, 1.0);\n}\n";

var quadFragCopy = "precision highp float;\n\nuniform sampler2D texColor;\nvarying vec2 texCoord;\n\n\nvoid main()\n{\n  gl_FragColor = texture2D(texColor, texCoord);\n}\n";

var quadFragPost = "precision highp float;\n\nuniform sampler2D texColor;\nuniform sampler2D texBlur;\n\n//OPTIONS\n//-----------------\nuniform float bloomFactor;\n\nuniform float colorAdjustBrightness;\nuniform float colorAdjustContrast;\nuniform float colorAdjustSaturation;\n//-----------------\n\n\nvarying vec2 texCoord;\n\n\nvoid main()\n{\n  //blend blur and non-blur\n  vec4 colorNonBlur = texture2D(texColor, texCoord);\n  vec4 colorBlur = texture2D(texBlur, texCoord);\n\n  //bloom\n  //--------------------------------\n  vec4 colorFinal = clamp(mix(colorNonBlur, colorBlur, bloomFactor), 0.0, 1.0);\n  vec3 color = colorFinal.rgb;\n  //--------------------------------\n  \n  //color adjust\n  //--------------------------------  \n  //adjust saturation\n  float lum = dot(color, vec3(0.299,0.587,0.144));\n  vec3 noSaturation = vec3(lum, lum, lum);\n  color = mix(noSaturation, color, colorAdjustSaturation);\n  \n  //adjust brightness\n  color += vec3(colorAdjustBrightness, colorAdjustBrightness, colorAdjustBrightness);\n  \n  //adjust contrast\n  vec3 noContrast = vec3(0.5, 0.5, 0.5);\n  color = mix(noContrast, color, colorAdjustContrast);\n  //--------------------------------\n  \n  gl_FragColor = clamp(vec4(colorFinal.a*color, colorFinal.a), 0.0, 1.0);\n}\n";

var quadVert = "attribute vec3 vertexPosition;\nattribute vec2 vertexTex;\n\nvarying vec2 texCoord;\n\n\nvoid main(){\n\n  texCoord = vertexTex;\n  gl_Position = vec4(vertexPosition, 1.0);\n}\n";

var wireframeFrag = "precision highp float;\n\nuniform float alpha;\nuniform vec4 wireframeColor;\n\n\nvoid main(){\n  //wireframeColor is not premultiplied alpha\n  float wfa = wireframeColor.a;\n  vec3 wfc = wireframeColor.rgb;\n  gl_FragColor = vec4(wfa*wfc, wfa);\n}\n";

var wireframeVert = "attribute vec3 vertexPosition;\n\nuniform vec3 camPosition;\nuniform mat3 camRotation;\nuniform mat4 camProjection;\n\nuniform mat3 objRotation;\nuniform vec3 objTranslation;\nuniform vec3 objOffset;\n\nuniform float zOffset;\n\n\nvoid main(){\n  vec3 position = objRotation * (vertexPosition - objOffset) + objTranslation;\n  vec3 vertexPosCam = camRotation * (position - camPosition);\n  \n  vertexPosCam.z = vertexPosCam.z + zOffset;\n    \n  gl_Position = camProjection * vec4(vertexPosCam, 1.0);\n}\n";

exports.depthColorFrag = depthColorFrag;
exports.depthColorVert = depthColorVert;
exports.depthFrag = depthFrag;
exports.depthVert = depthVert;
exports.inclGetColorFrag = inclGetColorFrag;
exports.inclGetColorTexFrag = inclGetColorTexFrag;
exports.inclGetColorTexVert = inclGetColorTexVert;
exports.inclGetColorVert = inclGetColorVert;
exports.inclIsShadowedPCF = inclIsShadowedPCF;
exports.inclLighting = inclLighting;
exports.inclPackDepthColor = inclPackDepthColor;
exports.lightpassFrag = lightpassFrag;
exports.lightpassShadowFrag = lightpassShadowFrag;
exports.lightpassShadowVert = lightpassShadowVert;
exports.lightpassVert = lightpassVert;
exports.prepassFrag = prepassFrag;
exports.prepassVert = prepassVert;
exports.quadFragBlurH = quadFragBlurH;
exports.quadFragBlurV = quadFragBlurV;
exports.quadFragCopy = quadFragCopy;
exports.quadFragPost = quadFragPost;
exports.quadVert = quadVert;
exports.wireframeFrag = wireframeFrag;
exports.wireframeVert = wireframeVert;

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Figure = exports.WGLTexture = exports.GeomText = exports.GeomVectors = exports.GeomSkeleton = exports.GeomSurface = exports.GeomCurve = exports.GeomOBJFile = exports.GeomQuad = exports.SceneObject = exports.GeomBundle = exports.GeomData = exports.Light = exports.ColorUtils = exports.Camera = exports.CameraRotation = exports.ObjectTransformation = exports.SO3 = exports.Mat4 = exports.Mat3 = exports.Vec4 = exports.Vec3 = undefined;

var _mathVec = require('./wglcore/mathVec');

var _mathMat = require('./wglcore/mathMat');

var _mathSO = require('./wglcore/mathSO3');

var _mathSO2 = _interopRequireDefault(_mathSO);

var _mathObjTrans = require('./wglcore/mathObjTrans');

var _mathObjTrans2 = _interopRequireDefault(_mathObjTrans);

var _mathCam = require('./wglcore/mathCam');

var _mathColor = require('./wglcore/mathColor');

var _mathColor2 = _interopRequireDefault(_mathColor);

var _light = require('./wglcore/light');

var _light2 = _interopRequireDefault(_light);

var _geomData = require('./wglcore/geomData');

var _geomData2 = _interopRequireDefault(_geomData);

var _geomBundle = require('./wglcore/geomBundle');

var _geomBundle2 = _interopRequireDefault(_geomBundle);

var _geomSceneObject = require('./wglcore/geomSceneObject');

var _geomSceneObject2 = _interopRequireDefault(_geomSceneObject);

var _geomQuad = require('./wglcore/geomQuad');

var _geomQuad2 = _interopRequireDefault(_geomQuad);

var _geomObjFile = require('./wglcore/geomObjFile');

var _geomObjFile2 = _interopRequireDefault(_geomObjFile);

var _geomCurve = require('./wglcore/geomCurve');

var _geomCurve2 = _interopRequireDefault(_geomCurve);

var _geomSurface = require('./wglcore/geomSurface');

var _geomSurface2 = _interopRequireDefault(_geomSurface);

var _geomSkeleton = require('./wglcore/geomSkeleton');

var _geomSkeleton2 = _interopRequireDefault(_geomSkeleton);

var _geomVectors = require('./wglcore/geomVectors');

var _geomVectors2 = _interopRequireDefault(_geomVectors);

var _geomText = require('./wglcore/geomText');

var _geomText2 = _interopRequireDefault(_geomText);

var _wglTexture = require('./wglcore/wglTexture');

var _figMain = require('./fig/figMain');

var _figMain2 = _interopRequireDefault(_figMain);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import WGLDrawable from './wglcore/wglDrawable';
// import WGLQuad from './wglcore/wglQuad';
// import WGLShaderProgram from './wglcore/wglShader';
exports.Vec3 = _mathVec.Vec3;
exports.Vec4 = _mathVec.Vec4;
exports.Mat3 = _mathMat.Mat3;
exports.Mat4 = _mathMat.Mat4;
exports.SO3 = _mathSO2.default;
exports.ObjectTransformation = _mathObjTrans2.default;
exports.CameraRotation = _mathCam.CameraRotation;
exports.Camera = _mathCam.Camera;
exports.ColorUtils = _mathColor2.default;
exports.Light = _light2.default;
exports.GeomData = _geomData2.default;
exports.GeomBundle = _geomBundle2.default;
exports.SceneObject = _geomSceneObject2.default;
exports.GeomQuad = _geomQuad2.default;
exports.GeomOBJFile = _geomObjFile2.default;
exports.GeomCurve = _geomCurve2.default;
exports.GeomSurface = _geomSurface2.default;
exports.GeomSkeleton = _geomSkeleton2.default;
exports.GeomVectors = _geomVectors2.default;
exports.GeomText = _geomText2.default;
exports.WGLTexture = _wglTexture.WGLTexture;
exports.Figure = _figMain2.default;

},{"./fig/figMain":1,"./wglcore/geomBundle":8,"./wglcore/geomCurve":9,"./wglcore/geomData":10,"./wglcore/geomObjFile":11,"./wglcore/geomQuad":12,"./wglcore/geomSceneObject":13,"./wglcore/geomSkeleton":14,"./wglcore/geomSurface":15,"./wglcore/geomText":16,"./wglcore/geomVectors":17,"./wglcore/light":18,"./wglcore/mathCam":19,"./wglcore/mathColor":20,"./wglcore/mathMat":21,"./wglcore/mathObjTrans":22,"./wglcore/mathSO3":23,"./wglcore/mathVec":24,"./wglcore/wglTexture":28}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

var _geomSceneObject = require('./geomSceneObject');

var _geomSceneObject2 = _interopRequireDefault(_geomSceneObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MAX_VERTICES = 65535;
var TEMP_U = [0, 0, 0];
var TEMP_V = [0, 0, 0];
var TEMP_W = [0, 0, 0];

// ===================================
// GEOMETRY: BUNDLE
// repeated copies of the same geometry
// ===================================

// TODO
// ideally would use instancing, i.e. glDrawElementsInstanced etc
// but this isn't in WebGL: could use ANGLE_instanced_arrays etc if available
// for now: pack all the copies into one (or more if > 65535 verts) geometry

/** A collection of instances with shared {@link GeomData}.
* @typedef {Object} GeomBundle~group
*
* @prop {GeomData} geometry - Defines the geometry of each instance
* @prop {GeomBundle~instance[]} instances - Instances of the geometry
*/

/** One instance of a geometry.  Properties describe how the reference geometry will be modified.
*   <ul>
*   <li>Multiply reference color coordinate-wise with "color",
*   <li>(1) multiply reference position coordinate-wise with "scale",
*   <li>(2) if "hasOrientation", rotate so that [1,0,0] -> "orientation" (any such rotation),
*   <li>(3) add "position" to the result.
*   </ul>
* @typedef {Object} GeomBundle~instance
*
* @prop {number[]} scale
* @prop {boolean} hasOrientation - If false, "orientation" is never used
* @prop {number[]} orientation - Need not be normalized
* @prop {number[]} position
* @prop {number[]} color
*/

/** Packs multiple instances of geometries into as few {@link GeomData} objects as possible.
*   <br>Must call {@link GeomBundle#generate} after creation, or to update instance properties.
*   <br>Use {@link GeomBundle#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomBundle~group[]} opts.groups - Each group specifies a geometry and its instances
*/

var GeomBundle = function () {
  function GeomBundle(opts) {
    _classCallCheck(this, GeomBundle);

    this.opts = opts;
    this._geomArray = []; // break geometry into multiple GeomDatas

    this._prepare();
  }

  // generate geometry
  // -----------------------------------
  // each instance will have:
  //  _gIndex (index of GeomData in this._geomArray)
  //  _es0 (starting position in GeomData elements array)
  //  _ew0 (starting position in GeomData elementsWire array)
  //  _v0 (starting position in GeomData vert attrib arrays)

  // set above properties and return array of GeomData initialization options


  _createClass(GeomBundle, [{
    key: '_setLayout',
    value: function _setLayout() {
      // running indices (see descr of 'instance' above)
      var gIndex = 0;
      var es0 = 0;var ew0 = 0;var v0 = 0;

      // list of options to be used to initialize each GeomData
      var gOptsArray = [];

      // push current geom opts and start the next
      var gNext = function gNext() {
        if (v0 > 0) {
          gOptsArray[gIndex] = {
            hasNormals: true,
            elementCount: es0,
            elementWireCount: ew0,
            vertexCount: v0
          };
          gIndex++;
        }
        es0 = 0;ew0 = 0;v0 = 0;
      };

      // set _gIndex, _es0, _ew0, _v0 on each instance and add to gOptsArray
      for (var i = 0; i < this.opts.groups.length; i++) {
        var gsrc = this.opts.groups[i].geometry;
        var instances = this.opts.groups[i].instances;


        for (var j = 0; j < instances.length; j++) {
          // if next instance won't fit in geom, start a new one
          var v0next = v0 + gsrc.vertexCount;
          if (v0next > MAX_VERTICES) {
            gNext();
          }

          instances[j]._gIndex = gIndex;
          instances[j]._es0 = es0;
          instances[j]._ew0 = ew0;
          instances[j]._v0 = v0;

          es0 += gsrc.elementCount;
          ew0 += gsrc.elementWireCount;
          v0 += gsrc.vertexCount;
        }

        // prepare geom opts for the next group
        gNext();
      }

      // return list of GeomData initialization options
      return gOptsArray;
    }

    // use "layout" to put reference geometry elements into GeomData

  }, {
    key: '_putElements',
    value: function _putElements(gsrc, instance) {
      var gdata = this._geomArray[instance._gIndex];

      var es0 = instance._es0;
      var ew0 = instance._ew0;
      var v0 = instance._v0;

      for (var i = 0; i < gsrc.elementCount; i++) {
        gdata.elements[es0 + i] = v0 + gsrc.elements[i];
      }

      for (var _i = 0; _i < gsrc.elementWireCount; _i++) {
        gdata.elementsWire[ew0 + _i] = v0 + gsrc.elementsWire[_i];
      }
    }

    // establish "layout", create GeomDatas, populate element arrays

  }, {
    key: '_prepare',
    value: function _prepare() {
      // get GeomData layout
      var gOptsArray = this._setLayout();

      // create the GeomDatas
      for (var i = 0; i < gOptsArray.length; i++) {
        var gdata = new _geomData2.default();
        gdata.initialize(gOptsArray[i]);
        this._geomArray.push(gdata);
      }

      // populate element arrays
      for (var _i2 = 0; _i2 < this.opts.groups.length; _i2++) {
        var gsrc = this.opts.groups[_i2].geometry;
        var instances = this.opts.groups[_i2].instances;


        for (var j = 0; j < instances.length; j++) {
          this._putElements(gsrc, instances[j]);
        }
      }
    }

    // call at least once, or to update instances
    // ===================================
    // use "layout" to put reference geometry vertex attribs into GeomData

    // write vertex attribs for instance using all instance options

  }, {
    key: '_putVertexAttribsR',
    value: function _putVertexAttribsR(gsrc, instance) {
      var gdata = this._geomArray[instance._gIndex];

      // normalize orientation
      _mathVec.Vec3.copy(TEMP_U, instance.orientation);
      _mathVec.Vec3.normalize(TEMP_U, TEMP_U);
      // v = any unit vector orthogonal to u
      _mathVec.Vec3.orthogonal(TEMP_V, TEMP_U);
      _mathVec.Vec3.normalize(TEMP_V, TEMP_V);
      // w = uxv
      _mathVec.Vec3.cross(TEMP_W, TEMP_U, TEMP_V);

      var invScale0 = 1 / instance.scale[0];
      var invScale1 = 1 / instance.scale[1];
      var invScale2 = 1 / instance.scale[2];

      var index0 = 3 * instance._v0;
      for (var i = 0; i < gsrc.vertexCount; i++) {
        var indexG = 3 * i;
        var index = index0 + indexG;

        // scale: position and normal
        var x = gsrc.position[indexG + 0] * instance.scale[0];
        var y = gsrc.position[indexG + 1] * instance.scale[1];
        var z = gsrc.position[indexG + 2] * instance.scale[2];

        var nx = gsrc.normal[indexG + 0] * invScale0;
        var ny = gsrc.normal[indexG + 1] * invScale1;
        var nz = gsrc.normal[indexG + 2] * invScale2;

        // rotate and offset: position and normal (NOTE: not bothering to normalize normal)
        gdata.position[index + 0] = x * TEMP_U[0] + y * TEMP_V[0] + z * TEMP_W[0] + instance.position[0];
        gdata.position[index + 1] = x * TEMP_U[1] + y * TEMP_V[1] + z * TEMP_W[1] + instance.position[1];
        gdata.position[index + 2] = x * TEMP_U[2] + y * TEMP_V[2] + z * TEMP_W[2] + instance.position[2];

        gdata.normal[index + 0] = nx * TEMP_U[0] + ny * TEMP_V[0] + nz * TEMP_W[0];
        gdata.normal[index + 1] = nx * TEMP_U[1] + ny * TEMP_V[1] + nz * TEMP_W[1];
        gdata.normal[index + 2] = nx * TEMP_U[2] + ny * TEMP_V[2] + nz * TEMP_W[2];

        // color
        gdata.color[index + 0] = gsrc.color[indexG + 0] * instance.color[0];
        gdata.color[index + 1] = gsrc.color[indexG + 1] * instance.color[1];
        gdata.color[index + 2] = gsrc.color[indexG + 2] * instance.color[2];
      }
    }

    // write vertex attribs for instance without applying "orientation" option

  }, {
    key: '_putVertexAttribsNR',
    value: function _putVertexAttribsNR(gsrc, instance) {
      var gdata = this._geomArray[instance._gIndex];

      var invScale0 = 1 / instance.scale[0];
      var invScale1 = 1 / instance.scale[1];
      var invScale2 = 1 / instance.scale[2];

      var index0 = 3 * instance._v0;
      for (var i = 0; i < gsrc.vertexCount; i++) {
        var indexG = 3 * i;
        var index = index0 + indexG;

        // position
        gdata.position[index + 0] = gsrc.position[indexG + 0] * instance.scale[0] + instance.position[0];
        gdata.position[index + 1] = gsrc.position[indexG + 1] * instance.scale[1] + instance.position[1];
        gdata.position[index + 2] = gsrc.position[indexG + 2] * instance.scale[2] + instance.position[2];

        // normal (NOTE: not bothering to normalize normal)
        gdata.normal[index + 0] = gsrc.normal[indexG + 0] * invScale0;
        gdata.normal[index + 1] = gsrc.normal[indexG + 1] * invScale1;
        gdata.normal[index + 2] = gsrc.normal[indexG + 2] * invScale2;

        // color
        gdata.color[index + 0] = gsrc.color[indexG + 0] * instance.color[0];
        gdata.color[index + 1] = gsrc.color[indexG + 1] * instance.color[1];
        gdata.color[index + 2] = gsrc.color[indexG + 2] * instance.color[2];
      }
    }

    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      for (var i = 0; i < this.opts.groups.length; i++) {
        var gsrc = this.opts.groups[i].geometry;
        var instances = this.opts.groups[i].instances;


        for (var j = 0; j < instances.length; j++) {
          var instance = instances[j];
          if (instance.hasOrientation) {
            // uses "orientation" option
            this._putVertexAttribsR(gsrc, instance);
          } else {
            // faster version with no "orientation" option
            this._putVertexAttribsNR(gsrc, instance);
          }
        }
      }
    }

    /** Create a new {@link SceneObject} with this geometry.
    * @return {SceneObject}
    */

  }, {
    key: 'instance',
    value: function instance() {
      return new _geomSceneObject2.default(this._geomArray);
    }
  }]);

  return GeomBundle;
}();

exports.default = GeomBundle;

},{"./geomData":10,"./geomSceneObject":13,"./mathVec":24}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var rangeLerp = function rangeLerp(range, lerp) {
  return range[0] + lerp * (range[1] - range[0]);
};

// ===================================
// GEOMETRY: CURVE
// tubeplot of parametric curve (f:R -> R^3)
// ===================================

/** Parametric curve, fattened into a tube.
*   <br>Must call {@link GeomCurve#generate} after creation, or to update position/color values.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {number} opts.tubeN - Number of vertices around the tube
* @param {number} opts.tubeRadius - Radius of the tube
* @param {number[]} opts.range - Range of t values: [tmin,tmax]
* @param {number} opts.n - Number of samples in range of t values
* @param {Function} opts.f - Position function: <code>function(t){ return [x,y,z]; }</code>
* @param {Function} opts.color - Color function (theta = angle around tube): <code>function(t,theta,[x,y,z]){ return [r,g,b]; }</code>
* @param {boolean} opts.loop - Set to true if f(tmin) = f(tmax)
*
* @extends GeomData
*/

var GeomCurve = function (_GeomData) {
  _inherits(GeomCurve, _GeomData);

  function GeomCurve(opts) {
    _classCallCheck(this, GeomCurve);

    var _this = _possibleConstructorReturn(this, (GeomCurve.__proto__ || Object.getPrototypeOf(GeomCurve)).call(this));

    _this.opts = opts;

    // create this._curvePoint/_curveSegments/_gridFaces arrays, GeomData elements
    _this._createGrid();
    return _this;
  }

  // generate grid geometry
  // -----------------------------------
  // create curvePoint


  _createClass(GeomCurve, [{
    key: '_createPoint',
    value: function _createPoint(lerp) {
      // create and add curvePoint object
      var cpIndex = this._curvePoints.length;

      var curvePoint = {
        t: rangeLerp(this.opts.range, lerp),
        position: [0, 0, 0], // set by "generate"
        frameT: [1, 0, 0], // set by "generate"
        frameN: [0, 1, 0], // set by "generate"
        frameB: [0, 0, 1], // set by "generate"
        tube: [] // objects representing actual vertices in the GeomData
      };
      this._curvePoints.push(curvePoint);

      // create "tube" vertices
      var vIndex0 = cpIndex * this.opts.tubeN;

      for (var i = 0; i < this.opts.tubeN; i++) {
        var theta = 2 * Math.PI * (i / this.opts.tubeN);
        var vertex = {
          _vindex: vIndex0 + i, // index in GeomData vertex arrays
          theta: theta,
          position: [0, 0, 0],
          normal: [0, 1, 0]
        };
        curvePoint.tube.push(vertex);
      }
    }

    // vi are "vertex" objects appearing in curvePoint.tube

  }, {
    key: '_createFace',
    value: function _createFace(v0, v1, v2, v3) {
      var f0 = this._gridFaces.length;

      this._gridFaces.push({ incidentVerts: [v0, v1, v2] });
      this._gridFaces.push({ incidentVerts: [v0, v2, v3] });

      this.setFace(f0++, 0, v0._vindex, v1._vindex, v2._vindex);
      this.setFace(f0++, 0, v0._vindex, v2._vindex, v3._vindex);
    }

    // creates _gridVerts, _gridFaces; initializes geometry and its faces

  }, {
    key: '_createGrid',
    value: function _createGrid() {
      var nt = this.opts.n;
      var ntube = this.opts.tubeN;

      // initialize GeomData
      var wireLinesL = (nt + 1) * ntube;
      var wireLinesT = nt * ntube;
      this.initialize({
        hasNormals: true,
        vertexCount: (nt + 1) * ntube,
        elementCount: 3 * (2 * nt * ntube),
        elementWireCount: 2 * (wireLinesL + wireLinesT)
      });

      // prepare curve points
      this._curvePoints = [];
      for (var it = 0; it <= nt; it++) {
        // lerp = point in input range [0,1]
        var lerp = it / nt;
        this._createPoint(lerp);
      }

      // prepare curve segments
      this._curveSegments = [];
      for (var _it = 0; _it < nt; _it++) {
        var ends = [this._curvePoints[_it], this._curvePoints[_it + 1]];
        this._curveSegments.push({
          frameT: [0, 0, 0],
          frameN: [0, 0, 0],
          incidentPoints: ends
        });
      }

      // prepare grid faces, GeomData elements
      this._gridFaces = [];
      for (var _it2 = 0; _it2 < nt; _it2++) {
        var p = this._curvePoints[_it2];
        var q = this._curvePoints[_it2 + 1];

        for (var ia = 0; ia < ntube; ia++) {
          var ja = (ia + 1) % ntube;
          this._createFace(p.tube[ia], q.tube[ia], q.tube[ja], p.tube[ja]);
        }
      }

      // prepare wireframe edges
      var edgeIndex = 0;

      for (var _it3 = 0; _it3 <= nt; _it3++) {
        var _p = this._curvePoints[_it3];
        for (var _ia = 0; _ia < ntube; _ia++) {
          var _ja = (_ia + 1) % ntube;
          this.setLine(edgeIndex++, 0, _p.tube[_ia]._vindex, _p.tube[_ja]._vindex);
        }
      }

      for (var _it4 = 0; _it4 < nt; _it4++) {
        var _p2 = this._curvePoints[_it4];
        var _q = this._curvePoints[_it4 + 1];
        for (var _ia2 = 0; _ia2 < ntube; _ia2++) {
          this.setLine(edgeIndex++, 0, _p2.tube[_ia2]._vindex, _q.tube[_ia2]._vindex);
        }
      }
    }

    // generate geometry
    // updates data contained in geometry, _gridVerts, _gridFaces, rangeOut
    // ===================================

    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      // compute position of curve points and reset frame
      for (var i = 0; i < this._curvePoints.length; i++) {
        var curvePoint = this._curvePoints[i];
        _mathVec.Vec3.copy(curvePoint.position, this.opts.f(curvePoint.t));
        _mathVec.Vec3.zero(curvePoint.frameT);
        _mathVec.Vec3.zero(curvePoint.frameN);
        _mathVec.Vec3.zero(curvePoint.frameB);
      }

      // compute frame (i.e., curvePoint.frameT/frameN/frameB)
      // -------------------------------
      // compute frameT on segments, add to endpoints
      for (var _i = 0; _i < this._curveSegments.length; _i++) {
        var segment = this._curveSegments[_i];
        var p = segment.incidentPoints[0];
        var q = segment.incidentPoints[1];

        // frameT = change in position
        _mathVec.Vec3.subtract(segment.frameT, q.position, p.position);
        _mathVec.Vec3.add(p.frameT, p.frameT, segment.frameT);
        _mathVec.Vec3.add(q.frameT, q.frameT, segment.frameT);
      }

      // compute frameT on curve points
      for (var _i2 = 0; _i2 < this._curvePoints.length; _i2++) {
        var _curvePoint = this._curvePoints[_i2];
        _mathVec.Vec3.normalize(_curvePoint.frameT, _curvePoint.frameT);
      }

      // compute frameN on segments, add to endpoints
      for (var _i3 = 0; _i3 < this._curveSegments.length; _i3++) {
        var _segment = this._curveSegments[_i3];
        var _p3 = _segment.incidentPoints[0];
        var _q2 = _segment.incidentPoints[1];

        // frameN = change in frameT
        _mathVec.Vec3.subtract(_segment.frameN, _q2.frameT, _p3.frameT);
        // Vec3.orthogonal(segment.frameN, segment.frameT);
        _mathVec.Vec3.add(_p3.frameN, _p3.frameN, _segment.frameN);
        _mathVec.Vec3.add(_q2.frameN, _q2.frameN, _segment.frameN);
      }

      // compute frameN on curve points
      for (var _i4 = 0; _i4 < this._curvePoints.length; _i4++) {
        var _curvePoint2 = this._curvePoints[_i4];
        _mathVec.Vec3.normalize(_curvePoint2.frameN, _curvePoint2.frameN);
      }

      // TODO glue: quick fix of frameT/frameN for looping curve
      if (this.opts.loop) {
        var _p4 = this._curvePoints[0];
        var _q3 = this._curvePoints[this._curvePoints.length - 1];

        // average p and q frameT then normalize
        _mathVec.Vec3.add(_p4.frameT, _p4.frameT, _q3.frameT);
        _mathVec.Vec3.normalize(_p4.frameT, _p4.frameT);
        _mathVec.Vec3.copy(_q3.frameT, _p4.frameT);

        // average p and q frameN then normalize
        _mathVec.Vec3.add(_p4.frameN, _p4.frameN, _q3.frameN);
        _mathVec.Vec3.normalize(_p4.frameN, _p4.frameN);
        _mathVec.Vec3.copy(_q3.frameN, _p4.frameN);
      }

      // compute frameB on curve points
      for (var _i5 = 0; _i5 < this._curvePoints.length; _i5++) {
        var _curvePoint3 = this._curvePoints[_i5];
        _mathVec.Vec3.cross(_curvePoint3.frameB, _curvePoint3.frameT, _curvePoint3.frameN);
      }
      // -------------------------------

      // compute position/color of tube vertices
      for (var _i6 = 0; _i6 < this._curvePoints.length; _i6++) {
        var _curvePoint4 = this._curvePoints[_i6];
        for (var j = 0; j < _curvePoint4.tube.length; j++) {
          var vertex = _curvePoint4.tube[j];

          // color
          var c = this.opts.color(_curvePoint4.t, vertex.theta, _curvePoint4.position);
          this.setColor(vertex._vindex, c);

          // position
          var ct = Math.cos(vertex.theta);
          var st = Math.sin(vertex.theta);

          _mathVec.Vec3.linearCombination(vertex.normal, ct, _curvePoint4.frameB, st, _curvePoint4.frameN);
          _mathVec.Vec3.addMultiple(vertex.position, _curvePoint4.position, vertex.normal, this.opts.tubeRadius);

          this.setPosition(vertex._vindex, vertex.position);
          this.setNormal(vertex._vindex, vertex.normal);
        }
      }
    }
  }]);

  return GeomCurve;
}(_geomData2.default);

exports.default = GeomCurve;

},{"./geomData":10,"./mathVec":24}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _geomSceneObject = require('./geomSceneObject');

var _geomSceneObject2 = _interopRequireDefault(_geomSceneObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// GEOMETRY: RAW DATA
// ===================================

/** Stores raw geometry data.  Call {@link GeomData#initialize} before doing anything.
*   <br>Element array entries refer to an index in the attribute arrays.
*   <br>Example: "1" means [position[3], position[4], position[5]], etc.
*   <br>Note: Only one of 'color' or 'tex' will be used, depending on the 'hasTexture' option.
*
* @prop {number} vertexCount - See {@link GeomData#initialize}
* @prop {number} elementCount - See {@link GeomData#initialize}
* @prop {number} elementWireCount - See {@link GeomData#initialize}
* @prop {number} hasNormals - See {@link GeomData#initialize}
* @prop {number} hasTexture - See {@link GeomData#initialize}
*
* @prop {Uint16Array} elements - Triangles as [i0,j0,k0, i1,j1,k1, ...]
* @prop {Uint16Array} elementsWire - Lines as [i0,j0, i1,j1, i2,j2, ...]
* @prop {Float32Array} position - Array of positions as [x0,y0,z0, x1,y1,z1, ...]
* @prop {Float32Array} normal - Array of normals as [x0,y0,z0, x1,y1,z1, ...] (or null if hasNormals is false)
* @prop {Float32Array} color - Array of colors as [r0,g0,b0, r1,g1,b1, ...] (or null if hasTexture is true)
* @prop {Float32Array} tex - Array of tex coords as [u0,v0, u1,v1, ...] (or null if hasTexture is false)
*/
var GeomData = function () {
  function GeomData() {
    _classCallCheck(this, GeomData);
  }

  _createClass(GeomData, [{
    key: 'initialize',

    /** Initialize arrays storing the geometry.
    * @param {Object} opts
    * @param {number} opts.vertexCount - Determines the size of vertex attribute arrays (should not exceed 65535)
    * @param {number} opts.elementCount - Size of elements array: 3*(num triangles)
    * @param {number} opts.elementWireCount - Size of elementsWire array: 2*(num lines)
    * @param {boolean} opts.hasNormals - True for triangle data, false for line data
    * @param {boolean} opts.hasTexture - (Optional, default false) Determine whether to use color or tex attribute array
    */
    value: function initialize(opts) {
      this.elementCount = opts.elementCount;
      this.elementWireCount = opts.elementWireCount;
      this.vertexCount = opts.vertexCount;

      this.hasNormals = opts.hasNormals;
      this.hasTexture = false;
      if (typeof opts.hasTexture !== 'undefined') {
        this.hasTexture = opts.hasTexture;
      }

      // indices cannot exceed max value of Uint16
      if (opts.vertexCount > 65535) {
        console.error('More than 65535 vertices!');
      }

      this.elements = new Uint16Array(opts.elementCount);
      this.elementsWire = new Uint16Array(opts.elementWireCount);
      this.position = new Float32Array(3 * opts.vertexCount);

      if (this.hasTexture) {
        this.tex = new Float32Array(2 * opts.vertexCount);
      } else {
        this.color = new Float32Array(3 * opts.vertexCount);
      }

      this.normal = null;
      if (this.hasNormals) {
        this.normal = new Float32Array(3 * opts.vertexCount);
      }
    }

    // convenience method for creating a SceneObject
    // -----------------------------------
    /** Create a new {@link SceneObject} with this geometry.
    * @return {SceneObject}
    */

  }, {
    key: 'instance',
    value: function instance() {
      return new _geomSceneObject2.default([this]);
    }

    // convenience methods for setting data
    // -----------------------------------
    /** Set a triangle's vertices.
    * @param {number} index - Index of the triangle, in [0,elementCount/3)
    * @param {number} vOffset - Offset for i, j, k, in [0,vertexCount)
    * @param {number} i - Index of vertex 0 is vOffset+i
    * @param {number} j - Index of vertex 1 is vOffset+j
    * @param {number} k - Index of vertex 2 is vOffset+k
    */

  }, {
    key: 'setFace',
    value: function setFace(index, vOffset, i, j, k) {
      this.elements[3 * index + 0] = vOffset + i;
      this.elements[3 * index + 1] = vOffset + j;
      this.elements[3 * index + 2] = vOffset + k;
    }

    /** Set a line segment's vertices.
    * @param {number} index - Index of the line segment, in [0,elementWireCount/2)
    * @param {number} vOffset - Offset for i, j, in [0,vertexCount)
    * @param {number} i - Index of vertex 0 is vOffset+i
    * @param {number} j - Index of vertex 1 is vOffset+j
    */

  }, {
    key: 'setLine',
    value: function setLine(index, vOffset, i, j) {
      this.elementsWire[2 * index + 0] = vOffset + i;
      this.elementsWire[2 * index + 1] = vOffset + j;
    }

    /** Set one vertex position.
    * @param {number} index - Index of the vertex, in [0,vertexCount)
    * @param {number[]} p - Position (x = p[0], y = p[1], z = p[2])
    */

  }, {
    key: 'setPosition',
    value: function setPosition(index, p) {
      this.position[3 * index + 0] = p[0];
      this.position[3 * index + 1] = p[1];
      this.position[3 * index + 2] = p[2];
    }

    /** Set one vertex normal.
    * @param {number} index - Index of the vertex, in [0,vertexCount)
    * @param {number[]} n - Normal vector (x = n[0], y = n[1], z = n[2])
    */

  }, {
    key: 'setNormal',
    value: function setNormal(index, n) {
      this.normal[3 * index + 0] = n[0];
      this.normal[3 * index + 1] = n[1];
      this.normal[3 * index + 2] = n[2];
    }

    /** Set one vertex color.
    * @param {number} index - Index of the vertex, in [0,vertexCount)
    * @param {number[]} c - Color vector (r = c[0], g = c[1], b = c[2])
    */

  }, {
    key: 'setColor',
    value: function setColor(index, c) {
      this.color[3 * index + 0] = c[0];
      this.color[3 * index + 1] = c[1];
      this.color[3 * index + 2] = c[2];
    }

    /** Set one texture coordinate.
    * @param {number} index - Index of the vertex, in [0,vertexCount)
    * @param {number[]} t - Texture coordinate (u = t[0], v = t[1])
    */

  }, {
    key: 'setTex',
    value: function setTex(index, t) {
      this.tex[2 * index + 0] = t[0];
      this.tex[2 * index + 1] = t[1];
    }
  }]);

  return GeomData;
}();

exports.default = GeomData;

},{"./geomSceneObject":13}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PRINT_ERR = function PRINT_ERR(msg) {
  return console.error('[GeomOBJFile] ' + msg);
};
var PRINT_WARN = function PRINT_WARN(msg) {
  return console.log('[GeomOBJFile] ' + msg);
};

// ===================================
// GEOMETRY: OBJ FILE
// ===================================

/** Creates geometry from an OBJ file.
* A scaling and translation may be applied to the vertices so that the bounding
* box is centered at (0,0,0) and does not exceed maxDims in size.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
* @param {string} objFileContentsString - Contents of OBJ file as string
* @param {number[]} maxDims - (optional) Max allowed dims of bounding box as [x,y,z]
* @extends GeomData
*/

var GeomOBJFile = function (_GeomData) {
  _inherits(GeomOBJFile, _GeomData);

  function GeomOBJFile(objFileContentsString, maxDims) {
    _classCallCheck(this, GeomOBJFile);

    // Get raw OBJ data
    // ----------------------------------------------
    var _this = _possibleConstructorReturn(this, (GeomOBJFile.__proto__ || Object.getPrototypeOf(GeomOBJFile)).call(this));

    _this.objDataV = [];
    _this.objDataVN = [];
    _this.objDataT = [];
    _this.objDataF = []; // array of [{ vi, vt?, vn? }, ...]

    var lines = objFileContentsString.split('\n');

    lines.forEach(function (line) {
      // Split into non-whitespace chunks
      var explodedLine = line.match(/\S+/g);
      if (!explodedLine) {
        return;
      }

      switch (explodedLine[0]) {
        case 'v':
          _this.readV(explodedLine);break;
        case 'vt':
          _this.readVT(explodedLine);break;
        case 'vn':
          _this.readVN(explodedLine);break;
        case 'f':
          _this.readF(explodedLine);break;
      }
    });
    // ----------------------------------------------

    // Apply transformation for bounding box restriction
    // ----------------------------------------------
    var rangeX = [0, 0];
    var rangeY = [0, 0];
    var rangeZ = [0, 0];

    var bbFirst = false;
    _this.objDataV.forEach(function (v) {
      var vx = v[0];
      var vy = v[1];
      var vz = v[2];
      if (!bbFirst) {
        rangeX[0] = vx;rangeX[1] = vx;
        rangeY[0] = vy;rangeY[1] = vy;
        rangeZ[0] = vz;rangeZ[1] = vz;
      } else {
        if (vx < rangeX[0]) {
          rangeX[0] = vx;
        }
        if (vx > rangeX[1]) {
          rangeX[1] = vx;
        }
        if (vy < rangeY[0]) {
          rangeY[0] = vy;
        }
        if (vy > rangeY[1]) {
          rangeY[1] = vy;
        }
        if (vz < rangeZ[0]) {
          rangeZ[0] = vz;
        }
        if (vz > rangeZ[1]) {
          rangeZ[1] = vz;
        }
      }
      bbFirst = true;
    });

    var bbDims = [rangeX[1] - rangeX[0], rangeY[1] - rangeY[0], rangeZ[1] - rangeZ[0]];
    var bbCenter = [(rangeX[0] + rangeX[1]) * 0.5, (rangeY[0] + rangeY[1]) * 0.5, (rangeZ[0] + rangeZ[1]) * 0.5];

    var scale = 1;
    if (maxDims) {
      var scaleX = bbDims[0] > maxDims[0] ? maxDims[0] / bbDims[0] : 1;
      var scaleY = bbDims[1] > maxDims[1] ? maxDims[1] / bbDims[1] : 1;
      var scaleZ = bbDims[2] > maxDims[2] ? maxDims[2] / bbDims[2] : 1;
      scale = Math.min(scaleX, scaleY, scaleZ);
    }

    _this.objDataV = _this.objDataV.map(function (v) {
      return [(v[0] - bbCenter[0]) * scale, (v[1] - bbCenter[1]) * scale, (v[2] - bbCenter[2]) * scale];
    });
    // ----------------------------------------------

    // Convert raw OBJ data -> GeomData
    // ----------------------------------------------
    var vertexCount = 0;
    var triCount = 0;
    var edgeCount = 0;
    _this.objDataF.forEach(function (f) {
      var faceVertCount = f.length;
      vertexCount += faceVertCount;
      triCount += faceVertCount - 2;
      edgeCount += faceVertCount;
    });

    _this.initialize({
      vertexCount: vertexCount,
      elementCount: 3 * triCount,
      elementWireCount: 2 * edgeCount,
      hasNormals: true,
      hasTexture: false
    });

    var vk = 0;
    var fk = 0;
    var ek = 0;

    _this.objDataF.forEach(function (f) {
      var vkStart = vk;

      f.forEach(function (fVertData) {
        var vi = fVertData.vi,
            vt = fVertData.vt,
            vn = fVertData.vn;

        _this.setPosition(vk, _this.objDataV[vi - 1]);
        // this.setTex(vk, this.objDataT[vt - 1]);
        _this.setColor(vk, [1, 0.5, 0.5]);
        _this.setNormal(vk, _this.objDataVN[vn - 1]);
        vk++;
      });

      for (var i = 0; i < f.length - 2; i++) {
        var vkCur = vkStart + i + 1;
        _this.setFace(fk, 0, vkStart, vkCur, vkCur + 1);
        fk++;
      }

      for (var _i = 0; _i < f.length; _i++) {
        var j = (_i + 1) % f.length;
        _this.setLine(ek, 0, vkStart + _i, vkStart + j);
        ek++;
      }
    });
    // ----------------------------------------------
    return _this;
  }

  // Methods which populate objDataV, objDataVN, objDataN, objDataF
  // ================================================================
  // parse "v # # #" -> objDataV


  _createClass(GeomOBJFile, [{
    key: 'readV',
    value: function readV(explodedLine) {
      if (explodedLine.length < 4) {
        PRINT_ERR('Line "' + explodedLine.join(' ') + '" has too few numbers, skipping...');
        return;
      } else if (explodedLine.length > 4) {
        PRINT_WARN('Line "' + explodedLine.join(' ') + '" has too many numbers, ignoring extras...');
      }

      var p = [parseFloat(explodedLine[1]), parseFloat(explodedLine[2]), parseFloat(explodedLine[3])];

      this.objDataV.push(p);
    }

    // parse "vn # # #" -> objDataVN

  }, {
    key: 'readVN',
    value: function readVN(explodedLine) {
      if (explodedLine.length < 4) {
        PRINT_ERR('Line "' + explodedLine.join(' ') + '" has too few numbers, skipping...');
        return;
      } else if (explodedLine.length > 4) {
        PRINT_WARN('Line "' + explodedLine.join(' ') + '" has too many numbers, ignoring extras...');
      }

      var n = [parseFloat(explodedLine[1]), parseFloat(explodedLine[2]), parseFloat(explodedLine[3])];

      this.objDataVN.push(n);
    }

    // parse "vt # #" -> objDataT

  }, {
    key: 'readVT',
    value: function readVT(explodedLine) {
      if (explodedLine.length < 3) {
        PRINT_ERR('Line "' + explodedLine.join(' ') + '" has too few numbers, skipping...');
        return;
      } else if (explodedLine.length > 3) {
        PRINT_WARN('Line "' + explodedLine.join(' ') + '" has too many numbers, ignoring extras...');
      }

      var t = [parseFloat(explodedLine[1]), parseFloat(explodedLine[2])];

      this.objDataT.push(t);
    }

    // parse "f v ...", OR "f v/vt ...", OR "f v/vt/vn ...", OR "f v//vn ..."
    // push [{ vi, vt?, vn? }, ...] -> objDataF

  }, {
    key: 'readF',
    value: function readF(explodedLine) {
      if (explodedLine.length < 3) {
        PRINT_ERR('Line "' + explodedLine.join(' ') + '" has too few entries, skipping...');
        return;
      }

      var f = [];
      for (var j = 1, jl = explodedLine.length; j < jl; j++) {
        var data = explodedLine[j].split('/');

        var fVertData = {};
        fVertData.vi = parseInt(data[0], 10);
        if (data.length > 1) {
          fVertData.vt = parseInt(data[1], 10);
        }
        if (data.length > 2) {
          fVertData.vn = parseInt(data[2], 10);
        }

        f.push(fVertData);
      }

      this.objDataF.push(f);
    }
    // ================================================================

  }]);

  return GeomOBJFile;
}(_geomData2.default);

exports.default = GeomOBJFile;

},{"./geomData":10,"./mathVec":24}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// ===================================
// GEOMETRY: QUAD
// ===================================

/** Quad [-1,1]x[-1,1] with z=0.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @extends GeomData
*/
var GeomQuad = function (_GeomData) {
  _inherits(GeomQuad, _GeomData);

  function GeomQuad() {
    _classCallCheck(this, GeomQuad);

    var _this = _possibleConstructorReturn(this, (GeomQuad.__proto__ || Object.getPrototypeOf(GeomQuad)).call(this));

    _this.initialize({
      vertexCount: 4,
      elementCount: 6,
      elementWireCount: 8,
      hasNormals: true,
      hasTexture: true
    });

    _this.setPosition(0, [-1, -1, 0]);
    _this.setPosition(1, [1, -1, 0]);
    _this.setPosition(2, [1, 1, 0]);
    _this.setPosition(3, [-1, 1, 0]);

    _this.setNormal(0, [0, 0, 1]);
    _this.setNormal(1, [0, 0, 1]);
    _this.setNormal(2, [0, 0, 1]);
    _this.setNormal(3, [0, 0, 1]);

    _this.setTex(0, [0, 0]);
    _this.setTex(1, [1, 0]);
    _this.setTex(2, [1, 1]);
    _this.setTex(3, [0, 1]);

    _this.setFace(0, 0, 0, 1, 2);
    _this.setFace(1, 0, 0, 2, 3);

    _this.setLine(0, 0, 0, 1);
    _this.setLine(1, 0, 1, 2);
    _this.setLine(2, 0, 2, 3);
    _this.setLine(3, 0, 3, 0);
    return _this;
  }

  return GeomQuad;
}(_geomData2.default);

exports.default = GeomQuad;

},{"./geomData":10,"./mathVec":24}],13:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathObjTrans = require('./mathObjTrans');

var _mathObjTrans2 = _interopRequireDefault(_mathObjTrans);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// SCENE OBJECT
// geometry plus object transformation
// geometry can be shared between objects (i.e. clones)
// ===================================

/** Some geometry (array of {@link GeomData}) plus an {@link ObjectTransformation} and some rendering options.
*   <br> Objects which generate geometry typically have a "instance" method which creates a SceneObject.
*   <br> Geometry can be shared between {@link SceneObject}s (e.g. {@link SceneObject#clone}).
* @param {GeomData[]} geomArray - Object geometry
*
* @prop {ObjectTransformation} transformation - The transformation applied to the object
* @prop {boolean} solidVisible - Option to display solid geometry
* @prop {boolean} castsShadows - Option to cast shadows
* @prop {boolean} translucent - Option to display as translucent
* @prop {number} alpha - Alpha value if displaying as translucent
*
* @prop {number} emissiveCoefficient - Emissive coefficient for lighting, in [0,1]
* @prop {number[]} emissiveColor - Emissive color (as [r,g,b])
* @prop {number} diffuseCoefficient - Diffuse coefficient for lighting, in [0,1]
* @prop {number} specularCoefficient - Specular coefficient for lighting, in [0,1]
* @prop {number} specularExponent - Specular exponent for Phong lighting model
*
* @prop {boolean} wireframeVisible - Option to display wireframe geometry
* @prop {number[]} wireframeColor - Wireframe color (as [r,g,b,a], no premultiplied alpha)
*/
var SceneObject = function () {
  function SceneObject(geomArray) {
    _classCallCheck(this, SceneObject);

    // geometry (array of GeomData)
    this.geomArray = geomArray;

    this.transformation = new _mathObjTrans2.default();

    // rendering options
    this.solidVisible = true;
    this.castsShadows = true;
    this.translucent = false;
    this.alpha = 0.5;

    this.wireframeVisible = false;
    this.wireframeColor = [0, 0, 0, 0.25];

    this.emissiveCoefficient = 0;
    this.emissiveColor = [1, 1, 1];

    this.diffuseCoefficient = 1.0;
    this.specularCoefficient = 0.5;
    this.specularExponent = 10.0;
  }

  /** Create a new {@link SceneObject} with reference to the same geometry.
  * Does not duplicate any other properties.
  * @return {SceneObject}
  */


  _createClass(SceneObject, [{
    key: 'clone',
    value: function clone() {
      return new SceneObject(this.geomArray);
    }

    /** Set target's transformation as the parent of this object's transformation.
    * @param {SceneObject} target - The object to stick to
    * @see {@link ObjectTransformation}
    */

  }, {
    key: 'stickTo',
    value: function stickTo(target) {
      this.transformation.parent = target.transformation;
    }

    /** Make this object's transformation have no parent.
    * @see {@link ObjectTransformation}
    */

  }, {
    key: 'unstick',
    value: function unstick() {
      this.transformation.parent = null;
    }
  }]);

  return SceneObject;
}();

exports.default = SceneObject;

},{"./mathObjTrans":22}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

var _geomBundle = require('./geomBundle');

var _geomBundle2 = _interopRequireDefault(_geomBundle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TEMP_PQ = [0, 0, 0];

// icosahedron
/* eslint-disable */
var BALL_V = [[0, -0.525731, 0.850651], [0.850651, 0, 0.525731], [0.850651, 0, -0.525731], [-0.850651, 0, -0.525731], [-0.850651, 0, 0.525731], [-0.525731, 0.850651, 0], [0.525731, 0.850651, 0], [0.525731, -0.850651, 0], [-0.525731, -0.850651, 0], [0, -0.525731, -0.850651], [0, 0.525731, -0.850651], [0, 0.525731, 0.850651]];
var BALL_F = [[1, 2, 6], [1, 7, 2], [3, 4, 5], [4, 3, 8], [6, 5, 11], [5, 6, 10], [9, 10, 2], [10, 9, 3], [7, 8, 9], [8, 7, 0], [11, 0, 1], [0, 11, 4], [6, 2, 10], [1, 6, 11], [3, 5, 10], [5, 4, 11], [2, 7, 9], [7, 1, 0], [3, 9, 8], [4, 8, 0]];
var BALL_E = [[0, 11], [0, 4], [0, 1], [11, 4], [11, 1], [9, 10], [9, 2], [9, 3], [10, 2], [10, 3], [1, 2], [1, 6], [1, 7], [2, 6], [2, 7], [4, 3], [4, 5], [4, 8], [3, 5], [3, 8], [5, 6], [5, 10], [5, 11], [6, 10], [6, 11], [7, 8], [7, 0], [7, 9], [8, 0], [8, 9]];
/* eslint-enable */

var _createBallGeometry = function _createBallGeometry() {
  // initialize GeomData
  var gdata = new _geomData2.default();
  gdata.initialize({
    hasNormals: true,
    elementCount: 3 * BALL_F.length,
    elementWireCount: 2 * BALL_E.length,
    vertexCount: BALL_V.length
  });

  // put vertex attributes
  // --------------------
  for (var i = 0; i < BALL_V.length; i++) {
    var p = BALL_V[i];
    gdata.setPosition(i, p);
    gdata.setNormal(i, p);
    gdata.setColor(i, [1, 1, 1]);
  }

  // put faces
  // --------------------
  for (var _i = 0; _i < BALL_F.length; _i++) {
    var f = BALL_F[_i];
    gdata.setFace(_i, 0, f[0], f[1], f[2]);
  }

  // wireframe edges
  // --------------------
  for (var _i2 = 0; _i2 < BALL_E.length; _i2++) {
    var e = BALL_E[_i2];
    gdata.setLine(_i2, 0, e[0], e[1]);
  }

  return gdata;
};

// (initially oriented from [0,0,0] to [1,0,0] w/ radius=1)
var _createTubeGeometry = function _createTubeGeometry() {
  // quality
  var m = 6; // tube vertices

  // initialize GeomData
  var gdata = new _geomData2.default();
  gdata.initialize({
    hasNormals: true,
    elementCount: 3 * (2 * m),
    elementWireCount: 2 * (3 * m),
    vertexCount: 2 * m
  });

  // put vertex attributes
  // --------------------
  var putAttribs = function putAttribs(index, c, x, y, z, nx, ny, nz) {
    gdata.setPosition(index, [x, y, z]);
    gdata.setNormal(index, [nx, ny, nz]);
    gdata.setColor(index, c);
  };

  for (var i = 0; i < m; i++) {
    var t = 2 * Math.PI * (i / m);
    var ct = Math.cos(t);
    var st = Math.sin(t);

    putAttribs(i, [1, 1, 1], 0, -ct, st, 0, -ct, st);
    putAttribs(m + i, [1, 1, 1], 1, -ct, st, 0, -ct, st);
  }

  // put faces
  // --------------------
  var f0 = 0;
  for (var _i3 = 0; _i3 < m; _i3++) {
    var j = (_i3 + 1) % m;
    gdata.setFace(f0++, 0, _i3, _i3 + m, j + m);
    gdata.setFace(f0++, 0, _i3, j + m, j);
  }

  // wireframe edges
  // --------------------
  var e0 = 0;
  for (var _i4 = 0; _i4 < m; _i4++) {
    var _j = (_i4 + 1) % m;
    gdata.setLine(e0++, 0, _i4, _j);
    gdata.setLine(e0++, m, _i4, _j);
    gdata.setLine(e0++, 0, _i4, _i4 + m);
  }

  return gdata;
};

// ===================================
// GEOMETRY: SKELETON
// made of balls joined by tubes
// ===================================

/** A single "ball" in the skeleton.
* @typedef {Object} GeomSkeleton~ball
*
* @property {Object} vertex - Any object with "position" property: <code>{ position: [x,y,z] }</code>
*/

/** A single "tube" in the skeleton.
*   <br>The "vertex" objects are exactly those that work in the "vertex" property of {@link GeomSkeleton~ball}.
* @typedef {Object} GeomSkeleton~tube
*
* @property {Object[]} vertices - Two "vertex" objects, as [vi,vj]
*/

/** Creates a "ball and tube" geometry.
*   <br>Must call {@link GeomSkeleton#generate} after creation, or to update positions/colors.
*   <br>Use {@link GeomSkeleton#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomSkeleton~ball[]} opts.balls - Collection of balls in the skeleton
* @param {GeomSkeleton~tube[]} opts.tubes - Collection of tubes in the skeleton
* @param {number} opts.ballRadius - Radius of each ball
* @param {number} opts.tubeRadius - Radius of each tube
* @param {number[]} opts.ballColor - Color of each ball, as [r,g,b]
* @param {number[]} opts.tubeColor - Color of each tube, as [r,g,b]
*/

var GeomSkeleton = function () {
  function GeomSkeleton(opts) {
    _classCallCheck(this, GeomSkeleton);

    this.opts = opts;

    // these are created by _prepare
    this.ballInstances = null;
    this.tubeInstances = null;
    this.bundle = null; // geometry given by an underlying GeomBundle

    this._prepare();
  }

  // create GeomBundle and instances
  // -----------------------------------


  _createClass(GeomSkeleton, [{
    key: '_prepare',
    value: function _prepare() {
      // prepare ball instances
      this.ballInstances = [];
      for (var i = 0; i < this.opts.balls.length; i++) {
        var instance = {
          scale: [this.opts.ballRadius, this.opts.ballRadius, this.opts.ballRadius],
          hasOrientation: false,
          position: [0, 0, 0],
          color: this.opts.ballColor
        };
        this.ballInstances.push(instance);
      }

      // prepare tube instances
      this.tubeInstances = [];
      for (var _i5 = 0; _i5 < this.opts.tubes.length; _i5++) {
        var _instance = {
          scale: [1, this.opts.tubeRadius, this.opts.tubeRadius],
          hasOrientation: true,
          orientation: [1, 0, 0],
          position: [0, 0, 0],
          color: this.opts.tubeColor
        };
        this.tubeInstances.push(_instance);
      }

      // create GeomBundle
      var ballGroup = {
        geometry: _createBallGeometry(),
        instances: this.ballInstances
      };
      var tubeGroup = {
        geometry: _createTubeGeometry(),
        instances: this.tubeInstances
      };
      var optsBundle = { groups: [ballGroup, tubeGroup] };
      this.bundle = new _geomBundle2.default(optsBundle);
    }

    // call at least once, or to update skeleton
    // ===================================
    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      // update ball instances
      for (var i = 0; i < this.opts.balls.length; i++) {
        var ball = this.opts.balls[i];
        var instance = this.ballInstances[i];
        _mathVec.Vec3.copy(instance.position, ball.vertex.position);
      }

      // update tube instances
      for (var _i6 = 0; _i6 < this.opts.tubes.length; _i6++) {
        var tube = this.opts.tubes[_i6];
        var _instance2 = this.tubeInstances[_i6];

        var p = tube.vertices[0].position;
        var q = tube.vertices[1].position;
        _mathVec.Vec3.subtract(TEMP_PQ, q, p);

        _instance2.scale[0] = _mathVec.Vec3.norm(TEMP_PQ);
        _mathVec.Vec3.copy(_instance2.position, p);
        _mathVec.Vec3.copy(_instance2.orientation, TEMP_PQ);
      }

      this.bundle.generate();
    }

    /** Create a new {@link SceneObject} with this geometry.
    * @return {SceneObject}
    */

  }, {
    key: 'instance',
    value: function instance() {
      return this.bundle.instance();
    }
  }]);

  return GeomSkeleton;
}();

exports.default = GeomSkeleton;

},{"./geomBundle":8,"./geomData":10,"./mathVec":24}],15:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TEMPVEC3A = [0, 0, 0];
var TEMPVEC3B = [0, 0, 0];

var rangeLerp = function rangeLerp(target, range, lerp) {
  target[0] = range.u[0] + lerp[0] * (range.u[1] - range.u[0]);
  target[1] = range.v[0] + lerp[1] * (range.v[1] - range.v[0]);
};

// const rangeUnLerp = (target, range, value) => {
//   target[0] = (value[0] - range.u[0]) / (range.u[1] - range.u[0]);
//   target[1] = (value[1] - range.v[0]) / (range.v[1] - range.v[0]);
// };

// for each i = 0...n, lerp along "range" at t = i/n
// for each number in "values", find which of these lerped values is closest
// corresponding indices get marked "true" in the returned array, otherwise false
// if "loop" is true, the index i=n is moved to i=0
var convertValuesToFilter = function convertValuesToFilter(range, n, values, loop) {
  var ret = [];

  // initially all false
  for (var k = 0; k <= n; k++) {
    ret[k] = false;
  }

  // convert values to indices
  for (var i = 0; i < values.length; i++) {
    // find index
    var t = (values[i] - range[0]) / (range[1] - range[0]);
    var _k = Math.round(n * t);
    // clamp
    if (_k < 0) {
      _k = 0;
    }
    if (_k > n) {
      _k = n;
    }
    if (loop && _k === n) {
      _k = 0;
    }

    ret[_k] = true;
  }

  return ret;
};

// ===================================
// GEOMETRY: SURFACE
// parametric surface (f:R^2 -> R^3)
// ===================================

/** Parametric surface.
*   <br>Must call {@link GeomSurface#generate} after creation, or to update position/color values.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {Object} opts.rangeIn - Range of u and v values: <code>{u:[min,max], v:[min,max]}</code>
* @param {number[]} opts.n - Number of samples along u and v direction: [nu, nv]
* @param {Function} opts.f - Position function: <code>function(u,v){ return [x,y,z]; }</code>
* @param {Function} opts.color - Color function: <code>function(u,v,[x,y,z]){ return [r,g,b]; }</code>
* @param {boolean} opts.loopU - Set to true if f(umin,v) = f(umax,v)
* @param {boolean} opts.loopV - Set to true if f(u,vmin) = f(u,vmax)
*
* @extends GeomData
*/

var GeomSurface = function (_GeomData) {
  _inherits(GeomSurface, _GeomData);

  function GeomSurface(opts) {
    _classCallCheck(this, GeomSurface);

    var _this = _possibleConstructorReturn(this, (GeomSurface.__proto__ || Object.getPrototypeOf(GeomSurface)).call(this));

    _this.opts = opts;

    // create this._gridVerts/_gridFaces arrays and GeomData elements
    _this._createGrid();
    return _this;
  }

  // generate grid geometry
  // -----------------------------------
  // create gridVertex


  _createClass(GeomSurface, [{
    key: '_createVertex',
    value: function _createVertex(lerp) {
      // create and add gridVertex object
      var gridVertex = {
        pIn: [0, 0],
        position: [0, 0, 0], // set by "generate"
        normal: [0, 0, 0] // set by "generate"
      };
      this._gridVerts.push(gridVertex);

      // set pIn
      rangeLerp(gridVertex.pIn, this.opts.rangeIn, lerp);
    }

    // add face to GeomData; create gridFace and update cross references
    // i,j,k are indices of grid vertices, in CCW order

  }, {
    key: '_createFace',
    value: function _createFace(i, j, k) {
      var faceIndex = this._gridFaces.length;

      // create and add gridFace object
      var gridFace = {
        incidentVerts: [this._gridVerts[i], this._gridVerts[j], this._gridVerts[k]]
      };
      this._gridFaces.push(gridFace);

      // add to GeomData
      this.setFace(faceIndex, 0, i, j, k);
    }

    // creates _gridVerts, _gridFaces; initializes geometry and its faces

  }, {
    key: '_createGrid',
    value: function _createGrid() {
      var nu = this.opts.n[0];
      var nv = this.opts.n[1];

      // initialize GeomData
      var wireLinesU = (nu + 1) * nv;
      var wireLinesV = nu * (nv + 1);
      this.initialize({
        hasNormals: true,
        vertexCount: (nu + 1) * (nv + 1),
        elementCount: 3 * (2 * nu * nv),
        elementWireCount: 2 * (wireLinesU + wireLinesV)
      });

      // prepare grid vertices
      this._gridVerts = [];

      var lerp = [0, 0];
      for (var iv = 0; iv <= nv; iv++) {
        for (var iu = 0; iu <= nu; iu++) {
          // lerp = point in input grid [0,1]^2
          lerp[0] = iu / nu;
          lerp[1] = iv / nv;
          this._createVertex(lerp);
        }
      }
      this._createGlue();

      // prepare grid faces, GeomData elements
      this._gridFaces = [];

      for (var _iv = 0; _iv < nv; _iv++) {
        for (var _iu = 0; _iu < nu; _iu++) {
          // grid indices at 4 corners of grid square
          // (vertex indices are in order of creation in loop above)
          var i00 = _iv * (nu + 1) + _iu;
          var i10 = i00 + 1;
          var i01 = i00 + (nu + 1);
          var i11 = i01 + 1;

          // make 2 tris for grid square, with alternating diagonal direction
          if ((_iu + _iv) % 2 === 0) {
            this._createFace(i00, i10, i11);
            this._createFace(i00, i11, i01);
          } else {
            this._createFace(i10, i01, i00);
            this._createFace(i10, i11, i01);
          }
        }
      }

      // prepare wireframe edges
      var edgeIndex = 0;
      for (var _iv2 = 0; _iv2 <= nv; _iv2++) {
        for (var _iu2 = 0; _iu2 < nu; _iu2++) {
          var _i = _iv2 * (nu + 1) + _iu2;
          var _i2 = _i + 1;
          this.setLine(edgeIndex++, 0, _i, _i2);
        }
      }
      for (var _iv3 = 0; _iv3 < nv; _iv3++) {
        for (var _iu3 = 0; _iu3 <= nu; _iu3++) {
          var _i3 = _iv3 * (nu + 1) + _iu3;
          var _i4 = _i3 + (nu + 1);
          this.setLine(edgeIndex++, 0, _i3, _i4);
        }
      }
    }

    // TODO quick fix for normals when surface loops in u or v direction
    // -----------------------------------

  }, {
    key: '_createGlue',
    value: function _createGlue() {
      var nu = this.opts.n[0];
      var nv = this.opts.n[1];

      // pairs of vertices that are optionally "glued" together
      this._glueU = [];
      for (var iv = 0; iv <= nv; iv++) {
        var uMin = this._gridVerts[iv * (nu + 1)];
        var uMax = this._gridVerts[iv * (nu + 1) + nu];
        this._glueU.push([uMin, uMax]);
      }

      this._glueV = [];
      for (var iu = 0; iu <= nu; iu++) {
        var vMin = this._gridVerts[iu];
        var vMax = this._gridVerts[nv * (nu + 1) + iu];
        this._glueV.push([vMin, vMax]);
      }
    }
  }, {
    key: '_glue',
    value: function _glue() {
      // need to add both U and V pairs before normalizing either: if
      // loopU and loopV are both true, one instance of 4 verts glued

      // add the normals of glued vertices
      if (this.opts.loopU) {
        for (var i = 0; i < this._glueU.length; i++) {
          var pn = this._glueU[i][0].normal;
          var qn = this._glueU[i][1].normal;
          _mathVec.Vec3.add(pn, pn, qn);
          _mathVec.Vec3.copy(qn, pn);
        }
      }

      if (this.opts.loopV) {
        for (var _i5 = 0; _i5 < this._glueV.length; _i5++) {
          var _pn = this._glueV[_i5][0].normal;
          var _qn = this._glueV[_i5][1].normal;
          _mathVec.Vec3.add(_pn, _pn, _qn);
          _mathVec.Vec3.copy(_qn, _pn);
        }
      }

      // normalize after any adding
      if (this.opts.loopU) {
        for (var _i6 = 0; _i6 < this._glueU.length; _i6++) {
          var _pn2 = this._glueU[_i6][0].normal;
          var _qn2 = this._glueU[_i6][1].normal;
          _mathVec.Vec3.normalize(_pn2, _pn2);
          _mathVec.Vec3.normalize(_qn2, _qn2);
        }
      }

      if (this.opts.loopV) {
        for (var _i7 = 0; _i7 < this._glueV.length; _i7++) {
          var _pn3 = this._glueV[_i7][0].normal;
          var _qn3 = this._glueV[_i7][1].normal;
          _mathVec.Vec3.normalize(_pn3, _pn3);
          _mathVec.Vec3.normalize(_qn3, _qn3);
        }
      }
    }

    // generate geometry
    // updates data contained in geometry, _gridVerts, _gridFaces, rangeOut
    // ===================================

    // compute normal at grid face using gridVertex.position data
    // add the normal to each incident vertex's normal

  }, {
    key: '_computeFaceNormal',
    value: function _computeFaceNormal(gridFace) {
      // pqr = face's grid vertices in CCW order
      var p = gridFace.incidentVerts[0];
      var q = gridFace.incidentVerts[1];
      var r = gridFace.incidentVerts[2];

      // compute n = (q-p) X (r-p)
      _mathVec.Vec3.subtract(TEMPVEC3A, q.position, p.position);
      _mathVec.Vec3.subtract(TEMPVEC3B, r.position, p.position);
      _mathVec.Vec3.cross(TEMPVEC3A, TEMPVEC3A, TEMPVEC3B);
      // normalize
      _mathVec.Vec3.normalize(TEMPVEC3A, TEMPVEC3A);

      // add to incident vertices' normals
      _mathVec.Vec3.add(p.normal, p.normal, TEMPVEC3A);
      _mathVec.Vec3.add(q.normal, q.normal, TEMPVEC3A);
      _mathVec.Vec3.add(r.normal, r.normal, TEMPVEC3A);
    }

    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      // compute function and color on each grid vertex
      for (var i = 0; i < this._gridVerts.length; i++) {
        var gridVertex = this._gridVerts[i];

        var pIn = gridVertex.pIn;

        _mathVec.Vec3.copy(gridVertex.position, this.opts.f(pIn[0], pIn[1]));
        var c = this.opts.color(pIn[0], pIn[1], gridVertex.position);

        // update GeomData
        this.setPosition(i, gridVertex.position);
        this.setColor(i, c);
      }

      // zero the vertex normals
      for (var _i8 = 0; _i8 < this._gridVerts.length; _i8++) {
        _mathVec.Vec3.zero(this._gridVerts[_i8].normal);
      }

      // for each face, add its normal to its incident vertices' normals
      for (var _i9 = 0; _i9 < this._gridFaces.length; _i9++) {
        this._computeFaceNormal(this._gridFaces[_i9]);
      }
      this._glue();

      // update GeomData normals
      for (var _i10 = 0; _i10 < this._gridVerts.length; _i10++) {
        var _gridVertex = this._gridVerts[_i10];

        _mathVec.Vec3.normalize(_gridVertex.normal, _gridVertex.normal);
        this.setNormal(_i10, _gridVertex.normal);
      }
    }

    // TODO: the following is just for testing, probably remove

    // get skeleton
    // ===================================

    // gives input value to GeomSkeleton

  }, {
    key: 'getSlices',
    value: function getSlices(uValues, vValues) {
      var nu = this.opts.n[0];
      var nv = this.opts.n[1];

      var rangeU = this.opts.rangeIn.u;
      var rangeV = this.opts.rangeIn.v;

      // ixVisible[i] = true iff corresponding value shows up in xValues
      var iuVisible = convertValuesToFilter(rangeU, nu, uValues, this.opts.loopU);
      var ivVisible = convertValuesToFilter(rangeV, nv, vValues, this.opts.loopV);

      // find balls
      var balls = [];

      var gvi = 0;
      for (var iv = 0; iv <= nv; iv++) {
        for (var iu = 0; iu <= nu; iu++) {
          if (iuVisible[iu] || ivVisible[iv]) {
            balls.push({ vertex: this._gridVerts[gvi] });
          }
          gvi++;
        }
      }

      // find tubes
      var tubes = [];

      for (var _iu4 = 0; _iu4 <= nu; _iu4++) {
        if (!iuVisible[_iu4]) {
          continue;
        }

        for (var _iv4 = 0; _iv4 < nv; _iv4++) {
          var index = _iv4 * (nu + 1) + _iu4;
          var p = this._gridVerts[index];
          var q = this._gridVerts[index + (nu + 1)];
          tubes.push({ vertices: [p, q] });
        }
      }

      for (var _iv5 = 0; _iv5 <= nv; _iv5++) {
        if (!ivVisible[_iv5]) {
          continue;
        }

        for (var _iu5 = 0; _iu5 < nu; _iu5++) {
          var _index = _iv5 * (nu + 1) + _iu5;
          var _p = this._gridVerts[_index];
          var _q = this._gridVerts[_index + 1];
          tubes.push({ vertices: [_p, _q] });
        }
      }

      // done
      return { balls: balls, tubes: tubes };
    }
  }]);

  return GeomSurface;
}(_geomData2.default);

exports.default = GeomSurface;

},{"./geomData":10,"./mathVec":24}],16:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

var _glfont = require('../font/glfont');

var _glfont2 = _interopRequireDefault(_glfont);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// TODO

// ===================================
// GEOMETRY: TEXT
// ===================================

/** Determines the metrics for each character in a font.  All return value units are in terms of font texture pixels.
* @typedef {Object} GeomText~fontMetrics
*
* @prop {string} fontName - String description of font
* @prop {number} spaceWidth - Returns space char width
* @prop {number} lineHeight - Vertical offset for newline
* @prop {function} getTexPos - (k) => [texPosX, texPosY]; ASCII value to pos in texture
* @prop {function} getSize - (k) => [width, height]; ASCII value to char size
* @prop {function} getOffset - (k) => [offsetX, offsetY]; ASCII value to placement offset
*/

/** Text geometry.
*   <br>Must call {@link GeomText#generate} after creation, or to update string.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomText~fontMetrics} opts.fontMetrics - Font metrics that specify how the geometry is generated
* @param {number} opts.maxlen - Maximum number of visible characters to display
* @param {string} opts.text - Initial string to display
* @param {number[]} opts.tessellation - (Default: [1,1]) Each char is split into a t[0]xt[1] grid, where t = tessellation
* @param {Function} opts.pos - Position function: <code>function(u,v){ return [x,y,z]; }</code>
*   <br>The input (u,v) has units of font texture pixels
*
* @extends GeomData
*/
var GeomText = function (_GeomData) {
  _inherits(GeomText, _GeomData);

  function GeomText(opts) {
    _classCallCheck(this, GeomText);

    var _this = _possibleConstructorReturn(this, (GeomText.__proto__ || Object.getPrototypeOf(GeomText)).call(this));

    _this.opts = opts;

    // defaults
    if (typeof _this.opts.tessellation === 'undefined') {
      _this.opts.tessellation = [1, 1];
    }

    // create this._gridChars array and GeomData elements
    _this._createGrid();
    return _this;
  }

  // add char to GeomData; return


  _createClass(GeomText, [{
    key: '_createChar',
    value: function _createChar(i) {
      // var tx = this.opts.tessellation[0], ty = this.opts.tessellation[1];

      // var indexV = 0;
      // var indexL = 0;
      // var indexF = 0;

      // //faces
      // for(var j=0; j<ty; j++){
      // for(var i=0; i<tx; i++){
      //   var i00=indexV*4, i10=i00+1, i01=i00+2, i11=i00+3;

      //   this.setFace(2*indexF  , 0, i00,i10,i11);
      //   this.setFace(2*indexF+1, 0, i00,i11,i01);
      //   indexF += 2;
      // }}

      var i00 = i * 4;
      var i10 = i00 + 1;
      var i01 = i00 + 2;
      var i11 = i00 + 3;

      this.setFace(2 * i + 0, 0, i00, i10, i11);
      this.setFace(2 * i + 1, 0, i00, i11, i01);

      this.setLine(4 * i + 0, 0, i00, i10);
      this.setLine(4 * i + 1, 0, i10, i11);
      this.setLine(4 * i + 2, 0, i11, i01);
      this.setLine(4 * i + 3, 0, i01, i00);

      return { i00: i00, i10: i10, i01: i01, i11: i11 };
    }

    // creates _gridChars; initializes geometry

  }, {
    key: '_createGrid',
    value: function _createGrid() {
      var n = this.opts.maxlen;

      // initialize GeomData
      var tx = this.opts.tessellation[0];
      var ty = this.opts.tessellation[1];

      var vertsPerChar = (tx + 1) * (ty + 1);
      var elementsPerChar = 3 * (tx * ty * 2);

      var wireLinesH = (tx + 1) * ty;
      var wireLinesV = tx * (ty + 1);
      var elementsPerCharWire = 2 * (wireLinesH + wireLinesV);

      this.initialize({
        hasNormals: true,
        hasTexture: true,
        vertexCount: n * vertsPerChar,
        elementCount: n * elementsPerChar,
        elementWireCount: n * elementsPerCharWire
      });

      // prepare grid chars
      this._gridChars = [];
      for (var i = 0; i < n; i++) {
        var newchar = this._createChar(i);
        this._gridChars.push(newchar);
      }
    }

    // generate geometry
    // ===================================
    // TODO
    // var tempVec3a = [0,0,0];
    // var tempVec3b = [0,0,0];

    // //compute normal at grid face using gridVertex.position data
    // //add the normal to each incident vertex's normal
    // _computeFaceNormal(gridFace){
    //   //pqr = face's grid vertices in CCW order
    //   var p = gridFace.incidentVerts[0];
    //   var q = gridFace.incidentVerts[1];
    //   var r = gridFace.incidentVerts[2];

    //   //compute n = (q-p) X (r-p)
    //   Vec3.subtract(tempVec3a, q.position, p.position);
    //   Vec3.subtract(tempVec3b, r.position, p.position);
    //   Vec3.cross(tempVec3a, tempVec3a, tempVec3b);
    //   //normalize
    //   Vec3.normalize(tempVec3a, tempVec3a);

    //   //add to incident vertices' normals
    //   Vec3.add(p.normal, p.normal, tempVec3a);
    //   Vec3.add(q.normal, q.normal, tempVec3a);
    //   Vec3.add(r.normal, r.normal, tempVec3a);
    // };

    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      var _this2 = this;

      var text = this.opts.text;

      // get number of used and unused chars

      var nvisible = text.length;
      if (nvisible > this._gridChars.length) {
        nvisible = this._gridChars.length;
      }
      // const nleftover = this._gridChars.length - nvisible;

      var normal = [1, 0, 0];

      // set attributes for visible chars
      // --------------------------------------
      var fm = this.opts.fontMetrics || _glfont2.default;

      // TODO font texture size
      var fontTexW = 1024;var fontTexWInv = 1 / fontTexW;
      var fontTexH = 1024;var fontTexHInv = 1 / fontTexH;

      // running position (font texture pixel units)
      var u = 0;var v = 0;
      var uv00 = [u, v];var uv01 = [u, v];
      var uv10 = [u, v];var uv11 = [u, v];

      // use this.setTex, this.setNormal, this.setPosition
      var putVisibleChar = function putVisibleChar(texPos, charSize, texOffset, gc) {
        var cw = charSize[0];
        var ch = charSize[1];
        var uoff = u + texOffset[0];
        var voff = v - texOffset[1];

        uv00[0] = uoff;uv00[1] = voff;
        uv01[0] = uoff;uv01[1] = voff + ch;
        uv10[0] = uoff + cw;uv10[1] = voff;
        uv11[0] = uoff + cw;uv11[1] = voff + ch;
        _this2.setPosition(gc.i00, _this2.opts.pos(uv00[0], uv00[1]));
        _this2.setPosition(gc.i01, _this2.opts.pos(uv01[0], uv01[1]));
        _this2.setPosition(gc.i10, _this2.opts.pos(uv10[0], uv10[1]));
        _this2.setPosition(gc.i11, _this2.opts.pos(uv11[0], uv11[1]));
        u = uoff + cw;

        var texX0 = texPos[0];var texX1 = texX0 + charSize[0];
        var texY0 = texPos[1];var texY1 = texY0 - charSize[1];
        _this2.setTex(gc.i00, [texX0 * fontTexWInv, texY0 * fontTexHInv]);
        _this2.setTex(gc.i01, [texX0 * fontTexWInv, texY1 * fontTexHInv]);
        _this2.setTex(gc.i10, [texX1 * fontTexWInv, texY0 * fontTexHInv]);
        _this2.setTex(gc.i11, [texX1 * fontTexWInv, texY1 * fontTexHInv]);

        _this2.setNormal(gc.i00, normal);
        _this2.setNormal(gc.i01, normal);
        _this2.setNormal(gc.i10, normal);
        _this2.setNormal(gc.i11, normal);
      };

      // TODO
      // const putInvalidChar = () => { };

      var spaceASCII = 32;
      for (var i = 0; i < text.length; i++) {
        var gc = this._gridChars[i];

        var k = text.charCodeAt(i);
        if (k === spaceASCII) {
          var texPos = [0, 1];
          var texSize = [fm.spaceWidth, 0];
          var offset = [0, 0];
          putVisibleChar(texPos, texSize, offset, gc);
        } else {
          var _texPos = fm.getTexPos(k);
          var _texSize = fm.getSize(k);
          var _offset = fm.getOffset(k);
          putVisibleChar(_texPos, _texSize, _offset, gc);
        }
      }
      // --------------------------------------

      // set attributes for invisible chars
      // TODO
    }
  }]);

  return GeomText;
}(_geomData2.default);

exports.default = GeomText;

},{"../font/glfont":5,"./geomData":10}],17:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _geomData = require('./geomData');

var _geomData2 = _interopRequireDefault(_geomData);

var _geomBundle = require('./geomBundle');

var _geomBundle2 = _interopRequireDefault(_geomBundle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var COLOR_BASE = [0, 0, 0];
var COLOR_VECT = [1, 1, 1];

// ===================================
// GEOMETRY: VECTORS
// collection of 3d vectors
// ===================================

/** A single vector in the collection
* @typedef {Object} GeomVectors~vector
*
* @property {number[]} position - Position of vector, as [x,y,z]
* @property {number[]} normal - Direction of vector, as [x,y,z] (need not be normalized)
*/

/** Creates geometry consisting of a collection of vectors.
*   <br>Must call {@link GeomVectors#generate} after creation, or to update vector properties.
*   <br>Use {@link GeomVectors#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomVectors~vector[]} opts.vectors - Collection of vectors
* @param {number} opts.radius - Radius of vector
* @param {number} opts.size - Length of each vector
* @param {number[]} opts.color - Color of each vector
*/

var GeomVectors = function () {
  function GeomVectors(opts) {
    _classCallCheck(this, GeomVectors);

    this.opts = opts;

    // these are created by _prepare
    this.instances = null;
    this.bundle = null; // geometry given by an underlying GeomBundle

    this._prepare();
  }

  // return a GeomData representing one vector
  // (initially oriented as [1,0,0], with radius=length=1)
  // -----------------------------------


  _createClass(GeomVectors, [{
    key: '_prepare',


    // create GeomBundle and instances
    // -----------------------------------
    value: function _prepare() {
      // prepare instances
      this.instances = [];
      for (var i = 0; i < this.opts.vectors.length; i++) {
        var instance = {
          scale: [this.opts.size, this.opts.radius, this.opts.radius],
          hasOrientation: true,
          orientation: [1, 0, 0],
          position: [0, 0, 0],
          color: this.opts.color
        };
        this.instances.push(instance);
      }

      // create GeomBundle
      var group = {
        geometry: GeomVectors._createGeometry(),
        instances: this.instances
      };
      var optsBundle = { groups: [group] };
      this.bundle = new _geomBundle2.default(optsBundle);
    }

    // call at least once, or to update vectors
    // ===================================
    /** Recompute vertex attributes: position, color, and normal. */

  }, {
    key: 'generate',
    value: function generate() {
      // update instances
      for (var i = 0; i < this.opts.vectors.length; i++) {
        var vector = this.opts.vectors[i];
        var instance = this.instances[i];

        _mathVec.Vec3.copy(instance.position, vector.position);
        _mathVec.Vec3.copy(instance.orientation, vector.normal);
      }

      this.bundle.generate();
    }

    /** Create a new {@link SceneObject} with this geometry.
    * @return {SceneObject}
    */

  }, {
    key: 'instance',
    value: function instance() {
      return this.bundle.instance();
    }
  }], [{
    key: '_createGeometry',
    value: function _createGeometry() {
      // set quality and proportions
      var m = 4; // tube vertices
      var n = 6; // head vertices

      var rTube = 0.333;var rHead = 1;
      var lTube = 0.5;var lHead = 0.5;
      var dInvHead = 1 / Math.sqrt(rHead * rHead + lHead * lHead);

      // initialize GeomData
      var gdata = new _geomData2.default();
      gdata.initialize({
        hasNormals: true,
        elementCount: 3 * (3 * m + 2 * n + -4),
        elementWireCount: 2 * (3 * m + 2 * n),
        vertexCount: 3 * m + 3 * n
      });

      // put vertex attributes
      // --------------------
      var putAttribs = function putAttribs(index, c, x, y, z, nx, ny, nz) {
        gdata.setPosition(index, [x, y, z]);
        gdata.setNormal(index, [nx, ny, nz]);
        gdata.setColor(index, c);
      };

      // vertices: stick part (base + tube)
      for (var i = 0; i < m; i++) {
        var t = 2 * Math.PI * (i / m);
        var ct = Math.cos(t);
        var st = Math.sin(t);

        // base
        putAttribs(i, COLOR_BASE, 0, -rTube * ct, rTube * st, -1, 0, 0);

        // tube
        putAttribs(m + i, COLOR_VECT, 0, -rTube * ct, rTube * st, 0, -ct, st);
        putAttribs(2 * m + i, COLOR_VECT, lTube, -rTube * ct, rTube * st, 0, -ct, st);
      }

      // vertices: head (base + cone)
      for (var _i = 0; _i < n; _i++) {
        var _t = 2 * Math.PI * (_i / n);
        var _ct = Math.cos(_t);
        var _st = Math.sin(_t);

        // base
        putAttribs(3 * m + _i, COLOR_BASE, lTube, -rHead * _ct, rHead * _st, -1, 0, 0);

        // cone
        var th = 2 * Math.PI * ((_i + 0.5) / n);
        var cth = Math.cos(th);
        var sth = Math.sin(th);

        putAttribs(3 * m + n + _i, COLOR_VECT, lTube, -rHead * _ct, rHead * _st, dInvHead * rHead, -dInvHead * lHead * _ct, dInvHead * lHead * _st);
        putAttribs(3 * m + 2 * n + _i, COLOR_VECT, lTube + lHead, 0, 0, dInvHead * rHead, -dInvHead * lHead * cth, dInvHead * lHead * sth);
      }

      // put faces
      // --------------------
      var f0 = 0;

      // base
      for (var _i2 = 1; _i2 < m - 1; _i2++) {
        gdata.setFace(f0++, 0, 0, _i2, _i2 + 1);
      }

      // tube
      for (var _i3 = 0; _i3 < m; _i3++) {
        var j = (_i3 + 1) % m;
        gdata.setFace(f0++, m, _i3, _i3 + m, j + m);
        gdata.setFace(f0++, m, _i3, j + m, j);
      }

      // head base
      for (var _i4 = 1; _i4 < n - 1; _i4++) {
        gdata.setFace(f0++, 3 * m, 0, _i4, _i4 + 1);
      }

      // head
      for (var _i5 = 0; _i5 < n; _i5++) {
        var _j = (_i5 + 1) % n;
        gdata.setFace(f0++, 3 * m + n, _i5, _i5 + n, _j);
      }

      // wireframe edges
      // --------------------
      var e0 = 0;

      // tube
      for (var _i6 = 0; _i6 < m; _i6++) {
        var _j2 = (_i6 + 1) % m;
        gdata.setLine(e0++, m, _i6, _j2);
        gdata.setLine(e0++, m, _i6, _i6 + m);
        gdata.setLine(e0++, 2 * m, _i6, _j2);
      }

      // head
      for (var _i7 = 0; _i7 < n; _i7++) {
        var _j3 = (_i7 + 1) % n;
        gdata.setLine(e0++, 3 * m + n, _i7, _j3);
        gdata.setLine(e0++, 3 * m + n, _i7, _i7 + n);
      }

      return gdata;
    }
  }]);

  return GeomVectors;
}();

exports.default = GeomVectors;

},{"./geomBundle":8,"./geomData":10,"./mathVec":24}],18:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mathCam = require('./mathCam');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// LIGHT
// ===================================

/** Represents a light source.
*
* @prop {boolean} shadows - True iff the light should cast shadows
* @prop {Camera} camera - Specifies the light position, direction, etc.
* @prop {number[]} color - Color of the light (as [r,g,b])
* @prop {number[]} attenuation - Intensity varies with the distance "d" from the light:
*   <br>If attenuation = [a,b,c], intensity is computed as 1/(a + b*d + c*d^2)
*/
var Light = function Light() {
  _classCallCheck(this, Light);

  this.shadows = true;
  this.camera = new _mathCam.Camera(90.0, 1.0, 0.1, 1000.0); // TODO
  this.color = [1.0, 1.0, 1.0];
  this.attenuation = [1.0, 0.02, 0.0];
};

exports.default = Light;

},{"./mathCam":19}],19:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Camera = exports.CameraRotation = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _mathMat = require('./mathMat');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TEMPVEC3 = [0, 0, 0];

// ===================================
// CAMERA ROTATION
// ===================================

/** Rotation used in a {@link Camera}.
*   <br>Has a fixed upward direction to prevent "roll" while changing "yaw" and "pitch".
*   <br>Don't modify properties directly--use the methods.
*
* @prop {number[]} forward - Direction camera is pointing
* @prop {number[]} fixedUp - Upward direction, independent of camera orientation
* @prop {number[]} side - Sideways direction (= forward X fixedUp)
* @prop {number[]} tiltedUp - Upward direction (= side X forward)
* @prop {Mat3} matrix - Rotation matrix; maps basis [side, tiltedUp, -forward] to standard basis
* @prop {Mat3} matrixInverse - Inverse of rotation matrix
*/

var CameraRotation = function () {
  function CameraRotation() {
    _classCallCheck(this, CameraRotation);

    this.forward = [0, 0, -1];
    this.fixedUp = [0, 1, 0];

    // derived quantities
    this.side = [1, 0, 0];
    this.tiltedUp = [0, 1, 0];
    this.matrix = _mathMat.Mat3.create();
    this.matrixInverse = _mathMat.Mat3.create();

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------


  _createClass(CameraRotation, [{
    key: '_recompute',
    value: function _recompute() {
      this._computeOrthoBasis();
      this._computeMatrix();
    }
  }, {
    key: '_computeOrthoBasis',
    value: function _computeOrthoBasis() {
      // compute side and tiltedUp
      _mathVec.Vec3.cross(this.side, this.forward, this.fixedUp);
      _mathVec.Vec3.normalize(this.side, this.side);
      _mathVec.Vec3.cross(this.tiltedUp, this.side, this.forward);
    }
  }, {
    key: '_computeMatrix',
    value: function _computeMatrix() {
      // change of basis [s, u, -f] -> [e1 e2 e3] (u = tiltedUp)
      // this is the matrix with rows s, u, -f
      // it is orthogonal so inverse is matrix with columns s, u, -f.

      _mathVec.Vec3.flip(TEMPVEC3, this.forward);
      _mathMat.Mat3.setRows(this.matrix, this.side, this.tiltedUp, TEMPVEC3);
      _mathMat.Mat3.setColumns(this.matrixInverse, this.side, this.tiltedUp, TEMPVEC3);
    }

    // displacement
    // -----------------------------------
    /** Set target = forward displacement (i.e., along this.forward).
    * @param {number[]} target - The vector which will store the result
    * @param {number} d - Displacement distance
    * @see {@link Camera#moveForward}
    */

  }, {
    key: 'displacementForward',
    value: function displacementForward(target, d) {
      _mathVec.Vec3.scale(target, this.forward, d);
    }

    /** Set target = sideways displacement (i.e., along this.side).
    * @param {number[]} target - The vector which will store the result
    * @param {number} d - Displacement distance
    * @see {@link Camera#moveSideways}
    */

  }, {
    key: 'displacementSideways',
    value: function displacementSideways(target, d) {
      _mathVec.Vec3.scale(target, this.side, d);
    }

    /** Set target = upward displacement (i.e., along this.fixedUp).
    * @param {number[]} target - The vector which will store the result
    * @param {number} d - Displacement distance
    * @see {@link Camera#moveUp}
    */

  }, {
    key: 'displacementUp',
    value: function displacementUp(target, d) {
      _mathVec.Vec3.scale(target, this.fixedUp, d);
    }

    /** Set target = forward displacement, perpendicular to fixedUp (i.e., along "ground").
    * @param {number[]} target - The vector which will store the result
    * @param {number} d - Displacement distance
    * @see {@link Camera#moveForwardPlanar}
    */

  }, {
    key: 'displacementForwardPlanar',
    value: function displacementForwardPlanar(target, d) {
      // project forward vector onto fixedUp perpendicular, then set length to d
      _mathVec.Vec3.projn(target, this.forward, this.fixedUp);
      var tlen = _mathVec.Vec3.norm(target);
      _mathVec.Vec3.scale(target, target, d / tlen);
    }

    // rotation
    // -----------------------------------
    /** Set the direction the camera is pointing.
    * @param {number[]} forward - Direction (need not be normalized)
    */

  }, {
    key: 'setForward',
    value: function setForward(forward) {
      _mathVec.Vec3.normalize(this.forward, forward);

      this._recompute();
    }

    /** Set the fixed upward direction.
    * @param {number[]} up - Fixed upward direction (need not be normalized)
    */

  }, {
    key: 'setUp',
    value: function setUp(up) {
      _mathVec.Vec3.normalize(this.fixedUp, up);

      this._recompute();
    }

    /** Rotate the camera (positive yaw = rotate left, positive pitch = tilt upward).
    * @param {number} yaw - Angle in radians; rotation about fixed upward direction
    * @param {number} pitch - Angle in radians; rotation about side vector
    */

  }, {
    key: 'rotate',
    value: function rotate(yaw, pitch) {
      var cp = Math.cos(pitch);
      var sp = Math.sin(pitch);
      var cy = Math.cos(yaw);
      var sy = Math.sin(yaw);

      _mathVec.Vec3.linearCombination(TEMPVEC3, cy, this.forward, -sy, this.side);
      _mathVec.Vec3.linearCombination(TEMPVEC3, cp, TEMPVEC3, sp, this.tiltedUp);
      _mathVec.Vec3.normalize(this.forward, TEMPVEC3);

      this._recompute();
    }

    // slerp
    // -----------------------------------
    // TODO

  }]);

  return CameraRotation;
}();

// ===================================
// CAMERA
// ===================================

/** A position, {@link CameraRotation}, and some perspective projection options.
*   <br>Don't modify perspective properties directly--use the methods.
*
* @prop {number[]} position - Position of camera
* @prop {CameraRotation} rotation - Orientation of camera
*
* @prop {number} fovy - Angle formed by top and bottom frustum planes
* @prop {number} aspect - Width/height
* @prop {number} zNear - Near z-clipping plane
* @prop {number} zFar - Far z-clipping plane
* @prop {Mat4} matrix - Perspective projection matrix
*/


var Camera = function () {
  function Camera() {
    _classCallCheck(this, Camera);

    // reasonable defaults
    this.fovy = 60;
    this.aspect = 1;
    this.zNear = 0.1;
    this.zFar = 1000;

    this.position = [0, 0, 0];
    this.rotation = new CameraRotation();

    // derived quantities
    this.matrix = _mathMat.Mat4.create(); // projection matrix

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------


  _createClass(Camera, [{
    key: '_recompute',
    value: function _recompute() {
      _mathMat.Mat4.gluPerspective(this.matrix, this.fovy, this.aspect, this.zNear, this.zFar);
    }

    // change projection
    // -----------------------------------
    /** Set FOVy and update projection matrix.
    * @param {number} fovy
    */

  }, {
    key: 'setFOVy',
    value: function setFOVy(fovy) {
      this.fovy = fovy;
      this._recompute();
    }

    /** Set aspect ratio and update projection matrix.
    * @param {number} aspect
    */

  }, {
    key: 'setAspect',
    value: function setAspect(aspect) {
      this.aspect = aspect;
      this._recompute();
    }

    /** Set z clipping planes and update projection matrix.
    * @param {number} zNear
    * @param {number} zFar
    */

  }, {
    key: 'setZClippingPlanes',
    value: function setZClippingPlanes(zNear, zFar) {
      this.zNear = zNear;
      this.zFar = zFar;
      this._recompute();
    }

    // rotation and displacement
    // -----------------------------------
    /** Rotate to look at the point p.
    * @param {number[]} p - The point to look at
    * @see {@link CameraRotation#displacementForward}
    */

  }, {
    key: 'lookAt',
    value: function lookAt(p) {
      _mathVec.Vec3.subtract(TEMPVEC3, p, this.position);
      this.rotation.setForward(TEMPVEC3);
    }

    /** Move forward (i.e., in the direction the camera is pointing).
    * @param {number} d - Distance to move
    * @see {@link CameraRotation#displacementSideways}
    */

  }, {
    key: 'moveForward',
    value: function moveForward(d) {
      this.rotation.displacementForward(TEMPVEC3, d);
      _mathVec.Vec3.add(this.position, this.position, TEMPVEC3);
    }

    /** Move sideways (negative d = left, positive d = right).
    * @param {number} d - Distance to move
    * @see {@link CameraRotation#displacementUp}
    */

  }, {
    key: 'moveSideways',
    value: function moveSideways(d) {
      this.rotation.displacementSideways(TEMPVEC3, d);
      _mathVec.Vec3.add(this.position, this.position, TEMPVEC3);
    }

    /** Move up (i.e., in fixed upward direction; independent of orientation).
    * @param {number} d - Distance to move
    * @see {@link CameraRotation#displacementForwardPlanar}
    */

  }, {
    key: 'moveUp',
    value: function moveUp(d) {
      this.rotation.displacementUp(TEMPVEC3, d);
      _mathVec.Vec3.add(this.position, this.position, TEMPVEC3);
    }

    /** Move forward, perpendicular to the fixed upward direction
    *   (i.e., move along the "ground").
    * @param {number} d - Distance to move
    */

  }, {
    key: 'moveForwardPlanar',
    value: function moveForwardPlanar(d) {
      this.rotation.displacementForwardPlanar(TEMPVEC3, d);
      _mathVec.Vec3.add(this.position, this.position, TEMPVEC3);
    }

    // relative positioning
    // -----------------------------------
    /** Get location relative to camera position and orientation.
    * @param {number[]} target - The vector which will store the result
    * @param {number[]} p - Vector with "x" along "side", "y" along "tiltedUp", "z" along "forward"
    */

  }, {
    key: 'getRelativePosition',
    value: function getRelativePosition(target, p) {
      _mathVec.Vec3.copy(target, this.position);
      _mathVec.Vec3.addMultiple(target, target, this.rotation.side, p[0]);
      _mathVec.Vec3.addMultiple(target, target, this.rotation.tiltedUp, p[1]);
      _mathVec.Vec3.addMultiple(target, target, this.rotation.forward, p[2]);
    }

    // slerp
    // -----------------------------------
    // TODO

  }]);

  return Camera;
}();

exports.CameraRotation = CameraRotation;
exports.Camera = Camera;

},{"./mathMat":21,"./mathVec":24}],20:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mathVec = require('./mathVec');

// ===================================
// COLOR
// these methods act on arrays [r,g,b] or [r,g,b,a]
// any method that produces a new array has a "target" parameter
// ===================================

/** Collection of functions relating to colors.
*   <br>Colors are stored as arrays [r,g,b] or [r,g,b,a]
*/
var ColorUtils = {
  /** Convert HSV to RGB
  * @param {number[]} target - The array which will store the result
  * @param h - Hue, in the range [0,360]
  * @param s - Saturation, in the range [0,1]
  * @param v - Value, in the range [0,1]
  */
  hsvToRgb: function hsvToRgb(target, h, s, v) {
    var hm = h % 360;

    if (hm < 60) {
      target[0] = 1;
      target[1] = hm / 60;
      target[2] = 0;
    } else if (hm < 120) {
      target[0] = 1 - (hm - 60) / 60;
      target[1] = 1;
      target[2] = 0;
    } else if (hm < 180) {
      target[0] = 0;
      target[1] = 1;
      target[2] = (hm - 120) / 60;
    } else if (hm < 240) {
      target[0] = 0;
      target[1] = 1 - (hm - 180) / 60;
      target[2] = 1;
    } else if (hm < 300) {
      target[0] = (hm - 240) / 60;
      target[1] = 0;
      target[2] = 1;
    } else {
      target[0] = 1;
      target[1] = 0;
      target[2] = 1 - (hm - 300) / 60;
    }

    _mathVec.Vec3.linearCombination(target, v * s, target, v * (1 - s), [1, 1, 1]);
    _mathVec.Vec3.clamp(target, target, 0, 1);
  }
};

exports.default = ColorUtils;

},{"./mathVec":24}],21:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var DEG_TO_RAD = Math.PI / 180;
var TEMPMAT3 = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
var TEMPMAT4 = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];

// ===================================
// MAT3
// "target" parameter stores results--can always be one of the operands
// ===================================

/** Collection of functions relating to 3x3 matrices.
*   <br>Matrices are just arrays, where m[i][j] = entry at ith row, jth column
*/
var Mat3 = {

  /** Create a new identity matrix.
  * @return {Mat3}
  */
  create: function create() {
    return [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
  },

  /** Set target equal to the identity matrix.
  * @param {Mat3} target - The matrix to modify
  */
  identity: function identity(target) {
    target[0][0] = 1;target[0][1] = 0;target[0][2] = 0;
    target[1][0] = 0;target[1][1] = 1;target[1][2] = 0;
    target[2][0] = 0;target[2][1] = 0;target[2][2] = 1;
  },

  /** Set target = s.
  * @param {Mat3} target - The matrix to modify
  * @param {Mat3} s - The matrix to copy
  */
  copy: function copy(target, s) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = s[i][j];
      }
    }
  },

  // basic methods
  // -----------------------------------
  /** Fill an array with matrix entries in row-major order:
  *   <br>(first 3 entries) = (first row of s),
  *   <br>(next 3 entries) = (second row of s), ...
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat3} s - The matrix to serialize
  */
  serializeRowMajor: function serializeRowMajor(target, s) {
    var k = 0;
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[k] = s[i][j];
        k++;
      }
    }
  },

  /** Fill an array with matrix entries in column-major order:
  *   <br>(first 3 entries) = (first column of s),
  *   <br>(next 3 entries) = (second column of s), ...
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat3} s - The matrix to serialize
  */
  serializeColumnMajor: function serializeColumnMajor(target, s) {
    var k = 0;
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[k] = s[j][i];
        k++;
      }
    }
  },

  /** Set the rows of a matrix.
  * @param {Mat3} target - The matrix to modify
  *
  * @param {number[]} u - Row 0
  * @param {number[]} v - Row 1
  * @param {number[]} w - Row 2
  */
  setRows: function setRows(target, u, v, w) {
    target[0][0] = u[0];target[0][1] = u[1];target[0][2] = u[2];
    target[1][0] = v[0];target[1][1] = v[1];target[1][2] = v[2];
    target[2][0] = w[0];target[2][1] = w[1];target[2][2] = w[2];
  },

  /** Set the columns of a matrix.
  * @param {Mat3} target - The matrix to modify
  *
  * @param {number[]} u - Column 0
  * @param {number[]} v - Column 1
  * @param {number[]} w - Column 2
  */
  setColumns: function setColumns(target, u, v, w) {
    target[0][0] = u[0];target[0][1] = v[0];target[0][2] = w[0];
    target[1][0] = u[1];target[1][1] = v[1];target[1][2] = w[1];
    target[2][0] = u[2];target[2][1] = v[2];target[2][2] = w[2];
  },

  /** Set target = k * s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix being scaled
  * @param {number} k - Scale factor
  */
  scale: function scale(target, s, k) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = k * s[i][j];
      }
    }
  },

  /** Set target = s + t.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Left matrix
  * @param {Mat3} t - Right matrix
  */
  add: function add(target, s, t) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = s[i][j] + t[i][j];
      }
    }
  },

  /** Set target = s - t.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Left matrix
  * @param {Mat3} t - Right matrix
  */
  subtract: function subtract(target, s, t) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = s[i][j] - t[i][j];
      }
    }
  },

  /** Set target = transpose of s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix to transpose
  */
  transpose: function transpose(target, s) {
    target[0][0] = s[0][0];
    target[1][1] = s[1][1];
    target[2][2] = s[2][2];

    for (var i = 0; i < 3; i++) {
      for (var j = i + 1; j < 3; j++) {
        var temp = s[i][j];
        target[i][j] = s[j][i];
        target[j][i] = temp;
      }
    }
  },

  // vector-related methods
  // -----------------------------------
  /** Set target = u \otimes v.
  *   (i.e., m[i][j] = u[i]*v[j])
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} u - Vector on the left
  * @param {number[]} v - Vector on the right
  */
  tensorProduct: function tensorProduct(target, u, v) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = u[i] * v[j];
      }
    }
  },

  /** Set target = u \otimes v + v \otimes u.
  *   (i.e., m[i][j] = u[i]*v[j] + u[j]*v[i])
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} u - Vector on the left
  * @param {number[]} v - Vector on the right
  */
  symmetricProduct: function symmetricProduct(target, u, v) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        target[i][j] = u[i] * v[j] + u[j] * v[i];
      }
    }
  },

  /** Set target = s * u.
  * @param {number[]} target - The vector which will store the result
  * @param {Mat3} s - Transformation
  * @param {number[]} u - Vector to transform
  */
  transform: function transform(target, s, u) {
    var x = s[0][0] * u[0] + s[0][1] * u[1] + s[0][2] * u[2];
    var y = s[1][0] * u[0] + s[1][1] * u[1] + s[1][2] * u[2];
    var z = s[2][0] * u[0] + s[2][1] * u[1] + s[2][2] * u[2];
    target[0] = x;target[1] = y;target[2] = z;
  },

  // inverse
  // -----------------------------------
  /** Return the (i,j) cofactor of a matrix.
  * @param {Mat3} s - The matrix
  * @param {number} i - Row
  * @param {number} j - Column
  * @return {number}
  */
  cofactor: function cofactor(s, i, j) {
    var i0 = void 0;var i1 = void 0;
    if (i === 0) {
      i0 = 1;i1 = 2;
    } else if (i === 1) {
      i0 = 0;i1 = 2;
    } else if (i === 2) {
      i0 = 0;i1 = 1;
    }

    var j0 = void 0;var j1 = void 0;
    if (j === 0) {
      j0 = 1;j1 = 2;
    } else if (j === 1) {
      j0 = 0;j1 = 2;
    } else if (j === 2) {
      j0 = 0;j1 = 1;
    }

    var minor = s[i0][j0] * s[i1][j1] - s[i0][j1] * s[i1][j0];
    return (i + j) % 2 === 0 ? minor : -minor;
  },

  /** Set target = inverse of s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix to invert
  */
  inverse: function inverse(target, s) {
    // set target = adjugate (transpose of matrix of cofactors)
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        TEMPMAT3[i][j] = Mat3.cofactor(s, j, i);
      }
    }

    // divide by det
    var det = s[0][0] * TEMPMAT3[0][0] + s[0][1] * TEMPMAT3[1][0] + s[0][2] * TEMPMAT3[2][0];
    Mat3.scale(target, TEMPMAT3, 1 / det);
  },

  // GL-like methods
  // -----------------------------------
  /** Set target = rotation matrix.
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} axis - Vector about which to rotate (need not be normalized)
  * @param {number} angle - Angle in radians
  */
  rotate: function rotate(target, axis, angle) {
    // "Rodrigues formula": v -> (v.n)n + (v-(v.n)n)c - (vxn)s
    // in matrix form: (1-c)*[n \otimes n] + c*[I] - s*[[0,n3,-n2][-n3,0,n1][n2,-n1,0]]

    var c = Math.cos(angle);
    var s = Math.sin(angle);

    // n = normalize(axis)
    var l = Math.sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2]);
    var n = [axis[0] / l, axis[1] / l, axis[2] / l];

    // row 0
    target[0][0] = (1 - c) * n[0] * n[0] + c;
    target[0][1] = (1 - c) * n[0] * n[1] - s * n[2];
    target[0][2] = (1 - c) * n[0] * n[2] + s * n[1];
    // row 1
    target[1][0] = (1 - c) * n[1] * n[0] + s * n[2];
    target[1][1] = (1 - c) * n[1] * n[1] + c;
    target[1][2] = (1 - c) * n[1] * n[2] - s * n[0];
    // row 2
    target[2][0] = (1 - c) * n[2] * n[0] - s * n[1];
    target[2][1] = (1 - c) * n[2] * n[1] + s * n[0];
    target[2][2] = (1 - c) * n[2] * n[2] + c;
  }
};

// ===================================
// MAT4
// "target" parameter stores results--can always be one of the operands
// ===================================

/** Collection of functions relating to 4x4 matrices.
*   <br>Matrices are just arrays, where m[i][j] = entry at ith row, jth column
*/
var Mat4 = {

  /** Create a new identity matrix.
  * @return {Mat4}
  */
  create: function create() {
    return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
  },

  /** Set target equal to the identity matrix.
  * @param {Mat4} target - The matrix to modify
  */
  identity: function identity(target) {
    target[0][0] = 1;target[0][1] = 0;target[0][2] = 0;target[0][3] = 0;
    target[1][0] = 0;target[1][1] = 1;target[1][2] = 0;target[1][3] = 0;
    target[2][0] = 0;target[2][1] = 0;target[2][2] = 1;target[2][3] = 0;
    target[3][0] = 0;target[3][1] = 0;target[3][2] = 0;target[3][3] = 1;
  },

  /** Set target = s.
  * @param {Mat4} target - The matrix to modify
  * @param {Mat4} s - The matrix to copy
  */
  copy: function copy(target, s) {
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[i][j] = s[i][j];
      }
    }
  },

  // basic methods
  // -----------------------------------
  /** Fill an array with matrix entries in row-major order:
  *   <br>(first 4 entries) = (first row of s),
  *   <br>(next 4 entries) = (second row of s), ...
  *
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat4} s - The matrix to serialize
  */
  serializeRowMajor: function serializeRowMajor(target, s) {
    var k = 0;
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[k] = s[i][j];
        k++;
      }
    }
  },

  /** Fill an array with matrix entries in column-major order:
  *   <br>(first 4 entries) = (first column of s),
  *   <br>(next 4 entries) = (second column of s), ...
  *
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat4} s - The matrix to serialize
  */
  serializeColumnMajor: function serializeColumnMajor(target, s) {
    var k = 0;
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[k] = s[j][i];
        k++;
      }
    }
  },

  /** Set the rows of a matrix.
  * @param {Mat4} target - The matrix to modify
  * @param {number[]} u - Row 0
  * @param {number[]} v - Row 1
  * @param {number[]} w - Row 2
  * @param {number[]} x - Row 3
  */
  setRows: function setRows(target, u, v, w, x) {
    target[0][0] = u[0];target[0][1] = u[1];target[0][2] = u[2];target[0][3] = u[3];
    target[1][0] = v[0];target[1][1] = v[1];target[1][2] = v[2];target[1][3] = v[3];
    target[2][0] = w[0];target[2][1] = w[1];target[2][2] = w[2];target[2][3] = w[3];
    target[3][0] = x[0];target[3][1] = x[1];target[3][2] = x[2];target[3][3] = x[3];
  },

  /** Set the columns of a matrix.
  * @param {Mat4} target - The matrix to modify
  * @param {number[]} u - Column 0
  * @param {number[]} v - Column 1
  * @param {number[]} w - Column 2
  * @param {number[]} x - Column 3
  */
  setColumns: function setColumns(target, u, v, w, x) {
    target[0][0] = u[0];target[0][1] = v[0];target[0][2] = w[0];target[0][3] = x[0];
    target[1][0] = u[1];target[1][1] = v[1];target[1][2] = w[1];target[1][3] = x[1];
    target[2][0] = u[2];target[2][1] = v[2];target[2][2] = w[2];target[2][3] = x[2];
    target[3][0] = u[3];target[3][1] = v[3];target[3][2] = w[3];target[3][3] = x[3];
  },

  /** Set target = k * s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix being scaled
  * @param {number} k - Scale factor
  */
  scale: function scale(target, s, k) {
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[i][j] = k * s[i][j];
      }
    }
  },

  /** Set target = s + t.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Left matrix
  * @param {Mat4} t - Right matrix
  */
  add: function add(target, s, t) {
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[i][j] = s[i][j] + t[i][j];
      }
    }
  },

  /** Set target = s - t.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Left matrix
  * @param {Mat4} t - Right matrix
  */
  subtract: function subtract(target, s, t) {
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        target[i][j] = s[i][j] - t[i][j];
      }
    }
  },

  /** Set target = transpose of s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix to transpose
  */
  transpose: function transpose(target, s) {
    target[0][0] = s[0][0];
    target[1][1] = s[1][1];
    target[2][2] = s[2][2];
    target[3][3] = s[3][3];

    for (var i = 0; i < 4; i++) {
      for (var j = i + 1; j < 4; j++) {
        var temp = s[i][j];
        target[i][j] = s[j][i];
        target[j][i] = temp;
      }
    }
  },

  // vector-related methods
  // -----------------------------------
  /** Set target = s * u.
  * @param {number[]} target - The vector which will store the result
  * @param {Mat4} s - Transformation
  * @param {number[]} u - Vector to transform
  */
  transform: function transform(target, s, u) {
    var x = s[0][0] * u[0] + s[0][1] * u[1] + s[0][2] * u[2] + s[0][3] * u[3];
    var y = s[1][0] * u[0] + s[1][1] * u[1] + s[1][2] * u[2] + s[1][3] * u[3];
    var z = s[2][0] * u[0] + s[2][1] * u[1] + s[2][2] * u[2] + s[2][3] * u[3];
    var w = s[3][0] * u[0] + s[3][1] * u[1] + s[3][2] * u[2] + s[3][3] * u[3];
    target[0] = x;target[1] = y;target[2] = z;target[3] = w;
  },

  /** Computes v = (4x4 matrix s) * [u.x, u.y, u.z, 1],
  *   then sets target = [v.x, v.y, v.z] / v.w.
  *   <br>Note that both "target" and "u" are 3-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {Mat4} s - Projective transformation
  * @param {number[]} u - Vector to transform
  */
  transformProjective: function transformProjective(target, s, u) {
    var x = s[0][0] * u[0] + s[0][1] * u[1] + s[0][2] * u[2] + s[0][3];
    var y = s[1][0] * u[0] + s[1][1] * u[1] + s[1][2] * u[2] + s[1][3];
    var z = s[2][0] * u[0] + s[2][1] * u[1] + s[2][2] * u[2] + s[2][3];
    var w = s[3][0] * u[0] + s[3][1] * u[1] + s[3][2] * u[2] + s[3][3];
    target[0] = x / w;target[1] = y / w;target[2] = z / w;
  },

  // inverse
  // -----------------------------------
  /** Return the (i,j) cofactor of a matrix.
  * @param {Mat4} s - The matrix
  * @param {number} i - Row
  * @param {number} j - Column
  * @return {number}
  */
  cofactor: function cofactor(s, i, j) {
    var i0 = void 0;var i1 = void 0;var i2 = void 0;
    if (i === 0) {
      i0 = 1;i1 = 2;i2 = 3;
    } else if (i === 1) {
      i0 = 0;i1 = 2;i2 = 3;
    } else if (i === 2) {
      i0 = 0;i1 = 1;i2 = 3;
    } else if (i === 3) {
      i0 = 0;i1 = 1;i2 = 2;
    }

    var j0 = void 0;var j1 = void 0;var j2 = void 0;
    if (j === 0) {
      j0 = 1;j1 = 2;j2 = 3;
    } else if (j === 1) {
      j0 = 0;j1 = 2;j2 = 3;
    } else if (j === 2) {
      j0 = 0;j1 = 1;j2 = 3;
    } else if (j === 3) {
      j0 = 0;j1 = 1;j2 = 2;
    }

    var det0 = s[i1][j1] * s[i2][j2] - s[i1][j2] * s[i2][j1];
    var det1 = s[i1][j0] * s[i2][j2] - s[i1][j2] * s[i2][j0];
    var det2 = s[i1][j0] * s[i2][j1] - s[i1][j1] * s[i2][j0];
    var minor = s[i0][j0] * det0 + -s[i0][j1] * det1 + s[i0][j2] * det2;
    return (i + j) % 2 === 0 ? minor : -minor;
  },

  /** Set target = inverse of s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix to invert
  */
  inverse: function inverse(target, s) {
    // set target = adjugate (transpose of matrix of cofactors)
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        TEMPMAT4[i][j] = Mat4.cofactor(s, j, i);
      }
    }

    // divide by det
    var det = s[0][0] * TEMPMAT4[0][0] + s[0][1] * TEMPMAT4[1][0] + s[0][2] * TEMPMAT4[2][0] + s[0][3] * TEMPMAT4[3][0];
    Mat4.scale(target, TEMPMAT4, 1 / det);
  },

  // GL-like methods
  // -----------------------------------
  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps frustum described below to [-1,1]^3)
  *  <br>
  *  <br>Frustum: Points on ray from origin to a point on rectangle, with -z in [n,f].
  *  <br>Rectangle: In z=-n plane; x range = [l,r], y range = [b,t].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} l
  * @param {number} r
  * @param {number} b
  * @param {number} t
  * @param {number} n
  * @param {number} f
  */
  glFrustum: function glFrustum(target, l, r, b, t, n, f) {
    // row 0
    target[0][0] = 2 * n / (r - l);
    target[0][1] = 0;
    target[0][2] = (r + l) / (r - l);
    target[0][3] = 0;
    // row 1
    target[1][0] = 0;
    target[1][1] = 2 * n / (t - b);
    target[1][2] = (t + b) / (t - b);
    target[1][3] = 0;
    // row 2
    target[2][0] = 0;
    target[2][1] = 0;
    target[2][2] = -(f + n) / (f - n);
    target[2][3] = -(2 * f * n) / (f - n);
    // row 3
    target[3][0] = 0;
    target[3][1] = 0;
    target[3][2] = -1;
    target[3][3] = 0;
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps frustum described below to [-1,1]^3)
  *  <br>
  *  <br>Frustum: Points on ray from origin to a point on rectangle, with -z in [zFront,zBack].
  *  <br>Rectangle: In z=-zFront plane; x range = [-w,w], y range = [-h,h];
  *  <br>h determined so angle formed by [0,-h,-zFront] and [0,h,-zFront] is fovy; w = h*aspect.
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} fovy
  * @param {number} aspect
  * @param {number} zFront
  * @param {number} zBack
  */
  gluPerspective: function gluPerspective(target, fovy, aspect, zFront, zBack) {
    var tanFovy = Math.tan(DEG_TO_RAD * fovy * 0.5);
    var h = zFront * tanFovy;
    var w = h * aspect;
    Mat4.glFrustum(target, -w, w, -h, h, zFront, zBack);
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps box described below to [-1,1]^3)
  *  <br>
  *  <br>Box: x range = [l,r], y range = [b,t], -z range = [n,f].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} l
  * @param {number} r
  * @param {number} b
  * @param {number} t
  * @param {number} n
  * @param {number} f
  */
  glOrtho: function glOrtho(target, l, r, b, t, n, f) {
    // row 0
    target[0][0] = 2 / (r - l);
    target[0][1] = 0;
    target[0][2] = 0;
    target[0][3] = -(r + l) / (r - l);
    // row 1
    target[1][0] = 0;
    target[1][1] = 2 / (t - b);
    target[1][2] = 0;
    target[1][3] = -(t + b) / (t - b);
    // row 2
    target[2][0] = 0;
    target[2][1] = 0;
    target[2][2] = -2 / (f - n);
    target[2][3] = -(f + n) / (f - n);
    // row 3
    target[3][0] = 0;
    target[3][1] = 0;
    target[3][2] = 0;
    target[3][3] = 1;
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps box described below to [-1,1]^3)
  *  <br>
  *  <br>Box: x range = [left,right], y range = [bottom,top], -z range = [-1,1].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} left
  * @param {number} right
  * @param {number} bottom
  * @param {number} top
  */
  gluOrtho2D: function gluOrtho2D(target, left, right, bottom, top) {
    Mat4.glOrtho(target, left, right, bottom, top, -1, 1);
  }
};

exports.Mat3 = Mat3;
exports.Mat4 = Mat4;

},{}],22:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _mathMat = require('./mathMat');

var _mathSO = require('./mathSO3');

var _mathSO2 = _interopRequireDefault(_mathSO);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// OBJECT TRANSFORMATION
// ===================================

/** A transformation to be applied to an object.
* The transformation is p -> rotation*(p - offset) + translation.
*
* @prop {SO3} rotation - Rotation applied to the object
* @prop {number[]} translation - Translation applied to the object after rotation
* @prop {number[]} offset - Offset applied to the object before rotation
* @prop {ObjectTransformation} parent - A transformation to be applied after this (default = null).
*   <br> The "flattened" transformation is p -> ... this.parent.parent * this.parent * this * p.
*   <br> If the parent is null, no subsequent transformation is applied.
*/
var ObjectTransformation = function () {
  function ObjectTransformation() {
    _classCallCheck(this, ObjectTransformation);

    // transformation
    this.rotation = new _mathSO2.default();
    this.translation = [0, 0, 0];
    this.offset = [0, 0, 0];

    // parent transformation: "flattened" transformation is
    // p -> ... this.parent.parent * this.parent * this * p
    // stops whenever a parent is null
    this.parent = null;
  }

  // methods IGNORING parent
  // -----------------------------------
  /** Set this transformation to the identity (does not modify "parent"). */


  _createClass(ObjectTransformation, [{
    key: 'identity',
    value: function identity() {
      this.rotation.identity();
      _mathVec.Vec3.zero(this.translation);
      _mathVec.Vec3.zero(this.offset);
    }

    /** Set this transformation equal to another (ignores "parent" property of both).
    * @param {ObjectTransformation} t - The transformation to copy
    */

  }, {
    key: 'copy',
    value: function copy(t) {
      this.rotation.copy(t.rotation);
      _mathVec.Vec3.copy(this.translation, t.translation);
      _mathVec.Vec3.copy(this.offset, t.offset);
    }

    /** Set target = this * p (ignores "parent" property).
    * @param {number[]} target - The transformed point
    * @param {number[]} p - The point to transform
    */

  }, {
    key: 'transform',
    value: function transform(target, p) {
      _mathVec.Vec3.subtract(target, p, this.offset);
      _mathMat.Mat3.transform(target, this.rotation.matrix, target);
      _mathVec.Vec3.add(target, target, this.translation);
    }

    /** Set this = t * this (ignores "parent" property of both).
    * @param {ObjectTransformation} t - The transformation to multiply on the left
    */

  }, {
    key: 'leftMultiply',
    value: function leftMultiply(t) {
      this.rotation.leftMultiplySO3(t.rotation);
      t.transform(this.translation, this.translation);
    }

    // methods USING parent
    // -----------------------------------
    /** Set this = (... t.parent.parent * t.parent * t) * this (does not modify "parent").
    * @param {ObjectTransformation} t - The transformation to flatten and multiply on the left
    */

  }, {
    key: 'leftMultiplyFlat',
    value: function leftMultiplyFlat(t) {
      this.leftMultiply(t);
      if (t.parent !== null) {
        this.leftMultiplyFlat(t.parent);
      }
    }

    /** Set this = ... t.parent.parent * t.parent * t (does not modify "parent").
    * @param {ObjectTransformation} t - The transformation to flatten and copy
    */

  }, {
    key: 'copyFlat',
    value: function copyFlat(t) {
      this.copy(t);
      if (t.parent !== null) {
        this.leftMultiplyFlat(t.parent);
      }
    }

    /** Set target = ... this.parent.parent * this.parent * this * p.
    * @param {number[]} target - The transformed point
    * @param {number[]} p - The point to transform
    */

  }, {
    key: 'transformFlat',
    value: function transformFlat(target, p) {
      this.transform(target, p);
      if (this.parent !== null) {
        this.parent.transformFlat(target, target);
      }
    }
  }]);

  return ObjectTransformation;
}();

exports.default = ObjectTransformation;

},{"./mathMat":21,"./mathSO3":23,"./mathVec":24}],23:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathVec = require('./mathVec');

var _mathMat = require('./mathMat');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TEMPVEC4 = [0, 0, 0, 0];

// ===================================
// SO3
// maintains quaternion and matrix/inverse
// ===================================

/** An element of SO(3).  Maintains a quaternion, 3x3 matrix, and its inverse.
*   <br>Don't modify properties directly--use the methods.
*
* @prop {number[]} q - Quaternion ([a,b,c,d] = a+bi+cj+dk)
* @prop {Mat3} matrix - 3x3 matrix representing the rotation
* @prop {Mat3} matrixInverse - 3x3 matrix representing the inverse of the rotation
*/

var SO3 = function () {
  function SO3() {
    _classCallCheck(this, SO3);

    // unit quaternion representing rotation
    this.q = [1, 0, 0, 0];

    // derived quantities
    this.matrix = _mathMat.Mat3.create();
    this.matrixInverse = _mathMat.Mat3.create();

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------
  /** Set target equal to the quaternion which rotates "angle" radians about "axis".
  * @param {number[]} target - The target quaternion ([a,b,c,d] = a+bi+cj+dk)
  * @param {number[]} axis - Vector about which to rotate (need not be normalized)
  * @param {number} angle - Angle in radians
  */


  _createClass(SO3, [{
    key: '_recompute',
    value: function _recompute() {
      this._computeMatrix();
    }
  }, {
    key: '_computeMatrix',
    value: function _computeMatrix() {
      var a = this.q[0];
      var b = this.q[1];
      var c = this.q[2];
      var d = this.q[3];

      // column 0
      this.matrix[0][0] = 1 - 2 * (c * c + d * d); // = a*a+b*b-c*c-d*d for unit q
      this.matrix[1][0] = 2 * (b * c + a * d);
      this.matrix[2][0] = 2 * (b * d - c * a);

      // column 1
      this.matrix[0][1] = 2 * (b * c - a * d);
      this.matrix[1][1] = 1 - 2 * (b * b + d * d); // = a*a-b*b+c*c-d*d for unit q
      this.matrix[2][1] = 2 * (c * d + a * b);

      // column 2
      this.matrix[0][2] = 2 * (b * d + a * c);
      this.matrix[1][2] = 2 * (c * d - a * b);
      this.matrix[2][2] = 1 - 2 * (b * b + c * c); // = a*a-b*b-c*c+d*d for unit q

      // matrix is orthogonal, so inverse = transpose
      _mathMat.Mat3.transpose(this.matrixInverse, this.matrix);
    }

    // modification
    // -----------------------------------
    /** Set this equal to the identity. */

  }, {
    key: 'identity',
    value: function identity() {
      this.q[0] = 1;this.q[1] = 0;this.q[2] = 0;this.q[3] = 0;

      // same effect as _recompute()
      _mathMat.Mat3.identity(this.matrix);
      _mathMat.Mat3.identity(this.matrixInverse);
    }

    /** Set this equal to another rotation.
    * @param {SO3} s - The other rotation
    */

  }, {
    key: 'copy',
    value: function copy(s) {
      _mathVec.Vec4.copy(this.q, s.q);

      // same effect as _recompute()
      _mathMat.Mat3.copy(this.matrix, s.matrix);
      _mathMat.Mat3.copy(this.matrixInverse, s.matrixInverse);
    }

    /** Set this = (rotation given by q).
    *   <br> Does not normalize "this.q" or "q".
    * @param {number[]} q - Quaternion defining a rotation ([a,b,c,d] = a+bi+cj+dk)
    */

  }, {
    key: 'setQ',
    value: function setQ(q) {
      _mathVec.Vec4.copy(this.q, q);
      this._recompute();
    }

    /** Set this = (rotation given by q) * this.
    *   <br> Does not normalize "q", but normalizes "this.q" after multiplying.
    * @param {number[]} q - Quaternion defining a rotation ([a,b,c,d] = a+bi+cj+dk)
    */

  }, {
    key: 'leftMultiplyQ',
    value: function leftMultiplyQ(q) {
      _mathVec.Vec4.qMultiply(this.q, q, this.q);
      _mathVec.Vec4.normalize(this.q, this.q);
      this._recompute();
    }

    /** Set this = r * this.
    * @param {SO3} r - A rotation
    */

  }, {
    key: 'leftMultiplySO3',
    value: function leftMultiplySO3(r) {
      this.leftMultiplyQ(r.q);
    }

    /** Set this = (rotation "angle" radians about "axis") * this.
    * @param {number[]} axis - Vector about which to rotate (need not be normalized)
    * @param {number} angle - Angle in radians
    */

  }, {
    key: 'leftMultiplyAxisAngle',
    value: function leftMultiplyAxisAngle(axis, angle) {
      SO3.qAxisAngle(TEMPVEC4, axis, angle);
      this.leftMultiplyQ(TEMPVEC4);
    }

    // slerp
    // -----------------------------------
    // TODO

  }], [{
    key: 'qAxisAngle',
    value: function qAxisAngle(target, axis, angle) {
      // general formula: q = cos(t/2) + (nxi + nyj + nzk)sin(t/2) (n = normalized axis)

      var c = Math.cos(angle * 0.5);
      var s = Math.sin(angle * 0.5);

      // l = length of axis
      var invl = 1 / _mathVec.Vec3.norm(axis);
      target[0] = c;
      target[1] = s * axis[0] * invl;
      target[2] = s * axis[1] * invl;
      target[3] = s * axis[2] * invl;
    }
  }]);

  return SO3;
}();

exports.default = SO3;

},{"./mathMat":21,"./mathVec":24}],24:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// ===================================
// VEC3
// these methods act on arrays [x,y,z]
// any method that produces a new vector has a "target" parameter
// ===================================

/** Collection of functions relating to 3-dimensional vectors.
*   <br>Vectors are just arrays, i.e. [x,y,z].
*/
var Vec3 = {

  /** Set target equal to the zero vector.
  * @param {number[]} target - The vector to modify
  */
  zero: function zero(target) {
    target[0] = 0;
    target[1] = 0;
    target[2] = 0;
  },

  /** Set target = u.
  * @param {number[]} target - The vector to modify
  * @param {number[]} u - The vector to copy
  */
  copy: function copy(target, u) {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
  },

  // basic methods
  // -----------------------------------
  /** Clamp each entry to the indicated interval
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} v - The vector with entries to be clamped
  * @param {number} a - Left end of the interval
  * @param {number} b - Right end of the interval
  */
  clamp: function clamp(target, v, a, b) {
    target[0] = v[0] < a ? a : v[0] > b ? b : v[0];
    target[1] = v[1] < a ? a : v[1] > b ? b : v[1];
    target[2] = v[2] < a ? a : v[2] > b ? b : v[2];
  },

  /** Set target = u + v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  add: function add(target, u, v) {
    target[0] = u[0] + v[0];
    target[1] = u[1] + v[1];
    target[2] = u[2] + v[2];
  },

  /** Set target = u - v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  subtract: function subtract(target, u, v) {
    target[0] = u[0] - v[0];
    target[1] = u[1] - v[1];
    target[2] = u[2] - v[2];
  },

  /** Set target = u + k*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @param {number} k - Scale factor for v
  */
  addMultiple: function addMultiple(target, u, v, k) {
    target[0] = u[0] + k * v[0];
    target[1] = u[1] + k * v[1];
    target[2] = u[2] + k * v[2];
  },

  /** Set target = a*u + b*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number} a - Scale factor for u
  * @param {number[]} u - Left vector
  * @param {number} b - Scale factor for v
  * @param {number[]} v - Right vector
  */
  linearCombination: function linearCombination(target, a, u, b, v) {
    target[0] = a * u[0] + b * v[0];
    target[1] = a * u[1] + b * v[1];
    target[2] = a * u[2] + b * v[2];
  },

  /** Set target = k*u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being scaled
  * @param {number} k - Scale factor for u
  */
  scale: function scale(target, u, k) {
    target[0] = k * u[0];
    target[1] = k * u[1];
    target[2] = k * u[2];
  },

  /** Set target = -u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being flipped
  */
  flip: function flip(target, u) {
    target[0] = -u[0];
    target[1] = -u[1];
    target[2] = -u[2];
  },

  /** Return the dot product of u and v.
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dot: function dot(u, v) {
    return u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
  },

  /** Return the square of the distance between u and v (i.e., dot(u-v, u-v)).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  distsq: function distsq(u, v) {
    var ret = 0;

    var temp = void 0;
    temp = u[0] - v[0];ret += temp * temp;
    temp = u[1] - v[1];ret += temp * temp;
    temp = u[2] - v[2];ret += temp * temp;

    return ret;
  },

  // derivative methods
  // -----------------------------------
  /** Return the square of the norm of u (i.e., dot(u, u)).
  * @param {number[]} u - The vector
  * @return {number}
  */
  normsq: function normsq(u) {
    return Vec3.dot(u, u);
  },

  /** Return the norm of u (i.e., sqrt(dot(u, u))).
  * @param {number[]} u - The vector
  * @return {number}
  */
  norm: function norm(u) {
    return Math.sqrt(Vec3.normsq(u));
  },

  /** Return the distance between u and v (i.e., sqrt(dot(u-v, u-v))).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dist: function dist(u, v) {
    return Math.sqrt(Vec3.distsq(u, v));
  },

  /** Set target = u / norm(u).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  normalize: function normalize(target, u) {
    var s = 1 / Vec3.norm(u);
    Vec3.scale(target, u, s);
  },

  /** Set target = the projection of u onto v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} v - The vector to project onto
  */
  proj: function proj(target, u, v) {
    var s = Vec3.dot(u, v) / Vec3.normsq(v);
    Vec3.scale(target, v, s);
  },

  /** Set target = the projection of u onto the plane with normal n.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} n - The normal of the plane being projected onto
  */
  projn: function projn(target, u, n) {
    var s = Vec3.dot(u, n) / Vec3.normsq(n);
    Vec3.linearCombination(target, 1, u, -s, n);
  },

  // dimension-specific methods
  // -----------------------------------
  /** Set target = u cross v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  cross: function cross(target, u, v) {
    var x = u[1] * v[2] - u[2] * v[1];
    var y = u[2] * v[0] - u[0] * v[2];
    var z = u[0] * v[1] - u[1] * v[0];
    target[0] = x;target[1] = y;target[2] = z;
  },

  /** Set target = [u.x, u.y, u.z, w].
  *   <br>Note that "target" is 4-dimensional and "u" is 3-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector to augment
  * @param {number} w - New w-value
  */
  augment: function augment(target, u, w) {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
    target[3] = w;
  },

  /** Set target = any vector perpendicular to u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  orthogonal: function orthogonal(target, u) {
    // if u[i]^2 <= |u|^2 / 3 (must be true for one i), cross with e_i
    var cmp = Vec3.normsq(u) / 3;

    if (u[0] * u[0] < cmp) {
      var temp = u[1];
      target[0] = 0;
      target[1] = u[2];
      target[2] = -temp;
    } else if (u[1] * u[1] < cmp) {
      var _temp = u[0];
      target[0] = -u[2];
      target[1] = 0;
      target[2] = _temp;
    } else {
      var _temp2 = u[0];
      target[0] = u[1];
      target[1] = -_temp2;
      target[2] = 0;
    }
  }
};

// ===================================
// VEC4
// these methods act on arrays [x,y,z,w]
// any method that produces a new vector has a "target" parameter
// ===================================

/** Collection of functions relating to 4-dimensional vectors.
*   <br>Vectors are just arrays, i.e. [x,y,z,w].
*/
var Vec4 = {

  /** Set target equal to the zero vector.
  * @param {number[]} target - The vector to modify
  */
  zero: function zero(target) {
    target[0] = 0;
    target[1] = 0;
    target[2] = 0;
    target[3] = 0;
  },

  /** Set target = u.
  * @param {number[]} target - The vector to modify
  * @param {number[]} u - The vector to copy
  */
  copy: function copy(target, u) {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
    target[3] = u[3];
  },

  // basic methods
  // -----------------------------------
  /** Set target = u + v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  add: function add(target, u, v) {
    target[0] = u[0] + v[0];
    target[1] = u[1] + v[1];
    target[2] = u[2] + v[2];
    target[3] = u[3] + v[3];
  },

  /** Set target = u - v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  subtract: function subtract(target, u, v) {
    target[0] = u[0] - v[0];
    target[1] = u[1] - v[1];
    target[2] = u[2] - v[2];
    target[3] = u[3] - v[3];
  },

  /** Set target = u + k*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @param {number} k - Scale factor for v
  */
  addMultiple: function addMultiple(target, u, v, k) {
    target[0] = u[0] + k * v[0];
    target[1] = u[1] + k * v[1];
    target[2] = u[2] + k * v[2];
    target[3] = u[3] + k * v[3];
  },

  /** Set target = a*u + b*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number} a - Scale factor for u
  * @param {number[]} u - Left vector
  * @param {number} b - Scale factor for v
  * @param {number[]} v - Right vector
  */
  linearCombination: function linearCombination(target, a, u, b, v) {
    target[0] = a * u[0] + b * v[0];
    target[1] = a * u[1] + b * v[1];
    target[2] = a * u[2] + b * v[2];
    target[3] = a * u[3] + b * v[3];
  },

  /** Set target = k*u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being scaled
  * @param {number} k - Scale factor for u
  */
  scale: function scale(target, u, k) {
    target[0] = k * u[0];
    target[1] = k * u[1];
    target[2] = k * u[2];
    target[3] = k * u[3];
  },

  /** Set target = -u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being flipped
  */
  flip: function flip(target, u) {
    target[0] = -u[0];
    target[1] = -u[1];
    target[2] = -u[2];
    target[3] = -u[3];
  },

  /** Return the dot product of u and v.
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dot: function dot(u, v) {
    return u[0] * v[0] + u[1] * v[1] + u[2] * v[2] + u[3] * v[3];
  },

  /** Return the square of the distance between u and v (i.e., dot(u-v, u-v)).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  distsq: function distsq(u, v) {
    var ret = 0;

    var temp = void 0;
    temp = u[0] - v[0];ret += temp * temp;
    temp = u[1] - v[1];ret += temp * temp;
    temp = u[2] - v[2];ret += temp * temp;
    temp = u[3] - v[3];ret += temp * temp;

    return ret;
  },

  // derivative methods
  // -----------------------------------
  /** Return the square of the norm of u (i.e., dot(u, u)).
  * @param {number[]} u - The vector
  * @return {number}
  */
  normsq: function normsq(u) {
    return Vec4.dot(u, u);
  },

  /** Return the norm of u (i.e., sqrt(dot(u, u))).
  * @param {number[]} u - The vector
  * @return {number}
  */
  norm: function norm(u) {
    return Math.sqrt(Vec4.normsq(u));
  },

  /** Return the distance between u and v (i.e., sqrt(dot(u-v, u-v))).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dist: function dist(u, v) {
    return Math.sqrt(Vec4.distsq(u, v));
  },

  /** Set target = u / norm(u).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  normalize: function normalize(target, u) {
    var s = 1 / Vec4.norm(u);
    Vec4.scale(target, u, s);
  },

  /** Set target = the projection of u onto v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} v - The vector to project onto
  */
  proj: function proj(target, u, v) {
    var s = Vec4.dot(u, v) / Vec4.normsq(v);
    Vec4.scale(target, v, s);
  },

  /** Set target = the projection of u onto the hyperplane with normal n.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} n - The normal of the hyperplane being projected onto
  */
  projn: function projn(target, u, n) {
    var s = Vec4.dot(u, n) / Vec4.normsq(n);
    Vec4.linearCombination(target, 1, u, -s, n);
  },

  // dimension-specific methods
  // -----------------------------------
  /** Set target = [u.x/u.w, u.y/u.w, u.z/u.w].
  *   <br>Note that "target" is 3-dimensional and "u" is 4-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector to dehomogenize
  */
  dehomogenize: function dehomogenize(target, u) {
    target[0] = u[0] / u[3];
    target[1] = u[1] / u[3];
    target[2] = u[2] / u[3];
  },

  // quaternions: [a,b,c,d] represents a + bi + cj + dk
  // -----------------------------------
  /** Set target = x * y, where target, x, y are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Left quaternion
  * @param {number[]} y - Right quaternion
  */
  qMultiply: function qMultiply(target, x, y) {
    var a = x[0] * y[0] - x[1] * y[1] - x[2] * y[2] - x[3] * y[3];
    var b = x[0] * y[1] + x[1] * y[0] + x[2] * y[3] - x[3] * y[2];
    var c = x[0] * y[2] - x[1] * y[3] + x[2] * y[0] + x[3] * y[1];
    var d = x[0] * y[3] + x[1] * y[2] - x[2] * y[1] + x[3] * y[0];
    target[0] = a;target[1] = b;target[2] = c;target[3] = d;
  },

  /** Set target = conjugate of x, where target and x are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Quaternion to conjugate
  */
  qConjugate: function qConjugate(target, x) {
    target[0] = x[0];
    target[1] = -x[1];
    target[2] = -x[2];
    target[3] = -x[3];
  },

  /** Set target = inverse of x, where target and x are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Quaternion to invert
  */
  qInverse: function qInverse(target, x) {
    Vec4.scale(target, x, 1 / Vec4.normsq(x));
    Vec4.qConjugate(target, target);
  }
};

exports.Vec3 = Vec3;
exports.Vec4 = Vec4;

},{}],25:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// DRAWABLE
// WebGL version of GeomData
// ===================================

/** Stores the data of a {@link GeomData} in WebGLBuffer objects.
* @param {WebGLRenderingContext} gl
* @param {GeomData} geometry
*
* @prop {WebGLBuffer} bufElement - Corresponds to {@link GeomData}.elements
* @prop {WebGLBuffer} bufElementWire - Corresponds to {@link GeomData}.elementsWire
* @prop {WebGLBuffer} bufPosition - Corresponds to {@link GeomData}.position
* @prop {WebGLBuffer} bufNormal - Corresponds to {@link GeomData}.normal
* @prop {WebGLBuffer} bufTex - Corresponds to {@link GeomData}.tex
* @prop {WebGLBuffer} bufColor - Corresponds to {@link GeomData}.color
*/
var WGLDrawable = function () {
  function WGLDrawable(gl, geometry) {
    _classCallCheck(this, WGLDrawable);

    // store some data so that modifying geometry doesn't break the drawable
    this.elementCount = geometry.elementCount;
    this.elementWireCount = geometry.elementWireCount;

    this.hasNormals = geometry.hasNormals;
    this.hasTexture = geometry.hasTexture;

    // tally total size of drawable data
    var intCount = 0;
    var floatCount = 0;

    // create index buffers
    intCount += geometry.elements.length;
    this.bufElement = WGLDrawable.createElementArrayBuffer(gl, geometry.elements);

    intCount += geometry.elementsWire.length;
    this.bufElementWire = WGLDrawable.createElementArrayBuffer(gl, geometry.elementsWire);

    // create attribute buffers
    floatCount += geometry.position.length;
    this.bufPosition = WGLDrawable.createVertexAttribBuffer(gl, geometry.position);

    if (this.hasNormals) {
      floatCount += geometry.normal.length;
      this.bufNormal = WGLDrawable.createVertexAttribBuffer(gl, geometry.normal);
    }

    if (this.hasTexture) {
      floatCount += geometry.tex.length;
      this.bufTex = WGLDrawable.createVertexAttribBuffer(gl, geometry.tex);
    } else {
      floatCount += geometry.color.length;
      this.bufColor = WGLDrawable.createVertexAttribBuffer(gl, geometry.color);
    }

    // debug: total size
    var intBytes = intCount * Uint16Array.BYTES_PER_ELEMENT;
    var floatBytes = floatCount * Float32Array.BYTES_PER_ELEMENT;
    var sizeKB = (intBytes + floatBytes) / 1024;
    console.log("Created buffers (" + intCount + " Uint16, " + floatCount + " Float32, " + sizeKB + " KB)");
  }

  // buffer creation
  // -----------------------------------


  _createClass(WGLDrawable, [{
    key: "update",


    // update buffers from geometry
    // -----------------------------------
    /** Use bufferSubData to update vertex attribute buffers (position, normal, color, and tex).
    *   <br>Source should be the same {@link GeomData} used to create this drawable.
    * @param {WebGLRenderingContext} gl
    * @param {GeomData} geometry
    */
    value: function update(gl, geometry) {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.bufPosition);
      gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.position);

      if (this.hasNormals) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.bufNormal);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.normal);
      }

      if (this.hasTexture) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.bufTex);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.tex);
      } else {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.bufColor);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.color);
      }
    }

    // buffer deletion
    // -----------------------------------
    /** Delete the buffers.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: "free",
    value: function free(gl) {
      gl.deleteBuffer(this.bufElement);
      gl.deleteBuffer(this.bufElementWire);

      gl.deleteBuffer(this.bufPosition);

      if (this.hasNormals) {
        gl.deleteBuffer(this.bufNormal);
      }

      if (this.hasTexture) {
        gl.deleteBuffer(this.bufTex);
      } else {
        gl.deleteBuffer(this.bufColor);
      }
    }

    // draw
    // -----------------------------------
    /** Draw using drawElements in TRIANGLES mode.
    *   <br>Use {@link WGLShaderCommon#prepareVertexAttribs} first.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: "drawTriangles",
    value: function drawTriangles(gl) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.bufElement);
      gl.drawElements(gl.TRIANGLES, this.elementCount, gl.UNSIGNED_SHORT, 0);
    }

    /** Draw using drawElements in LINES mode.
    *   <br>Use {@link WGLShaderCommon#prepareVertexAttribs} first.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: "drawLines",
    value: function drawLines(gl) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.bufElementWire);
      gl.drawElements(gl.LINES, this.elementWireCount, gl.UNSIGNED_SHORT, 0);
    }
  }], [{
    key: "createElementArrayBuffer",
    value: function createElementArrayBuffer(gl, raw) {
      var id = gl.createBuffer();

      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, id);
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, raw, gl.STATIC_DRAW);

      return id;
    }
  }, {
    key: "createVertexAttribBuffer",
    value: function createVertexAttribBuffer(gl, raw) {
      var id = gl.createBuffer();

      gl.bindBuffer(gl.ARRAY_BUFFER, id);
      gl.bufferData(gl.ARRAY_BUFFER, raw, gl.DYNAMIC_DRAW);

      return id;
    }
  }]);

  return WGLDrawable;
}();

exports.default = WGLDrawable;

},{}],26:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _wglDrawable = require('./wglDrawable');

var _wglDrawable2 = _interopRequireDefault(_wglDrawable);

var _geomQuad = require('./geomQuad');

var _geomQuad2 = _interopRequireDefault(_geomQuad);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// ===================================
// QUAD
// ===================================

/** Stores the data of a {@link GeomQuad} in WebGLBuffer objects.
* @param {WebGLRenderingContext} gl
*
* @extends WGLDrawable
*/
var WGLQuad = function (_WGLDrawable) {
  _inherits(WGLQuad, _WGLDrawable);

  function WGLQuad(gl) {
    _classCallCheck(this, WGLQuad);

    return _possibleConstructorReturn(this, (WGLQuad.__proto__ || Object.getPrototypeOf(WGLQuad)).call(this, gl, new _geomQuad2.default()));
  }

  return WGLQuad;
}(_wglDrawable2.default);

exports.default = WGLQuad;

},{"./geomQuad":12,"./wglDrawable":25}],27:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mathMat = require('./mathMat');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TEMPSERIAL4 = new Float32Array(16);
var TEMPSERIAL3 = new Float32Array(9);

var LOG_PREFIX = 'WGLShader';
var CHECK_ERRORS = true;

var checkCompileStatus = function checkCompileStatus(gl, description, id) {
  if (gl.getShaderParameter(id, gl.COMPILE_STATUS)) {
    console.log('[' + LOG_PREFIX + '] [' + description + '] Compile status is OK.');
  } else {
    console.error('[' + LOG_PREFIX + '] [' + description + '] Compile failed! Info log:');
    console.error(gl.getShaderInfoLog(id));
  }
};

var checkLinkStatus = function checkLinkStatus(gl, description, id) {
  if (gl.getProgramParameter(id, gl.LINK_STATUS)) {
    console.log('[' + LOG_PREFIX + '] [' + description + '] Link status is OK.');
  } else {
    console.error('[' + LOG_PREFIX + '] [' + description + '] Link failed! Info log:');
    console.error(gl.getProgramInfoLog(id));
  }
};

// ===================================
// SHADER PROGRAM (GENERIC)
// ===================================

/** Abstract wrapper for a shader program.
*   <br>Implement {@link WGLShaderProgram#_setVertexAttributes} and {@link WGLShaderProgram#_getUniformNames}.
*   <br>Call {@link WGLShaderProgram#_initialize} before doing anything.
*/

var WGLShaderProgram = function () {
  function WGLShaderProgram() {
    _classCallCheck(this, WGLShaderProgram);
  }

  _createClass(WGLShaderProgram, [{
    key: '_initialize',

    // initialize
    // -----------------------------------
    /** Create, compile and link WebGLShader and WebGLProgram objects.
    * @param {WebGLRenderingContext} gl
    * @param {string} description - Description for logging status
    * @param {string} svSrc - Vertex shader source
    * @param {string} sfSrc - Fragment shader source
    * @protected
    */
    value: function _initialize(gl, description, svSrc, sfSrc) {
      // vertex shader
      var idVert = gl.createShader(gl.VERTEX_SHADER);
      gl.shaderSource(idVert, svSrc);
      gl.compileShader(idVert);
      if (CHECK_ERRORS) {
        checkCompileStatus(gl, description + ' (vertex)', idVert);
      }

      // fragment shader
      var idFrag = gl.createShader(gl.FRAGMENT_SHADER);
      gl.shaderSource(idFrag, sfSrc);
      gl.compileShader(idFrag);
      if (CHECK_ERRORS) {
        checkCompileStatus(gl, description + ' (fragment)', idFrag);
      }

      // prepare shader program
      this.id = gl.createProgram();

      gl.attachShader(this.id, idVert);
      gl.attachShader(this.id, idFrag);

      // get a list of vertex attribute indices and bind them to their names before linking
      this._vertexAttributes = []; // array of vertex attribute indices
      this._setVertexAttributes(gl); // calls _addVertexAttribute for each attribute

      gl.linkProgram(this.id);
      if (CHECK_ERRORS) {
        checkLinkStatus(gl, description, this.id);
      }

      // get uniform names
      var uniformNames = []; // list of uniform variable names
      this._getUniformNames(uniformNames); // adds each uniform variable name to the list

      // define uniforms
      this._uniforms = {};
      for (var i = 0; i < uniformNames.length; i++) {
        var str = uniformNames[i];
        this._uniforms[str] = gl.getUniformLocation(this.id, str);
      }
    }

    // "abstract" and "protected" methods
    // -----------------------------------
    /** Call {@link WGLShaderProgram#_addVertexAttribute} for each vertex attribute to be used.
    * @param {WebGLRenderingContext} gl
    * @abstract
    */

  }, {
    key: '_setVertexAttributes',
    value: function _setVertexAttributes(gl) {}

    /** Use during {@link WGLShaderProgram#_setVertexAttributes} to specify vertex attributes.
    * @param {WebGLRenderingContext} gl
    * @param {number} index - Index of the vertex attribute (i.e., to be used in vertexAttribPointer)
    * @param {string} str - Name of the variable bound to the vertex attribute
    * @protected
    */

  }, {
    key: '_addVertexAttribute',
    value: function _addVertexAttribute(gl, index, str) {
      this._vertexAttributes.push(index);
      gl.bindAttribLocation(this.id, index, str);
    }

    /** Add each uniform variable name to the array.
    * @param {string[]} uniformNames - Array of uniform variable names
    * @abstract
    */

  }, {
    key: '_getUniformNames',
    value: function _getUniformNames(uniformNames) {}

    // convenience methods for setting uniforms
    // -----------------------------------

  }, {
    key: 'setU1i',
    value: function setU1i(gl, str, x) {
      gl.uniform1i(this._uniforms[str], x);
    }
  }, {
    key: 'setU2i',
    value: function setU2i(gl, str, x, y) {
      gl.uniform2i(this._uniforms[str], x, y);
    }
  }, {
    key: 'setU1f',
    value: function setU1f(gl, str, x) {
      gl.uniform1f(this._uniforms[str], x);
    }
  }, {
    key: 'setUVec3',
    value: function setUVec3(gl, str, u) {
      gl.uniform3f(this._uniforms[str], u[0], u[1], u[2]);
    }
  }, {
    key: 'setUVec4',
    value: function setUVec4(gl, str, u) {
      gl.uniform4f(this._uniforms[str], u[0], u[1], u[2], u[3]);
    }
  }, {
    key: 'setUMat3',
    value: function setUMat3(gl, str, s) {
      _mathMat.Mat3.serializeColumnMajor(TEMPSERIAL3, s);
      gl.uniformMatrix3fv(this._uniforms[str], false, TEMPSERIAL3);
    }
  }, {
    key: 'setUMat4',
    value: function setUMat4(gl, str, s) {
      _mathMat.Mat4.serializeColumnMajor(TEMPSERIAL4, s);
      gl.uniformMatrix4fv(this._uniforms[str], false, TEMPSERIAL4);
    }

    // use/unuse
    // -----------------------------------
    /** Prepare to use this shader program for rendering, and enable relevant vertex attributes.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'use',
    value: function use(gl) {
      // use shader program
      gl.useProgram(this.id);

      // enable vertex attribute arrays
      for (var i = 0; i < this._vertexAttributes.length; i++) {
        var index = this._vertexAttributes[i];
        gl.enableVertexAttribArray(index);
      }
    }

    /** Disable this program's vertex attributes and stop using the program (use null).
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'unuse',
    value: function unuse(gl) {
      // disable vertex attribute arrays
      for (var i = 0; i < this._vertexAttributes.length; i++) {
        var index = this._vertexAttributes[i];
        gl.disableVertexAttribArray(index);
      }

      // unuse shader program
      gl.useProgram(null);
    }
  }]);

  return WGLShaderProgram;
}();

exports.default = WGLShaderProgram;

},{"./mathMat":21}],28:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// FRAMEBUFFER OBJECT
// ===================================

/** Wrapper for WebGLFramebuffer.  Creates the FBO.
* @param {WebGLRenderingContext} gl
*/
var WGLFramebufferObject = function () {
  function WGLFramebufferObject(gl) {
    _classCallCheck(this, WGLFramebufferObject);

    this.id = gl.createFramebuffer();

    console.log('Created FBO');
  }

  // delete
  // -----------------------------------
  /** Delete the FBO.
  * @param {WebGLRenderingContext} gl
  */


  _createClass(WGLFramebufferObject, [{
    key: 'free',
    value: function free(gl) {
      gl.deleteFramebuffer(this.id);

      console.log('Deleted FBO');
    }

    // binding
    // -----------------------------------
    /** Bind the FBO.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'bind',
    value: function bind(gl) {
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.id);
    }

    /** Unbind the FBO (binds null).
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'unbind',
    value: function unbind(gl) {
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }
  }]);

  return WGLFramebufferObject;
}();

// ===================================
// TEXTURE
// ===================================

// NOTE:
// Some "GLenum" values will be manipulated by adding integers, e.g.:
//   gl.TEXTURE4 = gl.TEXTURE0 + 4 (etc)
//   gl.TEXTURE_CUBE_MAP_NEGATIVE_Z = TEXTURE_CUBE_MAP_POSITIVE_X + 5
// this will be correct according to WebGL spec

/** Creates a WebGLTexture (for color data) and WebGLRenderbuffer (for depth).
*   <br>Can be used as either source or target texture.
*   <br>Don't modify size directly--use the methods.
* @param {WebGLRenderingContext} gl
* @param {number} width - Texture width
* @param {number} height - Texture height
* @param {boolean} filter - Use LINEAR for TEXTURE_MIN/MAG_FILTER (otherwise NEAREST)?  Default true.
*/


var WGLTexture = function () {
  function WGLTexture(gl, width, height, filter) {
    _classCallCheck(this, WGLTexture);

    this.width = width;
    this.height = height;
    this.filter = filter === undefined ? true : filter;

    this._create(gl);
  }

  // create and delete
  // -----------------------------------
  /** Change size of this texture.
  * @param {WebGLRenderingContext} gl
  * @param {number} width - Texture width
  * @param {number} height - Texture height
  */


  _createClass(WGLTexture, [{
    key: 'resize',
    value: function resize(gl, width, height) {
      this.free(gl);

      this.width = width;
      this.height = height;

      this._create(gl);
    }
  }, {
    key: '_create',
    value: function _create(gl) {
      // render buffer
      this.idDepth = gl.createRenderbuffer();

      gl.bindRenderbuffer(gl.RENDERBUFFER, this.idDepth);
      gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.width, this.height);

      // color texture
      this.idColor = gl.createTexture();

      gl.bindTexture(gl.TEXTURE_2D, this.idColor);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, this.filter ? gl.LINEAR : gl.NEAREST); // allow nice-looking downsampling for anti-aliasing
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, this.filter ? gl.LINEAR : gl.NEAREST); // don't want to interpolate packed floats
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

      gl.bindTexture(gl.TEXTURE_2D, null);

      // done
      console.log('Created texture (' + this.width + 'x' + this.height + ')');
    }

    // TODO doc
    // optional callback

  }, {
    key: 'setImage',
    value: function setImage(gl, url, done) {
      var _this = this;

      var img = new Image();
      img.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, _this.idColor);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
        gl.bindTexture(gl.TEXTURE_2D, null);

        if (typeof done === 'function') {
          done();
        }
      };
      img.src = url;
    }

    /** Delete the texture.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'free',
    value: function free(gl) {
      gl.deleteRenderbuffer(this.idDepth);
      gl.deleteTexture(this.idColor);

      console.log('Deleted texture');
    }

    // binding
    // -----------------------------------
    /** Bind texture to specified texture unit.
    * @param {WebGLRenderingContext} gl
    * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
    */

  }, {
    key: 'bind',
    value: function bind(gl, texUnit) {
      gl.activeTexture(gl.TEXTURE0 + texUnit);
      gl.bindTexture(gl.TEXTURE_2D, this.idColor);
    }

    /** Unbind texture from specified texture unit (binds null).
    * @param {WebGLRenderingContext} gl
    * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
    */

  }, {
    key: 'unbind',
    value: function unbind(gl, texUnit) {
      gl.activeTexture(gl.TEXTURE0 + texUnit);
      gl.bindTexture(gl.TEXTURE_2D, null);
    }

    /** Bind texture for writing.  Assumes FBO is bound.
    *   <br>Attaches depth renderbuffer to DEPTH_ATTACHMENT and color texture to COLOR_ATTACHMENT0,
    *   then prepares viewport.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'bindWrite',
    value: function bindWrite(gl) {
      // ensure render buffer is not attached
      gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.idDepth);
      // attach texture
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.idColor, 0);

      // check status
      var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
      if (status !== gl.FRAMEBUFFER_COMPLETE) {
        switch (status) {
          case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT.');
          case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
            throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.');
          case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.');
          case gl.FRAMEBUFFER_UNSUPPORTED:
            throw new Error('FBO status is GL_FRAMEBUFFER_UNSUPPORTED.');
        }
      }

      // set appropriate viewport
      gl.viewport(0, 0, this.width, this.height);
    }

    /** Unbind texture (attaches null to COLOR_ATTACHMENT0 and DEPTH_ATTACHMENT).
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'unbindWrite',
    value: function unbindWrite(gl) {
      // detach texture and depth renderbuffer
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, null, 0);
      gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, null);
    }
  }]);

  return WGLTexture;
}();

// ===================================
// CUBE TEXTURE
// ===================================

/** Creates a texture cube with color data.
*   <br>Must be used as a source texture.
*   <br>Don't modify size directly--use the methods.
* @param {WebGLRenderingContext} gl
* @param {number} width - Texture width
* @param {number} height - Texture height
* @param {boolean} filter - Use LINEAR for TEXTURE_MIN/MAG_FILTER?  Optional; default true.
*/


var WGLTextureCube = function () {
  function WGLTextureCube(gl, width, height, filter) {
    _classCallCheck(this, WGLTextureCube);

    this.width = width;
    this.height = height;
    this.filter = filter === undefined ? true : filter;

    this._create(gl);
  }

  _createClass(WGLTextureCube, [{
    key: '_create',
    value: function _create(gl) {
      // color texture
      this.idColor = gl.createTexture();

      gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.idColor);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, this.filter ? gl.LINEAR : gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, this.filter ? gl.LINEAR : gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

      for (var i = 0; i < 6; i++) {
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGB, this.width, this.height, 0, gl.RGB, gl.UNSIGNED_BYTE, null);
      }

      gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);

      // done
      console.log('Created texture cube (6x' + this.width + 'x' + this.height + ')');
    }

    // TODO: doc

  }, {
    key: '_setImage',
    value: function _setImage(gl, url, i) {
      var _this2 = this;

      var img = new Image();
      img.onload = function () {
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, _this2.idColor);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, img);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
      };
      img.src = url;
    }
  }, {
    key: 'setRT',
    value: function setRT(gl, url) {
      this._setImage(gl, url, 0);
    }
  }, {
    key: 'setLF',
    value: function setLF(gl, url) {
      this._setImage(gl, url, 1);
    }
  }, {
    key: 'setUP',
    value: function setUP(gl, url) {
      this._setImage(gl, url, 2);
    }
  }, {
    key: 'setDN',
    value: function setDN(gl, url) {
      this._setImage(gl, url, 3);
    }
  }, {
    key: 'setFT',
    value: function setFT(gl, url) {
      this._setImage(gl, url, 4);
    }
  }, {
    key: 'setBK',
    value: function setBK(gl, url) {
      this._setImage(gl, url, 5);
    }

    /** Delete the texture.
    * @param {WebGLRenderingContext} gl
    */

  }, {
    key: 'free',
    value: function free(gl) {
      gl.deleteTexture(this.idColor);

      console.log('Deleted texture cube');
    }

    // binding
    // -----------------------------------
    /** Bind texture to specified texture unit.
    * @param {WebGLRenderingContext} gl
    * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
    */

  }, {
    key: 'bind',
    value: function bind(gl, texUnit) {
      gl.activeTexture(gl.TEXTURE0 + texUnit);
      gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.idColor);
    }

    /** Unbind texture from specified texture unit (binds null).
    * @param {WebGLRenderingContext} gl
    * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
    */

  }, {
    key: 'unbind',
    value: function unbind(gl, texUnit) {
      gl.activeTexture(gl.TEXTURE0 + texUnit);
      gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    }
  }]);

  return WGLTextureCube;
}();

exports.WGLFramebufferObject = WGLFramebufferObject;
exports.WGLTexture = WGLTexture;
exports.WGLTextureCube = WGLTextureCube;

},{}],29:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ===================================
// GLSL ES preprocessor
// -----------------------------------
// see GLSLESPreprocessor.java
// TODO: doesn't process commented out lines correctly
// ===================================

var PREPROCESSOR_TOKEN = 'shared';
var REGEXP_ALPHANUM = '[A-Za-z_]+[A-Za-z0-9_]*';
var REGEXP_SOURCE_IDENTIFIER = '[ \t]*#[ \t]*' + PREPROCESSOR_TOKEN + '[ \t]+(' + REGEXP_ALPHANUM + ')';
var regexpSrcID = new RegExp(REGEXP_SOURCE_IDENTIFIER, 'i');

// a little regexp help:
// ? makes the preceding token optional
// (?:stuff) is a non-capturing group
var GLSL_TYPE_QUALIFIER = 'const|attribute|varying|invariant varying|uniform';
var GLSL_PRECISION_QUALIFIER = 'high_precision|medium_precision|low_precision';
var GLSL_TYPE_SPECIFIER_NO_PREC = 'void|float|int|bool|vec2|vec3|vec4|bvec2|bvec3|bvec4|ivec2|ivec3|ivec4|mat2|mat3|mat4|sampler2d|samplercube';
var REGEXP_GLSL_IDENTIFIER = ['[ \t]*', '(?:(?:' + GLSL_TYPE_QUALIFIER + ')[ \t]+)?', '(?:(?:' + GLSL_PRECISION_QUALIFIER + ')[ \t]+)?', '(?:' + GLSL_TYPE_SPECIFIER_NO_PREC + ')[ \t]+', '(' + REGEXP_ALPHANUM + ')', '.*'].join('');
var regexpGLSLID = new RegExp(REGEXP_GLSL_IDENTIFIER, 'i');

//-----------------------------------

var GLSLPreprocessor = function () {
  function GLSLPreprocessor() {
    _classCallCheck(this, GLSLPreprocessor);
  }

  _createClass(GLSLPreprocessor, null, [{
    key: 'preprocessLine',
    value: function preprocessLine(ret, line, srcIdentifiers, includedSrc, includedGLSL) {
      var match = void 0;

      // check for include identifier
      match = regexpSrcID.exec(line);
      if (match) {
        // get identifier; update included
        var identifier = match[1];
        var redundant = Object.prototype.hasOwnProperty.call(includedSrc, identifier);
        includedSrc[identifier] = true;

        // if not redundant, recurse
        if (!redundant) {
          GLSLPreprocessor.preprocess(ret, srcIdentifiers[identifier], srcIdentifiers, includedSrc, includedGLSL);
        }
        return;
      }

      // check for GLSL identifier
      match = regexpGLSLID.exec(line);
      if (match) {
        // get identifier; update included
        var _identifier = match[1];
        var _redundant = Object.prototype.hasOwnProperty.call(includedGLSL, _identifier);
        includedGLSL[_identifier] = true;

        // if not redundant, append the line
        if (!_redundant) {
          ret.push(line);
        }
        return;
      }

      // if neither matches, just append the line
      ret.push(line);
    }
  }, {
    key: 'preprocess',
    value: function preprocess(ret, src, srcIdentifiers, includedSrc, includedGLSL) {
      var encounteredLeftBrace = false;

      var lines = src.split('\n');
      for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        var bracePos = line.indexOf('{');
        if (bracePos >= 0) {
          encounteredLeftBrace = true;
        }
        if (encounteredLeftBrace) {
          ret.push(lines[i]);
        } else {
          GLSLPreprocessor.preprocessLine(ret, lines[i], srcIdentifiers, includedSrc, includedGLSL);
        }
      }
    }
  }]);

  return GLSLPreprocessor;
}();
//-----------------------------------

// srcPrimary: GLSL string
// srcIdentifiers: object with props = identifier, value = GLSL string


var preprocessGLSLES = function preprocessGLSLES(src, srcIdentifiers) {
  // properties' existence indicates inclusion of identifier (property name = identifier)
  var includedSrc = {};
  var includedGLSL = {};

  var ret = [];
  GLSLPreprocessor.preprocess(ret, src, srcIdentifiers, includedSrc, includedGLSL);
  return ret.join('\n');
};

exports.default = preprocessGLSLES;

},{}],30:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var DEBUG_OUTPUT = false;
var CLICK_THRESHOLD_PX = 5;

/** Collection of callbacks describing touch actions.
 * The 'buttons' parameter comes from the mousedown/mousemove/mouseup event.
 * @typedef {Object} TouchEvents
 * @prop {function} click - (buttons) => void
 * @prop {function} start - (buttons, p) => void
 * @prop {function} move - (buttons, p, q) => void
 * @prop {function} end - (buttons, p) => void
 */

/** Add mousedown/move/up listeners to an HTML element which make it easy to handle drags/clicks.
 * Non-click events with matching buttons always occur in blocks of (start, move..., end).
 * @param {HTMLElement} target - Context for addEventListener calls
 * @param {TouchEvents} touchEvents - Describes events
 */
var addFancyTouchListener = function addFancyTouchListener(target, touchEvents) {
  // Prepare defaults for touchEvents
  // ---------------------------------------
  var ensureFunction = function ensureFunction(f) {
    return typeof f === 'function' ? f : function () {};
  };
  var eventClick = ensureFunction(touchEvents.click);
  var eventStart = ensureFunction(touchEvents.start);
  var eventMove = ensureFunction(touchEvents.move);
  var eventEnd = ensureFunction(touchEvents.end);

  // Wrappers for debugging
  // ---------------------------------------
  var wrapEventClick = function wrapEventClick(buttons, p) {
    if (DEBUG_OUTPUT) {
      console.log('[CLICK ' + buttons + '] ' + p);
    }
    eventClick(buttons, p);
  };
  var wrapEventStart = function wrapEventStart(buttons, p) {
    if (DEBUG_OUTPUT) {
      console.log('[START ' + buttons + '] ' + p);
    }
    eventStart(buttons, p);
  };
  var wrapEventMove = function wrapEventMove(buttons, p, q) {
    if (DEBUG_OUTPUT) {
      console.log('[MOVE ' + buttons + '] ' + p + ' -> ' + q);
    }
    eventMove(buttons, p, q);
  };
  var wrapEventEnd = function wrapEventEnd(buttons, p) {
    if (DEBUG_OUTPUT) {
      console.log('[END ' + buttons + '] ' + p);
    }
    eventEnd(buttons, p);
  };

  // Helper for getting position
  // ---------------------------------------
  var _offsetClientPoint = function _offsetClientPoint(x, y) {
    var rect = target.getBoundingClientRect();
    return [x - rect.left, y - rect.top];
  };

  // Current state and basic event listeners
  // ---------------------------------------
  var lastMovePos = [0, 0];
  var downPos = [0, 0];
  var eventButtons = 0;
  var eventDidStart = false;

  target.addEventListener('mousedown', function (e) {
    var p = _offsetClientPoint(e.clientX, e.clientY);

    // End any event that has started
    if (eventDidStart) {
      wrapEventEnd(eventButtons, p);
    }

    if (!eventButtons) {
      // [no buttons] -> [some buttons]: wait to see if click
      eventButtons = e.buttons;
      eventDidStart = false;
      downPos = p;
    } else {
      // [some buttons] -> [other buttons]: immediately swap events
      eventButtons = e.buttons;
      eventDidStart = true;
      wrapEventStart(eventButtons, p);
      lastMovePos = p;
    }

    e.preventDefault();
  }, false);

  target.addEventListener('mouseup', function (e) {
    var p = _offsetClientPoint(e.clientX, e.clientY);

    // End any event that has started
    if (eventDidStart) {
      wrapEventEnd(eventButtons, p);
    }

    // [some buttons] -> [no buttons]: Click if event not started
    if (!e.buttons && !eventDidStart) {
      wrapEventClick(eventButtons, p);
    }

    // [some buttons] -> [other OR no buttons]: immediately swap events
    eventButtons = e.buttons;
    eventDidStart = true;
    wrapEventStart(eventButtons, p);
    lastMovePos = p;

    e.preventDefault();
  }, false);

  target.addEventListener('mousemove', function (e) {
    var p = _offsetClientPoint(e.clientX, e.clientY);

    if (eventDidStart) {
      // If event has started, just do the move event
      wrapEventMove(eventButtons, lastMovePos, p);
      lastMovePos = p;
    } else {
      // If event hasn't started, check for threshold
      var dx = p[0] - downPos[0];
      var dy = p[1] - downPos[1];
      if (Math.sqrt(dx * dx + dy * dy) > CLICK_THRESHOLD_PX) {
        eventDidStart = true;
        wrapEventStart(eventButtons, downPos);
        lastMovePos = downPos;
      }
    }

    e.preventDefault();
  }, false);
};

exports.default = addFancyTouchListener;

},{}]},{},[7])(7)
});