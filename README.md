# WebGL figures

View a live demo at [http://tdm.bz/js3d/test.htm](http://tdm.bz/js3d/test.htm) or read the JSDoc at [http://tdm.bz/js3d/docs](http://tdm.bz/js3d/docs).

![](http://tdm.bz/img/js3d/blowup-thumb.png)
![](http://tdm.bz/img/js3d/disk-thumb.png)
![](http://tdm.bz/img/js3d/hopf-thumb.png)
![](http://tdm.bz/img/js3d/tnb-thumb.png)
![](http://tdm.bz/img/js3d/torustext-thumb.png)
![](http://tdm.bz/img/js3d/translucence-thumb.png)

## Setup

Include `js3d.min.js`.  To specify the size of the figures, use:
```css
.fig-canvas{
  width: 768px;
  height: 768px;
}
```

## Example

#### 1. Define the geometry

First create some type of geometry.  In this example we will create a `GeomSurface`.  A geometry must be told to `generate` its vertices' position, color, and normal vectors.  Our geometry will depend on a variable `t`.

```javascript
var t = 0;

var graph = new JS3D.GeomSurface({
  n: [40,40],
  rangeIn: {u:[-2,2], v:[-2,2]},
  loopU: false, loopV: false,
  f: function(u,v){
    return [u,v, 0.5*Math.sin(t+u*v)];
  },
  color: function(u,v,p){
    return [0.5, 0.5, 0.5+p[2]];
  }
});

graph.generate();
var graphSO = graph.instance();
```

The last line creates a `SceneObject`.  This holds an `ObjectTransformation` and material properties for the geometry.  A single geometry can be used for multiple `SceneObject`s: either use the geometry to create multiple instances directly or use `SceneObject.prototype.clone`.  A `SceneObject` can be stuck to another using `SceneObject.prototype.stickTo`.

#### 2. Define the figure

In this function, `fig` is the `Figure` that will be created.  This function will pass on the geometry to WebGL using `Figure.prototype.addToScene` and specify what to do when animating (if anything; `animationLoop` is optional).  Animation is toggled by clicking on the figure.  The commented out line demonstrates how to modify `WGLRenderer` options.

```javascript
var onInitialize = function(gl, fig){
  fig.addToScene(gl, graphSO);
  //fig.renderer.enableShadows = false;
  
  fig.animationLoop = function(dt){
    t += dt*0.002;
    graph.generate();
    fig.update(gl, graphSO);
    
    graphSO.transformation.rotation.leftMultiplyAxisAngle([1,2,3], dt*0.001);
  };
};
```

After modifying `t` and updating the geometry, this new information must be passed on to WebGL.  This is done using `Figure.prototype.update` (which will automatically update any clones within the same `Figure`).  This is not necessary after modifying an `ObjectTransformation`, however.

#### 3. Initialize the figure

The only thing left to do is initialize WebGL and create the `Figure`:

```javascript
var fig = new JS3D.Figure(document.body, onInitialize);
```
* `document.body` specifies the HTMLElement to which the `Canvas` should be appended.

At this point the graph should show up.  Left click to toggle animation and right click to dump the canvas as a PNG.
