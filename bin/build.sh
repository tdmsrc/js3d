#!/bin/sh

echo "STEP 1/3: Bundling GLSL..."
node staticrc/src/staticrc.js src/glsl/*.glsl -o src/glsl/index.js

echo "STEP 2/3: Copying assets..."
cp src/font/*.png examples/img

mkdir -p build

echo "STEP 3a/3: Making js3d.js (transform and bundle)..."
browserify src/index.js --standalone JS3D -o build/js3d.js -t [ babelify --presets [ env ] ]
echo "STEP 3b/3: Making js3d.min.js (transform, uglify, then bundle)..."
browserify src/index.js --standalone JS3D -o build/js3d.min.js -t [ babelify --presets [ env ] ] -g [ uglifyify --no-sourcemap ]

echo "STEP 4: Copying JS to examples folder..."
#cp build/js3d.js examples/lib
cp build/js3d.min.js examples/lib

echo "Done!"
