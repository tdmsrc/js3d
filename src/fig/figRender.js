import { Camera } from '../wglcore/mathCam';

import {
  WGLFramebufferObject,
  WGLTexture,
} from '../wglcore/wglTexture';
import WGLQuad from '../wglcore/wglQuad';

import {
  TEXUNIT_QUAD_COLOR,
  TEXUNIT_QUAD_BLUR,
  WGLShaderQuadCopy,
  WGLShaderQuadBlur,
  WGLShaderQuadPost,
} from './figShaderQuad';
import {
  TEXUNIT_OBJ_SHADOW_MAP,
  TEXUNIT_OBJ_COLOR,
  WGLShaderPrepass,
  WGLShaderDepthColor,
  WGLShaderLightpass,
  WGLShaderLightpassShadow,
  WGLShaderDepth,
  WGLShaderWireframe,
} from './figShader';

import * as GLSL from '../glsl';
import preprocessGLSLES from '../wgltools/preprocessGLSLES';

// ===================================
// RENDERER
// ===================================

const LINE_WIDTH = 3;
const SUPERSAMPLE_FACTOR = 2;

const SHADOW_MAP_WIDTH = 512;
const SHADOW_MAP_HEIGHT = 512;

// TODO used for blur; ideally pegged to size of canvas
const MINI_BUFFER_WIDTH = 512;
const MINI_BUFFER_HEIGHT = 512;

/** The methods of the handler will be invoked repeatedly:
*   <br>For each {@link SceneObject}, call "filterSceneObject"; for each {@link WGLDrawable}
*   making up the geometry of the object, call "filterDrawable". If both are true, call "draw".
*   <br>The order is: "before" (once) -> loop{ "execSceneObject" (multiple) -> "execDrawable" (multiple) } -> "after" (once)
* @typedef {Object} WGLRenderer~sceneTraversalHandler
*
* @prop {Function} before - <code>function(){ ... }</code>;
*   <br>Guaranteed to be invoked before any execSceneObject, but may never be used during a traversal.
* @prop {Function} filterSceneObject - <code>function(sObj){ ... }</code>; "sObj" is a {@link SceneObject}.
*   <br>Return true if this object's geometry should be passed to prepareDrawable; return false otherwise.
* @prop {Function} filterDrawable - <code>function(drawable){ ... }</code>; "drawable" is a {@link WGLDrawable}.
*   <br>Return true if this drawable should be handled via "draw"; return false otherwise.
* @prop {Function} execSceneObject - <code>function(sObj){ ... }</code>; "sObj" is a {@link SceneObject}.
* @prop {Function} execDrawable - <code>function(drawable){ ... }</code>; "drawable" is a {@link WGLDrawable}
* @prop {Function} after - <code>function(){ ... }</code>;
*   <br>Guaranteed to be invoked after every execDrawable has been completed, but may never be used during a traversal.
*/

/** Renderer for a scene.
*   <br>The properties below define what is rendered; these can be modified directly.
*   <br>Call {@link WGLRenderer#initialize} before rendering or resizing.
*
* @prop {number[]} backgroundColor - Clear color (as [r,g,b,a], no premultiplied alpha)
* @prop {Light[]} lights - Unlimited light sources
* @prop {Camera} camera
* @prop {boolean} enableTranslucent - If false, skip any {@link SceneObject} marked as translucent
* @prop {boolean} enableShadows - If false, override individual {@link Light} options and disable all shadows
* @prop {number} blurAmount - How much to blend a blurred layer into the final image
* @prop {number} colorAdjustBrightness
* @prop {number} colorAdjustContrast
* @prop {number} colorAdjustSaturation
* @prop {Function} sceneTraversal - Function which performs a traversal of the scene, utilizing the handler
*   <br>(<code>function(handler){ ... }</code>; "handler" is a {@link WGLRenderer~sceneTraversalHandler})
*/
class WGLRenderer {
  constructor() {
    // options, can be modified/replaced directly without incident
    this.lights = [];
    this.camera = new Camera();

    this.backgroundColor = [0, 0, 0, 0]; // no premultiplied alpha

    this.blurAmount = 0.3;
    this.colorAdjustBrightness = 0.0;
    this.colorAdjustContrast = 1.0;
    this.colorAdjustSaturation = 1.0;

    this.enableShadows = true;
    this.enableTranslucent = true;

    // handler: see doc for WGLRenderer~sceneTraversalHandler
    this.sceneTraversal = (handler) => {};
  }

  // initialization
  // -----------------------------------
  /** Get context for a canvas and prepare necessary shaders, textures, etc.
  * @param {HTMLCanvasElement} canvas
  * @return {WebGLRenderingContext}
  */
  initialize(canvas) {
    // get context
    let gl = null;

    const opts = {
      premultipliedAlpha: true, // is default, but make sure
      preserveDrawingBuffer: true, // allow toDataURL (snapshots), etc
    };
    try {
      gl = canvas.getContext('webgl', opts) || canvas.getContext('experimental-webgl', opts);
    } catch (e) {
      console.error(e);
    }

    if (!gl) {
      console.error('Unable to obtain WebGLRenderingContext!');
      return null;
    }

    // proprocess GLSL
    const texIncludes = {
      lighting: GLSL.inclLighting,
      packDepthColor: GLSL.inclPackDepthColor,
      isShadowedPCF: GLSL.inclIsShadowedPCF,
      getColorVert: GLSL.inclGetColorTexVert,
      getColorFrag: GLSL.inclGetColorTexFrag,
    };

    const colorIncludes = {
      lighting: GLSL.inclLighting,
      packDepthColor: GLSL.inclPackDepthColor,
      isShadowedPCF: GLSL.inclIsShadowedPCF,
      getColorVert: GLSL.inclGetColorVert,
      getColorFrag: GLSL.inclGetColorFrag,
    };

    // no tex version (color attribute only)
    this.shaderPrepass = new WGLShaderPrepass(
      gl, false,
      preprocessGLSLES(GLSL.prepassVert, colorIncludes),
      preprocessGLSLES(GLSL.prepassFrag, colorIncludes)
    );

    this.shaderDepth = new WGLShaderDepth(
      gl, false,
      preprocessGLSLES(GLSL.depthVert, colorIncludes),
      preprocessGLSLES(GLSL.depthFrag, colorIncludes)
    );
    this.shaderDepthColor = new WGLShaderDepthColor(
      gl, false,
      preprocessGLSLES(GLSL.depthColorVert, colorIncludes),
      preprocessGLSLES(GLSL.depthColorFrag, colorIncludes)
    );

    this.shaderLightpass = new WGLShaderLightpass(
      gl, false,
      preprocessGLSLES(GLSL.lightpassVert, colorIncludes),
      preprocessGLSLES(GLSL.lightpassFrag, colorIncludes)
    );
    this.shaderLightpassShadow = new WGLShaderLightpassShadow(
      gl, false,
      preprocessGLSLES(GLSL.lightpassShadowVert, colorIncludes),
      preprocessGLSLES(GLSL.lightpassShadowFrag, colorIncludes)
    );

    // tex version
    this.shaderPrepassTex = new WGLShaderPrepass(
      gl, true,
      preprocessGLSLES(GLSL.prepassVert, texIncludes),
      preprocessGLSLES(GLSL.prepassFrag, texIncludes)
    );

    this.shaderDepthTex = new WGLShaderDepth(
      gl, true,
      preprocessGLSLES(GLSL.depthVert, texIncludes),
      preprocessGLSLES(GLSL.depthFrag, texIncludes)
    );
    this.shaderDepthColorTex = new WGLShaderDepthColor(
      gl, true,
      preprocessGLSLES(GLSL.depthColorVert, texIncludes),
      preprocessGLSLES(GLSL.depthColorFrag, texIncludes)
    );

    this.shaderLightpassTex = new WGLShaderLightpass(
      gl, true,
      preprocessGLSLES(GLSL.lightpassVert, texIncludes),
      preprocessGLSLES(GLSL.lightpassFrag, texIncludes)
    );
    this.shaderLightpassShadowTex = new WGLShaderLightpassShadow(
      gl, true,
      preprocessGLSLES(GLSL.lightpassShadowVert, texIncludes),
      preprocessGLSLES(GLSL.lightpassShadowFrag, texIncludes)
    );

    // create shaders
    this.shaderWireframe = new WGLShaderWireframe(
      gl,
      GLSL.wireframeVert,
      GLSL.wireframeFrag
    );

    this.shaderQuadCopy = new WGLShaderQuadCopy(
      gl, 'quad copy',
      GLSL.quadVert,
      GLSL.quadFragCopy
    );
    this.shaderQuadBlurH = new WGLShaderQuadBlur(
      gl, 'quad blur H',
      GLSL.quadVert,
      GLSL.quadFragBlurH
    );
    this.shaderQuadBlurV = new WGLShaderQuadBlur(
      gl, 'quad blur V',
      GLSL.quadVert,
      GLSL.quadFragBlurV
    );
    this.shaderQuadPost = new WGLShaderQuadPost(
      gl, 'quad post-processing',
      GLSL.quadVert,
      GLSL.quadFragPost
    );

    // create FBO and texture(s)
    this.fbo = new WGLFramebufferObject(gl);
    this.texRender = new WGLTexture(gl, gl.canvas.width, gl.canvas.height);
    this.texBlend = new WGLTexture(gl, gl.canvas.width, gl.canvas.height);
    this.texShadowMap = new WGLTexture(gl, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT, false);

    this.texMiniFront = new WGLTexture(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
    this.texMiniBack = new WGLTexture(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);

    // create quad
    this.quad = new WGLQuad(gl);


    // TODO
    // ---------------------------------------
    this.texTest = new WGLTexture(gl, gl.canvas.width, gl.canvas.height);
    this.texTest.setImage(gl, 'img/glfont.png', () => {
      this.render(gl);
    });
    //---------------------------------------


    // return context
    return gl;
  }

  /** Resize canvas and drawing buffer to specified size.
  *   <br>Also changes the aspect ratio of the currently set {@link Camera}.
  * @param {WebGLRenderingContext} gl
  * @param {number} w - New canvas width
  * @param {number} h - New canvas height
  */
  setSize(gl, w, h) {
    gl.canvas.width = w;
    gl.canvas.height = h;

    this.camera.setAspect(w / h);

    this.texRender.resize(gl, SUPERSAMPLE_FACTOR * w, SUPERSAMPLE_FACTOR * h);
    this.texBlend.resize(gl, w, h);
  }

  /** Resize canvas and drawing buffer if clientWidth/clientHeight do not match.
  * @param {WebGLRenderingContext} gl
  * @return {boolean} - Returns true iff a resize occurred
  * @see {@link WGLRenderer#setSize}
  */
  resize(gl) {
    const w = gl.canvas.clientWidth;
    const h = gl.canvas.clientHeight;

    const resizeW = (w > 0) && (gl.canvas.width !== w);
    const resizeH = (h > 0) && (gl.canvas.height !== h);

    // this will not trigger if w==0 and h==0, which
    // seems to happen if the canvas is hidden
    if (resizeW || resizeH) {
      this.setSize(gl, w, h);
      return true;
    }
    return false;
  }


  // QUAD DRAWING/POST-PROCESSING
  // ==================================================
  _drawQuadInitialize(gl, texSrc, shader) {
    shader.use(gl);
    shader.prepareVertexAttribs(gl, this.quad);
    texSrc.bind(gl, TEXUNIT_QUAD_COLOR);
  }

  _drawQuadFinish(gl, texSrc, shader) {
    this.quad.drawTriangles(gl);
    texSrc.unbind(gl, TEXUNIT_QUAD_COLOR);
    shader.unuse(gl);
  }

  // copy texSrc onto currently bound write texture
  _drawQuadCopy(gl, texSrc) {
    this._drawQuadInitialize(gl, texSrc, this.shaderQuadCopy);
    this._drawQuadFinish(gl, texSrc, this.shaderQuadCopy);
  }


  // blur texture "texSrc" into "texMiniBack"
  _performBlur(gl, texSrc) {
    // copy texSrc into texMiniBack
    this.texMiniBack.bindWrite(gl);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    this._drawQuadCopy(gl, texSrc);

    this.texMiniBack.unbindWrite(gl);

    // copy texMiniBack to texMiniFront using blur H
    this.texMiniFront.bindWrite(gl);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    this._drawQuadInitialize(gl, this.texMiniBack, this.shaderQuadBlurH);
    this.shaderQuadBlurH.setTextureSize(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
    this._drawQuadFinish(gl, this.texMiniBack, this.shaderQuadBlurH);

    this.texMiniFront.unbindWrite(gl);

    // copy texMiniFront to texMiniBack using blur V
    this.texMiniBack.bindWrite(gl);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    this._drawQuadInitialize(gl, this.texMiniFront, this.shaderQuadBlurV);
    this.shaderQuadBlurV.setTextureSize(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
    this._drawQuadFinish(gl, this.texMiniFront, this.shaderQuadBlurV);

    this.texMiniBack.unbindWrite(gl);
  }
  // ==================================================


  // RENDERING CORE
  // ==================================================

  /** Render a scene to the default framebuffer.
  * @param {WebGLRenderingContext} gl
  */
  render(gl) {
    // everything is two-sided
    gl.disable(gl.CULL_FACE);
    gl.lineWidth(LINE_WIDTH);

    this.fbo.bind(gl);

    // opaque pass
    // ----------------------
    // tex cleared with backgroundColor
    // render opaque objects using premultiplied alpha
    this._renderOpaque(gl, this.texRender);

    // copy texRender -> texBlend
    this.texBlend.bindWrite(gl);
    gl.disable(gl.DEPTH_TEST);

    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    this._drawQuadCopy(gl, this.texRender);

    this.texBlend.unbindWrite(gl);

    // translucent pass
    // ----------------------
    if (this.enableTranslucent) {
      // render to texRender; use premultiplied alpha
      // texRender cleared with (0,0,0,0), but zbuffer maintained
      this._renderTranslucent(gl, this.texRender);

      // blend tex into texBlend
      this.texBlend.bindWrite(gl);
      gl.disable(gl.DEPTH_TEST);

      gl.enable(gl.BLEND);
      gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
      this._drawQuadCopy(gl, this.texRender);
      gl.disable(gl.BLEND);

      this.texBlend.unbindWrite(gl);
    }

    // copy texBlend to canvas
    // ----------------------
    // draw blur -> texMiniBack
    this._performBlur(gl, this.texBlend);

    // prepare to draw to canvas
    this.fbo.unbind(gl);
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

    // copy texBlend -> canvas using QuadPost
    gl.disable(gl.DEPTH_TEST);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    this._drawQuadInitialize(gl, this.texBlend, this.shaderQuadPost);
    this.shaderQuadPost.setOptions(
      gl, this.blurAmount,
      this.colorAdjustBrightness,
      this.colorAdjustContrast,
      this.colorAdjustSaturation
    );

    this.texMiniBack.bind(gl, TEXUNIT_QUAD_BLUR);
    this._drawQuadFinish(gl, this.texBlend, this.shaderQuadPost);
    this.texMiniBack.unbind(gl, TEXUNIT_QUAD_BLUR);
  }

  _renderOpaque(gl) {
    // prepare: clear and prepass
    // ---------------------------
    // start render to texRender
    this.texRender.bindWrite(gl);

    // everything rendered here needs to use premultiplied alpha
    const a = this.backgroundColor[3];
    gl.clearColor(
      a * this.backgroundColor[0],
      a * this.backgroundColor[1],
      a * this.backgroundColor[2],
      a
    );
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    // solid objects
    gl.enable(gl.DEPTH_TEST);
    this._renderPrepass(gl, false);

    // stop render to texRender
    this.texRender.unbindWrite(gl);
    // ---------------------------

    // solid objects
    for (let i = 0; i < this.lights.length; i++) {
      const light = this.lights[i];

      if (light.shadows && this.enableShadows) {
        this._renderShadowMapDepth(gl, light, false);
        if (this.enableTranslucent) {
          this._renderShadowMapColor(gl, light);
        }
        this._renderLightpassShadow(gl, light, false);
      } else {
        this._renderLightpassNoShadow(gl, light, false);
      }
    }

    // wireframe objects
    this._renderWireframe(gl, false);
  }

  _renderTranslucent(gl) {
    // prepare: clear and prepass
    // ---------------------------
    // start render to texRender
    this.texRender.bindWrite(gl);

    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    // solid objects
    gl.enable(gl.DEPTH_TEST);
    this._renderPrepass(gl, true);

    // stop render to texRender
    this.texRender.unbindWrite(gl);
    // ---------------------------

    // solid objects
    for (let i = 0; i < this.lights.length; i++) {
      const light = this.lights[i];

      if (light.shadows && this.enableShadows) {
        this._renderShadowMapDepth(gl, light, true);
        this._renderLightpassShadow(gl, light, true);
      } else {
        this._renderLightpassNoShadow(gl, light, true);
      }
    }

    // wireframe objects
    this._renderWireframe(gl, true);
  }


  // RENDER PASSES/SCENE TRAVERSALS
  // ==================================================
  // prepass: no need to bind/unbind texture
  // ----------------------------------------
  _renderPrepass(gl, translucent) {
    const traversal = (hasTexture, shader) => ({
      // filter
      filterSceneObject: sObj =>
        sObj.solidVisible && sObj.translucent === translucent,
      filterDrawable: drawable =>
        drawable.hasTexture === hasTexture,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, this.camera);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
        shader.setEmissiveUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        drawable.drawTriangles(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    gl.depthFunc(gl.LESS);

    // TODO color vs tex
    // ---------------------------
    this.sceneTraversal(traversal(false, this.shaderPrepass));

    this.texTest.bind(gl, TEXUNIT_OBJ_COLOR);
    this.sceneTraversal(traversal(true, this.shaderPrepassTex));
    this.texTest.unbind(gl, TEXUNIT_OBJ_COLOR);
    // ---------------------------
  }

  // wireframe pass
  // ----------------------------------------
  _renderWireframe(gl, translucent) {
    const traversal = shader => ({
      // filter
      filterSceneObject: sObj =>
        sObj.wireframeVisible && sObj.translucent === translucent,
      filterDrawable: drawable =>
        true,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, this.camera);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
        shader.setWireframeUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        drawable.drawLines(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    // start render to texRender
    this.texRender.bindWrite(gl);

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

    gl.depthFunc(gl.LEQUAL);
    gl.depthMask(false);

    this.sceneTraversal(traversal(this.shaderWireframe));

    gl.depthMask(true);
    gl.disable(gl.BLEND);

    // stop render to texRender
    this.texRender.unbindWrite(gl);
  }

  // render objects; single light, no shadow mapping
  // ----------------------------------------
  _renderLightpassNoShadow(gl, light, translucent) {
    const traversal = (hasTexture, shader) => ({
      // filter
      filterSceneObject: sObj =>
        sObj.solidVisible && sObj.translucent === translucent,
      filterDrawable: drawable =>
        drawable.hasTexture === hasTexture,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, this.camera);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        shader.setLightUniforms(gl, light);
        drawable.drawTriangles(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    // start render to texRender
    this.texRender.bindWrite(gl);

    gl.depthFunc(gl.LEQUAL);

    gl.enable(gl.BLEND);
    gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ZERO, gl.ONE);

    // TODO color vs tex
    // ---------------------------
    this.sceneTraversal(traversal(false, this.shaderLightpass));

    this.texTest.bind(gl, TEXUNIT_OBJ_COLOR);
    this.sceneTraversal(traversal(true, this.shaderLightpassTex));
    this.texTest.unbind(gl, TEXUNIT_OBJ_COLOR);
    // ---------------------------

    gl.disable(gl.BLEND);

    // stop render to texRender
    this.texRender.unbindWrite(gl);
  }

  // render objects; single light with shadow mapping
  // ----------------------------------------
  _renderLightpassShadow(gl, light, translucent) {
    const traversal = (hasTexture, shader) => ({
      // filter
      filterSceneObject: sObj =>
        sObj.solidVisible && sObj.translucent === translucent,
      filterDrawable: drawable =>
        drawable.hasTexture === hasTexture,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, this.camera);
        shader.setLightCameraUniforms(gl, light);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        shader.setLightUniforms(gl, light);
        drawable.drawTriangles(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    // start render to texRender
    this.texRender.bindWrite(gl);

    gl.depthFunc(gl.LEQUAL);

    gl.enable(gl.BLEND);
    gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ZERO, gl.ONE);

    this.texShadowMap.bind(gl, TEXUNIT_OBJ_SHADOW_MAP);

    // TODO color vs tex
    // ---------------------------
    this.sceneTraversal(traversal(false, this.shaderLightpassShadow));

    this.texTest.bind(gl, TEXUNIT_OBJ_COLOR);
    this.sceneTraversal(traversal(true, this.shaderLightpassShadowTex));
    this.texTest.unbind(gl, TEXUNIT_OBJ_COLOR);
    // ---------------------------

    this.texShadowMap.unbind(gl, TEXUNIT_OBJ_SHADOW_MAP);

    gl.disable(gl.BLEND);

    // stop render to texRender
    this.texRender.unbindWrite(gl);
  }

  _renderShadowMapDepth(gl, light, translucent) {
    const traversal = (hasTexture, shader) => ({
      // filter
      filterSceneObject: (sObj) => {
        if (!sObj.solidVisible) { return false; }
        if (!sObj.castsShadows) { return false; }

        // render all if translucent renderpass, render only solid if solid renderpass
        if (translucent) { return true; }
        return (sObj.translucent === translucent);
      },
      filterDrawable: drawable =>
        drawable.hasTexture === hasTexture,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, light.camera);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        drawable.drawTriangles(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    // start render to texShadowMap
    this.texShadowMap.bindWrite(gl);

    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    gl.colorMask(true, true, false, false);

    // TODO color vs tex
    // ---------------------------
    this.sceneTraversal(traversal(false, this.shaderDepth));

    this.texTest.bind(gl, TEXUNIT_OBJ_COLOR);
    this.sceneTraversal(traversal(true, this.shaderDepthTex));
    this.texTest.unbind(gl, TEXUNIT_OBJ_COLOR);
    // ---------------------------

    gl.colorMask(true, true, true, true);

    // stop render to texShadowMap
    this.texShadowMap.unbindWrite(gl);
  }

  _renderShadowMapColor(gl, light) {
    const traversal = (hasTexture, shader) => ({
      // filter
      filterSceneObject: sObj =>
        sObj.solidVisible && sObj.castsShadows && sObj.translucent,
      filterDrawable: drawable =>
        drawable.hasTexture === hasTexture,

      // draw
      before: () => {
        shader.use(gl);
        shader.setCameraUniforms(gl, light.camera);
      },
      execSceneObject: (sObj) => {
        shader.setObjectUniforms(gl, sObj);
      },
      execDrawable: (drawable) => {
        shader.prepareVertexAttribs(gl, drawable);
        drawable.drawTriangles(gl);
      },
      after: () => {
        shader.unuse(gl);
      },
    });

    // start render to texShadowMap
    this.texShadowMap.bindWrite(gl);

    gl.colorMask(false, false, true, true);

    // TODO color vs tex
    // ---------------------------
    this.sceneTraversal(traversal(false, this.shaderDepthColor));

    this.texTest.bind(gl, TEXUNIT_OBJ_COLOR);
    this.sceneTraversal(traversal(true, this.shaderDepthColorTex));
    this.texTest.unbind(gl, TEXUNIT_OBJ_COLOR);
    // ---------------------------

    gl.colorMask(true, true, true, true);

    // stop render to texShadowMap
    this.texShadowMap.unbindWrite(gl);
  }
  // ==================================================
}

export default WGLRenderer;
