import WGLShaderProgram from '../wglcore/wglShader';

const VERTEX_POSITION_ATTRIBUTE = 0;
const VERTEX_TEX_ATTRIBUTE = 1;

const TEXUNIT_QUAD_COLOR = 0;
const TEXUNIT_QUAD_BLUR = 1;

// ===================================
// SHADER PROGRAM: QUAD COMMON
// ===================================

/** Shader program for use with {@link WGLQuad}.
*
* @extends WGLShaderProgram
*/
class WGLShaderQuadCommon extends WGLShaderProgram {
  _setVertexAttributes(gl) {
    this._addVertexAttribute(gl, VERTEX_POSITION_ATTRIBUTE, 'vertexPosition');
    this._addVertexAttribute(gl, VERTEX_TEX_ATTRIBUTE, 'vertexTex');
  }

  /** Specify a quad's vertex attributes.  Use this before {@link WGLQuad#draw}.
  * @param {WebGLRenderingContext} gl
  * @param {WGLQuad} quad
  */
  prepareVertexAttribs(gl, quad) {
    gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufPosition);
    gl.vertexAttribPointer(VERTEX_POSITION_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufTex);
    gl.vertexAttribPointer(VERTEX_TEX_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
  }
}

// ===================================
// SHADER PROGRAM: QUAD COPY
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/
class WGLShaderQuadCopy extends WGLShaderQuadCommon {
  constructor(gl, description, shaderVert, shaderFrag) {
    super();

    this._initialize(gl, description, shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    uniformNames.push('texColor');
  }

  use(gl) {
    super.use(gl);

    this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
  }
}

// ===================================
// SHADER PROGRAM: QUAD BLUR V/H
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/
class WGLShaderQuadBlur extends WGLShaderQuadCommon {
  constructor(gl, description, shaderVert, shaderFrag) {
    super();

    this._initialize(gl, description, shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    uniformNames.push('texColor');

    // TODO
    uniformNames.push('texSize');
  }

  use(gl) {
    super.use(gl);

    this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
  }

  // TODO
  /** Set uniforms associated with the source texture's pixel size.
  * @param {WebGLRenderingContext} gl
  * @param {number} texWidth
  * @param {number} texHeight
  */
  setTextureSize(gl, texWidth, texHeight) {
    this.setU2i(gl, 'texSize', texWidth, texHeight);
  }
}

// ===================================
// SHADER PROGRAM: QUAD POST-PROCESSING
// ===================================

/** See {@link WGLShaderProgram#_initialize}
*
* @extends WGLShaderQuadCommon
*/
class WGLShaderQuadPost extends WGLShaderQuadCommon {
  constructor(gl, description, shaderVert, shaderFrag) {
    super();

    this._initialize(gl, description, shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    uniformNames.push(
      'texColor', 'texBlur',
      'bloomFactor',
      'colorAdjustBrightness', 'colorAdjustContrast', 'colorAdjustSaturation'
    );
  }

  use(gl) {
    super.use(gl);

    this.setU1i(gl, 'texColor', TEXUNIT_QUAD_COLOR);
    this.setU1i(gl, 'texBlur', TEXUNIT_QUAD_BLUR);
  }

  /** Set uniforms associated with post-processing effects.
  * @param {WebGLRenderingContext} gl
  * @param {number} bloomFactor
  * @param {number} colorAdjustBrightness
  * @param {number} colorAdjustContrast
  * @param {number} colorAdjustSaturation
  */
  setOptions(gl, bloomFactor, colorAdjustBrightness, colorAdjustContrast, colorAdjustSaturation) {
    this.setU1f(gl, 'bloomFactor', bloomFactor);
    this.setU1f(gl, 'colorAdjustBrightness', colorAdjustBrightness);
    this.setU1f(gl, 'colorAdjustContrast', colorAdjustContrast);
    this.setU1f(gl, 'colorAdjustSaturation', colorAdjustSaturation);
  }
}

export {
  TEXUNIT_QUAD_COLOR,
  TEXUNIT_QUAD_BLUR,
  WGLShaderQuadCopy,
  WGLShaderQuadBlur,
  WGLShaderQuadPost,
};
