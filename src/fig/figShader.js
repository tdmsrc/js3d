import ObjectTransformation from '../wglcore/mathObjTrans';
import WGLShaderProgram from '../wglcore/wglShader';

const VERTEX_POSITION_ATTRIBUTE = 0;
const VERTEX_TEX_ATTRIBUTE = 1;
const VERTEX_COLOR_ATTRIBUTE = 2;
const VERTEX_NORMAL_ATTRIBUTE = 3;

const TEXUNIT_OBJ_SHADOW_MAP = 0;
const TEXUNIT_OBJ_COLOR = 1;

const TEMP_OBJTRANS = new ObjectTransformation();

// ===================================
// COMMON SHADER
// common superclass for the default shaders
// ===================================

/** Abstract shader program for use with {@link SceneObject} and {@link WGLDrawable}.
* <br>(When extending: call _setEnabledAttributes and _initialize)
* @see {@link WGLShaderPrepass}
* @see {@link WGLShaderDepth}
* @see {@link WGLShaderLightpass}
* @see {@link WGLShaderLightpassShadow}
* @see {@link WGLShaderWireframe}
*
* @extends WGLShaderProgram
*/
class WGLShaderCommon extends WGLShaderProgram {
  /**
  * @param {boolean} useTex - Indicates that the texture coord vertex attribute will be used
  * @param {boolean} useColor - Indicates that the color vertex attribute will be used
  * @param {boolean} useNormal - Indicates that the normal vertex attribute will be used
  */
  _setEnabledAttributes(useTex, useColor, useNormal) {
    this._useTex = useTex;
    this._useColor = useColor;
    this._useNormal = useNormal;
  }

  _setVertexAttributes(gl) {
    this._addVertexAttribute(gl, VERTEX_POSITION_ATTRIBUTE, 'vertexPosition');

    if (this._useTex) {
      this._addVertexAttribute(gl, VERTEX_TEX_ATTRIBUTE, 'vertexTex');
    }
    if (this._useColor) {
      this._addVertexAttribute(gl, VERTEX_COLOR_ATTRIBUTE, 'vertexColor');
    }
    if (this._useNormal) {
      this._addVertexAttribute(gl, VERTEX_NORMAL_ATTRIBUTE, 'vertexNormal');
    }
  }

  /** Specify a drawable's vertex attributes.
  *   Use this before {@link WGLDrawable#drawTriangles} or {@link WGLDrawable#drawLines}.
  * @param {WebGLRenderingContext} gl
  * @param {WGLDrawable} drawable
  */
  prepareVertexAttribs(gl, drawable) {
    gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufPosition);
    gl.vertexAttribPointer(VERTEX_POSITION_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);

    if (this._useTex) {
      gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufTex);
      gl.vertexAttribPointer(VERTEX_TEX_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
    }
    if (this._useColor) {
      gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufColor);
      gl.vertexAttribPointer(VERTEX_COLOR_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);
    }
    if (this._useNormal) {
      gl.bindBuffer(gl.ARRAY_BUFFER, drawable.bufNormal);
      gl.vertexAttribPointer(VERTEX_NORMAL_ATTRIBUTE, 3, gl.FLOAT, false, 0, 0);
    }
  }

  _getUniformNames(uniformNames) {
    uniformNames.push(
      'camPosition', 'camProjection', 'camRotation',
      'objRotation', 'objTranslation', 'objOffset',
      'alpha'
    );
  }

  /** Set uniforms associated with a {@link Camera}.
  * @param {WebGLRenderingContext} gl
  * @param {Camera} camera
  */
  setCameraUniforms(gl, camera) {
    this.setUVec3(gl, 'camPosition', camera.position);
    this.setUMat4(gl, 'camProjection', camera.matrix);
    this.setUMat3(gl, 'camRotation', camera.rotation.matrix);
  }

  /** Set basic uniforms associated with a {@link SceneObject}.
  * @param {WebGLRenderingContext} gl
  * @param {SceneObject} object
  */
  setObjectUniforms(gl, object) {
    // object transformation
    let t = object.transformation;
    if (t.parent !== null) {
      TEMP_OBJTRANS.copyFlat(t);
      t = TEMP_OBJTRANS;
    }

    this.setUMat3(gl, 'objRotation', t.rotation.matrix);
    this.setUVec3(gl, 'objTranslation', t.translation);
    this.setUVec3(gl, 'objOffset', t.offset);

    this.setU1f(gl, 'alpha', object.translucent ? object.alpha : 1);
  }
}

// ===================================
// PREPASS SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for emissive/ambient lighting; normals, light properties, and some material properties are not used.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderPrepass extends WGLShaderCommon {
  constructor(gl, hasTexture, shaderVert, shaderFrag) {
    super();

    this.hasTexture = hasTexture;
    this._setEnabledAttributes(hasTexture, !hasTexture, false);
    this._initialize(gl, 'prepass', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    uniformNames.push('emissiveCoefficient', 'emissiveColor');
    if (this.hasTexture) { uniformNames.push('texColor'); }
  }

  /** Set emissive coefficient uniform for a {@link SceneObject}.
  * @param {WebGLRenderingContext} gl
  * @param {SceneObject} object
  */
  setEmissiveUniforms(gl, object) {
    this.setU1f(gl, 'emissiveCoefficient', object.emissiveCoefficient);
    this.setUVec3(gl, 'emissiveColor', object.emissiveColor);
  }

  use(gl) {
    super.use(gl);

    if (this.hasTexture) {
      this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
    }
  }
}

// ===================================
// DEPTH SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for writing depth only; no lighting or material properties are used.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderDepth extends WGLShaderCommon {
  constructor(gl, hasTexture, shaderVert, shaderFrag) {
    super();

    this.hasTexture = hasTexture;
    this._setEnabledAttributes(hasTexture, !hasTexture, false);
    this._initialize(gl, 'depth', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    if (this.hasTexture) { uniformNames.push('texColor'); }
  }

  use(gl) {
    super.use(gl);

    if (this.hasTexture) {
      this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
    }
  }
}

// ===================================
// DEPTH COLOR SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for writing translucent layer on top of existing shadow map.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderDepthColor extends WGLShaderCommon {
  constructor(gl, hasTexture, shaderVert, shaderFrag) {
    super();

    this.hasTexture = hasTexture;
    this._setEnabledAttributes(hasTexture, !hasTexture, false);
    this._initialize(gl, 'depth/color', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    if (this.hasTexture) { uniformNames.push('texColor'); }
  }

  use(gl) {
    super.use(gl);

    if (this.hasTexture) {
      this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
    }
  }
}

// ===================================
// LIGHTPASS (NO SHADOW) SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for rendering an object lit by a "shadows = false" {@link Light}.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderLightpass extends WGLShaderCommon {
  constructor(gl, hasTexture, shaderVert, shaderFrag) {
    super();

    this.hasTexture = hasTexture;
    this._setEnabledAttributes(hasTexture, !hasTexture, true);
    this._initialize(gl, 'lightpass', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    uniformNames.push(
      'lightPosition', 'lightColor', 'lightAttenuation',
      'diffuseCoefficient', 'specularCoefficient', 'specularExponent'
    );

    if (this.hasTexture) { uniformNames.push('texColor'); }
  }

  /** Set uniforms associated with a {@link Light}.
  * @param {WebGLRenderingContext} gl
  * @param {Light} light
  */
  setLightUniforms(gl, light) {
    this.setUVec3(gl, 'lightPosition', light.camera.position);
    this.setUVec3(gl, 'lightColor', light.color);
    this.setUVec3(gl, 'lightAttenuation', light.attenuation);
  }

  setObjectUniforms(gl, object) {
    super.setObjectUniforms(gl, object);

    this.setU1f(gl, 'diffuseCoefficient', object.diffuseCoefficient);
    this.setU1f(gl, 'specularCoefficient', object.specularCoefficient);
    this.setU1f(gl, 'specularExponent', object.specularExponent);
  }

  use(gl) {
    super.use(gl);

    if (this.hasTexture) {
      this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
    }
  }
}

// ===================================
// LIGHTPASS (SHADOW) SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used for rendering an object lit by a "shadows = true" {@link Light}.
* @param {WebGLRenderingContext} gl
* @param {boolean} hasTexture - Specify whether to use the texture coordinate vertex attribute
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderLightpassShadow extends WGLShaderCommon {
  constructor(gl, hasTexture, shaderVert, shaderFrag) {
    super();

    this.hasTexture = hasTexture;
    this._setEnabledAttributes(hasTexture, !hasTexture, true);
    this._initialize(gl, 'lightpass shadow', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    uniformNames.push(
      'lightPosition', 'lightColor', 'lightAttenuation',
      'diffuseCoefficient', 'specularCoefficient', 'specularExponent',
      'texShadowMap', 'lightCamProjection', 'lightCamRotation'
    );

    if (this.hasTexture) { uniformNames.push('texColor'); }
  }

  /** Set uniforms associated with a {@link Light}.
  * @param {WebGLRenderingContext} gl
  * @param {Light} light
  */
  setLightUniforms(gl, light) {
    this.setUVec3(gl, 'lightPosition', light.camera.position);
    this.setUVec3(gl, 'lightColor', light.color);
    this.setUVec3(gl, 'lightAttenuation', light.attenuation);
  }

  setObjectUniforms(gl, object) {
    super.setObjectUniforms(gl, object);

    this.setU1f(gl, 'diffuseCoefficient', object.diffuseCoefficient);
    this.setU1f(gl, 'specularCoefficient', object.specularCoefficient);
    this.setU1f(gl, 'specularExponent', object.specularExponent);
  }

  use(gl) {
    super.use(gl);

    this.setU1i(gl, 'texShadowMap', TEXUNIT_OBJ_SHADOW_MAP);
    if (this.hasTexture) {
      this.setU1i(gl, 'texColor', TEXUNIT_OBJ_COLOR);
    }
  }

  /** Set uniforms associated with the {@link Camera} for the {@link Light} used to draw the shadow map.
  * @param {WebGLRenderingContext} gl
  * @param {Light} light
  */
  setLightCameraUniforms(gl, light) {
    this.setUMat4(gl, 'lightCamProjection', light.camera.matrix);
    this.setUMat3(gl, 'lightCamRotation', light.camera.rotation.matrix);
  }
}

// ===================================
// WIREFRAME SHADER
// ===================================

/** Shader program for use with {@link SceneObject} and {@link WGLDrawable}.
*   <br>Used to draw wireframe objects; normals, light properties, and some material properties are not used.
* @param {WebGLRenderingContext} gl
* @param {string} shaderVert - Vertex shader source
* @param {string} shaderFrag - Fragment shader source
*
* @extends WGLShaderCommon
*/
class WGLShaderWireframe extends WGLShaderCommon {
  constructor(gl, shaderVert, shaderFrag) {
    super();

    this._setEnabledAttributes(false, false, false);
    this._initialize(gl, 'wireframe', shaderVert, shaderFrag);
  }

  _getUniformNames(uniformNames) {
    super._getUniformNames(uniformNames);

    uniformNames.push('zOffset', 'wireframeColor');
  }

  /** Set wireframe options for a {@link SceneObject}.
  * @param {WebGLRenderingContext} gl
  * @param {SceneObject} object
  */
  setWireframeUniforms(gl, object) {
    this.setU1f(gl, 'zOffset', 0.01);
    this.setUVec4(gl, 'wireframeColor', object.wireframeColor);
  }
}

export {
  TEXUNIT_OBJ_SHADOW_MAP,
  TEXUNIT_OBJ_COLOR,
  WGLShaderPrepass,
  WGLShaderDepth,
  WGLShaderDepthColor,
  WGLShaderLightpass,
  WGLShaderLightpassShadow,
  WGLShaderWireframe,
};
