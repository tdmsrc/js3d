import { Camera } from '../wglcore/mathCam';
import Light from '../wglcore/light';
import SceneObject from '../wglcore/geomSceneObject';
import WGLDrawable from '../wglcore/wglDrawable';
import WGLRenderer from './figRender';
import addFancyTouchListener from '../wgltools/touches';

let FIGURE_COUNT = 0;

// ===================================
// FIGURE
// single-canvas figure
// ===================================

/** Maintains a collection of {@link SceneObject} to be rendered.
*   <ul>
*   <li>HTMLCanvasElement and {@link WGLRenderer} are required for rendering.
*     Creation and maintenance of these is handled automatically.
*   <li>{@link GeomData} need associated {@link WGLDrawable} to be rendered.
*     Creation and association of drawables to geometry is handled automatically.
*   </ul>
*
* @param {HTMLElement} target - Where the canvas will be put
* @param {Function} onInitialize - Invoked when WebGLRenderingContext is ready
*   <br>(<code>function(gl, fig){ ... }</code>, "gl" is WebGLRenderingContext, "fig" is this {@link Figure})
*
* @prop {WGLRenderer} renderer - The {@link WGLRenderer} for this figure
* @prop {boolean} animate - If true, will continually invoke animationLoop
* @prop {Function} animationLoop - Can be modified freely
*   <br>(<code>function(dt){ ... }</code>, "dt" = time elapsed since last frame)
* @prop {autoResize} boolean - Continually check for canvas resize during main loop (default true)
*/
class Figure {
  constructor(target, onInitialize) {
    this._uniqueID = FIGURE_COUNT++;

    // scene
    this._objects = []; // SceneObject[]

    // figure options: auto-resize, animate
    this.autoResize = true;
    this.animate = false;
    this.animationLoop = (dt) => {};

    // init canvas, renderer, and GL
    const canvas = document.createElement('canvas');
    canvas.className = 'fig-canvas';
    target.appendChild(canvas);

    const renderer = new WGLRenderer();
    const gl = renderer.initialize(canvas);
    if (!gl) { return; } // TODO

    this.renderer = renderer;

    // default camera and light(s)
    renderer.camera.position = [0, 0, 6];
    renderer.camera.lookAt([0, 0, 0]);

    const light1 = new Light();
    renderer.lights.push(light1);
    light1.camera.position = [2, 2, 5];
    light1.camera.lookAt([0, 0, 0]);
    light1.color = [1, 1, 1];

    // tell renderer how to traverse the scene
    renderer.sceneTraversal = (handler) => {
      this.traverseScene(handler);
    };

    // user initialization
    onInitialize(gl, this);
    this._postInitialize(gl, canvas, renderer);
  }

  // -----------------------------------
  _postInitialize(gl, canvas, renderer) {
    // prevent right-click context menu
    canvas.oncontextmenu = () => false;

    // TODO: (this.animate = !this.animate) on left click (?)
    // TODO: canvas.toDataURL('image/png') on right click (?)
    const mouseDP = [0, 0];
    let mouseMoved = false;
    addFancyTouchListener(canvas, {
      start: (buttons, p) => { },
      end: (buttons, p) => { },
      move: (buttons, p, q) => {
        if (buttons !== 1) { return; }
        mouseMoved = true;
        mouseDP[0] += q[0] - p[0];
        mouseDP[1] += q[1] - p[1];
      },
      click: (buttons, p) => {
        if (buttons === 1) {
          this.animate = !this.animate;
        } else if (buttons === 2) {
          console.log(canvas.toDataURL('image/png'));
        }
      },
    });

    // main loop
    let frameCount = 0;
    let lastTime = Date.now();
    let lastFPS = lastTime;

    const loop = () => {
      requestAnimationFrame(loop);

      // timing and fps
      const curTime = Date.now();

      const dt = curTime - lastTime;
      const dtFPS = curTime - lastFPS;

      lastTime = curTime;
      if (dtFPS > 2000) {
        frameCount = 0;
        lastFPS = curTime;
      }

      frameCount++;

      // check for redraw
      let redraw = false;

      // auto-resize
      if (this.autoResize) {
        const resized = renderer.resize(gl);
        if (resized) { redraw = true; }
      }

      // handle mousemove
      if (mouseMoved) {
        mouseMoved = false;
        redraw = true;

        const dx = mouseDP[0];
        const dy = mouseDP[1];
        mouseDP[0] = 0; mouseDP[1] = 0;

        if (typeof this.onMousemove === 'function') {
          this.onMousemove(dx, dy, dt);
        }
      }

      // handle animation
      if (this.animate) {
        redraw = true;
        this.animationLoop(dt);
      }

      if (redraw) { this.draw(gl); }
    };
    loop();

    // initial draw
    this.draw(gl);
  }

  // store drawables as geometry._drawable[fig._uniqueID]
  // NOTE: why not just store a WGLDrawable directly in the geometry?
  // because a geometry can be shared across multiple figures, and
  // each figure must have its own WGLDrawable instance)
  // -----------------------------------
  _getDrawable(geometry) {
    // ensure array is at least not undefined or null
    if (!geometry._drawable) { geometry._drawable = []; }

    // entry for this Figure may be undefined or null
    return geometry._drawable[this._uniqueID];
  }

  _createDrawable(gl, geometry) {
    // ensure array is at least not undefined or null
    if (!geometry._drawable) { geometry._drawable = []; }

    // create and set drawable
    const drawable = new WGLDrawable(gl, geometry);
    geometry._drawable[this._uniqueID] = drawable;
    return drawable;
  }

  // -----------------------------------
  /** Add a {@link SceneObject} to the scene.
  *   <br>Creates any necessary {@link WGLDrawable} objects.
  * @param {WebGLRenderingContext} gl
  * @param {SceneObject} sceneObject
  */
  addToScene(gl, sceneObject) {
    const { geomArray } = sceneObject;

    for (let i = 0; i < geomArray.length; i++) {
      const geometry = geomArray[i];

      // make sure the geometry has an associated drawable
      if (!this._getDrawable(geometry)) {
        this._createDrawable(gl, geometry);
      }
    }

    // add to scene
    this._objects.push(sceneObject);
  }

  /** Update any {@link WGLDrawable} objects underlying a {@link SceneObject}.
  * @param {WebGLRenderingContext} gl
  * @param {SceneObject} sceneObject
  */
  update(gl, sceneObject) {
    const { geomArray } = sceneObject;

    for (let i = 0; i < geomArray.length; i++) {
      const geometry = geomArray[i];

      // create if drawable doesn't exist, else just update existing
      const drawable = this._getDrawable(geometry);
      if (drawable) { drawable.update(gl, geometry); }
    }
  }

  /** Traverse the scene, utilizing the handler.
  * @param {WGLRenderer~sceneTraversalHandler} handler
  */
  traverseScene(handler) {
    // [lazy evaluation] reset flag signifying whether a "draw" occurred
    let _hadFirstDraw = false;

    for (let k = 0; k < this._objects.length; k++) {
      const sObj = this._objects[k];

      // if handler rejects sceneObject, do nothing
      if (!handler.filterSceneObject(sObj)) { continue; }

      // [lazy evaluation] reset flag signifying whether a drawable for sObj passed the filter
      let _hadFirstDrawable = false;

      // loop through each of sObj's drawables
      const { geomArray } = sObj;
      for (let i = 0; i < geomArray.length; i++) {
        const drawable = this._getDrawable(geomArray[i]);

        // if handler rejects drawable, do nothing
        if (!handler.filterDrawable(drawable)) { continue; }

        // [lazy evaluation] invoke "before"
        if (!_hadFirstDraw) {
          _hadFirstDraw = true;
          handler.before();
        }

        // [lazy evaluation] invoke "execSceneObject"
        if (!_hadFirstDrawable) {
          _hadFirstDrawable = true;
          handler.execSceneObject(sObj);
        }

        // draw the drawable
        handler.execDrawable(drawable);
      }
    }

    // [lazy evaluation] invoke "after"
    if (_hadFirstDraw) { handler.after(); }
  }

  /** Render the scene.
  * @param {WebGLRenderingContext} gl
  */
  draw(gl) {
    this.renderer.render(gl);
  }
}

export default Figure;
