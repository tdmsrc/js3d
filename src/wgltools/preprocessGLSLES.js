// ===================================
// GLSL ES preprocessor
// -----------------------------------
// see GLSLESPreprocessor.java
// TODO: doesn't process commented out lines correctly
// ===================================

const PREPROCESSOR_TOKEN = 'shared';
const REGEXP_ALPHANUM = '[A-Za-z_]+[A-Za-z0-9_]*';
const REGEXP_SOURCE_IDENTIFIER = `[ \t]*#[ \t]*${PREPROCESSOR_TOKEN}[ \t]+(${REGEXP_ALPHANUM})`;
const regexpSrcID = new RegExp(REGEXP_SOURCE_IDENTIFIER, 'i');

// a little regexp help:
// ? makes the preceding token optional
// (?:stuff) is a non-capturing group
const GLSL_TYPE_QUALIFIER = 'const|attribute|varying|invariant varying|uniform';
const GLSL_PRECISION_QUALIFIER = 'high_precision|medium_precision|low_precision';
const GLSL_TYPE_SPECIFIER_NO_PREC = 'void|float|int|bool|vec2|vec3|vec4|bvec2|bvec3|bvec4|ivec2|ivec3|ivec4|mat2|mat3|mat4|sampler2d|samplercube';
const REGEXP_GLSL_IDENTIFIER = [
  '[ \t]*',
  `(?:(?:${GLSL_TYPE_QUALIFIER})[ \t]+)?`,
  `(?:(?:${GLSL_PRECISION_QUALIFIER})[ \t]+)?`,
  `(?:${GLSL_TYPE_SPECIFIER_NO_PREC})[ \t]+`,
  `(${REGEXP_ALPHANUM})`,
  '.*',
].join('');
const regexpGLSLID = new RegExp(REGEXP_GLSL_IDENTIFIER, 'i');

//-----------------------------------
class GLSLPreprocessor {
  static preprocessLine(ret, line, srcIdentifiers, includedSrc, includedGLSL) {
    let match;

    // check for include identifier
    match = regexpSrcID.exec(line);
    if (match) {
      // get identifier; update included
      const identifier = match[1];
      const redundant = Object.prototype.hasOwnProperty.call(includedSrc, identifier);
      includedSrc[identifier] = true;

      // if not redundant, recurse
      if (!redundant) {
        GLSLPreprocessor.preprocess(ret, srcIdentifiers[identifier], srcIdentifiers, includedSrc, includedGLSL);
      }
      return;
    }

    // check for GLSL identifier
    match = regexpGLSLID.exec(line);
    if (match) {
      // get identifier; update included
      const identifier = match[1];
      const redundant = Object.prototype.hasOwnProperty.call(includedGLSL, identifier);
      includedGLSL[identifier] = true;

      // if not redundant, append the line
      if (!redundant) {
        ret.push(line);
      }
      return;
    }

    // if neither matches, just append the line
    ret.push(line);
  }

  static preprocess(ret, src, srcIdentifiers, includedSrc, includedGLSL) {
    let encounteredLeftBrace = false;

    const lines = src.split('\n');
    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];

      const bracePos = line.indexOf('{');
      if (bracePos >= 0) { encounteredLeftBrace = true; }
      if (encounteredLeftBrace) {
        ret.push(lines[i]);
      } else {
        GLSLPreprocessor.preprocessLine(ret, lines[i], srcIdentifiers, includedSrc, includedGLSL);
      }
    }
  }
}
//-----------------------------------

// srcPrimary: GLSL string
// srcIdentifiers: object with props = identifier, value = GLSL string
const preprocessGLSLES = (src, srcIdentifiers) => {
  // properties' existence indicates inclusion of identifier (property name = identifier)
  const includedSrc = {};
  const includedGLSL = {};

  const ret = [];
  GLSLPreprocessor.preprocess(ret, src, srcIdentifiers, includedSrc, includedGLSL);
  return ret.join('\n');
};

export default preprocessGLSLES;
