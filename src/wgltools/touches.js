const DEBUG_OUTPUT = false;
const CLICK_THRESHOLD_PX = 5;

/** Collection of callbacks describing touch actions.
 * The 'buttons' parameter comes from the mousedown/mousemove/mouseup event.
 * @typedef {Object} TouchEvents
 * @prop {function} click - (buttons) => void
 * @prop {function} start - (buttons, p) => void
 * @prop {function} move - (buttons, p, q) => void
 * @prop {function} end - (buttons, p) => void
 */

/** Add mousedown/move/up listeners to an HTML element which make it easy to handle drags/clicks.
 * Non-click events with matching buttons always occur in blocks of (start, move..., end).
 * @param {HTMLElement} target - Context for addEventListener calls
 * @param {TouchEvents} touchEvents - Describes events
 */
const addFancyTouchListener = (target, touchEvents) => {
  // Prepare defaults for touchEvents
  // ---------------------------------------
  const ensureFunction = f => ((typeof f === 'function') ? f : () => {});
  const eventClick = ensureFunction(touchEvents.click);
  const eventStart = ensureFunction(touchEvents.start);
  const eventMove = ensureFunction(touchEvents.move);
  const eventEnd = ensureFunction(touchEvents.end);

  // Wrappers for debugging
  // ---------------------------------------
  const wrapEventClick = (buttons, p) => {
    if (DEBUG_OUTPUT) {
      console.log(`[CLICK ${buttons}] ${p}`);
    }
    eventClick(buttons, p);
  };
  const wrapEventStart = (buttons, p) => {
    if (DEBUG_OUTPUT) {
      console.log(`[START ${buttons}] ${p}`);
    }
    eventStart(buttons, p);
  };
  const wrapEventMove = (buttons, p, q) => {
    if (DEBUG_OUTPUT) {
      console.log(`[MOVE ${buttons}] ${p} -> ${q}`);
    }
    eventMove(buttons, p, q);
  };
  const wrapEventEnd = (buttons, p) => {
    if (DEBUG_OUTPUT) {
      console.log(`[END ${buttons}] ${p}`);
    }
    eventEnd(buttons, p);
  };

  // Helper for getting position
  // ---------------------------------------
  const _offsetClientPoint = (x, y) => {
    const rect = target.getBoundingClientRect();
    return [x - rect.left, y - rect.top];
  };

  // Current state and basic event listeners
  // ---------------------------------------
  let lastMovePos = [0, 0];
  let downPos = [0, 0];
  let eventButtons = 0;
  let eventDidStart = false;

  target.addEventListener('mousedown', (e) => {
    const p = _offsetClientPoint(e.clientX, e.clientY);

    // End any event that has started
    if (eventDidStart) { wrapEventEnd(eventButtons, p); }

    if (!eventButtons) {
      // [no buttons] -> [some buttons]: wait to see if click
      eventButtons = e.buttons;
      eventDidStart = false;
      downPos = p;
    } else {
      // [some buttons] -> [other buttons]: immediately swap events
      eventButtons = e.buttons;
      eventDidStart = true;
      wrapEventStart(eventButtons, p);
      lastMovePos = p;
    }

    e.preventDefault();
  }, false);

  target.addEventListener('mouseup', (e) => {
    const p = _offsetClientPoint(e.clientX, e.clientY);

    // End any event that has started
    if (eventDidStart) { wrapEventEnd(eventButtons, p); }

    // [some buttons] -> [no buttons]: Click if event not started
    if (!e.buttons && !eventDidStart) { wrapEventClick(eventButtons, p); }

    // [some buttons] -> [other OR no buttons]: immediately swap events
    eventButtons = e.buttons;
    eventDidStart = true;
    wrapEventStart(eventButtons, p);
    lastMovePos = p;

    e.preventDefault();
  }, false);

  target.addEventListener('mousemove', (e) => {
    const p = _offsetClientPoint(e.clientX, e.clientY);

    if (eventDidStart) {
      // If event has started, just do the move event
      wrapEventMove(eventButtons, lastMovePos, p);
      lastMovePos = p;
    } else {
      // If event hasn't started, check for threshold
      const dx = p[0] - downPos[0];
      const dy = p[1] - downPos[1];
      if (Math.sqrt((dx * dx) + (dy * dy)) > CLICK_THRESHOLD_PX) {
        eventDidStart = true;
        wrapEventStart(eventButtons, downPos);
        lastMovePos = downPos;
      }
    }

    e.preventDefault();
  }, false);
};

export default addFancyTouchListener;
