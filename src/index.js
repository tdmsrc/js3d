import { Vec3, Vec4 } from './wglcore/mathVec';
import { Mat3, Mat4 } from './wglcore/mathMat';
import SO3 from './wglcore/mathSO3';
import ObjectTransformation from './wglcore/mathObjTrans';
import { CameraRotation, Camera } from './wglcore/mathCam';
import ColorUtils from './wglcore/mathColor';
import Light from './wglcore/light';

import GeomData from './wglcore/geomData';
import GeomBundle from './wglcore/geomBundle';
import SceneObject from './wglcore/geomSceneObject';
import GeomQuad from './wglcore/geomQuad';
import GeomOBJFile from './wglcore/geomObjFile';
import GeomCurve from './wglcore/geomCurve';
import GeomSurface from './wglcore/geomSurface';
import GeomSkeleton from './wglcore/geomSkeleton';
import GeomVectors from './wglcore/geomVectors';
import GeomText from './wglcore/geomText';

// import WGLDrawable from './wglcore/wglDrawable';
// import WGLQuad from './wglcore/wglQuad';
// import WGLShaderProgram from './wglcore/wglShader';
import {
  // WGLFramebufferObject,
  WGLTexture,
  // WGLTextureCube,
} from './wglcore/wglTexture';

import Figure from './fig/figMain';

export {
  Vec3,
  Vec4,
  Mat3,
  Mat4,
  SO3,
  ObjectTransformation,
  CameraRotation,
  Camera,
  ColorUtils,
  Light,

  GeomData,
  GeomBundle,
  SceneObject,
  GeomQuad,
  GeomOBJFile,
  GeomCurve,
  GeomSurface,
  GeomSkeleton,
  GeomVectors,
  GeomText,

  // WGLDrawable,
  // WGLQuad,
  // WGLShaderProgram,
  // WGLFramebufferObject,
  WGLTexture,
  // WGLTextureCube,

  Figure,
};
