precision highp float;

uniform sampler2D texColor;
uniform sampler2D texBlur;

//OPTIONS
//-----------------
uniform float bloomFactor;

uniform float colorAdjustBrightness;
uniform float colorAdjustContrast;
uniform float colorAdjustSaturation;
//-----------------


varying vec2 texCoord;


void main()
{
  //blend blur and non-blur
  vec4 colorNonBlur = texture2D(texColor, texCoord);
  vec4 colorBlur = texture2D(texBlur, texCoord);

  //bloom
  //--------------------------------
  vec4 colorFinal = clamp(mix(colorNonBlur, colorBlur, bloomFactor), 0.0, 1.0);
  vec3 color = colorFinal.rgb;
  //--------------------------------
  
  //color adjust
  //--------------------------------  
  //adjust saturation
  float lum = dot(color, vec3(0.299,0.587,0.144));
  vec3 noSaturation = vec3(lum, lum, lum);
  color = mix(noSaturation, color, colorAdjustSaturation);
  
  //adjust brightness
  color += vec3(colorAdjustBrightness, colorAdjustBrightness, colorAdjustBrightness);
  
  //adjust contrast
  vec3 noContrast = vec3(0.5, 0.5, 0.5);
  color = mix(noContrast, color, colorAdjustContrast);
  //--------------------------------
  
  gl_FragColor = clamp(vec4(colorFinal.a*color, colorFinal.a), 0.0, 1.0);
}
