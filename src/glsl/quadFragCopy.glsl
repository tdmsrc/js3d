precision highp float;

uniform sampler2D texColor;
varying vec2 texCoord;


void main()
{
  gl_FragColor = texture2D(texColor, texCoord);
}
