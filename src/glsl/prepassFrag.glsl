precision highp float;

uniform vec3 camPosition;

uniform float alpha;
uniform float emissiveCoefficient;
uniform vec3 emissiveColor;

varying vec3 position;

#shared getColorFrag


void main(){
  //TODO: discard if alpha < thresh
  //---------------------------
  vec4 fc4 = getFragColor();
  vec3 fragColor = fc4.rgb;
  if(fc4.a < 0.9){ discard; }
  //---------------------------

  vec3 finalColor = emissiveCoefficient*emissiveColor*fragColor;
  
  gl_FragColor = vec4(alpha*clamp(finalColor,0.0,1.0), alpha);
}
