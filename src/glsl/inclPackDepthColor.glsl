
//----------------------------------------------------
//PACK/UNPACK DEPTH AND COLOR
//----------------------------------------------------

//depth
//----------------------------------------------------
//pack: float in [0,1] -> 2 bytes in [0,1]
vec2 packDepth(float d){
  float dpack = d * 255.0;
  
  float dpackx = floor(dpack) / 255.0;
  float dpacky = fract(dpack);
  
  return vec2(dpackx, dpacky);
}

//unpack: 2 bytes in [0,1] -> float in [0,1]
float unpackDepth(vec2 v){
  return (floor(v.x * 255.0) + v.y) / 255.0;
}

//color
//----------------------------------------------------
//pack: 4 bytes in [0,1] -> 2 bytes [0,1]
vec2 packDepthColor(vec4 c){
  vec4 cint = floor(15.0 * c);
  float rgpack = (16.0*cint.r + cint.g) / 255.0;
  float bapack = (16.0*cint.b + cint.a) / 255.0;
  
  return vec2(rgpack, bapack);
}

//unpack: 2 bytes in [0,1] -> 4 bytes in [0,1]
vec4 unpackDepthColor(vec2 v){
  float rgpack = floor(v.x * 255.0) / 16.0;
  float bapack = floor(v.y * 255.0) / 16.0;
  
  float r = floor(rgpack) / 16.0;
  float g = fract(rgpack);
  float b = floor(bapack) / 16.0;
  float a = fract(bapack);
  
  return vec4(r,g,b,a);
}

