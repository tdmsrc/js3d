precision highp float;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform float alpha;

varying vec3 position;
varying vec3 normal;
varying vec2 posClipXY;

#shared getColorFrag
#shared lighting
#shared packDepthColor
#shared isShadowedPCF


void main(){
  //TODO: discard if alpha < thresh
  //---------------------------
  vec4 fc4 = getFragColor();
  vec3 fragColor = fc4.rgb;
  if(fc4.a < 0.5){ discard; }
  //---------------------------
  
  //flip normal if face is back-facing
  vec3 twoSidedNormal = gl_FrontFacing ? normal : (-1.0*normal);
  twoSidedNormal = normalize(twoSidedNormal);
  
  //normalized frag to light vector
  vec3 normalizedFragToLightVector = normalize(lightPosition - position);
  
  //shadow mapping
  //----------------------
  float fragLightPercentage;
  bool fragShadowed;
  vec4 fragDepthColor;
  
  vec3 fragPositionSM = position + 0.05*normalizedFragToLightVector; //shift to avoid z-fighting
  shadowFilter(fragPositionSM, posClipXY, twoSidedNormal, fragShadowed, fragLightPercentage, fragDepthColor);
  if(fragShadowed){ discard; } //optional
  
  vec3 lightColorFinal = mix(fragDepthColor.rgb*lightColor, lightColor, fragLightPercentage);
  lightColorFinal *= fragLightPercentage;
  //----------------------
  
  gl_FragColor = vec4(alpha*getLighting(twoSidedNormal, position, lightColorFinal, fragColor), alpha);
}
