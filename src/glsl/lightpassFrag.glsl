precision highp float;

uniform vec3 lightColor;

uniform float alpha;

varying vec3 position;
varying vec3 normal;

#shared getColorFrag
#shared lighting


void main(){
  //TODO: discard if alpha < thresh
  //---------------------------
  vec4 fc4 = getFragColor();
  vec3 fragColor = fc4.rgb;
  if(fc4.a < 0.9){ discard; }
  //---------------------------
  
  //flip normal if face is back-facing
  vec3 twoSidedNormal = gl_FrontFacing ? normal : (-1.0*normal);
  twoSidedNormal = normalize(twoSidedNormal);
  
  gl_FragColor = vec4(alpha*getLighting(twoSidedNormal, position, lightColor, fragColor), alpha);
}
