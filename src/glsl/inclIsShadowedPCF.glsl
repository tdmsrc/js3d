
uniform sampler2D texShadowMap;

uniform mat4 lightCamProjection;
uniform mat3 lightCamRotation;


#shared packDepthColor


//----------------------
//SHADOW MAPPING (PCF)
//----------------------

//use camera properties and shadow map to determine if a point in space is shadowed
//also, determine color of translucent overlay (only relevant if not shadowed)
void isFragShadowed(in vec3 fragPosition, out bool fragShadowed, out vec4 fragDepthColor){
  
  //get frag position in light's eye coordinates
  vec3 fragPositionLEC = lightCamRotation * (fragPosition - lightPosition);
  
  //get frag position in light's clip coordinates
  vec4 fragPositionHomogLCC = lightCamProjection * vec4(fragPositionLEC, 1.0);
  vec3 fragPositionLCC = vec3(fragPositionHomogLCC / fragPositionHomogLCC.w);
  
  //rescale clip coords [-1,1] -> [0,1]
  vec3 fragPositionLCCAdjusted = (fragPositionLCC + vec3(1.0, 1.0, 1.0))/2.0;
  
  //check if this is outside the light's frustum
  if(fragPositionLCC.z >  1.0 ||
     fragPositionLCC.x < -1.0 || fragPositionLCC.x > 1.0 ||
     fragPositionLCC.y < -1.0 || fragPositionLCC.y > 1.0 ){
    
    fragShadowed = true;
    return;
  }
  
  //unpack depth and translucent overlay
  vec4 packedShadowMap = texture2D(texShadowMap, fragPositionLCCAdjusted.xy);  
  float depthShadowMap = unpackDepth(packedShadowMap.rg);
  
  fragShadowed = (fragPositionLCCAdjusted.z > depthShadowMap);
  
  if(fragShadowed){
    fragDepthColor = vec4(0.0, 0.0, 0.0, 1.0);
  }else{
    fragDepthColor = unpackDepthColor(packedShadowMap.ba);
  }
}

//get any vector orthogonal to v
vec3 getPerp(vec3 v){
  
  if((v.x > -0.6) && (v.x < 0.6)){
    return vec3(0, v.z, -v.y);
  }else if((v.y > -0.6) && (v.y < 0.6)){
    return vec3(-v.z, 0, v.x);
  }else{
    return vec3(v.y, -v.x, 0);
  }
}

//PCF: shadow map anti-aliasing
void shadowFilter(
  in vec3 fragPosition, in vec2 kernelOffset, in vec3 normalizedNormal, 
  out bool fragShadowed, out float fragLightPercentage, out vec4 fragDepthColor){
  
  //randomly rotating PCF kernel
  //--------------------------------
  vec2 offset[8]; float w[8];
  
  float rand = fract(sin(dot(kernelOffset,vec2(12.9898,78.233))) * 43758.5453);
  
  float dsk = 0.02;
  float ra = rand;
  float rs = sin(ra);
  float rc = cos(ra);
  offset[0] = dsk*vec2( rc, rs); w[0] = 0.15;
  offset[1] = dsk*vec2(-rs, rc); w[1] = 0.15;
  offset[2] = dsk*vec2(-rc,-rs); w[2] = 0.15;
  offset[3] = dsk*vec2( rs,-rc); w[3] = 0.15;
  
  dsk *= 2.0;
  ra += 0.785398163;
  rs = sin(ra);
  rc = cos(ra);
  offset[4] = dsk*vec2( rc, rs); w[4] = 0.1;
  offset[5] = dsk*vec2(-rs, rc); w[5] = 0.1;
  offset[6] = dsk*vec2(-rc,-rs); w[6] = 0.1;
  offset[7] = dsk*vec2( rs,-rc); w[7] = 0.1;
  //--------------------------------
  
  vec3 perp = normalize(getPerp(normalizedNormal));
  vec3 perp2 = cross(normalizedNormal, perp);
  
  //initial values for outputs
  fragShadowed = true;
  fragLightPercentage = 1.0;
  fragDepthColor = vec4(0.0, 0.0, 0.0, 0.0);
  
  //use shadow map to correct values
  bool fragShadowedTemp;
  vec4 fragDepthColorTemp;
  
  for(int i=0; i<8; i++){
    vec3 perpOffset = offset[i].x*perp + offset[i].y*perp2;
    isFragShadowed(fragPosition+perpOffset, fragShadowedTemp, fragDepthColorTemp);
    
    fragShadowed = fragShadowed && fragShadowedTemp;
    fragLightPercentage -= fragDepthColorTemp.a * w[i];
    fragDepthColor += fragDepthColorTemp * w[i];
  }
}

