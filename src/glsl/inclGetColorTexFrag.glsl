uniform sampler2D texColor;

varying vec2 tex;


//4x4 ordered dithering
/*float ditherThresh(){
  
  vec2 texTargetSize = vec2(1536.0, 1536.0);
  
  float tarr[16];
  tarr[ 0] =  1.0; tarr[ 1] =  9.0; tarr[ 2] =  3.0; tarr[ 3] = 11.0;
  tarr[ 4] = 13.0; tarr[ 5] =  5.0; tarr[ 6] = 15.0; tarr[ 7] =  7.0;
  tarr[ 8] =  4.0; tarr[ 9] = 12.0; tarr[10] =  2.0; tarr[11] = 10.0;
  tarr[12] = 16.0; tarr[13] =  8.0; tarr[14] = 14.0; tarr[15] =  6.0;
  
  int tci = int(mod(tex.x * float(texTargetSize.x), 4.0));
  int tcj = int(mod(tex.y * float(texTargetSize.y), 4.0));
  
  float thresh = 1.0;
  for(int i=0; i<4; i++){
  for(int j=0; j<4; j++){
    if((i == tci) && (j == tcj)){ thresh = tarr[i*4+j]/17.0; }
  }}
  
  return thresh;
}*/

vec4 getFragColor(){
  vec4 tc = texture2D(texColor, tex);
  
  float thresh = 0.75; //ditherThresh();
  float alphaClamp = (tc.a < thresh) ? 0.0 : 1.0;
  
  vec3 baseColor = tc.rgb;
  return vec4(baseColor, alphaClamp);
}
