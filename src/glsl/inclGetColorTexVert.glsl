attribute vec2 vertexTex;

varying vec3 color;
varying vec2 tex;


vec3 getVertexColor(){
  return vec3(1.0, 0.5, 0.0);
}

vec2 getVertexTex(){
  return vertexTex;
}
