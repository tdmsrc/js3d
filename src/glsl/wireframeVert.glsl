attribute vec3 vertexPosition;

uniform vec3 camPosition;
uniform mat3 camRotation;
uniform mat4 camProjection;

uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

uniform float zOffset;


void main(){
  vec3 position = objRotation * (vertexPosition - objOffset) + objTranslation;
  vec3 vertexPosCam = camRotation * (position - camPosition);
  
  vertexPosCam.z = vertexPosCam.z + zOffset;
    
  gl_Position = camProjection * vec4(vertexPosCam, 1.0);
}
