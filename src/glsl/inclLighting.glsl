uniform vec3 camPosition;

uniform vec3 lightPosition;
uniform vec3 lightAttenuation;

uniform float diffuseCoefficient;
uniform float specularCoefficient;
uniform float specularExponent;

//----------------------------------------------------
//PHONG/CEL SHADING
//compute diffuse and specular intensity using light uniforms and specified normal and position
//----------------------------------------------------

//PHONG SHADING
//compute diffuse and specular intensity using light uniforms and specified normal and position
void getPhong(out float intensityDiffuse, out float intensitySpecular, vec3 normal, vec3 fragPosition){
  
  //frag to light vector and distance
  vec3 fragToLightVector = lightPosition - fragPosition;  
  float dLight = length(fragToLightVector);  
  vec3 normalizedFragToLightVector = fragToLightVector / dLight;
  
  //compute light attenuation
  float invAttenuation = dot(lightAttenuation, vec3(1.0, dLight, dLight*dLight));
  float factorAttenuation = clamp(1.0/invAttenuation, 0.0, 1.0);
  
  //compute diffuse intensity
  intensityDiffuse = clamp(dot(normal, normalizedFragToLightVector), 0.0, 1.0);
  
  //compute specular intensity
  vec3 normalizedFragToEyeVector = normalize(camPosition - fragPosition);
  vec3 blinnH = normalize(normalizedFragToLightVector + normalizedFragToEyeVector);
  intensitySpecular = pow(clamp(dot(normal, blinnH), 0.0, 1.0), specularExponent);
  
  //adjust for coefficients and attenuation
  intensityDiffuse = intensityDiffuse * diffuseCoefficient * factorAttenuation;
  intensitySpecular = intensitySpecular * specularCoefficient * factorAttenuation;
}

//CEL SHADING
//snap light intensity to one of three values, with smooth transition between regions
//can mix with original light intensity for a more subtle effect
void getCel(inout float li, float alpha){
  
  //light intensities in dark, medium, and bright regions
  float outA = 0.5, outB = 0.8, outC = 1.2;  
  //original light intensities bounding dark, medium, and bright regions
  float inAB = 0.5, inBC = 0.8; 
  //governs transition between regions; smaller = sharper, bigger = smoother
  float bw = 0.01; 
  
  float tli = 0.0;
  if(li < inAB-bw){ tli = outA; }
  else if(li < inAB+bw){ tli = outA + (outB-outA)*(li-inAB+bw)/(2.0*bw); }
  else if(li < inBC-bw){ tli = outB; }
  else if(li < inBC+bw){ tli = outB + (outC-outB)*(li-inBC+bw)/(2.0*bw); }
  else{ tli = outC; }
  
  li = mix(li, tli, alpha);
}

vec3 getLighting(vec3 twoSidedNormal, vec3 fragPosition, vec3 lightColor, vec3 color){
  
  //shadowed/discarded if facing away from light
  //----------------------
  //frag to light vector
  vec3 normalizedFragToLightVector = normalize(lightPosition - fragPosition);
  
  //cosNL < this value should be discarded or hidden (avoids some shadow map artifacts)
  float minCosNL = 0.2, wCosNL = 0.05; //smaller = sharper transition, bigger = smoother
  
  float cosNL = dot(twoSidedNormal, normalizedFragToLightVector);
  float lightFacingPercentage = clamp((cosNL - minCosNL)/wCosNL, 0.0, 1.0);
  //----------------------
  
  //phong/cel light model
  //----------------------
  float celCoefficient = 0.3;
  float intensityDiffuse;
  float intensitySpecular;
  
  getPhong(intensityDiffuse, intensitySpecular, twoSidedNormal, fragPosition);
  getCel(intensityDiffuse, celCoefficient);
  
  //blend final color
  vec3 colorDiffuse = color * lightColor;
  vec3 colorSpecular = lightColor; 
  
  vec3 finalColor = clamp(intensityDiffuse*colorDiffuse + intensitySpecular*colorSpecular, 0.0, 1.0);
  //----------------------
  
  //outline
  //----------------------
  //frag to cam vector and distance
  vec3 normalizedFragToCamVector = normalize(camPosition - fragPosition);
  
  //outline intensity
  float intensityOutline = clamp(2.0 * dot(twoSidedNormal, normalizedFragToCamVector), 0.0, 1.0);
  //----------------------
  
  return clamp(intensityOutline*lightFacingPercentage*finalColor, 0.0, 1.0);
}
