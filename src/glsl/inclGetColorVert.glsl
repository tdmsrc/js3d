attribute vec3 vertexColor;

varying vec3 color;
varying vec2 tex;


vec3 getVertexColor(){
  return clamp(vertexColor, 0.0, 1.0);
}

vec2 getVertexTex(){
  return vec2(0.0, 0.0);
}
