varying vec3 color;


vec4 getFragColor(){
  return vec4(color, 1.0);
}
