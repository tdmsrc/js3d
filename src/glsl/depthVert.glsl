attribute vec3 vertexPosition;

uniform vec3 camPosition;
uniform mat3 camRotation;
uniform mat4 camProjection;

uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

varying vec3 vertexPosCam;

#shared getColorVert


void main(){
  vec3 position = objRotation * (vertexPosition - objOffset) + objTranslation;
  vertexPosCam = camRotation * (position - camPosition);
  
  tex = getVertexTex();
  color = getVertexColor();
  
  gl_Position = camProjection * vec4(vertexPosCam, 1.0);
}
