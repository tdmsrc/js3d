precision highp float;

uniform float alpha;
uniform vec4 wireframeColor;


void main(){
  //wireframeColor is not premultiplied alpha
  float wfa = wireframeColor.a;
  vec3 wfc = wireframeColor.rgb;
  gl_FragColor = vec4(wfa*wfc, wfa);
}
