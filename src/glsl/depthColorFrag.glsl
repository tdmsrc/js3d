precision highp float;

uniform mat4 camProjection;

uniform float alpha;

varying vec3 position;

#shared getColorFrag
#shared packDepthColor


void main(){
  //TODO: discard if alpha < thresh
  //---------------------------
  vec4 fc4 = getFragColor();
  vec3 fragColor = fc4.rgb;
  if(fc4.a < 0.9){ discard; }
  //---------------------------
  
  vec4 rgba = vec4(fragColor, alpha);
  vec2 packedRGBA = packDepthColor(rgba);
  
  gl_FragColor = vec4(1.0, 1.0, packedRGBA.x, packedRGBA.y);
}
