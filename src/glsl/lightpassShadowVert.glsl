attribute vec3 vertexPosition;
attribute vec3 vertexNormal;

uniform vec3 camPosition;
uniform mat3 camRotation;
uniform mat4 camProjection;

uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

varying vec3 position;
varying vec3 normal;
varying vec2 posClipXY;

#shared getColorVert

void main(){
  position = objRotation * (vertexPosition - objOffset) + objTranslation;
  vec3 vertexPosCam = camRotation * (position - camPosition);
  
  tex = getVertexTex();
  color = getVertexColor();
  
  normal = normalize(objRotation * vertexNormal);
  
  vec4 posClip = camProjection * vec4(vertexPosCam, 1.0);
  posClipXY = posClip.xy/posClip.w;
  gl_Position = posClip;
}
