attribute vec3 vertexPosition;

uniform vec3 camPosition;
uniform mat3 camRotation;
uniform mat4 camProjection;

uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

varying vec3 position;

#shared getColorVert


void main(){
  position = objRotation * (vertexPosition - objOffset) + objTranslation;
  vec3 vertexPosCam = camRotation * (position - camPosition);
  
  tex = getVertexTex();
  color = getVertexColor();
  
  gl_Position = camProjection * vec4(vertexPosCam, 1.0);
}
