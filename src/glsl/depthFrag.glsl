precision highp float;

uniform mat4 camProjection;

varying vec3 vertexPosCam;

#shared getColorFrag
#shared packDepthColor


void main(){
  //TODO: discard if alpha < thresh
  //---------------------------
  vec4 fc4 = getFragColor();
  vec3 fragColor = fc4.rgb;
  if(fc4.a < 0.9){ discard; }
  //---------------------------
  
  //get z clip coordinate
  vec4 vertexPosHomogClip = camProjection * vec4(vertexPosCam, 1.0);
  float vertexPosClipZ = vertexPosHomogClip.z / vertexPosHomogClip.w;
  
  //rescale clip coord [-1,1] -> [0,1]
  float vertexPosClipZAdjusted = (vertexPosClipZ + 1.0)/2.0;
  
  vec2 packedDepth = packDepth(vertexPosClipZAdjusted);
  gl_FragColor = vec4(packedDepth, 0.0, 1.0);
}
