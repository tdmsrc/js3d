import { Vec3 } from './mathVec';
import GeomData from './geomData';
import SceneObject from './geomSceneObject';

const MAX_VERTICES = 65535;
const TEMP_U = [0, 0, 0];
const TEMP_V = [0, 0, 0];
const TEMP_W = [0, 0, 0];

// ===================================
// GEOMETRY: BUNDLE
// repeated copies of the same geometry
// ===================================

// TODO
// ideally would use instancing, i.e. glDrawElementsInstanced etc
// but this isn't in WebGL: could use ANGLE_instanced_arrays etc if available
// for now: pack all the copies into one (or more if > 65535 verts) geometry

/** A collection of instances with shared {@link GeomData}.
* @typedef {Object} GeomBundle~group
*
* @prop {GeomData} geometry - Defines the geometry of each instance
* @prop {GeomBundle~instance[]} instances - Instances of the geometry
*/

/** One instance of a geometry.  Properties describe how the reference geometry will be modified.
*   <ul>
*   <li>Multiply reference color coordinate-wise with "color",
*   <li>(1) multiply reference position coordinate-wise with "scale",
*   <li>(2) if "hasOrientation", rotate so that [1,0,0] -> "orientation" (any such rotation),
*   <li>(3) add "position" to the result.
*   </ul>
* @typedef {Object} GeomBundle~instance
*
* @prop {number[]} scale
* @prop {boolean} hasOrientation - If false, "orientation" is never used
* @prop {number[]} orientation - Need not be normalized
* @prop {number[]} position
* @prop {number[]} color
*/

/** Packs multiple instances of geometries into as few {@link GeomData} objects as possible.
*   <br>Must call {@link GeomBundle#generate} after creation, or to update instance properties.
*   <br>Use {@link GeomBundle#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomBundle~group[]} opts.groups - Each group specifies a geometry and its instances
*/
class GeomBundle {
  constructor(opts) {
    this.opts = opts;
    this._geomArray = []; // break geometry into multiple GeomDatas

    this._prepare();
  }

  // generate geometry
  // -----------------------------------
  // each instance will have:
  //  _gIndex (index of GeomData in this._geomArray)
  //  _es0 (starting position in GeomData elements array)
  //  _ew0 (starting position in GeomData elementsWire array)
  //  _v0 (starting position in GeomData vert attrib arrays)

  // set above properties and return array of GeomData initialization options
  _setLayout() {
    // running indices (see descr of 'instance' above)
    let gIndex = 0;
    let es0 = 0; let ew0 = 0; let v0 = 0;

    // list of options to be used to initialize each GeomData
    const gOptsArray = [];

    // push current geom opts and start the next
    const gNext = () => {
      if (v0 > 0) {
        gOptsArray[gIndex] = {
          hasNormals: true,
          elementCount: es0,
          elementWireCount: ew0,
          vertexCount: v0,
        };
        gIndex++;
      }
      es0 = 0; ew0 = 0; v0 = 0;
    };

    // set _gIndex, _es0, _ew0, _v0 on each instance and add to gOptsArray
    for (let i = 0; i < this.opts.groups.length; i++) {
      const gsrc = this.opts.groups[i].geometry;
      const { instances } = this.opts.groups[i];

      for (let j = 0; j < instances.length; j++) {
        // if next instance won't fit in geom, start a new one
        const v0next = v0 + gsrc.vertexCount;
        if (v0next > MAX_VERTICES) { gNext(); }

        instances[j]._gIndex = gIndex;
        instances[j]._es0 = es0;
        instances[j]._ew0 = ew0;
        instances[j]._v0 = v0;

        es0 += gsrc.elementCount;
        ew0 += gsrc.elementWireCount;
        v0 += gsrc.vertexCount;
      }

      // prepare geom opts for the next group
      gNext();
    }

    // return list of GeomData initialization options
    return gOptsArray;
  }

  // use "layout" to put reference geometry elements into GeomData
  _putElements(gsrc, instance) {
    const gdata = this._geomArray[instance._gIndex];

    const es0 = instance._es0;
    const ew0 = instance._ew0;
    const v0 = instance._v0;

    for (let i = 0; i < gsrc.elementCount; i++) {
      gdata.elements[es0 + i] = v0 + gsrc.elements[i];
    }

    for (let i = 0; i < gsrc.elementWireCount; i++) {
      gdata.elementsWire[ew0 + i] = v0 + gsrc.elementsWire[i];
    }
  }

  // establish "layout", create GeomDatas, populate element arrays
  _prepare() {
    // get GeomData layout
    const gOptsArray = this._setLayout();

    // create the GeomDatas
    for (let i = 0; i < gOptsArray.length; i++) {
      const gdata = new GeomData();
      gdata.initialize(gOptsArray[i]);
      this._geomArray.push(gdata);
    }

    // populate element arrays
    for (let i = 0; i < this.opts.groups.length; i++) {
      const gsrc = this.opts.groups[i].geometry;
      const { instances } = this.opts.groups[i];

      for (let j = 0; j < instances.length; j++) {
        this._putElements(gsrc, instances[j]);
      }
    }
  }

  // call at least once, or to update instances
  // ===================================
  // use "layout" to put reference geometry vertex attribs into GeomData

  // write vertex attribs for instance using all instance options
  _putVertexAttribsR(gsrc, instance) {
    const gdata = this._geomArray[instance._gIndex];

    // normalize orientation
    Vec3.copy(TEMP_U, instance.orientation);
    Vec3.normalize(TEMP_U, TEMP_U);
    // v = any unit vector orthogonal to u
    Vec3.orthogonal(TEMP_V, TEMP_U);
    Vec3.normalize(TEMP_V, TEMP_V);
    // w = uxv
    Vec3.cross(TEMP_W, TEMP_U, TEMP_V);

    const invScale0 = 1 / instance.scale[0];
    const invScale1 = 1 / instance.scale[1];
    const invScale2 = 1 / instance.scale[2];

    const index0 = 3 * instance._v0;
    for (let i = 0; i < gsrc.vertexCount; i++) {
      const indexG = 3 * i;
      const index = index0 + indexG;

      // scale: position and normal
      const x = gsrc.position[indexG + 0] * instance.scale[0];
      const y = gsrc.position[indexG + 1] * instance.scale[1];
      const z = gsrc.position[indexG + 2] * instance.scale[2];

      const nx = gsrc.normal[indexG + 0] * invScale0;
      const ny = gsrc.normal[indexG + 1] * invScale1;
      const nz = gsrc.normal[indexG + 2] * invScale2;

      // rotate and offset: position and normal (NOTE: not bothering to normalize normal)
      gdata.position[index + 0] = (x * TEMP_U[0]) + (y * TEMP_V[0]) + (z * TEMP_W[0]) + instance.position[0];
      gdata.position[index + 1] = (x * TEMP_U[1]) + (y * TEMP_V[1]) + (z * TEMP_W[1]) + instance.position[1];
      gdata.position[index + 2] = (x * TEMP_U[2]) + (y * TEMP_V[2]) + (z * TEMP_W[2]) + instance.position[2];

      gdata.normal[index + 0] = (nx * TEMP_U[0]) + (ny * TEMP_V[0]) + (nz * TEMP_W[0]);
      gdata.normal[index + 1] = (nx * TEMP_U[1]) + (ny * TEMP_V[1]) + (nz * TEMP_W[1]);
      gdata.normal[index + 2] = (nx * TEMP_U[2]) + (ny * TEMP_V[2]) + (nz * TEMP_W[2]);

      // color
      gdata.color[index + 0] = gsrc.color[indexG + 0] * instance.color[0];
      gdata.color[index + 1] = gsrc.color[indexG + 1] * instance.color[1];
      gdata.color[index + 2] = gsrc.color[indexG + 2] * instance.color[2];
    }
  }

  // write vertex attribs for instance without applying "orientation" option
  _putVertexAttribsNR(gsrc, instance) {
    const gdata = this._geomArray[instance._gIndex];

    const invScale0 = 1 / instance.scale[0];
    const invScale1 = 1 / instance.scale[1];
    const invScale2 = 1 / instance.scale[2];

    const index0 = 3 * instance._v0;
    for (let i = 0; i < gsrc.vertexCount; i++) {
      const indexG = 3 * i;
      const index = index0 + indexG;

      // position
      gdata.position[index + 0] = (gsrc.position[indexG + 0] * instance.scale[0]) + instance.position[0];
      gdata.position[index + 1] = (gsrc.position[indexG + 1] * instance.scale[1]) + instance.position[1];
      gdata.position[index + 2] = (gsrc.position[indexG + 2] * instance.scale[2]) + instance.position[2];

      // normal (NOTE: not bothering to normalize normal)
      gdata.normal[index + 0] = gsrc.normal[indexG + 0] * invScale0;
      gdata.normal[index + 1] = gsrc.normal[indexG + 1] * invScale1;
      gdata.normal[index + 2] = gsrc.normal[indexG + 2] * invScale2;

      // color
      gdata.color[index + 0] = gsrc.color[indexG + 0] * instance.color[0];
      gdata.color[index + 1] = gsrc.color[indexG + 1] * instance.color[1];
      gdata.color[index + 2] = gsrc.color[indexG + 2] * instance.color[2];
    }
  }

  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    for (let i = 0; i < this.opts.groups.length; i++) {
      const gsrc = this.opts.groups[i].geometry;
      const { instances } = this.opts.groups[i];

      for (let j = 0; j < instances.length; j++) {
        const instance = instances[j];
        if (instance.hasOrientation) {
          // uses "orientation" option
          this._putVertexAttribsR(gsrc, instance);
        } else {
          // faster version with no "orientation" option
          this._putVertexAttribsNR(gsrc, instance);
        }
      }
    }
  }

  /** Create a new {@link SceneObject} with this geometry.
  * @return {SceneObject}
  */
  instance() {
    return new SceneObject(this._geomArray);
  }
}

export default GeomBundle;
