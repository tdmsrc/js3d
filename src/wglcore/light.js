import { Camera } from './mathCam';

// ===================================
// LIGHT
// ===================================

/** Represents a light source.
*
* @prop {boolean} shadows - True iff the light should cast shadows
* @prop {Camera} camera - Specifies the light position, direction, etc.
* @prop {number[]} color - Color of the light (as [r,g,b])
* @prop {number[]} attenuation - Intensity varies with the distance "d" from the light:
*   <br>If attenuation = [a,b,c], intensity is computed as 1/(a + b*d + c*d^2)
*/
class Light {
  constructor() {
    this.shadows = true;
    this.camera = new Camera(90.0, 1.0, 0.1, 1000.0); // TODO
    this.color = [1.0, 1.0, 1.0];
    this.attenuation = [1.0, 0.02, 0.0];
  }
}

export default Light;
