import { Vec3 } from './mathVec';
import GeomData from './geomData';

const TEMPVEC3A = [0, 0, 0];
const TEMPVEC3B = [0, 0, 0];

const rangeLerp = (target, range, lerp) => {
  target[0] = range.u[0] + lerp[0] * (range.u[1] - range.u[0]);
  target[1] = range.v[0] + lerp[1] * (range.v[1] - range.v[0]);
};

// const rangeUnLerp = (target, range, value) => {
//   target[0] = (value[0] - range.u[0]) / (range.u[1] - range.u[0]);
//   target[1] = (value[1] - range.v[0]) / (range.v[1] - range.v[0]);
// };

// for each i = 0...n, lerp along "range" at t = i/n
// for each number in "values", find which of these lerped values is closest
// corresponding indices get marked "true" in the returned array, otherwise false
// if "loop" is true, the index i=n is moved to i=0
const convertValuesToFilter = (range, n, values, loop) => {
  const ret = [];

  // initially all false
  for (let k = 0; k <= n; k++) { ret[k] = false; }

  // convert values to indices
  for (let i = 0; i < values.length; i++) {
    // find index
    const t = (values[i] - range[0]) / (range[1] - range[0]);
    let k = Math.round(n * t);
    // clamp
    if (k < 0) { k = 0; }
    if (k > n) { k = n; }
    if (loop && (k === n)) { k = 0; }

    ret[k] = true;
  }

  return ret;
};

// ===================================
// GEOMETRY: SURFACE
// parametric surface (f:R^2 -> R^3)
// ===================================

/** Parametric surface.
*   <br>Must call {@link GeomSurface#generate} after creation, or to update position/color values.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {Object} opts.rangeIn - Range of u and v values: <code>{u:[min,max], v:[min,max]}</code>
* @param {number[]} opts.n - Number of samples along u and v direction: [nu, nv]
* @param {Function} opts.f - Position function: <code>function(u,v){ return [x,y,z]; }</code>
* @param {Function} opts.color - Color function: <code>function(u,v,[x,y,z]){ return [r,g,b]; }</code>
* @param {boolean} opts.loopU - Set to true if f(umin,v) = f(umax,v)
* @param {boolean} opts.loopV - Set to true if f(u,vmin) = f(u,vmax)
*
* @extends GeomData
*/
class GeomSurface extends GeomData {
  constructor(opts) {
    super();
    this.opts = opts;

    // create this._gridVerts/_gridFaces arrays and GeomData elements
    this._createGrid();
  }

  // generate grid geometry
  // -----------------------------------
  // create gridVertex
  _createVertex(lerp) {
    // create and add gridVertex object
    const gridVertex = {
      pIn: [0, 0],
      position: [0, 0, 0], // set by "generate"
      normal: [0, 0, 0], // set by "generate"
    };
    this._gridVerts.push(gridVertex);

    // set pIn
    rangeLerp(gridVertex.pIn, this.opts.rangeIn, lerp);
  }

  // add face to GeomData; create gridFace and update cross references
  // i,j,k are indices of grid vertices, in CCW order
  _createFace(i, j, k) {
    const faceIndex = this._gridFaces.length;

    // create and add gridFace object
    const gridFace = {
      incidentVerts: [this._gridVerts[i], this._gridVerts[j], this._gridVerts[k]],
    };
    this._gridFaces.push(gridFace);

    // add to GeomData
    this.setFace(faceIndex, 0, i, j, k);
  }

  // creates _gridVerts, _gridFaces; initializes geometry and its faces
  _createGrid() {
    const nu = this.opts.n[0];
    const nv = this.opts.n[1];

    // initialize GeomData
    const wireLinesU = (nu + 1) * nv;
    const wireLinesV = nu * (nv + 1);
    this.initialize({
      hasNormals: true,
      vertexCount: (nu + 1) * (nv + 1),
      elementCount: 3 * (2 * nu * nv),
      elementWireCount: 2 * (wireLinesU + wireLinesV),
    });

    // prepare grid vertices
    this._gridVerts = [];

    const lerp = [0, 0];
    for (let iv = 0; iv <= nv; iv++) {
      for (let iu = 0; iu <= nu; iu++) {
        // lerp = point in input grid [0,1]^2
        lerp[0] = iu / nu;
        lerp[1] = iv / nv;
        this._createVertex(lerp);
      }
    }
    this._createGlue();

    // prepare grid faces, GeomData elements
    this._gridFaces = [];

    for (let iv = 0; iv < nv; iv++) {
      for (let iu = 0; iu < nu; iu++) {
        // grid indices at 4 corners of grid square
        // (vertex indices are in order of creation in loop above)
        const i00 = (iv * (nu + 1)) + iu;
        const i10 = i00 + 1;
        const i01 = i00 + (nu + 1);
        const i11 = i01 + 1;

        // make 2 tris for grid square, with alternating diagonal direction
        if (((iu + iv) % 2) === 0) {
          this._createFace(i00, i10, i11);
          this._createFace(i00, i11, i01);
        } else {
          this._createFace(i10, i01, i00);
          this._createFace(i10, i11, i01);
        }
      }
    }

    // prepare wireframe edges
    let edgeIndex = 0;
    for (let iv = 0; iv <= nv; iv++) {
      for (let iu = 0; iu < nu; iu++) {
        const i00 = (iv * (nu + 1)) + iu;
        const i10 = i00 + 1;
        this.setLine(edgeIndex++, 0, i00, i10);
      }
    }
    for (let iv = 0; iv < nv; iv++) {
      for (let iu = 0; iu <= nu; iu++) {
        const i00 = (iv * (nu + 1)) + iu;
        const i01 = i00 + (nu + 1);
        this.setLine(edgeIndex++, 0, i00, i01);
      }
    }
  }

  // TODO quick fix for normals when surface loops in u or v direction
  // -----------------------------------
  _createGlue() {
    const nu = this.opts.n[0];
    const nv = this.opts.n[1];

    // pairs of vertices that are optionally "glued" together
    this._glueU = [];
    for (let iv = 0; iv <= nv; iv++) {
      const uMin = this._gridVerts[iv * (nu + 1)];
      const uMax = this._gridVerts[(iv * (nu + 1)) + nu];
      this._glueU.push([uMin, uMax]);
    }

    this._glueV = [];
    for (let iu = 0; iu <= nu; iu++) {
      const vMin = this._gridVerts[iu];
      const vMax = this._gridVerts[(nv * (nu + 1)) + iu];
      this._glueV.push([vMin, vMax]);
    }
  }

  _glue() {
    // need to add both U and V pairs before normalizing either: if
    // loopU and loopV are both true, one instance of 4 verts glued

    // add the normals of glued vertices
    if (this.opts.loopU) {
      for (let i = 0; i < this._glueU.length; i++) {
        const pn = this._glueU[i][0].normal;
        const qn = this._glueU[i][1].normal;
        Vec3.add(pn, pn, qn);
        Vec3.copy(qn, pn);
      }
    }

    if (this.opts.loopV) {
      for (let i = 0; i < this._glueV.length; i++) {
        const pn = this._glueV[i][0].normal;
        const qn = this._glueV[i][1].normal;
        Vec3.add(pn, pn, qn);
        Vec3.copy(qn, pn);
      }
    }

    // normalize after any adding
    if (this.opts.loopU) {
      for (let i = 0; i < this._glueU.length; i++) {
        const pn = this._glueU[i][0].normal;
        const qn = this._glueU[i][1].normal;
        Vec3.normalize(pn, pn);
        Vec3.normalize(qn, qn);
      }
    }

    if (this.opts.loopV) {
      for (let i = 0; i < this._glueV.length; i++) {
        const pn = this._glueV[i][0].normal;
        const qn = this._glueV[i][1].normal;
        Vec3.normalize(pn, pn);
        Vec3.normalize(qn, qn);
      }
    }
  }

  // generate geometry
  // updates data contained in geometry, _gridVerts, _gridFaces, rangeOut
  // ===================================

  // compute normal at grid face using gridVertex.position data
  // add the normal to each incident vertex's normal
  _computeFaceNormal(gridFace) {
    // pqr = face's grid vertices in CCW order
    const p = gridFace.incidentVerts[0];
    const q = gridFace.incidentVerts[1];
    const r = gridFace.incidentVerts[2];

    // compute n = (q-p) X (r-p)
    Vec3.subtract(TEMPVEC3A, q.position, p.position);
    Vec3.subtract(TEMPVEC3B, r.position, p.position);
    Vec3.cross(TEMPVEC3A, TEMPVEC3A, TEMPVEC3B);
    // normalize
    Vec3.normalize(TEMPVEC3A, TEMPVEC3A);

    // add to incident vertices' normals
    Vec3.add(p.normal, p.normal, TEMPVEC3A);
    Vec3.add(q.normal, q.normal, TEMPVEC3A);
    Vec3.add(r.normal, r.normal, TEMPVEC3A);
  }

  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    // compute function and color on each grid vertex
    for (let i = 0; i < this._gridVerts.length; i++) {
      const gridVertex = this._gridVerts[i];

      const { pIn } = gridVertex;
      Vec3.copy(gridVertex.position, this.opts.f(pIn[0], pIn[1]));
      const c = this.opts.color(pIn[0], pIn[1], gridVertex.position);

      // update GeomData
      this.setPosition(i, gridVertex.position);
      this.setColor(i, c);
    }

    // zero the vertex normals
    for (let i = 0; i < this._gridVerts.length; i++) {
      Vec3.zero(this._gridVerts[i].normal);
    }

    // for each face, add its normal to its incident vertices' normals
    for (let i = 0; i < this._gridFaces.length; i++) {
      this._computeFaceNormal(this._gridFaces[i]);
    }
    this._glue();

    // update GeomData normals
    for (let i = 0; i < this._gridVerts.length; i++) {
      const gridVertex = this._gridVerts[i];

      Vec3.normalize(gridVertex.normal, gridVertex.normal);
      this.setNormal(i, gridVertex.normal);
    }
  }

  // TODO: the following is just for testing, probably remove

  // get skeleton
  // ===================================

  // gives input value to GeomSkeleton
  getSlices(uValues, vValues) {
    const nu = this.opts.n[0];
    const nv = this.opts.n[1];

    const rangeU = this.opts.rangeIn.u;
    const rangeV = this.opts.rangeIn.v;

    // ixVisible[i] = true iff corresponding value shows up in xValues
    const iuVisible = convertValuesToFilter(rangeU, nu, uValues, this.opts.loopU);
    const ivVisible = convertValuesToFilter(rangeV, nv, vValues, this.opts.loopV);

    // find balls
    const balls = [];

    let gvi = 0;
    for (let iv = 0; iv <= nv; iv++) {
      for (let iu = 0; iu <= nu; iu++) {
        if (iuVisible[iu] || ivVisible[iv]) {
          balls.push({ vertex: this._gridVerts[gvi] });
        }
        gvi++;
      }
    }

    // find tubes
    const tubes = [];

    for (let iu = 0; iu <= nu; iu++) {
      if (!iuVisible[iu]) { continue; }

      for (let iv = 0; iv < nv; iv++) {
        const index = (iv * (nu + 1)) + iu;
        const p = this._gridVerts[index];
        const q = this._gridVerts[index + (nu + 1)];
        tubes.push({ vertices: [p, q] });
      }
    }

    for (let iv = 0; iv <= nv; iv++) {
      if (!ivVisible[iv]) { continue; }

      for (let iu = 0; iu < nu; iu++) {
        const index = (iv * (nu + 1)) + iu;
        const p = this._gridVerts[index];
        const q = this._gridVerts[index + 1];
        tubes.push({ vertices: [p, q] });
      }
    }

    // done
    return { balls, tubes };
  }
}

export default GeomSurface;
