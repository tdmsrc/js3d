import { Vec3 } from './mathVec';
import GeomData from './geomData';
import GeomBundle from './geomBundle';

const COLOR_BASE = [0, 0, 0];
const COLOR_VECT = [1, 1, 1];

// ===================================
// GEOMETRY: VECTORS
// collection of 3d vectors
// ===================================

/** A single vector in the collection
* @typedef {Object} GeomVectors~vector
*
* @property {number[]} position - Position of vector, as [x,y,z]
* @property {number[]} normal - Direction of vector, as [x,y,z] (need not be normalized)
*/

/** Creates geometry consisting of a collection of vectors.
*   <br>Must call {@link GeomVectors#generate} after creation, or to update vector properties.
*   <br>Use {@link GeomVectors#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomVectors~vector[]} opts.vectors - Collection of vectors
* @param {number} opts.radius - Radius of vector
* @param {number} opts.size - Length of each vector
* @param {number[]} opts.color - Color of each vector
*/
class GeomVectors {
  constructor(opts) {
    this.opts = opts;

    // these are created by _prepare
    this.instances = null;
    this.bundle = null; // geometry given by an underlying GeomBundle

    this._prepare();
  }

  // return a GeomData representing one vector
  // (initially oriented as [1,0,0], with radius=length=1)
  // -----------------------------------
  static _createGeometry() {
    // set quality and proportions
    const m = 4; // tube vertices
    const n = 6; // head vertices

    const rTube = 0.333; const rHead = 1;
    const lTube = 0.5; const lHead = 0.5;
    const dInvHead = 1 / Math.sqrt((rHead * rHead) + (lHead * lHead));

    // initialize GeomData
    const gdata = new GeomData();
    gdata.initialize({
      hasNormals: true,
      elementCount: 3 * ((3 * m) + (2 * n) + -4),
      elementWireCount: 2 * ((3 * m) + (2 * n)),
      vertexCount: (3 * m) + (3 * n),
    });

    // put vertex attributes
    // --------------------
    const putAttribs = (index, c, x, y, z, nx, ny, nz) => {
      gdata.setPosition(index, [x, y, z]);
      gdata.setNormal(index, [nx, ny, nz]);
      gdata.setColor(index, c);
    };

    // vertices: stick part (base + tube)
    for (let i = 0; i < m; i++) {
      const t = (2 * Math.PI) * (i / m);
      const ct = Math.cos(t);
      const st = Math.sin(t);

      // base
      putAttribs(
        i, COLOR_BASE,
        0, -rTube * ct, rTube * st,
        -1, 0, 0
      );

      // tube
      putAttribs(
        m + i, COLOR_VECT,
        0, -rTube * ct, rTube * st,
        0, -ct, st
      );
      putAttribs(
        (2 * m) + i, COLOR_VECT,
        lTube, -rTube * ct, rTube * st,
        0, -ct, st
      );
    }

    // vertices: head (base + cone)
    for (let i = 0; i < n; i++) {
      const t = (2 * Math.PI) * (i / n);
      const ct = Math.cos(t);
      const st = Math.sin(t);

      // base
      putAttribs(
        (3 * m) + i, COLOR_BASE,
        lTube, -rHead * ct, rHead * st,
        -1, 0, 0
      );

      // cone
      const th = (2 * Math.PI) * ((i + 0.5) / n);
      const cth = Math.cos(th);
      const sth = Math.sin(th);

      putAttribs(
        (3 * m) + n + i, COLOR_VECT,
        lTube, -rHead * ct, rHead * st,
        dInvHead * rHead, -dInvHead * lHead * ct, dInvHead * lHead * st
      );
      putAttribs(
        (3 * m) + (2 * n) + i, COLOR_VECT,
        lTube + lHead, 0, 0,
        dInvHead * rHead, -dInvHead * lHead * cth, dInvHead * lHead * sth
      );
    }

    // put faces
    // --------------------
    let f0 = 0;

    // base
    for (let i = 1; i < m - 1; i++) {
      gdata.setFace(f0++, 0, 0, i, i + 1);
    }

    // tube
    for (let i = 0; i < m; i++) {
      const j = (i + 1) % m;
      gdata.setFace(f0++, m, i, i + m, j + m);
      gdata.setFace(f0++, m, i, j + m, j);
    }

    // head base
    for (let i = 1; i < n - 1; i++) {
      gdata.setFace(f0++, 3 * m, 0, i, i + 1);
    }

    // head
    for (let i = 0; i < n; i++) {
      const j = (i + 1) % n;
      gdata.setFace(f0++, (3 * m) + n, i, i + n, j);
    }

    // wireframe edges
    // --------------------
    let e0 = 0;

    // tube
    for (let i = 0; i < m; i++) {
      const j = (i + 1) % m;
      gdata.setLine(e0++, m, i, j);
      gdata.setLine(e0++, m, i, i + m);
      gdata.setLine(e0++, 2 * m, i, j);
    }

    // head
    for (let i = 0; i < n; i++) {
      const j = (i + 1) % n;
      gdata.setLine(e0++, (3 * m) + n, i, j);
      gdata.setLine(e0++, (3 * m) + n, i, i + n);
    }

    return gdata;
  }

  // create GeomBundle and instances
  // -----------------------------------
  _prepare() {
    // prepare instances
    this.instances = [];
    for (let i = 0; i < this.opts.vectors.length; i++) {
      const instance = {
        scale: [this.opts.size, this.opts.radius, this.opts.radius],
        hasOrientation: true,
        orientation: [1, 0, 0],
        position: [0, 0, 0],
        color: this.opts.color,
      };
      this.instances.push(instance);
    }

    // create GeomBundle
    const group = {
      geometry: GeomVectors._createGeometry(),
      instances: this.instances,
    };
    const optsBundle = { groups: [group] };
    this.bundle = new GeomBundle(optsBundle);
  }

  // call at least once, or to update vectors
  // ===================================
  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    // update instances
    for (let i = 0; i < this.opts.vectors.length; i++) {
      const vector = this.opts.vectors[i];
      const instance = this.instances[i];

      Vec3.copy(instance.position, vector.position);
      Vec3.copy(instance.orientation, vector.normal);
    }

    this.bundle.generate();
  }

  /** Create a new {@link SceneObject} with this geometry.
  * @return {SceneObject}
  */
  instance() {
    return this.bundle.instance();
  }
}

export default GeomVectors;
