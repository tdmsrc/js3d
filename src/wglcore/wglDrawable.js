// ===================================
// DRAWABLE
// WebGL version of GeomData
// ===================================

/** Stores the data of a {@link GeomData} in WebGLBuffer objects.
* @param {WebGLRenderingContext} gl
* @param {GeomData} geometry
*
* @prop {WebGLBuffer} bufElement - Corresponds to {@link GeomData}.elements
* @prop {WebGLBuffer} bufElementWire - Corresponds to {@link GeomData}.elementsWire
* @prop {WebGLBuffer} bufPosition - Corresponds to {@link GeomData}.position
* @prop {WebGLBuffer} bufNormal - Corresponds to {@link GeomData}.normal
* @prop {WebGLBuffer} bufTex - Corresponds to {@link GeomData}.tex
* @prop {WebGLBuffer} bufColor - Corresponds to {@link GeomData}.color
*/
class WGLDrawable {
  constructor(gl, geometry) {
    // store some data so that modifying geometry doesn't break the drawable
    this.elementCount = geometry.elementCount;
    this.elementWireCount = geometry.elementWireCount;

    this.hasNormals = geometry.hasNormals;
    this.hasTexture = geometry.hasTexture;

    // tally total size of drawable data
    let intCount = 0;
    let floatCount = 0;

    // create index buffers
    intCount += geometry.elements.length;
    this.bufElement = WGLDrawable.createElementArrayBuffer(gl, geometry.elements);

    intCount += geometry.elementsWire.length;
    this.bufElementWire = WGLDrawable.createElementArrayBuffer(gl, geometry.elementsWire);

    // create attribute buffers
    floatCount += geometry.position.length;
    this.bufPosition = WGLDrawable.createVertexAttribBuffer(gl, geometry.position);

    if (this.hasNormals) {
      floatCount += geometry.normal.length;
      this.bufNormal = WGLDrawable.createVertexAttribBuffer(gl, geometry.normal);
    }

    if (this.hasTexture) {
      floatCount += geometry.tex.length;
      this.bufTex = WGLDrawable.createVertexAttribBuffer(gl, geometry.tex);
    } else {
      floatCount += geometry.color.length;
      this.bufColor = WGLDrawable.createVertexAttribBuffer(gl, geometry.color);
    }

    // debug: total size
    const intBytes = intCount * Uint16Array.BYTES_PER_ELEMENT;
    const floatBytes = floatCount * Float32Array.BYTES_PER_ELEMENT;
    const sizeKB = (intBytes + floatBytes) / 1024;
    console.log(`Created buffers (${intCount} Uint16, ${floatCount} Float32, ${sizeKB} KB)`);
  }

  // buffer creation
  // -----------------------------------
  static createElementArrayBuffer(gl, raw) {
    const id = gl.createBuffer();

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, id);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, raw, gl.STATIC_DRAW);

    return id;
  }

  static createVertexAttribBuffer(gl, raw) {
    const id = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, id);
    gl.bufferData(gl.ARRAY_BUFFER, raw, gl.DYNAMIC_DRAW);

    return id;
  }

  // update buffers from geometry
  // -----------------------------------
  /** Use bufferSubData to update vertex attribute buffers (position, normal, color, and tex).
  *   <br>Source should be the same {@link GeomData} used to create this drawable.
  * @param {WebGLRenderingContext} gl
  * @param {GeomData} geometry
  */
  update(gl, geometry) {
    gl.bindBuffer(gl.ARRAY_BUFFER, this.bufPosition);
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.position);

    if (this.hasNormals) {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.bufNormal);
      gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.normal);
    }

    if (this.hasTexture) {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.bufTex);
      gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.tex);
    } else {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.bufColor);
      gl.bufferSubData(gl.ARRAY_BUFFER, 0, geometry.color);
    }
  }

  // buffer deletion
  // -----------------------------------
  /** Delete the buffers.
  * @param {WebGLRenderingContext} gl
  */
  free(gl) {
    gl.deleteBuffer(this.bufElement);
    gl.deleteBuffer(this.bufElementWire);

    gl.deleteBuffer(this.bufPosition);

    if (this.hasNormals) {
      gl.deleteBuffer(this.bufNormal);
    }

    if (this.hasTexture) {
      gl.deleteBuffer(this.bufTex);
    } else {
      gl.deleteBuffer(this.bufColor);
    }
  }

  // draw
  // -----------------------------------
  /** Draw using drawElements in TRIANGLES mode.
  *   <br>Use {@link WGLShaderCommon#prepareVertexAttribs} first.
  * @param {WebGLRenderingContext} gl
  */
  drawTriangles(gl) {
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.bufElement);
    gl.drawElements(gl.TRIANGLES, this.elementCount, gl.UNSIGNED_SHORT, 0);
  }

  /** Draw using drawElements in LINES mode.
  *   <br>Use {@link WGLShaderCommon#prepareVertexAttribs} first.
  * @param {WebGLRenderingContext} gl
  */
  drawLines(gl) {
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.bufElementWire);
    gl.drawElements(gl.LINES, this.elementWireCount, gl.UNSIGNED_SHORT, 0);
  }
}

export default WGLDrawable;
