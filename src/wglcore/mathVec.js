// ===================================
// VEC3
// these methods act on arrays [x,y,z]
// any method that produces a new vector has a "target" parameter
// ===================================

/** Collection of functions relating to 3-dimensional vectors.
*   <br>Vectors are just arrays, i.e. [x,y,z].
*/
const Vec3 = {

  /** Set target equal to the zero vector.
  * @param {number[]} target - The vector to modify
  */
  zero: (target) => {
    target[0] = 0;
    target[1] = 0;
    target[2] = 0;
  },

  /** Set target = u.
  * @param {number[]} target - The vector to modify
  * @param {number[]} u - The vector to copy
  */
  copy: (target, u) => {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
  },

  // basic methods
  // -----------------------------------
  /** Clamp each entry to the indicated interval
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} v - The vector with entries to be clamped
  * @param {number} a - Left end of the interval
  * @param {number} b - Right end of the interval
  */
  clamp: (target, v, a, b) => {
    target[0] = (v[0] < a) ? a : (v[0] > b) ? b : v[0];
    target[1] = (v[1] < a) ? a : (v[1] > b) ? b : v[1];
    target[2] = (v[2] < a) ? a : (v[2] > b) ? b : v[2];
  },

  /** Set target = u + v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  add: (target, u, v) => {
    target[0] = u[0] + v[0];
    target[1] = u[1] + v[1];
    target[2] = u[2] + v[2];
  },

  /** Set target = u - v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  subtract: (target, u, v) => {
    target[0] = u[0] - v[0];
    target[1] = u[1] - v[1];
    target[2] = u[2] - v[2];
  },

  /** Set target = u + k*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @param {number} k - Scale factor for v
  */
  addMultiple: (target, u, v, k) => {
    target[0] = u[0] + (k * v[0]);
    target[1] = u[1] + (k * v[1]);
    target[2] = u[2] + (k * v[2]);
  },

  /** Set target = a*u + b*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number} a - Scale factor for u
  * @param {number[]} u - Left vector
  * @param {number} b - Scale factor for v
  * @param {number[]} v - Right vector
  */
  linearCombination: (target, a, u, b, v) => {
    target[0] = (a * u[0]) + (b * v[0]);
    target[1] = (a * u[1]) + (b * v[1]);
    target[2] = (a * u[2]) + (b * v[2]);
  },

  /** Set target = k*u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being scaled
  * @param {number} k - Scale factor for u
  */
  scale: (target, u, k) => {
    target[0] = k * u[0];
    target[1] = k * u[1];
    target[2] = k * u[2];
  },

  /** Set target = -u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being flipped
  */
  flip: (target, u) => {
    target[0] = -u[0];
    target[1] = -u[1];
    target[2] = -u[2];
  },

  /** Return the dot product of u and v.
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dot: (u, v) => (u[0] * v[0]) + (u[1] * v[1]) + (u[2] * v[2]),

  /** Return the square of the distance between u and v (i.e., dot(u-v, u-v)).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  distsq: (u, v) => {
    let ret = 0;

    let temp;
    temp = u[0] - v[0]; ret += temp * temp;
    temp = u[1] - v[1]; ret += temp * temp;
    temp = u[2] - v[2]; ret += temp * temp;

    return ret;
  },

  // derivative methods
  // -----------------------------------
  /** Return the square of the norm of u (i.e., dot(u, u)).
  * @param {number[]} u - The vector
  * @return {number}
  */
  normsq: u => Vec3.dot(u, u),

  /** Return the norm of u (i.e., sqrt(dot(u, u))).
  * @param {number[]} u - The vector
  * @return {number}
  */
  norm: u => Math.sqrt(Vec3.normsq(u)),

  /** Return the distance between u and v (i.e., sqrt(dot(u-v, u-v))).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dist: (u, v) => Math.sqrt(Vec3.distsq(u, v)),

  /** Set target = u / norm(u).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  normalize: (target, u) => {
    const s = 1 / Vec3.norm(u);
    Vec3.scale(target, u, s);
  },

  /** Set target = the projection of u onto v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} v - The vector to project onto
  */
  proj: (target, u, v) => {
    const s = Vec3.dot(u, v) / Vec3.normsq(v);
    Vec3.scale(target, v, s);
  },

  /** Set target = the projection of u onto the plane with normal n.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} n - The normal of the plane being projected onto
  */
  projn: (target, u, n) => {
    const s = Vec3.dot(u, n) / Vec3.normsq(n);
    Vec3.linearCombination(target, 1, u, -s, n);
  },

  // dimension-specific methods
  // -----------------------------------
  /** Set target = u cross v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  cross: (target, u, v) => {
    const x = (u[1] * v[2]) - (u[2] * v[1]);
    const y = (u[2] * v[0]) - (u[0] * v[2]);
    const z = (u[0] * v[1]) - (u[1] * v[0]);
    target[0] = x; target[1] = y; target[2] = z;
  },

  /** Set target = [u.x, u.y, u.z, w].
  *   <br>Note that "target" is 4-dimensional and "u" is 3-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector to augment
  * @param {number} w - New w-value
  */
  augment: (target, u, w) => {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
    target[3] = w;
  },

  /** Set target = any vector perpendicular to u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  orthogonal: (target, u) => {
    // if u[i]^2 <= |u|^2 / 3 (must be true for one i), cross with e_i
    const cmp = Vec3.normsq(u) / 3;

    if (u[0] * u[0] < cmp) {
      const temp = u[1];
      target[0] = 0;
      target[1] = u[2];
      target[2] = -temp;
    } else if (u[1] * u[1] < cmp) {
      const temp = u[0];
      target[0] = -u[2];
      target[1] = 0;
      target[2] = temp;
    } else {
      const temp = u[0];
      target[0] = u[1];
      target[1] = -temp;
      target[2] = 0;
    }
  },
};

// ===================================
// VEC4
// these methods act on arrays [x,y,z,w]
// any method that produces a new vector has a "target" parameter
// ===================================

/** Collection of functions relating to 4-dimensional vectors.
*   <br>Vectors are just arrays, i.e. [x,y,z,w].
*/
const Vec4 = {

  /** Set target equal to the zero vector.
  * @param {number[]} target - The vector to modify
  */
  zero: (target) => {
    target[0] = 0;
    target[1] = 0;
    target[2] = 0;
    target[3] = 0;
  },

  /** Set target = u.
  * @param {number[]} target - The vector to modify
  * @param {number[]} u - The vector to copy
  */
  copy: (target, u) => {
    target[0] = u[0];
    target[1] = u[1];
    target[2] = u[2];
    target[3] = u[3];
  },

  // basic methods
  // -----------------------------------
  /** Set target = u + v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  add: (target, u, v) => {
    target[0] = u[0] + v[0];
    target[1] = u[1] + v[1];
    target[2] = u[2] + v[2];
    target[3] = u[3] + v[3];
  },

  /** Set target = u - v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  */
  subtract: (target, u, v) => {
    target[0] = u[0] - v[0];
    target[1] = u[1] - v[1];
    target[2] = u[2] - v[2];
    target[3] = u[3] - v[3];
  },

  /** Set target = u + k*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @param {number} k - Scale factor for v
  */
  addMultiple: (target, u, v, k) => {
    target[0] = u[0] + (k * v[0]);
    target[1] = u[1] + (k * v[1]);
    target[2] = u[2] + (k * v[2]);
    target[3] = u[3] + (k * v[3]);
  },

  /** Set target = a*u + b*v.
  * @param {number[]} target - The vector which will store the result
  * @param {number} a - Scale factor for u
  * @param {number[]} u - Left vector
  * @param {number} b - Scale factor for v
  * @param {number[]} v - Right vector
  */
  linearCombination: (target, a, u, b, v) => {
    target[0] = (a * u[0]) + (b * v[0]);
    target[1] = (a * u[1]) + (b * v[1]);
    target[2] = (a * u[2]) + (b * v[2]);
    target[3] = (a * u[3]) + (b * v[3]);
  },

  /** Set target = k*u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being scaled
  * @param {number} k - Scale factor for u
  */
  scale: (target, u, k) => {
    target[0] = k * u[0];
    target[1] = k * u[1];
    target[2] = k * u[2];
    target[3] = k * u[3];
  },

  /** Set target = -u.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector being flipped
  */
  flip: (target, u) => {
    target[0] = -u[0];
    target[1] = -u[1];
    target[2] = -u[2];
    target[3] = -u[3];
  },

  /** Return the dot product of u and v.
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dot: (u, v) => (u[0] * v[0]) + (u[1] * v[1]) + (u[2] * v[2]) + (u[3] * v[3]),

  /** Return the square of the distance between u and v (i.e., dot(u-v, u-v)).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  distsq: (u, v) => {
    let ret = 0;

    let temp;
    temp = u[0] - v[0]; ret += temp * temp;
    temp = u[1] - v[1]; ret += temp * temp;
    temp = u[2] - v[2]; ret += temp * temp;
    temp = u[3] - v[3]; ret += temp * temp;

    return ret;
  },

  // derivative methods
  // -----------------------------------
  /** Return the square of the norm of u (i.e., dot(u, u)).
  * @param {number[]} u - The vector
  * @return {number}
  */
  normsq: u => Vec4.dot(u, u),

  /** Return the norm of u (i.e., sqrt(dot(u, u))).
  * @param {number[]} u - The vector
  * @return {number}
  */
  norm: u => Math.sqrt(Vec4.normsq(u)),

  /** Return the distance between u and v (i.e., sqrt(dot(u-v, u-v))).
  * @param {number[]} u - Left vector
  * @param {number[]} v - Right vector
  * @return {number}
  */
  dist: (u, v) => Math.sqrt(Vec4.distsq(u, v)),

  /** Set target = u / norm(u).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector
  */
  normalize: (target, u) => {
    const s = 1 / Vec4.norm(u);
    Vec4.scale(target, u, s);
  },

  /** Set target = the projection of u onto v.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} v - The vector to project onto
  */
  proj: (target, u, v) => {
    const s = Vec4.dot(u, v) / Vec4.normsq(v);
    Vec4.scale(target, v, s);
  },

  /** Set target = the projection of u onto the hyperplane with normal n.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - The vector being projected
  * @param {number[]} n - The normal of the hyperplane being projected onto
  */
  projn: (target, u, n) => {
    const s = Vec4.dot(u, n) / Vec4.normsq(n);
    Vec4.linearCombination(target, 1, u, -s, n);
  },

  // dimension-specific methods
  // -----------------------------------
  /** Set target = [u.x/u.w, u.y/u.w, u.z/u.w].
  *   <br>Note that "target" is 3-dimensional and "u" is 4-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} u - Vector to dehomogenize
  */
  dehomogenize: (target, u) => {
    target[0] = u[0] / u[3];
    target[1] = u[1] / u[3];
    target[2] = u[2] / u[3];
  },

  // quaternions: [a,b,c,d] represents a + bi + cj + dk
  // -----------------------------------
  /** Set target = x * y, where target, x, y are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Left quaternion
  * @param {number[]} y - Right quaternion
  */
  qMultiply: (target, x, y) => {
    const a = (x[0] * y[0]) - (x[1] * y[1]) - (x[2] * y[2]) - (x[3] * y[3]);
    const b = (x[0] * y[1]) + (x[1] * y[0]) + (x[2] * y[3]) - (x[3] * y[2]);
    const c = (x[0] * y[2]) - (x[1] * y[3]) + (x[2] * y[0]) + (x[3] * y[1]);
    const d = (x[0] * y[3]) + (x[1] * y[2]) - (x[2] * y[1]) + (x[3] * y[0]);
    target[0] = a; target[1] = b; target[2] = c; target[3] = d;
  },

  /** Set target = conjugate of x, where target and x are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Quaternion to conjugate
  */
  qConjugate: (target, x) => {
    target[0] = x[0];
    target[1] = -x[1];
    target[2] = -x[2];
    target[3] = -x[3];
  },

  /** Set target = inverse of x, where target and x are quaternions
  *   ([a,b,c,d] = a+bi+cj+dk).
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} x - Quaternion to invert
  */
  qInverse: (target, x) => {
    Vec4.scale(target, x, 1 / Vec4.normsq(x));
    Vec4.qConjugate(target, target);
  },
};

export {
  Vec3,
  Vec4,
};
