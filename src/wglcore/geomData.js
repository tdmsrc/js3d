import SceneObject from './geomSceneObject';

// ===================================
// GEOMETRY: RAW DATA
// ===================================

/** Stores raw geometry data.  Call {@link GeomData#initialize} before doing anything.
*   <br>Element array entries refer to an index in the attribute arrays.
*   <br>Example: "1" means [position[3], position[4], position[5]], etc.
*   <br>Note: Only one of 'color' or 'tex' will be used, depending on the 'hasTexture' option.
*
* @prop {number} vertexCount - See {@link GeomData#initialize}
* @prop {number} elementCount - See {@link GeomData#initialize}
* @prop {number} elementWireCount - See {@link GeomData#initialize}
* @prop {number} hasNormals - See {@link GeomData#initialize}
* @prop {number} hasTexture - See {@link GeomData#initialize}
*
* @prop {Uint16Array} elements - Triangles as [i0,j0,k0, i1,j1,k1, ...]
* @prop {Uint16Array} elementsWire - Lines as [i0,j0, i1,j1, i2,j2, ...]
* @prop {Float32Array} position - Array of positions as [x0,y0,z0, x1,y1,z1, ...]
* @prop {Float32Array} normal - Array of normals as [x0,y0,z0, x1,y1,z1, ...] (or null if hasNormals is false)
* @prop {Float32Array} color - Array of colors as [r0,g0,b0, r1,g1,b1, ...] (or null if hasTexture is true)
* @prop {Float32Array} tex - Array of tex coords as [u0,v0, u1,v1, ...] (or null if hasTexture is false)
*/
class GeomData {
  /** Initialize arrays storing the geometry.
  * @param {Object} opts
  * @param {number} opts.vertexCount - Determines the size of vertex attribute arrays (should not exceed 65535)
  * @param {number} opts.elementCount - Size of elements array: 3*(num triangles)
  * @param {number} opts.elementWireCount - Size of elementsWire array: 2*(num lines)
  * @param {boolean} opts.hasNormals - True for triangle data, false for line data
  * @param {boolean} opts.hasTexture - (Optional, default false) Determine whether to use color or tex attribute array
  */
  initialize(opts) {
    this.elementCount = opts.elementCount;
    this.elementWireCount = opts.elementWireCount;
    this.vertexCount = opts.vertexCount;

    this.hasNormals = opts.hasNormals;
    this.hasTexture = false;
    if (typeof opts.hasTexture !== 'undefined') {
      this.hasTexture = opts.hasTexture;
    }

    // indices cannot exceed max value of Uint16
    if (opts.vertexCount > 65535) {
      console.error('More than 65535 vertices!');
    }

    this.elements = new Uint16Array(opts.elementCount);
    this.elementsWire = new Uint16Array(opts.elementWireCount);
    this.position = new Float32Array(3 * opts.vertexCount);

    if (this.hasTexture) {
      this.tex = new Float32Array(2 * opts.vertexCount);
    } else {
      this.color = new Float32Array(3 * opts.vertexCount);
    }

    this.normal = null;
    if (this.hasNormals) {
      this.normal = new Float32Array(3 * opts.vertexCount);
    }
  }

  // convenience method for creating a SceneObject
  // -----------------------------------
  /** Create a new {@link SceneObject} with this geometry.
  * @return {SceneObject}
  */
  instance() {
    return new SceneObject([this]);
  }

  // convenience methods for setting data
  // -----------------------------------
  /** Set a triangle's vertices.
  * @param {number} index - Index of the triangle, in [0,elementCount/3)
  * @param {number} vOffset - Offset for i, j, k, in [0,vertexCount)
  * @param {number} i - Index of vertex 0 is vOffset+i
  * @param {number} j - Index of vertex 1 is vOffset+j
  * @param {number} k - Index of vertex 2 is vOffset+k
  */
  setFace(index, vOffset, i, j, k) {
    this.elements[(3 * index) + 0] = vOffset + i;
    this.elements[(3 * index) + 1] = vOffset + j;
    this.elements[(3 * index) + 2] = vOffset + k;
  }

  /** Set a line segment's vertices.
  * @param {number} index - Index of the line segment, in [0,elementWireCount/2)
  * @param {number} vOffset - Offset for i, j, in [0,vertexCount)
  * @param {number} i - Index of vertex 0 is vOffset+i
  * @param {number} j - Index of vertex 1 is vOffset+j
  */
  setLine(index, vOffset, i, j) {
    this.elementsWire[(2 * index) + 0] = vOffset + i;
    this.elementsWire[(2 * index) + 1] = vOffset + j;
  }

  /** Set one vertex position.
  * @param {number} index - Index of the vertex, in [0,vertexCount)
  * @param {number[]} p - Position (x = p[0], y = p[1], z = p[2])
  */
  setPosition(index, p) {
    this.position[(3 * index) + 0] = p[0];
    this.position[(3 * index) + 1] = p[1];
    this.position[(3 * index) + 2] = p[2];
  }

  /** Set one vertex normal.
  * @param {number} index - Index of the vertex, in [0,vertexCount)
  * @param {number[]} n - Normal vector (x = n[0], y = n[1], z = n[2])
  */
  setNormal(index, n) {
    this.normal[(3 * index) + 0] = n[0];
    this.normal[(3 * index) + 1] = n[1];
    this.normal[(3 * index) + 2] = n[2];
  }

  /** Set one vertex color.
  * @param {number} index - Index of the vertex, in [0,vertexCount)
  * @param {number[]} c - Color vector (r = c[0], g = c[1], b = c[2])
  */
  setColor(index, c) {
    this.color[(3 * index) + 0] = c[0];
    this.color[(3 * index) + 1] = c[1];
    this.color[(3 * index) + 2] = c[2];
  }

  /** Set one texture coordinate.
  * @param {number} index - Index of the vertex, in [0,vertexCount)
  * @param {number[]} t - Texture coordinate (u = t[0], v = t[1])
  */
  setTex(index, t) {
    this.tex[(2 * index) + 0] = t[0];
    this.tex[(2 * index) + 1] = t[1];
  }
}

export default GeomData;
