import { Vec3 } from './mathVec';
import GeomData from './geomData';
import GeomBundle from './geomBundle';

const TEMP_PQ = [0, 0, 0];

// icosahedron
/* eslint-disable */
const BALL_V = [
  [ 0,  -0.525731, 0.850651], [ 0.850651,  0,  0.525731],
  [ 0.850651,  0, -0.525731], [-0.850651,  0, -0.525731],
  [-0.850651,  0,  0.525731], [-0.525731,  0.850651,  0],
  [ 0.525731,  0.850651,  0], [ 0.525731, -0.850651,  0],
  [-0.525731, -0.850651,  0], [ 0, -0.525731, -0.850651],
  [ 0,  0.525731, -0.850651], [ 0,  0.525731,  0.850651],
];
const BALL_F = [
  [ 1, 2, 6], [ 1, 7, 2], [ 3, 4, 5], [ 4, 3, 8],
  [ 6, 5,11], [ 5, 6,10], [ 9,10, 2], [10, 9, 3],
  [ 7, 8, 9], [ 8, 7, 0], [11, 0, 1], [ 0,11, 4],
  [ 6, 2,10], [ 1, 6,11], [ 3, 5,10], [ 5, 4,11],
  [ 2, 7, 9], [ 7, 1, 0], [ 3, 9, 8], [ 4, 8, 0],
];
const BALL_E = [
  [ 0,11], [ 0, 4], [ 0, 1], [11, 4], [11, 1],
  [ 9,10], [ 9, 2], [ 9, 3], [10, 2], [10, 3],
  [ 1, 2], [ 1, 6], [ 1, 7], [ 2, 6], [ 2, 7],
  [ 4, 3], [ 4, 5], [ 4, 8], [ 3, 5], [ 3, 8],
  [ 5, 6], [ 5,10], [ 5,11], [ 6,10], [ 6,11],
  [ 7, 8], [ 7, 0], [ 7, 9], [ 8, 0], [ 8, 9],
];
/* eslint-enable */

const _createBallGeometry = () => {
  // initialize GeomData
  const gdata = new GeomData();
  gdata.initialize({
    hasNormals: true,
    elementCount: 3 * BALL_F.length,
    elementWireCount: 2 * BALL_E.length,
    vertexCount: BALL_V.length,
  });

  // put vertex attributes
  // --------------------
  for (let i = 0; i < BALL_V.length; i++) {
    const p = BALL_V[i];
    gdata.setPosition(i, p);
    gdata.setNormal(i, p);
    gdata.setColor(i, [1, 1, 1]);
  }

  // put faces
  // --------------------
  for (let i = 0; i < BALL_F.length; i++) {
    const f = BALL_F[i];
    gdata.setFace(i, 0, f[0], f[1], f[2]);
  }

  // wireframe edges
  // --------------------
  for (let i = 0; i < BALL_E.length; i++) {
    const e = BALL_E[i];
    gdata.setLine(i, 0, e[0], e[1]);
  }

  return gdata;
};

// (initially oriented from [0,0,0] to [1,0,0] w/ radius=1)
const _createTubeGeometry = () => {
  // quality
  const m = 6; // tube vertices

  // initialize GeomData
  const gdata = new GeomData();
  gdata.initialize({
    hasNormals: true,
    elementCount: 3 * (2 * m),
    elementWireCount: 2 * (3 * m),
    vertexCount: 2 * m,
  });

  // put vertex attributes
  // --------------------
  const putAttribs = (index, c, x, y, z, nx, ny, nz) => {
    gdata.setPosition(index, [x, y, z]);
    gdata.setNormal(index, [nx, ny, nz]);
    gdata.setColor(index, c);
  };

  for (let i = 0; i < m; i++) {
    const t = (2 * Math.PI) * (i / m);
    const ct = Math.cos(t);
    const st = Math.sin(t);

    putAttribs(
      i, [1, 1, 1],
      0, -ct, st,
      0, -ct, st
    );
    putAttribs(
      m + i, [1, 1, 1],
      1, -ct, st,
      0, -ct, st
    );
  }

  // put faces
  // --------------------
  let f0 = 0;
  for (let i = 0; i < m; i++) {
    const j = (i + 1) % m;
    gdata.setFace(f0++, 0, i, i + m, j + m);
    gdata.setFace(f0++, 0, i, j + m, j);
  }

  // wireframe edges
  // --------------------
  let e0 = 0;
  for (let i = 0; i < m; i++) {
    const j = (i + 1) % m;
    gdata.setLine(e0++, 0, i, j);
    gdata.setLine(e0++, m, i, j);
    gdata.setLine(e0++, 0, i, i + m);
  }

  return gdata;
};

// ===================================
// GEOMETRY: SKELETON
// made of balls joined by tubes
// ===================================

/** A single "ball" in the skeleton.
* @typedef {Object} GeomSkeleton~ball
*
* @property {Object} vertex - Any object with "position" property: <code>{ position: [x,y,z] }</code>
*/

/** A single "tube" in the skeleton.
*   <br>The "vertex" objects are exactly those that work in the "vertex" property of {@link GeomSkeleton~ball}.
* @typedef {Object} GeomSkeleton~tube
*
* @property {Object[]} vertices - Two "vertex" objects, as [vi,vj]
*/

/** Creates a "ball and tube" geometry.
*   <br>Must call {@link GeomSkeleton#generate} after creation, or to update positions/colors.
*   <br>Use {@link GeomSkeleton#instance} to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomSkeleton~ball[]} opts.balls - Collection of balls in the skeleton
* @param {GeomSkeleton~tube[]} opts.tubes - Collection of tubes in the skeleton
* @param {number} opts.ballRadius - Radius of each ball
* @param {number} opts.tubeRadius - Radius of each tube
* @param {number[]} opts.ballColor - Color of each ball, as [r,g,b]
* @param {number[]} opts.tubeColor - Color of each tube, as [r,g,b]
*/
class GeomSkeleton {
  constructor(opts) {
    this.opts = opts;

    // these are created by _prepare
    this.ballInstances = null;
    this.tubeInstances = null;
    this.bundle = null; // geometry given by an underlying GeomBundle

    this._prepare();
  }

  // create GeomBundle and instances
  // -----------------------------------
  _prepare() {
    // prepare ball instances
    this.ballInstances = [];
    for (let i = 0; i < this.opts.balls.length; i++) {
      const instance = {
        scale: [this.opts.ballRadius, this.opts.ballRadius, this.opts.ballRadius],
        hasOrientation: false,
        position: [0, 0, 0],
        color: this.opts.ballColor,
      };
      this.ballInstances.push(instance);
    }

    // prepare tube instances
    this.tubeInstances = [];
    for (let i = 0; i < this.opts.tubes.length; i++) {
      const instance = {
        scale: [1, this.opts.tubeRadius, this.opts.tubeRadius],
        hasOrientation: true,
        orientation: [1, 0, 0],
        position: [0, 0, 0],
        color: this.opts.tubeColor,
      };
      this.tubeInstances.push(instance);
    }

    // create GeomBundle
    const ballGroup = {
      geometry: _createBallGeometry(),
      instances: this.ballInstances,
    };
    const tubeGroup = {
      geometry: _createTubeGeometry(),
      instances: this.tubeInstances,
    };
    const optsBundle = { groups: [ballGroup, tubeGroup] };
    this.bundle = new GeomBundle(optsBundle);
  }

  // call at least once, or to update skeleton
  // ===================================
  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    // update ball instances
    for (let i = 0; i < this.opts.balls.length; i++) {
      const ball = this.opts.balls[i];
      const instance = this.ballInstances[i];
      Vec3.copy(instance.position, ball.vertex.position);
    }

    // update tube instances
    for (let i = 0; i < this.opts.tubes.length; i++) {
      const tube = this.opts.tubes[i];
      const instance = this.tubeInstances[i];

      const p = tube.vertices[0].position;
      const q = tube.vertices[1].position;
      Vec3.subtract(TEMP_PQ, q, p);

      instance.scale[0] = Vec3.norm(TEMP_PQ);
      Vec3.copy(instance.position, p);
      Vec3.copy(instance.orientation, TEMP_PQ);
    }

    this.bundle.generate();
  }

  /** Create a new {@link SceneObject} with this geometry.
  * @return {SceneObject}
  */
  instance() {
    return this.bundle.instance();
  }
}

export default GeomSkeleton;
