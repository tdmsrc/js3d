import { Mat3, Mat4 } from './mathMat';

const TEMPSERIAL4 = new Float32Array(16);
const TEMPSERIAL3 = new Float32Array(9);

const LOG_PREFIX = 'WGLShader';
const CHECK_ERRORS = true;

const checkCompileStatus = (gl, description, id) => {
  if (gl.getShaderParameter(id, gl.COMPILE_STATUS)) {
    console.log(`[${LOG_PREFIX}] [${description}] Compile status is OK.`);
  } else {
    console.error(`[${LOG_PREFIX}] [${description}] Compile failed! Info log:`);
    console.error(gl.getShaderInfoLog(id));
  }
};

const checkLinkStatus = (gl, description, id) => {
  if (gl.getProgramParameter(id, gl.LINK_STATUS)) {
    console.log(`[${LOG_PREFIX}] [${description}] Link status is OK.`);
  } else {
    console.error(`[${LOG_PREFIX}] [${description}] Link failed! Info log:`);
    console.error(gl.getProgramInfoLog(id));
  }
};

// ===================================
// SHADER PROGRAM (GENERIC)
// ===================================

/** Abstract wrapper for a shader program.
*   <br>Implement {@link WGLShaderProgram#_setVertexAttributes} and {@link WGLShaderProgram#_getUniformNames}.
*   <br>Call {@link WGLShaderProgram#_initialize} before doing anything.
*/
class WGLShaderProgram {
  // initialize
  // -----------------------------------
  /** Create, compile and link WebGLShader and WebGLProgram objects.
  * @param {WebGLRenderingContext} gl
  * @param {string} description - Description for logging status
  * @param {string} svSrc - Vertex shader source
  * @param {string} sfSrc - Fragment shader source
  * @protected
  */
  _initialize(gl, description, svSrc, sfSrc) {
    // vertex shader
    const idVert = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(idVert, svSrc);
    gl.compileShader(idVert);
    if (CHECK_ERRORS) {
      checkCompileStatus(gl, `${description} (vertex)`, idVert);
    }

    // fragment shader
    const idFrag = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(idFrag, sfSrc);
    gl.compileShader(idFrag);
    if (CHECK_ERRORS) {
      checkCompileStatus(gl, `${description} (fragment)`, idFrag);
    }

    // prepare shader program
    this.id = gl.createProgram();

    gl.attachShader(this.id, idVert);
    gl.attachShader(this.id, idFrag);

    // get a list of vertex attribute indices and bind them to their names before linking
    this._vertexAttributes = []; // array of vertex attribute indices
    this._setVertexAttributes(gl); // calls _addVertexAttribute for each attribute

    gl.linkProgram(this.id);
    if (CHECK_ERRORS) {
      checkLinkStatus(gl, description, this.id);
    }

    // get uniform names
    const uniformNames = []; // list of uniform variable names
    this._getUniformNames(uniformNames); // adds each uniform variable name to the list

    // define uniforms
    this._uniforms = {};
    for (let i = 0; i < uniformNames.length; i++) {
      const str = uniformNames[i];
      this._uniforms[str] = gl.getUniformLocation(this.id, str);
    }
  }

  // "abstract" and "protected" methods
  // -----------------------------------
  /** Call {@link WGLShaderProgram#_addVertexAttribute} for each vertex attribute to be used.
  * @param {WebGLRenderingContext} gl
  * @abstract
  */
  _setVertexAttributes(gl) { }

  /** Use during {@link WGLShaderProgram#_setVertexAttributes} to specify vertex attributes.
  * @param {WebGLRenderingContext} gl
  * @param {number} index - Index of the vertex attribute (i.e., to be used in vertexAttribPointer)
  * @param {string} str - Name of the variable bound to the vertex attribute
  * @protected
  */
  _addVertexAttribute(gl, index, str) {
    this._vertexAttributes.push(index);
    gl.bindAttribLocation(this.id, index, str);
  }

  /** Add each uniform variable name to the array.
  * @param {string[]} uniformNames - Array of uniform variable names
  * @abstract
  */
  _getUniformNames(uniformNames) { }

  // convenience methods for setting uniforms
  // -----------------------------------
  setU1i(gl, str, x) {
    gl.uniform1i(this._uniforms[str], x);
  }

  setU2i(gl, str, x, y) {
    gl.uniform2i(this._uniforms[str], x, y);
  }

  setU1f(gl, str, x) {
    gl.uniform1f(this._uniforms[str], x);
  }

  setUVec3(gl, str, u) {
    gl.uniform3f(this._uniforms[str], u[0], u[1], u[2]);
  }

  setUVec4(gl, str, u) {
    gl.uniform4f(this._uniforms[str], u[0], u[1], u[2], u[3]);
  }

  setUMat3(gl, str, s) {
    Mat3.serializeColumnMajor(TEMPSERIAL3, s);
    gl.uniformMatrix3fv(this._uniforms[str], false, TEMPSERIAL3);
  }

  setUMat4(gl, str, s) {
    Mat4.serializeColumnMajor(TEMPSERIAL4, s);
    gl.uniformMatrix4fv(this._uniforms[str], false, TEMPSERIAL4);
  }

  // use/unuse
  // -----------------------------------
  /** Prepare to use this shader program for rendering, and enable relevant vertex attributes.
  * @param {WebGLRenderingContext} gl
  */
  use(gl) {
    // use shader program
    gl.useProgram(this.id);

    // enable vertex attribute arrays
    for (let i = 0; i < this._vertexAttributes.length; i++) {
      const index = this._vertexAttributes[i];
      gl.enableVertexAttribArray(index);
    }
  }

  /** Disable this program's vertex attributes and stop using the program (use null).
  * @param {WebGLRenderingContext} gl
  */
  unuse(gl) {
    // disable vertex attribute arrays
    for (let i = 0; i < this._vertexAttributes.length; i++) {
      const index = this._vertexAttributes[i];
      gl.disableVertexAttribArray(index);
    }

    // unuse shader program
    gl.useProgram(null);
  }
}

export default WGLShaderProgram;
