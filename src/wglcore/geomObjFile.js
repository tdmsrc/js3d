import { Vec3 } from './mathVec';
import GeomData from './geomData';

const PRINT_ERR = msg => console.error(`[GeomOBJFile] ${msg}`);
const PRINT_WARN = msg => console.log(`[GeomOBJFile] ${msg}`);

// ===================================
// GEOMETRY: OBJ FILE
// ===================================

/** Creates geometry from an OBJ file.
* A scaling and translation may be applied to the vertices so that the bounding
* box is centered at (0,0,0) and does not exceed maxDims in size.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
* @param {string} objFileContentsString - Contents of OBJ file as string
* @param {number[]} maxDims - (optional) Max allowed dims of bounding box as [x,y,z]
* @extends GeomData
*/
class GeomOBJFile extends GeomData {
  constructor(objFileContentsString, maxDims) {
    super();

    // Get raw OBJ data
    // ----------------------------------------------
    this.objDataV = [];
    this.objDataVN = [];
    this.objDataT = [];
    this.objDataF = []; // array of [{ vi, vt?, vn? }, ...]

    const lines = objFileContentsString.split('\n');

    lines.forEach((line) => {
      // Split into non-whitespace chunks
      const explodedLine = line.match(/\S+/g);
      if (!explodedLine) { return; }

      switch (explodedLine[0]) {
        case 'v': this.readV(explodedLine); break;
        case 'vt': this.readVT(explodedLine); break;
        case 'vn': this.readVN(explodedLine); break;
        case 'f': this.readF(explodedLine); break;
      }
    });
    // ----------------------------------------------

    // Apply transformation for bounding box restriction
    // ----------------------------------------------
    const rangeX = [0, 0];
    const rangeY = [0, 0];
    const rangeZ = [0, 0];

    let bbFirst = false;
    this.objDataV.forEach((v) => {
      const vx = v[0];
      const vy = v[1];
      const vz = v[2];
      if (!bbFirst) {
        rangeX[0] = vx; rangeX[1] = vx;
        rangeY[0] = vy; rangeY[1] = vy;
        rangeZ[0] = vz; rangeZ[1] = vz;
      } else {
        if (vx < rangeX[0]) { rangeX[0] = vx; }
        if (vx > rangeX[1]) { rangeX[1] = vx; }
        if (vy < rangeY[0]) { rangeY[0] = vy; }
        if (vy > rangeY[1]) { rangeY[1] = vy; }
        if (vz < rangeZ[0]) { rangeZ[0] = vz; }
        if (vz > rangeZ[1]) { rangeZ[1] = vz; }
      }
      bbFirst = true;
    });

    const bbDims = [
      rangeX[1] - rangeX[0],
      rangeY[1] - rangeY[0],
      rangeZ[1] - rangeZ[0],
    ];
    const bbCenter = [
      (rangeX[0] + rangeX[1]) * 0.5,
      (rangeY[0] + rangeY[1]) * 0.5,
      (rangeZ[0] + rangeZ[1]) * 0.5,
    ];

    let scale = 1;
    if (maxDims) {
      const scaleX = (bbDims[0] > maxDims[0]) ? maxDims[0] / bbDims[0] : 1;
      const scaleY = (bbDims[1] > maxDims[1]) ? maxDims[1] / bbDims[1] : 1;
      const scaleZ = (bbDims[2] > maxDims[2]) ? maxDims[2] / bbDims[2] : 1;
      scale = Math.min(scaleX, scaleY, scaleZ);
    }

    this.objDataV = this.objDataV.map(v => [
      (v[0] - bbCenter[0]) * scale,
      (v[1] - bbCenter[1]) * scale,
      (v[2] - bbCenter[2]) * scale,
    ]);
    // ----------------------------------------------

    // Convert raw OBJ data -> GeomData
    // ----------------------------------------------
    let vertexCount = 0;
    let triCount = 0;
    let edgeCount = 0;
    this.objDataF.forEach((f) => {
      const faceVertCount = f.length;
      vertexCount += faceVertCount;
      triCount += faceVertCount - 2;
      edgeCount += faceVertCount;
    });

    this.initialize({
      vertexCount,
      elementCount: 3 * triCount,
      elementWireCount: 2 * edgeCount,
      hasNormals: true,
      hasTexture: false,
    });

    let vk = 0;
    let fk = 0;
    let ek = 0;

    this.objDataF.forEach((f) => {
      const vkStart = vk;

      f.forEach((fVertData) => {
        const { vi, vt, vn } = fVertData;
        this.setPosition(vk, this.objDataV[vi - 1]);
        // this.setTex(vk, this.objDataT[vt - 1]);
        this.setColor(vk, [1, 0.5, 0.5]);
        this.setNormal(vk, this.objDataVN[vn - 1]);
        vk++;
      });

      for (let i = 0; i < f.length - 2; i++) {
        const vkCur = vkStart + i + 1;
        this.setFace(fk, 0, vkStart, vkCur, vkCur + 1);
        fk++;
      }

      for (let i = 0; i < f.length; i++) {
        const j = (i + 1) % f.length;
        this.setLine(ek, 0, vkStart + i, vkStart + j);
        ek++;
      }
    });
    // ----------------------------------------------
  }

  // Methods which populate objDataV, objDataVN, objDataN, objDataF
  // ================================================================
  // parse "v # # #" -> objDataV
  readV(explodedLine) {
    if (explodedLine.length < 4) {
      PRINT_ERR(`Line "${explodedLine.join(' ')}" has too few numbers, skipping...`);
      return;
    } else if (explodedLine.length > 4) {
      PRINT_WARN(`Line "${explodedLine.join(' ')}" has too many numbers, ignoring extras...`);
    }

    const p = [
      parseFloat(explodedLine[1]),
      parseFloat(explodedLine[2]),
      parseFloat(explodedLine[3]),
    ];

    this.objDataV.push(p);
  }

  // parse "vn # # #" -> objDataVN
  readVN(explodedLine) {
    if (explodedLine.length < 4) {
      PRINT_ERR(`Line "${explodedLine.join(' ')}" has too few numbers, skipping...`);
      return;
    } else if (explodedLine.length > 4) {
      PRINT_WARN(`Line "${explodedLine.join(' ')}" has too many numbers, ignoring extras...`);
    }

    const n = [
      parseFloat(explodedLine[1]),
      parseFloat(explodedLine[2]),
      parseFloat(explodedLine[3]),
    ];

    this.objDataVN.push(n);
  }

  // parse "vt # #" -> objDataT
  readVT(explodedLine) {
    if (explodedLine.length < 3) {
      PRINT_ERR(`Line "${explodedLine.join(' ')}" has too few numbers, skipping...`);
      return;
    } else if (explodedLine.length > 3) {
      PRINT_WARN(`Line "${explodedLine.join(' ')}" has too many numbers, ignoring extras...`);
    }

    const t = [
      parseFloat(explodedLine[1]),
      parseFloat(explodedLine[2]),
    ];

    this.objDataT.push(t);
  }

  // parse "f v ...", OR "f v/vt ...", OR "f v/vt/vn ...", OR "f v//vn ..."
  // push [{ vi, vt?, vn? }, ...] -> objDataF
  readF(explodedLine) {
    if (explodedLine.length < 3) {
      PRINT_ERR(`Line "${explodedLine.join(' ')}" has too few entries, skipping...`);
      return;
    }

    const f = [];
    for (let j = 1, jl = explodedLine.length; j < jl; j++) {
      const data = explodedLine[j].split('/');

      const fVertData = {};
      fVertData.vi = parseInt(data[0], 10);
      if (data.length > 1) { fVertData.vt = parseInt(data[1], 10); }
      if (data.length > 2) { fVertData.vn = parseInt(data[2], 10); }

      f.push(fVertData);
    }

    this.objDataF.push(f);
  }
  // ================================================================
}

export default GeomOBJFile;
