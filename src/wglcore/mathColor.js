import { Vec3 } from './mathVec';

// ===================================
// COLOR
// these methods act on arrays [r,g,b] or [r,g,b,a]
// any method that produces a new array has a "target" parameter
// ===================================

/** Collection of functions relating to colors.
*   <br>Colors are stored as arrays [r,g,b] or [r,g,b,a]
*/
const ColorUtils = {
  /** Convert HSV to RGB
  * @param {number[]} target - The array which will store the result
  * @param h - Hue, in the range [0,360]
  * @param s - Saturation, in the range [0,1]
  * @param v - Value, in the range [0,1]
  */
  hsvToRgb: (target, h, s, v) => {
    const hm = h % 360;

    if (hm < 60) {
      target[0] = 1;
      target[1] = hm / 60;
      target[2] = 0;
    } else if (hm < 120) {
      target[0] = 1 - ((hm - 60) / 60);
      target[1] = 1;
      target[2] = 0;
    } else if (hm < 180) {
      target[0] = 0;
      target[1] = 1;
      target[2] = (hm - 120) / 60;
    } else if (hm < 240) {
      target[0] = 0;
      target[1] = 1 - ((hm - 180) / 60);
      target[2] = 1;
    } else if (hm < 300) {
      target[0] = (hm - 240) / 60;
      target[1] = 0;
      target[2] = 1;
    } else {
      target[0] = 1;
      target[1] = 0;
      target[2] = 1 - ((hm - 300) / 60);
    }

    Vec3.linearCombination(target, v * s, target, v * (1 - s), [1, 1, 1]);
    Vec3.clamp(target, target, 0, 1);
  },
};

export default ColorUtils;
