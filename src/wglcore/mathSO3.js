import { Vec3, Vec4 } from './mathVec';
import { Mat3 } from './mathMat';

const TEMPVEC4 = [0, 0, 0, 0];

// ===================================
// SO3
// maintains quaternion and matrix/inverse
// ===================================

/** An element of SO(3).  Maintains a quaternion, 3x3 matrix, and its inverse.
*   <br>Don't modify properties directly--use the methods.
*
* @prop {number[]} q - Quaternion ([a,b,c,d] = a+bi+cj+dk)
* @prop {Mat3} matrix - 3x3 matrix representing the rotation
* @prop {Mat3} matrixInverse - 3x3 matrix representing the inverse of the rotation
*/
class SO3 {
  constructor() {
    // unit quaternion representing rotation
    this.q = [1, 0, 0, 0];

    // derived quantities
    this.matrix = Mat3.create();
    this.matrixInverse = Mat3.create();

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------
  /** Set target equal to the quaternion which rotates "angle" radians about "axis".
  * @param {number[]} target - The target quaternion ([a,b,c,d] = a+bi+cj+dk)
  * @param {number[]} axis - Vector about which to rotate (need not be normalized)
  * @param {number} angle - Angle in radians
  */
  static qAxisAngle(target, axis, angle) {
    // general formula: q = cos(t/2) + (nxi + nyj + nzk)sin(t/2) (n = normalized axis)

    const c = Math.cos(angle * 0.5);
    const s = Math.sin(angle * 0.5);

    // l = length of axis
    const invl = 1 / Vec3.norm(axis);
    target[0] = c;
    target[1] = s * axis[0] * invl;
    target[2] = s * axis[1] * invl;
    target[3] = s * axis[2] * invl;
  }

  _recompute() {
    this._computeMatrix();
  }

  _computeMatrix() {
    const a = this.q[0];
    const b = this.q[1];
    const c = this.q[2];
    const d = this.q[3];

    // column 0
    this.matrix[0][0] = 1 - (2 * ((c * c) + (d * d))); // = a*a+b*b-c*c-d*d for unit q
    this.matrix[1][0] = 2 * ((b * c) + (a * d));
    this.matrix[2][0] = 2 * ((b * d) - (c * a));

    // column 1
    this.matrix[0][1] = 2 * ((b * c) - (a * d));
    this.matrix[1][1] = 1 - (2 * ((b * b) + (d * d))); // = a*a-b*b+c*c-d*d for unit q
    this.matrix[2][1] = 2 * ((c * d) + (a * b));

    // column 2
    this.matrix[0][2] = 2 * ((b * d) + (a * c));
    this.matrix[1][2] = 2 * ((c * d) - (a * b));
    this.matrix[2][2] = 1 - (2 * ((b * b) + (c * c))); // = a*a-b*b-c*c+d*d for unit q

    // matrix is orthogonal, so inverse = transpose
    Mat3.transpose(this.matrixInverse, this.matrix);
  }

  // modification
  // -----------------------------------
  /** Set this equal to the identity. */
  identity() {
    this.q[0] = 1; this.q[1] = 0; this.q[2] = 0; this.q[3] = 0;

    // same effect as _recompute()
    Mat3.identity(this.matrix);
    Mat3.identity(this.matrixInverse);
  }

  /** Set this equal to another rotation.
  * @param {SO3} s - The other rotation
  */
  copy(s) {
    Vec4.copy(this.q, s.q);

    // same effect as _recompute()
    Mat3.copy(this.matrix, s.matrix);
    Mat3.copy(this.matrixInverse, s.matrixInverse);
  }

  /** Set this = (rotation given by q).
  *   <br> Does not normalize "this.q" or "q".
  * @param {number[]} q - Quaternion defining a rotation ([a,b,c,d] = a+bi+cj+dk)
  */
  setQ(q) {
    Vec4.copy(this.q, q);
    this._recompute();
  }

  /** Set this = (rotation given by q) * this.
  *   <br> Does not normalize "q", but normalizes "this.q" after multiplying.
  * @param {number[]} q - Quaternion defining a rotation ([a,b,c,d] = a+bi+cj+dk)
  */
  leftMultiplyQ(q) {
    Vec4.qMultiply(this.q, q, this.q);
    Vec4.normalize(this.q, this.q);
    this._recompute();
  }

  /** Set this = r * this.
  * @param {SO3} r - A rotation
  */
  leftMultiplySO3(r) {
    this.leftMultiplyQ(r.q);
  }

  /** Set this = (rotation "angle" radians about "axis") * this.
  * @param {number[]} axis - Vector about which to rotate (need not be normalized)
  * @param {number} angle - Angle in radians
  */
  leftMultiplyAxisAngle(axis, angle) {
    SO3.qAxisAngle(TEMPVEC4, axis, angle);
    this.leftMultiplyQ(TEMPVEC4);
  }

  // slerp
  // -----------------------------------
  // TODO
}

export default SO3;
