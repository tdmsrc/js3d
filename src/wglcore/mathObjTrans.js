import { Vec3 } from './mathVec';
import { Mat3 } from './mathMat';
import SO3 from './mathSO3';

// ===================================
// OBJECT TRANSFORMATION
// ===================================

/** A transformation to be applied to an object.
* The transformation is p -> rotation*(p - offset) + translation.
*
* @prop {SO3} rotation - Rotation applied to the object
* @prop {number[]} translation - Translation applied to the object after rotation
* @prop {number[]} offset - Offset applied to the object before rotation
* @prop {ObjectTransformation} parent - A transformation to be applied after this (default = null).
*   <br> The "flattened" transformation is p -> ... this.parent.parent * this.parent * this * p.
*   <br> If the parent is null, no subsequent transformation is applied.
*/
class ObjectTransformation {
  constructor() {
    // transformation
    this.rotation = new SO3();
    this.translation = [0, 0, 0];
    this.offset = [0, 0, 0];

    // parent transformation: "flattened" transformation is
    // p -> ... this.parent.parent * this.parent * this * p
    // stops whenever a parent is null
    this.parent = null;
  }

  // methods IGNORING parent
  // -----------------------------------
  /** Set this transformation to the identity (does not modify "parent"). */
  identity() {
    this.rotation.identity();
    Vec3.zero(this.translation);
    Vec3.zero(this.offset);
  }

  /** Set this transformation equal to another (ignores "parent" property of both).
  * @param {ObjectTransformation} t - The transformation to copy
  */
  copy(t) {
    this.rotation.copy(t.rotation);
    Vec3.copy(this.translation, t.translation);
    Vec3.copy(this.offset, t.offset);
  }

  /** Set target = this * p (ignores "parent" property).
  * @param {number[]} target - The transformed point
  * @param {number[]} p - The point to transform
  */
  transform(target, p) {
    Vec3.subtract(target, p, this.offset);
    Mat3.transform(target, this.rotation.matrix, target);
    Vec3.add(target, target, this.translation);
  }

  /** Set this = t * this (ignores "parent" property of both).
  * @param {ObjectTransformation} t - The transformation to multiply on the left
  */
  leftMultiply(t) {
    this.rotation.leftMultiplySO3(t.rotation);
    t.transform(this.translation, this.translation);
  }

  // methods USING parent
  // -----------------------------------
  /** Set this = (... t.parent.parent * t.parent * t) * this (does not modify "parent").
  * @param {ObjectTransformation} t - The transformation to flatten and multiply on the left
  */
  leftMultiplyFlat(t) {
    this.leftMultiply(t);
    if (t.parent !== null) {
      this.leftMultiplyFlat(t.parent);
    }
  }

  /** Set this = ... t.parent.parent * t.parent * t (does not modify "parent").
  * @param {ObjectTransformation} t - The transformation to flatten and copy
  */
  copyFlat(t) {
    this.copy(t);
    if (t.parent !== null) {
      this.leftMultiplyFlat(t.parent);
    }
  }

  /** Set target = ... this.parent.parent * this.parent * this * p.
  * @param {number[]} target - The transformed point
  * @param {number[]} p - The point to transform
  */
  transformFlat(target, p) {
    this.transform(target, p);
    if (this.parent !== null) {
      this.parent.transformFlat(target, target);
    }
  }
}

export default ObjectTransformation;
