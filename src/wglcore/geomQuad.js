import { Vec3 } from './mathVec';
import GeomData from './geomData';

// ===================================
// GEOMETRY: QUAD
// ===================================

/** Quad [-1,1]x[-1,1] with z=0.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @extends GeomData
*/
class GeomQuad extends GeomData {
  constructor() {
    super();

    this.initialize({
      vertexCount: 4,
      elementCount: 6,
      elementWireCount: 8,
      hasNormals: true,
      hasTexture: true,
    });

    this.setPosition(0, [-1, -1, 0]);
    this.setPosition(1, [1, -1, 0]);
    this.setPosition(2, [1, 1, 0]);
    this.setPosition(3, [-1, 1, 0]);

    this.setNormal(0, [0, 0, 1]);
    this.setNormal(1, [0, 0, 1]);
    this.setNormal(2, [0, 0, 1]);
    this.setNormal(3, [0, 0, 1]);

    this.setTex(0, [0, 0]);
    this.setTex(1, [1, 0]);
    this.setTex(2, [1, 1]);
    this.setTex(3, [0, 1]);

    this.setFace(0, 0, 0, 1, 2);
    this.setFace(1, 0, 0, 2, 3);

    this.setLine(0, 0, 0, 1);
    this.setLine(1, 0, 1, 2);
    this.setLine(2, 0, 2, 3);
    this.setLine(3, 0, 3, 0);
  }
}

export default GeomQuad;
