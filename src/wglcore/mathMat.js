const DEG_TO_RAD = Math.PI / 180;
const TEMPMAT3 = [
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1],
];
const TEMPMAT4 = [
  [1, 0, 0, 0],
  [0, 1, 0, 0],
  [0, 0, 1, 0],
  [0, 0, 0, 1],
];

// ===================================
// MAT3
// "target" parameter stores results--can always be one of the operands
// ===================================

/** Collection of functions relating to 3x3 matrices.
*   <br>Matrices are just arrays, where m[i][j] = entry at ith row, jth column
*/
const Mat3 = {

  /** Create a new identity matrix.
  * @return {Mat3}
  */
  create: () => [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1],
  ],

  /** Set target equal to the identity matrix.
  * @param {Mat3} target - The matrix to modify
  */
  identity: (target) => {
    target[0][0] = 1; target[0][1] = 0; target[0][2] = 0;
    target[1][0] = 0; target[1][1] = 1; target[1][2] = 0;
    target[2][0] = 0; target[2][1] = 0; target[2][2] = 1;
  },

  /** Set target = s.
  * @param {Mat3} target - The matrix to modify
  * @param {Mat3} s - The matrix to copy
  */
  copy: (target, s) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = s[i][j];
      }
    }
  },

  // basic methods
  // -----------------------------------
  /** Fill an array with matrix entries in row-major order:
  *   <br>(first 3 entries) = (first row of s),
  *   <br>(next 3 entries) = (second row of s), ...
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat3} s - The matrix to serialize
  */
  serializeRowMajor: (target, s) => {
    let k = 0;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[k] = s[i][j];
        k++;
      }
    }
  },

  /** Fill an array with matrix entries in column-major order:
  *   <br>(first 3 entries) = (first column of s),
  *   <br>(next 3 entries) = (second column of s), ...
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat3} s - The matrix to serialize
  */
  serializeColumnMajor: (target, s) => {
    let k = 0;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[k] = s[j][i];
        k++;
      }
    }
  },

  /** Set the rows of a matrix.
  * @param {Mat3} target - The matrix to modify
  *
  * @param {number[]} u - Row 0
  * @param {number[]} v - Row 1
  * @param {number[]} w - Row 2
  */
  setRows: (target, u, v, w) => {
    target[0][0] = u[0]; target[0][1] = u[1]; target[0][2] = u[2];
    target[1][0] = v[0]; target[1][1] = v[1]; target[1][2] = v[2];
    target[2][0] = w[0]; target[2][1] = w[1]; target[2][2] = w[2];
  },

  /** Set the columns of a matrix.
  * @param {Mat3} target - The matrix to modify
  *
  * @param {number[]} u - Column 0
  * @param {number[]} v - Column 1
  * @param {number[]} w - Column 2
  */
  setColumns: (target, u, v, w) => {
    target[0][0] = u[0]; target[0][1] = v[0]; target[0][2] = w[0];
    target[1][0] = u[1]; target[1][1] = v[1]; target[1][2] = w[1];
    target[2][0] = u[2]; target[2][1] = v[2]; target[2][2] = w[2];
  },

  /** Set target = k * s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix being scaled
  * @param {number} k - Scale factor
  */
  scale: (target, s, k) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = k * s[i][j];
      }
    }
  },

  /** Set target = s + t.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Left matrix
  * @param {Mat3} t - Right matrix
  */
  add: (target, s, t) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = s[i][j] + t[i][j];
      }
    }
  },

  /** Set target = s - t.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Left matrix
  * @param {Mat3} t - Right matrix
  */
  subtract: (target, s, t) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = s[i][j] - t[i][j];
      }
    }
  },

  /** Set target = transpose of s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix to transpose
  */
  transpose: (target, s) => {
    target[0][0] = s[0][0];
    target[1][1] = s[1][1];
    target[2][2] = s[2][2];

    for (let i = 0; i < 3; i++) {
      for (let j = i + 1; j < 3; j++) {
        const temp = s[i][j];
        target[i][j] = s[j][i];
        target[j][i] = temp;
      }
    }
  },

  // vector-related methods
  // -----------------------------------
  /** Set target = u \otimes v.
  *   (i.e., m[i][j] = u[i]*v[j])
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} u - Vector on the left
  * @param {number[]} v - Vector on the right
  */
  tensorProduct: (target, u, v) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = u[i] * v[j];
      }
    }
  },

  /** Set target = u \otimes v + v \otimes u.
  *   (i.e., m[i][j] = u[i]*v[j] + u[j]*v[i])
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} u - Vector on the left
  * @param {number[]} v - Vector on the right
  */
  symmetricProduct: (target, u, v) => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        target[i][j] = (u[i] * v[j]) + (u[j] * v[i]);
      }
    }
  },

  /** Set target = s * u.
  * @param {number[]} target - The vector which will store the result
  * @param {Mat3} s - Transformation
  * @param {number[]} u - Vector to transform
  */
  transform: (target, s, u) => {
    const x = (s[0][0] * u[0]) + (s[0][1] * u[1]) + (s[0][2] * u[2]);
    const y = (s[1][0] * u[0]) + (s[1][1] * u[1]) + (s[1][2] * u[2]);
    const z = (s[2][0] * u[0]) + (s[2][1] * u[1]) + (s[2][2] * u[2]);
    target[0] = x; target[1] = y; target[2] = z;
  },

  // inverse
  // -----------------------------------
  /** Return the (i,j) cofactor of a matrix.
  * @param {Mat3} s - The matrix
  * @param {number} i - Row
  * @param {number} j - Column
  * @return {number}
  */
  cofactor: (s, i, j) => {
    let i0; let i1;
    if (i === 0) {
      i0 = 1; i1 = 2;
    } else if (i === 1) {
      i0 = 0; i1 = 2;
    } else if (i === 2) {
      i0 = 0; i1 = 1;
    }

    let j0; let j1;
    if (j === 0) {
      j0 = 1; j1 = 2;
    } else if (j === 1) {
      j0 = 0; j1 = 2;
    } else if (j === 2) {
      j0 = 0; j1 = 1;
    }

    const minor = (s[i0][j0] * s[i1][j1]) - (s[i0][j1] * s[i1][j0]);
    return (((i + j) % 2) === 0) ? minor : -minor;
  },

  /** Set target = inverse of s.
  * @param {Mat3} target - The matrix which will store the result
  * @param {Mat3} s - Matrix to invert
  */
  inverse: (target, s) => {
    // set target = adjugate (transpose of matrix of cofactors)
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        TEMPMAT3[i][j] = Mat3.cofactor(s, j, i);
      }
    }

    // divide by det
    const det =
      (s[0][0] * TEMPMAT3[0][0]) +
      (s[0][1] * TEMPMAT3[1][0]) +
      (s[0][2] * TEMPMAT3[2][0]);
    Mat3.scale(target, TEMPMAT3, 1 / det);
  },

  // GL-like methods
  // -----------------------------------
  /** Set target = rotation matrix.
  * @param {Mat3} target - The matrix which will store the result
  * @param {number[]} axis - Vector about which to rotate (need not be normalized)
  * @param {number} angle - Angle in radians
  */
  rotate: (target, axis, angle) => {
    // "Rodrigues formula": v -> (v.n)n + (v-(v.n)n)c - (vxn)s
    // in matrix form: (1-c)*[n \otimes n] + c*[I] - s*[[0,n3,-n2][-n3,0,n1][n2,-n1,0]]

    const c = Math.cos(angle);
    const s = Math.sin(angle);

    // n = normalize(axis)
    const l = Math.sqrt((axis[0] * axis[0]) + (axis[1] * axis[1]) + (axis[2] * axis[2]));
    const n = [axis[0] / l, axis[1] / l, axis[2] / l];

    // row 0
    target[0][0] = ((1 - c) * n[0] * n[0]) + c;
    target[0][1] = ((1 - c) * n[0] * n[1]) - (s * n[2]);
    target[0][2] = ((1 - c) * n[0] * n[2]) + (s * n[1]);
    // row 1
    target[1][0] = ((1 - c) * n[1] * n[0]) + (s * n[2]);
    target[1][1] = ((1 - c) * n[1] * n[1]) + c;
    target[1][2] = ((1 - c) * n[1] * n[2]) - (s * n[0]);
    // row 2
    target[2][0] = ((1 - c) * n[2] * n[0]) - (s * n[1]);
    target[2][1] = ((1 - c) * n[2] * n[1]) + (s * n[0]);
    target[2][2] = ((1 - c) * n[2] * n[2]) + c;
  },
};

// ===================================
// MAT4
// "target" parameter stores results--can always be one of the operands
// ===================================

/** Collection of functions relating to 4x4 matrices.
*   <br>Matrices are just arrays, where m[i][j] = entry at ith row, jth column
*/
const Mat4 = {

  /** Create a new identity matrix.
  * @return {Mat4}
  */
  create: () => [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ],

  /** Set target equal to the identity matrix.
  * @param {Mat4} target - The matrix to modify
  */
  identity: (target) => {
    target[0][0] = 1; target[0][1] = 0; target[0][2] = 0; target[0][3] = 0;
    target[1][0] = 0; target[1][1] = 1; target[1][2] = 0; target[1][3] = 0;
    target[2][0] = 0; target[2][1] = 0; target[2][2] = 1; target[2][3] = 0;
    target[3][0] = 0; target[3][1] = 0; target[3][2] = 0; target[3][3] = 1;
  },

  /** Set target = s.
  * @param {Mat4} target - The matrix to modify
  * @param {Mat4} s - The matrix to copy
  */
  copy: (target, s) => {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[i][j] = s[i][j];
      }
    }
  },

  // basic methods
  // -----------------------------------
  /** Fill an array with matrix entries in row-major order:
  *   <br>(first 4 entries) = (first row of s),
  *   <br>(next 4 entries) = (second row of s), ...
  *
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat4} s - The matrix to serialize
  */
  serializeRowMajor: (target, s) => {
    let k = 0;
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[k] = s[i][j];
        k++;
      }
    }
  },

  /** Fill an array with matrix entries in column-major order:
  *   <br>(first 4 entries) = (first column of s),
  *   <br>(next 4 entries) = (second column of s), ...
  *
  * @param {number[]} target - Array to be populated by matrix entries
  * @param {Mat4} s - The matrix to serialize
  */
  serializeColumnMajor: (target, s) => {
    let k = 0;
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[k] = s[j][i];
        k++;
      }
    }
  },

  /** Set the rows of a matrix.
  * @param {Mat4} target - The matrix to modify
  * @param {number[]} u - Row 0
  * @param {number[]} v - Row 1
  * @param {number[]} w - Row 2
  * @param {number[]} x - Row 3
  */
  setRows: (target, u, v, w, x) => {
    target[0][0] = u[0]; target[0][1] = u[1]; target[0][2] = u[2]; target[0][3] = u[3];
    target[1][0] = v[0]; target[1][1] = v[1]; target[1][2] = v[2]; target[1][3] = v[3];
    target[2][0] = w[0]; target[2][1] = w[1]; target[2][2] = w[2]; target[2][3] = w[3];
    target[3][0] = x[0]; target[3][1] = x[1]; target[3][2] = x[2]; target[3][3] = x[3];
  },

  /** Set the columns of a matrix.
  * @param {Mat4} target - The matrix to modify
  * @param {number[]} u - Column 0
  * @param {number[]} v - Column 1
  * @param {number[]} w - Column 2
  * @param {number[]} x - Column 3
  */
  setColumns: (target, u, v, w, x) => {
    target[0][0] = u[0]; target[0][1] = v[0]; target[0][2] = w[0]; target[0][3] = x[0];
    target[1][0] = u[1]; target[1][1] = v[1]; target[1][2] = w[1]; target[1][3] = x[1];
    target[2][0] = u[2]; target[2][1] = v[2]; target[2][2] = w[2]; target[2][3] = x[2];
    target[3][0] = u[3]; target[3][1] = v[3]; target[3][2] = w[3]; target[3][3] = x[3];
  },


  /** Set target = k * s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix being scaled
  * @param {number} k - Scale factor
  */
  scale: (target, s, k) => {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[i][j] = k * s[i][j];
      }
    }
  },

  /** Set target = s + t.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Left matrix
  * @param {Mat4} t - Right matrix
  */
  add: (target, s, t) => {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[i][j] = s[i][j] + t[i][j];
      }
    }
  },

  /** Set target = s - t.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Left matrix
  * @param {Mat4} t - Right matrix
  */
  subtract: (target, s, t) => {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        target[i][j] = s[i][j] - t[i][j];
      }
    }
  },

  /** Set target = transpose of s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix to transpose
  */
  transpose: (target, s) => {
    target[0][0] = s[0][0];
    target[1][1] = s[1][1];
    target[2][2] = s[2][2];
    target[3][3] = s[3][3];

    for (let i = 0; i < 4; i++) {
      for (let j = i + 1; j < 4; j++) {
        const temp = s[i][j];
        target[i][j] = s[j][i];
        target[j][i] = temp;
      }
    }
  },

  // vector-related methods
  // -----------------------------------
  /** Set target = s * u.
  * @param {number[]} target - The vector which will store the result
  * @param {Mat4} s - Transformation
  * @param {number[]} u - Vector to transform
  */
  transform: (target, s, u) => {
    const x = (s[0][0] * u[0]) + (s[0][1] * u[1]) + (s[0][2] * u[2]) + (s[0][3] * u[3]);
    const y = (s[1][0] * u[0]) + (s[1][1] * u[1]) + (s[1][2] * u[2]) + (s[1][3] * u[3]);
    const z = (s[2][0] * u[0]) + (s[2][1] * u[1]) + (s[2][2] * u[2]) + (s[2][3] * u[3]);
    const w = (s[3][0] * u[0]) + (s[3][1] * u[1]) + (s[3][2] * u[2]) + (s[3][3] * u[3]);
    target[0] = x; target[1] = y; target[2] = z; target[3] = w;
  },

  /** Computes v = (4x4 matrix s) * [u.x, u.y, u.z, 1],
  *   then sets target = [v.x, v.y, v.z] / v.w.
  *   <br>Note that both "target" and "u" are 3-dimensional.
  *
  * @param {number[]} target - The vector which will store the result
  * @param {Mat4} s - Projective transformation
  * @param {number[]} u - Vector to transform
  */
  transformProjective: (target, s, u) => {
    const x = (s[0][0] * u[0]) + (s[0][1] * u[1]) + (s[0][2] * u[2]) + s[0][3];
    const y = (s[1][0] * u[0]) + (s[1][1] * u[1]) + (s[1][2] * u[2]) + s[1][3];
    const z = (s[2][0] * u[0]) + (s[2][1] * u[1]) + (s[2][2] * u[2]) + s[2][3];
    const w = (s[3][0] * u[0]) + (s[3][1] * u[1]) + (s[3][2] * u[2]) + s[3][3];
    target[0] = x / w; target[1] = y / w; target[2] = z / w;
  },

  // inverse
  // -----------------------------------
  /** Return the (i,j) cofactor of a matrix.
  * @param {Mat4} s - The matrix
  * @param {number} i - Row
  * @param {number} j - Column
  * @return {number}
  */
  cofactor: (s, i, j) => {
    let i0; let i1; let i2;
    if (i === 0) {
      i0 = 1; i1 = 2; i2 = 3;
    } else if (i === 1) {
      i0 = 0; i1 = 2; i2 = 3;
    } else if (i === 2) {
      i0 = 0; i1 = 1; i2 = 3;
    } else if (i === 3) {
      i0 = 0; i1 = 1; i2 = 2;
    }

    let j0; let j1; let j2;
    if (j === 0) {
      j0 = 1; j1 = 2; j2 = 3;
    } else if (j === 1) {
      j0 = 0; j1 = 2; j2 = 3;
    } else if (j === 2) {
      j0 = 0; j1 = 1; j2 = 3;
    } else if (j === 3) {
      j0 = 0; j1 = 1; j2 = 2;
    }

    const det0 = (s[i1][j1] * s[i2][j2]) - (s[i1][j2] * s[i2][j1]);
    const det1 = (s[i1][j0] * s[i2][j2]) - (s[i1][j2] * s[i2][j0]);
    const det2 = (s[i1][j0] * s[i2][j1]) - (s[i1][j1] * s[i2][j0]);
    const minor = (s[i0][j0] * det0) + (-s[i0][j1] * det1) + (s[i0][j2] * det2);
    return (((i + j) % 2) === 0) ? minor : -minor;
  },

  /** Set target = inverse of s.
  * @param {Mat4} target - The matrix which will store the result
  * @param {Mat4} s - Matrix to invert
  */
  inverse: (target, s) => {
    // set target = adjugate (transpose of matrix of cofactors)
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 4; j++) {
        TEMPMAT4[i][j] = Mat4.cofactor(s, j, i);
      }
    }

    // divide by det
    const det =
      (s[0][0] * TEMPMAT4[0][0]) +
      (s[0][1] * TEMPMAT4[1][0]) +
      (s[0][2] * TEMPMAT4[2][0]) +
      (s[0][3] * TEMPMAT4[3][0]);
    Mat4.scale(target, TEMPMAT4, 1 / det);
  },

  // GL-like methods
  // -----------------------------------
  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps frustum described below to [-1,1]^3)
  *  <br>
  *  <br>Frustum: Points on ray from origin to a point on rectangle, with -z in [n,f].
  *  <br>Rectangle: In z=-n plane; x range = [l,r], y range = [b,t].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} l
  * @param {number} r
  * @param {number} b
  * @param {number} t
  * @param {number} n
  * @param {number} f
  */
  glFrustum: (target, l, r, b, t, n, f) => {
    // row 0
    target[0][0] = (2 * n) / (r - l);
    target[0][1] = 0;
    target[0][2] = (r + l) / (r - l);
    target[0][3] = 0;
    // row 1
    target[1][0] = 0;
    target[1][1] = (2 * n) / (t - b);
    target[1][2] = (t + b) / (t - b);
    target[1][3] = 0;
    // row 2
    target[2][0] = 0;
    target[2][1] = 0;
    target[2][2] = -(f + n) / (f - n);
    target[2][3] = -(2 * f * n) / (f - n);
    // row 3
    target[3][0] = 0;
    target[3][1] = 0;
    target[3][2] = -1;
    target[3][3] = 0;
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps frustum described below to [-1,1]^3)
  *  <br>
  *  <br>Frustum: Points on ray from origin to a point on rectangle, with -z in [zFront,zBack].
  *  <br>Rectangle: In z=-zFront plane; x range = [-w,w], y range = [-h,h];
  *  <br>h determined so angle formed by [0,-h,-zFront] and [0,h,-zFront] is fovy; w = h*aspect.
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} fovy
  * @param {number} aspect
  * @param {number} zFront
  * @param {number} zBack
  */
  gluPerspective: (target, fovy, aspect, zFront, zBack) => {
    const tanFovy = Math.tan(DEG_TO_RAD * fovy * 0.5);
    const h = zFront * tanFovy;
    const w = h * aspect;
    Mat4.glFrustum(target, -w, w, -h, h, zFront, zBack);
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps box described below to [-1,1]^3)
  *  <br>
  *  <br>Box: x range = [l,r], y range = [b,t], -z range = [n,f].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} l
  * @param {number} r
  * @param {number} b
  * @param {number} t
  * @param {number} n
  * @param {number} f
  */
  glOrtho: (target, l, r, b, t, n, f) => {
    // row 0
    target[0][0] = 2 / (r - l);
    target[0][1] = 0;
    target[0][2] = 0;
    target[0][3] = -(r + l) / (r - l);
    // row 1
    target[1][0] = 0;
    target[1][1] = 2 / (t - b);
    target[1][2] = 0;
    target[1][3] = -(t + b) / (t - b);
    // row 2
    target[2][0] = 0;
    target[2][1] = 0;
    target[2][2] = -2 / (f - n);
    target[2][3] = -(f + n) / (f - n);
    // row 3
    target[3][0] = 0;
    target[3][1] = 0;
    target[3][2] = 0;
    target[3][3] = 1;
  },

  /** Set target = projection matrix mapping "eye coordinates" to "clip coordinates".
  *  <br>(i.e., maps box described below to [-1,1]^3)
  *  <br>
  *  <br>Box: x range = [left,right], y range = [bottom,top], -z range = [-1,1].
  *
  * @param {Mat4} target - The matrix which will store the result
  * @param {number} left
  * @param {number} right
  * @param {number} bottom
  * @param {number} top
  */
  gluOrtho2D: (target, left, right, bottom, top) => {
    Mat4.glOrtho(target, left, right, bottom, top, -1, 1);
  },
};

export {
  Mat3,
  Mat4,
};
