import GeomData from './geomData';
import fontMetrics from '../font/glfont'; // TODO

// ===================================
// GEOMETRY: TEXT
// ===================================

/** Determines the metrics for each character in a font.  All return value units are in terms of font texture pixels.
* @typedef {Object} GeomText~fontMetrics
*
* @prop {string} fontName - String description of font
* @prop {number} spaceWidth - Returns space char width
* @prop {number} lineHeight - Vertical offset for newline
* @prop {function} getTexPos - (k) => [texPosX, texPosY]; ASCII value to pos in texture
* @prop {function} getSize - (k) => [width, height]; ASCII value to char size
* @prop {function} getOffset - (k) => [offsetX, offsetY]; ASCII value to placement offset
*/

/** Text geometry.
*   <br>Must call {@link GeomText#generate} after creation, or to update string.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {GeomText~fontMetrics} opts.fontMetrics - Font metrics that specify how the geometry is generated
* @param {number} opts.maxlen - Maximum number of visible characters to display
* @param {string} opts.text - Initial string to display
* @param {number[]} opts.tessellation - (Default: [1,1]) Each char is split into a t[0]xt[1] grid, where t = tessellation
* @param {Function} opts.pos - Position function: <code>function(u,v){ return [x,y,z]; }</code>
*   <br>The input (u,v) has units of font texture pixels
*
* @extends GeomData
*/
class GeomText extends GeomData {
  constructor(opts) {
    super();
    this.opts = opts;

    // defaults
    if (typeof this.opts.tessellation === 'undefined') {
      this.opts.tessellation = [1, 1];
    }

    // create this._gridChars array and GeomData elements
    this._createGrid();
  }

  // add char to GeomData; return
  _createChar(i) {
    // var tx = this.opts.tessellation[0], ty = this.opts.tessellation[1];

    // var indexV = 0;
    // var indexL = 0;
    // var indexF = 0;

    // //faces
    // for(var j=0; j<ty; j++){
    // for(var i=0; i<tx; i++){
    //   var i00=indexV*4, i10=i00+1, i01=i00+2, i11=i00+3;

    //   this.setFace(2*indexF  , 0, i00,i10,i11);
    //   this.setFace(2*indexF+1, 0, i00,i11,i01);
    //   indexF += 2;
    // }}

    const i00 = i * 4;
    const i10 = i00 + 1;
    const i01 = i00 + 2;
    const i11 = i00 + 3;

    this.setFace((2 * i) + 0, 0, i00, i10, i11);
    this.setFace((2 * i) + 1, 0, i00, i11, i01);

    this.setLine((4 * i) + 0, 0, i00, i10);
    this.setLine((4 * i) + 1, 0, i10, i11);
    this.setLine((4 * i) + 2, 0, i11, i01);
    this.setLine((4 * i) + 3, 0, i01, i00);

    return { i00, i10, i01, i11 };
  }

  // creates _gridChars; initializes geometry
  _createGrid() {
    const n = this.opts.maxlen;

    // initialize GeomData
    const tx = this.opts.tessellation[0];
    const ty = this.opts.tessellation[1];

    const vertsPerChar = (tx + 1) * (ty + 1);
    const elementsPerChar = 3 * (tx * ty * 2);

    const wireLinesH = (tx + 1) * ty;
    const wireLinesV = tx * (ty + 1);
    const elementsPerCharWire = 2 * (wireLinesH + wireLinesV);

    this.initialize({
      hasNormals: true,
      hasTexture: true,
      vertexCount: n * vertsPerChar,
      elementCount: n * elementsPerChar,
      elementWireCount: n * elementsPerCharWire,
    });

    // prepare grid chars
    this._gridChars = [];
    for (let i = 0; i < n; i++) {
      const newchar = this._createChar(i);
      this._gridChars.push(newchar);
    }
  }

  // generate geometry
  // ===================================
  // TODO
  // var tempVec3a = [0,0,0];
  // var tempVec3b = [0,0,0];

  // //compute normal at grid face using gridVertex.position data
  // //add the normal to each incident vertex's normal
  // _computeFaceNormal(gridFace){
  //   //pqr = face's grid vertices in CCW order
  //   var p = gridFace.incidentVerts[0];
  //   var q = gridFace.incidentVerts[1];
  //   var r = gridFace.incidentVerts[2];

  //   //compute n = (q-p) X (r-p)
  //   Vec3.subtract(tempVec3a, q.position, p.position);
  //   Vec3.subtract(tempVec3b, r.position, p.position);
  //   Vec3.cross(tempVec3a, tempVec3a, tempVec3b);
  //   //normalize
  //   Vec3.normalize(tempVec3a, tempVec3a);

  //   //add to incident vertices' normals
  //   Vec3.add(p.normal, p.normal, tempVec3a);
  //   Vec3.add(q.normal, q.normal, tempVec3a);
  //   Vec3.add(r.normal, r.normal, tempVec3a);
  // };

  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    const { text } = this.opts;

    // get number of used and unused chars
    let nvisible = text.length;
    if (nvisible > this._gridChars.length) {
      nvisible = this._gridChars.length;
    }
    // const nleftover = this._gridChars.length - nvisible;

    const normal = [1, 0, 0];

    // set attributes for visible chars
    // --------------------------------------
    const fm = this.opts.fontMetrics || fontMetrics;

    // TODO font texture size
    const fontTexW = 1024; const fontTexWInv = 1 / fontTexW;
    const fontTexH = 1024; const fontTexHInv = 1 / fontTexH;

    // running position (font texture pixel units)
    let u = 0; const v = 0;
    const uv00 = [u, v]; const uv01 = [u, v];
    const uv10 = [u, v]; const uv11 = [u, v];

    // use this.setTex, this.setNormal, this.setPosition
    const putVisibleChar = (texPos, charSize, texOffset, gc) => {
      const cw = charSize[0];
      const ch = charSize[1];
      const uoff = u + texOffset[0];
      const voff = v - texOffset[1];

      uv00[0] = uoff;      uv00[1] = voff;
      uv01[0] = uoff;      uv01[1] = voff + ch;
      uv10[0] = uoff + cw; uv10[1] = voff;
      uv11[0] = uoff + cw; uv11[1] = voff + ch;
      this.setPosition(gc.i00, this.opts.pos(uv00[0], uv00[1]));
      this.setPosition(gc.i01, this.opts.pos(uv01[0], uv01[1]));
      this.setPosition(gc.i10, this.opts.pos(uv10[0], uv10[1]));
      this.setPosition(gc.i11, this.opts.pos(uv11[0], uv11[1]));
      u = uoff + cw;

      const texX0 = texPos[0]; const texX1 = texX0 + charSize[0];
      const texY0 = texPos[1]; const texY1 = texY0 - charSize[1];
      this.setTex(gc.i00, [texX0 * fontTexWInv, texY0 * fontTexHInv]);
      this.setTex(gc.i01, [texX0 * fontTexWInv, texY1 * fontTexHInv]);
      this.setTex(gc.i10, [texX1 * fontTexWInv, texY0 * fontTexHInv]);
      this.setTex(gc.i11, [texX1 * fontTexWInv, texY1 * fontTexHInv]);

      this.setNormal(gc.i00, normal);
      this.setNormal(gc.i01, normal);
      this.setNormal(gc.i10, normal);
      this.setNormal(gc.i11, normal);
    };

    // TODO
    // const putInvalidChar = () => { };

    const spaceASCII = 32;
    for (let i = 0; i < text.length; i++) {
      const gc = this._gridChars[i];

      const k = text.charCodeAt(i);
      if (k === spaceASCII) {
        const texPos = [0, 1];
        const texSize = [fm.spaceWidth, 0];
        const offset = [0, 0];
        putVisibleChar(texPos, texSize, offset, gc);
      } else {
        const texPos = fm.getTexPos(k);
        const texSize = fm.getSize(k);
        const offset = fm.getOffset(k);
        putVisibleChar(texPos, texSize, offset, gc);
      }
    }
    // --------------------------------------

    // set attributes for invisible chars
    // TODO
  }
}

export default GeomText;
