// ===================================
// FRAMEBUFFER OBJECT
// ===================================

/** Wrapper for WebGLFramebuffer.  Creates the FBO.
* @param {WebGLRenderingContext} gl
*/
class WGLFramebufferObject {
  constructor(gl) {
    this.id = gl.createFramebuffer();

    console.log('Created FBO');
  }

  // delete
  // -----------------------------------
  /** Delete the FBO.
  * @param {WebGLRenderingContext} gl
  */
  free(gl) {
    gl.deleteFramebuffer(this.id);

    console.log('Deleted FBO');
  }

  // binding
  // -----------------------------------
  /** Bind the FBO.
  * @param {WebGLRenderingContext} gl
  */
  bind(gl) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.id);
  }

  /** Unbind the FBO (binds null).
  * @param {WebGLRenderingContext} gl
  */
  unbind(gl) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }
}

// ===================================
// TEXTURE
// ===================================

// NOTE:
// Some "GLenum" values will be manipulated by adding integers, e.g.:
//   gl.TEXTURE4 = gl.TEXTURE0 + 4 (etc)
//   gl.TEXTURE_CUBE_MAP_NEGATIVE_Z = TEXTURE_CUBE_MAP_POSITIVE_X + 5
// this will be correct according to WebGL spec

/** Creates a WebGLTexture (for color data) and WebGLRenderbuffer (for depth).
*   <br>Can be used as either source or target texture.
*   <br>Don't modify size directly--use the methods.
* @param {WebGLRenderingContext} gl
* @param {number} width - Texture width
* @param {number} height - Texture height
* @param {boolean} filter - Use LINEAR for TEXTURE_MIN/MAG_FILTER (otherwise NEAREST)?  Default true.
*/
class WGLTexture {
  constructor(gl, width, height, filter) {
    this.width = width;
    this.height = height;
    this.filter = (filter === undefined) ? true : filter;

    this._create(gl);
  }

  // create and delete
  // -----------------------------------
  /** Change size of this texture.
  * @param {WebGLRenderingContext} gl
  * @param {number} width - Texture width
  * @param {number} height - Texture height
  */
  resize(gl, width, height) {
    this.free(gl);

    this.width = width;
    this.height = height;

    this._create(gl);
  }

  _create(gl) {
    // render buffer
    this.idDepth = gl.createRenderbuffer();

    gl.bindRenderbuffer(gl.RENDERBUFFER, this.idDepth);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.width, this.height);

    // color texture
    this.idColor = gl.createTexture();

    gl.bindTexture(gl.TEXTURE_2D, this.idColor);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, (this.filter ? gl.LINEAR : gl.NEAREST)); // allow nice-looking downsampling for anti-aliasing
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, (this.filter ? gl.LINEAR : gl.NEAREST)); // don't want to interpolate packed floats
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

    gl.bindTexture(gl.TEXTURE_2D, null);

    // done
    console.log(`Created texture (${this.width}x${this.height})`);
  }

  // TODO doc
  // optional callback
  setImage(gl, url, done) {
    const img = new Image();
    img.onload = () => {
      gl.bindTexture(gl.TEXTURE_2D, this.idColor);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
      gl.bindTexture(gl.TEXTURE_2D, null);

      if (typeof done === 'function') { done(); }
    };
    img.src = url;
  }

  /** Delete the texture.
  * @param {WebGLRenderingContext} gl
  */
  free(gl) {
    gl.deleteRenderbuffer(this.idDepth);
    gl.deleteTexture(this.idColor);

    console.log('Deleted texture');
  }

  // binding
  // -----------------------------------
  /** Bind texture to specified texture unit.
  * @param {WebGLRenderingContext} gl
  * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
  */
  bind(gl, texUnit) {
    gl.activeTexture(gl.TEXTURE0 + texUnit);
    gl.bindTexture(gl.TEXTURE_2D, this.idColor);
  }

  /** Unbind texture from specified texture unit (binds null).
  * @param {WebGLRenderingContext} gl
  * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
  */
  unbind(gl, texUnit) {
    gl.activeTexture(gl.TEXTURE0 + texUnit);
    gl.bindTexture(gl.TEXTURE_2D, null);
  }

  /** Bind texture for writing.  Assumes FBO is bound.
  *   <br>Attaches depth renderbuffer to DEPTH_ATTACHMENT and color texture to COLOR_ATTACHMENT0,
  *   then prepares viewport.
  * @param {WebGLRenderingContext} gl
  */
  bindWrite(gl) {
    // ensure render buffer is not attached
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.idDepth);
    // attach texture
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.idColor, 0);

    // check status
    const status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
    if (status !== gl.FRAMEBUFFER_COMPLETE) {
      switch (status) {
        case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
          throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT.');
        case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
          throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.');
        case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
          throw new Error('FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.');
        case gl.FRAMEBUFFER_UNSUPPORTED:
          throw new Error('FBO status is GL_FRAMEBUFFER_UNSUPPORTED.');
      }
    }

    // set appropriate viewport
    gl.viewport(0, 0, this.width, this.height);
  }

  /** Unbind texture (attaches null to COLOR_ATTACHMENT0 and DEPTH_ATTACHMENT).
  * @param {WebGLRenderingContext} gl
  */
  unbindWrite(gl) {
    // detach texture and depth renderbuffer
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, null, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, null);
  }
}

// ===================================
// CUBE TEXTURE
// ===================================

/** Creates a texture cube with color data.
*   <br>Must be used as a source texture.
*   <br>Don't modify size directly--use the methods.
* @param {WebGLRenderingContext} gl
* @param {number} width - Texture width
* @param {number} height - Texture height
* @param {boolean} filter - Use LINEAR for TEXTURE_MIN/MAG_FILTER?  Optional; default true.
*/
class WGLTextureCube {
  constructor(gl, width, height, filter) {
    this.width = width;
    this.height = height;
    this.filter = (filter === undefined) ? true : filter;

    this._create(gl);
  }

  _create(gl) {
    // color texture
    this.idColor = gl.createTexture();

    gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.idColor);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, (this.filter ? gl.LINEAR : gl.NEAREST));
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, (this.filter ? gl.LINEAR : gl.NEAREST));
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    for (let i = 0; i < 6; i++) {
      gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGB, this.width, this.height, 0, gl.RGB, gl.UNSIGNED_BYTE, null);
    }

    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);

    // done
    console.log(`Created texture cube (6x${this.width}x${this.height})`);
  }

  // TODO: doc
  _setImage(gl, url, i) {
    const img = new Image();
    img.onload = () => {
      gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.idColor);
      gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, img);
      gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    };
    img.src = url;
  }

  setRT(gl, url) { this._setImage(gl, url, 0); }
  setLF(gl, url) { this._setImage(gl, url, 1); }
  setUP(gl, url) { this._setImage(gl, url, 2); }
  setDN(gl, url) { this._setImage(gl, url, 3); }
  setFT(gl, url) { this._setImage(gl, url, 4); }
  setBK(gl, url) { this._setImage(gl, url, 5); }

  /** Delete the texture.
  * @param {WebGLRenderingContext} gl
  */
  free(gl) {
    gl.deleteTexture(this.idColor);

    console.log('Deleted texture cube');
  }

  // binding
  // -----------------------------------
  /** Bind texture to specified texture unit.
  * @param {WebGLRenderingContext} gl
  * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
  */
  bind(gl, texUnit) {
    gl.activeTexture(gl.TEXTURE0 + texUnit);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.idColor);
  }

  /** Unbind texture from specified texture unit (binds null).
  * @param {WebGLRenderingContext} gl
  * @param {number} texUnit - The texture unit, in [0, gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS)
  */
  unbind(gl, texUnit) {
    gl.activeTexture(gl.TEXTURE0 + texUnit);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
  }
}

export {
  WGLFramebufferObject,
  WGLTexture,
  WGLTextureCube,
};
