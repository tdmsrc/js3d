import { Vec3 } from './mathVec';
import { Mat3, Mat4 } from './mathMat';

const TEMPVEC3 = [0, 0, 0];

// ===================================
// CAMERA ROTATION
// ===================================

/** Rotation used in a {@link Camera}.
*   <br>Has a fixed upward direction to prevent "roll" while changing "yaw" and "pitch".
*   <br>Don't modify properties directly--use the methods.
*
* @prop {number[]} forward - Direction camera is pointing
* @prop {number[]} fixedUp - Upward direction, independent of camera orientation
* @prop {number[]} side - Sideways direction (= forward X fixedUp)
* @prop {number[]} tiltedUp - Upward direction (= side X forward)
* @prop {Mat3} matrix - Rotation matrix; maps basis [side, tiltedUp, -forward] to standard basis
* @prop {Mat3} matrixInverse - Inverse of rotation matrix
*/
class CameraRotation {
  constructor() {
    this.forward = [0, 0, -1];
    this.fixedUp = [0, 1, 0];

    // derived quantities
    this.side = [1, 0, 0];
    this.tiltedUp = [0, 1, 0];
    this.matrix = Mat3.create();
    this.matrixInverse = Mat3.create();

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------
  _recompute() {
    this._computeOrthoBasis();
    this._computeMatrix();
  }

  _computeOrthoBasis() {
    // compute side and tiltedUp
    Vec3.cross(this.side, this.forward, this.fixedUp);
    Vec3.normalize(this.side, this.side);
    Vec3.cross(this.tiltedUp, this.side, this.forward);
  }

  _computeMatrix() {
    // change of basis [s, u, -f] -> [e1 e2 e3] (u = tiltedUp)
    // this is the matrix with rows s, u, -f
    // it is orthogonal so inverse is matrix with columns s, u, -f.

    Vec3.flip(TEMPVEC3, this.forward);
    Mat3.setRows(this.matrix, this.side, this.tiltedUp, TEMPVEC3);
    Mat3.setColumns(this.matrixInverse, this.side, this.tiltedUp, TEMPVEC3);
  }

  // displacement
  // -----------------------------------
  /** Set target = forward displacement (i.e., along this.forward).
  * @param {number[]} target - The vector which will store the result
  * @param {number} d - Displacement distance
  * @see {@link Camera#moveForward}
  */
  displacementForward(target, d) {
    Vec3.scale(target, this.forward, d);
  }

  /** Set target = sideways displacement (i.e., along this.side).
  * @param {number[]} target - The vector which will store the result
  * @param {number} d - Displacement distance
  * @see {@link Camera#moveSideways}
  */
  displacementSideways(target, d) {
    Vec3.scale(target, this.side, d);
  }

  /** Set target = upward displacement (i.e., along this.fixedUp).
  * @param {number[]} target - The vector which will store the result
  * @param {number} d - Displacement distance
  * @see {@link Camera#moveUp}
  */
  displacementUp(target, d) {
    Vec3.scale(target, this.fixedUp, d);
  }

  /** Set target = forward displacement, perpendicular to fixedUp (i.e., along "ground").
  * @param {number[]} target - The vector which will store the result
  * @param {number} d - Displacement distance
  * @see {@link Camera#moveForwardPlanar}
  */
  displacementForwardPlanar(target, d) {
    // project forward vector onto fixedUp perpendicular, then set length to d
    Vec3.projn(target, this.forward, this.fixedUp);
    const tlen = Vec3.norm(target);
    Vec3.scale(target, target, d / tlen);
  }

  // rotation
  // -----------------------------------
  /** Set the direction the camera is pointing.
  * @param {number[]} forward - Direction (need not be normalized)
  */
  setForward(forward) {
    Vec3.normalize(this.forward, forward);

    this._recompute();
  }

  /** Set the fixed upward direction.
  * @param {number[]} up - Fixed upward direction (need not be normalized)
  */
  setUp(up) {
    Vec3.normalize(this.fixedUp, up);

    this._recompute();
  }

  /** Rotate the camera (positive yaw = rotate left, positive pitch = tilt upward).
  * @param {number} yaw - Angle in radians; rotation about fixed upward direction
  * @param {number} pitch - Angle in radians; rotation about side vector
  */
  rotate(yaw, pitch) {
    const cp = Math.cos(pitch);
    const sp = Math.sin(pitch);
    const cy = Math.cos(yaw);
    const sy = Math.sin(yaw);

    Vec3.linearCombination(TEMPVEC3, cy, this.forward, -sy, this.side);
    Vec3.linearCombination(TEMPVEC3, cp, TEMPVEC3, sp, this.tiltedUp);
    Vec3.normalize(this.forward, TEMPVEC3);

    this._recompute();
  }

  // slerp
  // -----------------------------------
  // TODO
}

// ===================================
// CAMERA
// ===================================

/** A position, {@link CameraRotation}, and some perspective projection options.
*   <br>Don't modify perspective properties directly--use the methods.
*
* @prop {number[]} position - Position of camera
* @prop {CameraRotation} rotation - Orientation of camera
*
* @prop {number} fovy - Angle formed by top and bottom frustum planes
* @prop {number} aspect - Width/height
* @prop {number} zNear - Near z-clipping plane
* @prop {number} zFar - Far z-clipping plane
* @prop {Mat4} matrix - Perspective projection matrix
*/
class Camera {
  constructor() {
    // reasonable defaults
    this.fovy = 60;
    this.aspect = 1;
    this.zNear = 0.1;
    this.zFar = 1000;

    this.position = [0, 0, 0];
    this.rotation = new CameraRotation();

    // derived quantities
    this.matrix = Mat4.create(); // projection matrix

    // recompute derived quantities
    this._recompute();
  }

  // compute derived quantities
  // -----------------------------------
  _recompute() {
    Mat4.gluPerspective(this.matrix, this.fovy, this.aspect, this.zNear, this.zFar);
  }

  // change projection
  // -----------------------------------
  /** Set FOVy and update projection matrix.
  * @param {number} fovy
  */
  setFOVy(fovy) {
    this.fovy = fovy;
    this._recompute();
  }

  /** Set aspect ratio and update projection matrix.
  * @param {number} aspect
  */
  setAspect(aspect) {
    this.aspect = aspect;
    this._recompute();
  }

  /** Set z clipping planes and update projection matrix.
  * @param {number} zNear
  * @param {number} zFar
  */
  setZClippingPlanes(zNear, zFar) {
    this.zNear = zNear;
    this.zFar = zFar;
    this._recompute();
  }

  // rotation and displacement
  // -----------------------------------
  /** Rotate to look at the point p.
  * @param {number[]} p - The point to look at
  * @see {@link CameraRotation#displacementForward}
  */
  lookAt(p) {
    Vec3.subtract(TEMPVEC3, p, this.position);
    this.rotation.setForward(TEMPVEC3);
  }

  /** Move forward (i.e., in the direction the camera is pointing).
  * @param {number} d - Distance to move
  * @see {@link CameraRotation#displacementSideways}
  */
  moveForward(d) {
    this.rotation.displacementForward(TEMPVEC3, d);
    Vec3.add(this.position, this.position, TEMPVEC3);
  }

  /** Move sideways (negative d = left, positive d = right).
  * @param {number} d - Distance to move
  * @see {@link CameraRotation#displacementUp}
  */
  moveSideways(d) {
    this.rotation.displacementSideways(TEMPVEC3, d);
    Vec3.add(this.position, this.position, TEMPVEC3);
  }

  /** Move up (i.e., in fixed upward direction; independent of orientation).
  * @param {number} d - Distance to move
  * @see {@link CameraRotation#displacementForwardPlanar}
  */
  moveUp(d) {
    this.rotation.displacementUp(TEMPVEC3, d);
    Vec3.add(this.position, this.position, TEMPVEC3);
  }

  /** Move forward, perpendicular to the fixed upward direction
  *   (i.e., move along the "ground").
  * @param {number} d - Distance to move
  */
  moveForwardPlanar(d) {
    this.rotation.displacementForwardPlanar(TEMPVEC3, d);
    Vec3.add(this.position, this.position, TEMPVEC3);
  }

  // relative positioning
  // -----------------------------------
  /** Get location relative to camera position and orientation.
  * @param {number[]} target - The vector which will store the result
  * @param {number[]} p - Vector with "x" along "side", "y" along "tiltedUp", "z" along "forward"
  */
  getRelativePosition(target, p) {
    Vec3.copy(target, this.position);
    Vec3.addMultiple(target, target, this.rotation.side, p[0]);
    Vec3.addMultiple(target, target, this.rotation.tiltedUp, p[1]);
    Vec3.addMultiple(target, target, this.rotation.forward, p[2]);
  }

  // slerp
  // -----------------------------------
  // TODO
}

export {
  CameraRotation,
  Camera,
};
