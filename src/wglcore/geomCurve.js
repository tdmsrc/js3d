import { Vec3 } from './mathVec';
import GeomData from './geomData';

const rangeLerp = (range, lerp) =>
  range[0] + lerp * (range[1] - range[0]);

// ===================================
// GEOMETRY: CURVE
// tubeplot of parametric curve (f:R -> R^3)
// ===================================

/** Parametric curve, fattened into a tube.
*   <br>Must call {@link GeomCurve#generate} after creation, or to update position/color values.
*   <br>Inherits {@link GeomData#instance}; use this to create a {@link SceneObject}.
*
* @param {Object} opts
* @param {number} opts.tubeN - Number of vertices around the tube
* @param {number} opts.tubeRadius - Radius of the tube
* @param {number[]} opts.range - Range of t values: [tmin,tmax]
* @param {number} opts.n - Number of samples in range of t values
* @param {Function} opts.f - Position function: <code>function(t){ return [x,y,z]; }</code>
* @param {Function} opts.color - Color function (theta = angle around tube): <code>function(t,theta,[x,y,z]){ return [r,g,b]; }</code>
* @param {boolean} opts.loop - Set to true if f(tmin) = f(tmax)
*
* @extends GeomData
*/
class GeomCurve extends GeomData {
  constructor(opts) {
    super();
    this.opts = opts;

    // create this._curvePoint/_curveSegments/_gridFaces arrays, GeomData elements
    this._createGrid();
  }

  // generate grid geometry
  // -----------------------------------
  // create curvePoint
  _createPoint(lerp) {
    // create and add curvePoint object
    const cpIndex = this._curvePoints.length;

    const curvePoint = {
      t: rangeLerp(this.opts.range, lerp),
      position: [0, 0, 0], // set by "generate"
      frameT: [1, 0, 0], // set by "generate"
      frameN: [0, 1, 0], // set by "generate"
      frameB: [0, 0, 1], // set by "generate"
      tube: [], // objects representing actual vertices in the GeomData
    };
    this._curvePoints.push(curvePoint);

    // create "tube" vertices
    const vIndex0 = cpIndex * this.opts.tubeN;

    for (let i = 0; i < this.opts.tubeN; i++) {
      const theta = (2 * Math.PI) * (i / this.opts.tubeN);
      const vertex = {
        _vindex: vIndex0 + i, // index in GeomData vertex arrays
        theta,
        position: [0, 0, 0],
        normal: [0, 1, 0],
      };
      curvePoint.tube.push(vertex);
    }
  }

  // vi are "vertex" objects appearing in curvePoint.tube
  _createFace(v0, v1, v2, v3) {
    let f0 = this._gridFaces.length;

    this._gridFaces.push({ incidentVerts: [v0, v1, v2] });
    this._gridFaces.push({ incidentVerts: [v0, v2, v3] });

    this.setFace(f0++, 0, v0._vindex, v1._vindex, v2._vindex);
    this.setFace(f0++, 0, v0._vindex, v2._vindex, v3._vindex);
  }

  // creates _gridVerts, _gridFaces; initializes geometry and its faces
  _createGrid() {
    const nt = this.opts.n;
    const ntube = this.opts.tubeN;

    // initialize GeomData
    const wireLinesL = (nt + 1) * ntube;
    const wireLinesT = nt * ntube;
    this.initialize({
      hasNormals: true,
      vertexCount: (nt + 1) * ntube,
      elementCount: 3 * (2 * nt * ntube),
      elementWireCount: 2 * (wireLinesL + wireLinesT),
    });

    // prepare curve points
    this._curvePoints = [];
    for (let it = 0; it <= nt; it++) {
      // lerp = point in input range [0,1]
      const lerp = it / nt;
      this._createPoint(lerp);
    }

    // prepare curve segments
    this._curveSegments = [];
    for (let it = 0; it < nt; it++) {
      const ends = [this._curvePoints[it], this._curvePoints[it + 1]];
      this._curveSegments.push({
        frameT: [0, 0, 0],
        frameN: [0, 0, 0],
        incidentPoints: ends,
      });
    }

    // prepare grid faces, GeomData elements
    this._gridFaces = [];
    for (let it = 0; it < nt; it++) {
      const p = this._curvePoints[it];
      const q = this._curvePoints[it + 1];

      for (let ia = 0; ia < ntube; ia++) {
        const ja = (ia + 1) % ntube;
        this._createFace(p.tube[ia], q.tube[ia], q.tube[ja], p.tube[ja]);
      }
    }

    // prepare wireframe edges
    let edgeIndex = 0;

    for (let it = 0; it <= nt; it++) {
      const p = this._curvePoints[it];
      for (let ia = 0; ia < ntube; ia++) {
        const ja = (ia + 1) % ntube;
        this.setLine(edgeIndex++, 0, p.tube[ia]._vindex, p.tube[ja]._vindex);
      }
    }

    for (let it = 0; it < nt; it++) {
      const p = this._curvePoints[it];
      const q = this._curvePoints[it + 1];
      for (let ia = 0; ia < ntube; ia++) {
        this.setLine(edgeIndex++, 0, p.tube[ia]._vindex, q.tube[ia]._vindex);
      }
    }
  }

  // generate geometry
  // updates data contained in geometry, _gridVerts, _gridFaces, rangeOut
  // ===================================

  /** Recompute vertex attributes: position, color, and normal. */
  generate() {
    // compute position of curve points and reset frame
    for (let i = 0; i < this._curvePoints.length; i++) {
      const curvePoint = this._curvePoints[i];
      Vec3.copy(curvePoint.position, this.opts.f(curvePoint.t));
      Vec3.zero(curvePoint.frameT);
      Vec3.zero(curvePoint.frameN);
      Vec3.zero(curvePoint.frameB);
    }

    // compute frame (i.e., curvePoint.frameT/frameN/frameB)
    // -------------------------------
    // compute frameT on segments, add to endpoints
    for (let i = 0; i < this._curveSegments.length; i++) {
      const segment = this._curveSegments[i];
      const p = segment.incidentPoints[0];
      const q = segment.incidentPoints[1];

      // frameT = change in position
      Vec3.subtract(segment.frameT, q.position, p.position);
      Vec3.add(p.frameT, p.frameT, segment.frameT);
      Vec3.add(q.frameT, q.frameT, segment.frameT);
    }

    // compute frameT on curve points
    for (let i = 0; i < this._curvePoints.length; i++) {
      const curvePoint = this._curvePoints[i];
      Vec3.normalize(curvePoint.frameT, curvePoint.frameT);
    }

    // compute frameN on segments, add to endpoints
    for (let i = 0; i < this._curveSegments.length; i++) {
      const segment = this._curveSegments[i];
      const p = segment.incidentPoints[0];
      const q = segment.incidentPoints[1];

      // frameN = change in frameT
      Vec3.subtract(segment.frameN, q.frameT, p.frameT);
      // Vec3.orthogonal(segment.frameN, segment.frameT);
      Vec3.add(p.frameN, p.frameN, segment.frameN);
      Vec3.add(q.frameN, q.frameN, segment.frameN);
    }

    // compute frameN on curve points
    for (let i = 0; i < this._curvePoints.length; i++) {
      const curvePoint = this._curvePoints[i];
      Vec3.normalize(curvePoint.frameN, curvePoint.frameN);
    }

    // TODO glue: quick fix of frameT/frameN for looping curve
    if (this.opts.loop) {
      const p = this._curvePoints[0];
      const q = this._curvePoints[this._curvePoints.length - 1];

      // average p and q frameT then normalize
      Vec3.add(p.frameT, p.frameT, q.frameT);
      Vec3.normalize(p.frameT, p.frameT);
      Vec3.copy(q.frameT, p.frameT);

      // average p and q frameN then normalize
      Vec3.add(p.frameN, p.frameN, q.frameN);
      Vec3.normalize(p.frameN, p.frameN);
      Vec3.copy(q.frameN, p.frameN);
    }

    // compute frameB on curve points
    for (let i = 0; i < this._curvePoints.length; i++) {
      const curvePoint = this._curvePoints[i];
      Vec3.cross(curvePoint.frameB, curvePoint.frameT, curvePoint.frameN);
    }
    // -------------------------------

    // compute position/color of tube vertices
    for (let i = 0; i < this._curvePoints.length; i++) {
      const curvePoint = this._curvePoints[i];
      for (let j = 0; j < curvePoint.tube.length; j++) {
        const vertex = curvePoint.tube[j];

        // color
        const c = this.opts.color(curvePoint.t, vertex.theta, curvePoint.position);
        this.setColor(vertex._vindex, c);

        // position
        const ct = Math.cos(vertex.theta);
        const st = Math.sin(vertex.theta);

        Vec3.linearCombination(vertex.normal, ct, curvePoint.frameB, st, curvePoint.frameN);
        Vec3.addMultiple(vertex.position, curvePoint.position, vertex.normal, this.opts.tubeRadius);

        this.setPosition(vertex._vindex, vertex.position);
        this.setNormal(vertex._vindex, vertex.normal);
      }
    }
  }
}

export default GeomCurve;
