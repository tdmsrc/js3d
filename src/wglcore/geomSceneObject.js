import ObjectTransformation from './mathObjTrans';

// ===================================
// SCENE OBJECT
// geometry plus object transformation
// geometry can be shared between objects (i.e. clones)
// ===================================

/** Some geometry (array of {@link GeomData}) plus an {@link ObjectTransformation} and some rendering options.
*   <br> Objects which generate geometry typically have a "instance" method which creates a SceneObject.
*   <br> Geometry can be shared between {@link SceneObject}s (e.g. {@link SceneObject#clone}).
* @param {GeomData[]} geomArray - Object geometry
*
* @prop {ObjectTransformation} transformation - The transformation applied to the object
* @prop {boolean} solidVisible - Option to display solid geometry
* @prop {boolean} castsShadows - Option to cast shadows
* @prop {boolean} translucent - Option to display as translucent
* @prop {number} alpha - Alpha value if displaying as translucent
*
* @prop {number} emissiveCoefficient - Emissive coefficient for lighting, in [0,1]
* @prop {number[]} emissiveColor - Emissive color (as [r,g,b])
* @prop {number} diffuseCoefficient - Diffuse coefficient for lighting, in [0,1]
* @prop {number} specularCoefficient - Specular coefficient for lighting, in [0,1]
* @prop {number} specularExponent - Specular exponent for Phong lighting model
*
* @prop {boolean} wireframeVisible - Option to display wireframe geometry
* @prop {number[]} wireframeColor - Wireframe color (as [r,g,b,a], no premultiplied alpha)
*/
class SceneObject {
  constructor(geomArray) {
    // geometry (array of GeomData)
    this.geomArray = geomArray;

    this.transformation = new ObjectTransformation();

    // rendering options
    this.solidVisible = true;
    this.castsShadows = true;
    this.translucent = false;
    this.alpha = 0.5;

    this.wireframeVisible = false;
    this.wireframeColor = [0, 0, 0, 0.25];

    this.emissiveCoefficient = 0;
    this.emissiveColor = [1, 1, 1];

    this.diffuseCoefficient = 1.0;
    this.specularCoefficient = 0.5;
    this.specularExponent = 10.0;
  }

  /** Create a new {@link SceneObject} with reference to the same geometry.
  * Does not duplicate any other properties.
  * @return {SceneObject}
  */
  clone() {
    return new SceneObject(this.geomArray);
  }

  /** Set target's transformation as the parent of this object's transformation.
  * @param {SceneObject} target - The object to stick to
  * @see {@link ObjectTransformation}
  */
  stickTo(target) {
    this.transformation.parent = target.transformation;
  }

  /** Make this object's transformation have no parent.
  * @see {@link ObjectTransformation}
  */
  unstick() {
    this.transformation.parent = null;
  }
}

export default SceneObject;
