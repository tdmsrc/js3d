import WGLDrawable from './wglDrawable';
import GeomQuad from './geomQuad';

// ===================================
// QUAD
// ===================================

/** Stores the data of a {@link GeomQuad} in WebGLBuffer objects.
* @param {WebGLRenderingContext} gl
*
* @extends WGLDrawable
*/
class WGLQuad extends WGLDrawable {
  constructor(gl) {
    super(gl, new GeomQuad());
  }
}

export default WGLQuad;
